#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

#SITEURL = 'http://horoiomuy6xignjv.onion/'
RELATIVE_URLS = False
PLUGINS = ["better_codeblock_line_numbering",
           'tipue_search',
           'neighbors',
           'series',
           'extract_toc',
           'tag_cloud',
           'sitemap',
           'summary',
           'twitter_bootstrap_rst_directives',
           'feed_summary',
           'materialize'
           ]
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
FEED_USE_SUMMARY = False
SIMPLE = True
