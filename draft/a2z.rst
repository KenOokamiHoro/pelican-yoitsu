浏览器A到Z
=======================================

:slug: a2z
:date: 2016-5-17
:save_as: a2z.html
:issueid: 43

首先看 :fref:`phoenixlzx` 那有一篇这样的文章,貌似很好玩的样子？

咱也试试看?

* A aur.archlinux.org # AUR
* B blog.yoitsu.moe # 这 😂
* C cn.bing.com # :ruby:`中文|中国` 版必应 😂
* D disqus.com # 评论框用的 😂
* E en.wikipedia.org # 其实第一个是 "e - Google 搜索" (逃
* F fiveyellowmice.com # :fref:`fiveyellowmice`
* G github.com # :del:`Gayhub` 😂
* H hjc.im # :del:`大土壕` :fref:`hjc4869`
* I img.vim-cn.com # 百合仙子 :fref:`lilydjwg` 维护的图床
* J jquery.com
* K kenengba.com # 长草的可能吧
* L localhost:8000 # 嗯 ...... 咱本地测试 Pelican 常用的端口 😂
* M music.163.com # 网易云音乐 ......
* N noahgao.net # 咱某个时段同学的博客
* O outlook.com # 前 Hotmail
* P program-think.blogspot.com # :fref:`program-think`
* Q quininer.github.io # 奎妮 :fref:`quininer` 😃
* R raspberrypi.org # 那个不能吃的树莓派
* S status.github.com # Github 状态回报
* T typeblog.net # :fref:`petercxy`
* U uss.tust.edu.cn # 学校的校园网自助服务门户
* V visualstudio.com # :ruby:`微软|巨硬` 的 VisualStudio
* W wiki.archlinux.org # ArchWiki 大法好!
* X xieyuheng.github.io # 小妖狐 :fref:`xyh`
* Y yoitsu.moe # ......
* Z zhihu.com # 前膜蛤景点 😂
