Sailfish OS 移植中（4）- 不同的子系统调适指南（上）
======================================================

:slug: porting_sailfishos_4
:lang: zh
:date: 2019-01-22
:series: Sailfish OS 移植中
:tags: Sailfish OS
:desc: 尝试让剩下的部分工作 _(:з」∠)_

如果汝运气足够好的话，现在汝的手机上就有一个能够启动的 Sailfish OS 了呗~
虽然有很大的可能不少设备工作不了（例如蓝牙和手机网络等等），这一部分
就要试着让它们起床干活啦~

    对应 HADK 文档的第十三章。

震动和反馈
-------------------------------------------------

    基本上有 LineageOS 移植的手机应该不用做什么修改就能用的样子……

    （以及为啥要这么做就懒得翻译了，自己去看 HADK 的文档 😂）

主要的任务大概有这几步：

* 在内核选项中启用 CONFIG_INPUT_FF_MEMLESS 、 停用 CONFIG_ANDROID_TIMED_OUTPUT
* 修改 ff-memless.c ，把 ffmemless effects 的最大数量提高到 64：

.. code-block:: patch

    diff --git a/drivers/input/ff-memless.c b/drivers/input/ff-memless.c
    index 117a59a..fa53611 100644
    --- a/drivers/input/ff-memless.c
    +++ b/drivers/input/ff-memless.c
    @@ -39,7 +39,7 @@ MODULE_AUTHOR("Anssi Hannula <anssi.hannula@gmail.com>");
    MODULE_DESCRIPTION("Force feedback support for memoryless devices");
    /* Number of effects handled with memoryless devices */
    -#define FF_MEMLESS_EFFECTS 16
    +#define FF_MEMLESS_EFFECTS 64
    /* Envelope update interval in ms */
    #define FF_ENVELOPE_INTERVAL 50

