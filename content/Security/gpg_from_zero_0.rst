从零开始的 GnuPG 学习笔记 [0] - PGP,GPG 等相关概念扫盲
===========================================================

:slug: gpg_from_zero_0
:lang: zh
:date: 2016-08-06
:tags: GnuPG,Software,Cryptology
:series: 从零开始的 GnuPG 学习笔记
:desc: GnuPG 密钥从创建到注销 （雾
:issueid: 39

.. PELICAN_BEGIN_SUMMAY

GnuPG 密钥从创建到注销，顺便丢一只雷姆 （雾  [#]_

.. PELICAN_END_SUMMARY

.. [#]  雷姆（レム/Rem）是动画 「Re：从零开始的异世界生活 」（日文：Re:ゼロから始める異世界生活）的角色之一。

.. contents::

为啥需要密码学技术？
-------------------------------

    对于计算机的使用后果，没人比乔治·奥威尔在《1984》中的预言错得更离谱了。到目前为止，计算机创造的几乎所有实际的可能性空间都表明，计算机是权威的终结而非权威的开始。

    -- 《失控》

在计算机和互联网的领域，自由和开放大概是主流。那些试图管制和封闭互联网的方法几乎都失败了呗~

类似 `Tor <https://www.torproject.org>`_  ，`I2p <https://geti2p.net>`_  一类的匿名网络访问工具正被无数人（可能汝也是其中之一😋）用来因为各种原因隐藏自己的身份。
而类似 `ZeroNet  <https://zeronet.io/>`_ 的新兴软件更多的用到了 Bitorrent 一类的分布式技术。

而这些工具要如何安全的进行数据交换呢，就需要用到各种密码学技术了呐，
其实在上网的过程中，汝已经不知不觉的和各种密码学技术打交道了呢~

    *  在浏览各种 https:// 网站 （例如咱这里）时，汝的浏览器会和网站的服务器进行认证来保证汝要访问的服务器真的是这个网站所使用的服务器呐~
    * 在登录到各类网络服务时，总不能明文（就是按原样）传输汝的个人信息（其中可能会有汝的密码哦）吧？这时就需要客户端和服务器约定一种相互理解的方法来加密/解密各类数据呗~
    * ……

而各类密码学技术和工具正是帮助汝在这个无法完全（或是根本不能）信任的互联网中相对安全的传递数据的帮手咯~

好啦好啦……其实咱并不是密码学专家啦😋


PGP，OpenPGP 和 GPG（GnuPG）都是些啥玩意？
-----------------------------------------------------------------------------

（啥，汝到现在连这三个词都没听说过？😂😂）

良好隐私密码法（英语：Pretty Good Privacy，缩写为PGP），一套用于讯息加密、验证的应用程序，采用IDEA的散列算法作为加密与验证之用。

PGP的主要开发者是菲尔·齐默尔曼（Phil Zimmermann）。齐默曼于1991年将PGP在互联网上免费发布。

后来，PGP 被 Symantec 公司收购成为了商业软件。

OpenPGP 是一套标准，大多数 PGP 软件（例如 GnuPG）都遵循这一标准。

GNU Privacy Guard（GnuPG或GPG）是一种加密软件，它是PGP加密软件的满足GPL的替代物。GnuPG依照由IETF订定的OpenPGP技术标准设计。GnuPG用于加密、数字签名及产生非对称匙对的软件。

GnuPG是自由软件基金会的GNU计划的一部分，目前受德国政府资助。以GNU通用公共许可证第三版授权。

而大多数 Linux 发行版中内置（或者在软件仓库中）的应该都是 GnuPG 啦。汝可以在终端里输入 :code:`gpg --version` 来查看一下版本呗~（如果已经安装了的话）

.. code-block:: bash

    #咱的 gpg --version 大概像这样，某些发行版（例如 Ubuntu ）里可能还会有 gpg2~
    gpg (GnuPG) 2.1.14
    libgcrypt 1.7.2
    Copyright (C) 2016 Free Software Foundation, Inc.
    License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Home: /home/horo/.gnupg
    支持的算法：
    公钥：RSA, ELG, DSA, ECDH, ECDSA, EDDSA
    对称加密：IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256,
        TWOFISH, CAMELLIA128, CAMELLIA192, CAMELLIA256
    散列：SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
    压缩：不压缩, ZIP, ZLIB, BZIP2


GnuPG 的作用是？
-------------------------------
至少咱自己经历过的作用有这两种呗~

* 签名

    用来证明这个文件是某个人创建的，而且在到达汝的过程中没有被篡改呗~

    大部分 Linux 发行版的软件包管理器已经用上这个用途了呐~
    （所以咱推测 gpg 应该已经安装在汝的系统上了呗~）

* 加密

    在某处用汝自己的密钥对加密某些文字，或是文件。然后传输到某个地方以后再使用汝自己的密钥对解密这些内容，在传输的过程中，如果私钥不泄露的话，其他人只会看到约等于无意义的乱码而已呐~

然后请允许咱从维基百科上抄一张图下来😂😂

.. image:: /images/PGP_diagram_.png
    :alt: PGP 加密/解密的原理图

-------------------


于是这另一个新坑就挖好了呢（雾😂😂，下次就文字直播生成一个汝自己的密钥对好啦~
