从零开始的 GnuPG 学习笔记 [1] - 创建密钥对
===========================================================

:slug: gpg_from_zero_1
:lang: zh
:date: 2016-09-29
:tags: GnuPG,Software,Cryptology
:series: 从零开始的 GnuPG 学习笔记
:desc: 用 gpg 创建自己的密钥对
:issueid: 50


.. PELICAN_BEGIN_SUMMAY

在 GNU/Linux 上用 gpg 创建自己的密钥对~

.. PELICAN_END_SUMMARY


.. contents::

对于 Windows 用户,可以试试 `Gpg4win <https://www.gpg4win.org/>`_ ,
Gpg4win 提供了一套 Windows 下可用的 GPG 解决方案.

Gpg4win内置的是 Kleopatra ,
`官方文档在这 <https://docs.kde.org/stable/en/kdepim/kleopatra/index.html>`_ ,
其它人写的教程 Google 一下应该也找的到.

创建密钥对
-----------------------

从终端中运行 :code:`gpg --full-gen-key --expert` :

    --full-gen-key 是使用完整的步骤创建密钥对.

    --expert 顾名思义就是 :del:`砖` 专家模式啦,
    例如可以选择更多种(例如新式的)加密方式呗~

    有些发行版可能会用 gpg2 来代表版本 2+ 的 gpg.

.. code-block:: bash

    $ gpg --full-gen-key --expert
    gpg (GnuPG) 2.1.15; Copyright (C) 2016 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    请选择您要使用的密钥种类：
       (1) RSA and RSA (default)
       (2) DSA and Elgamal
       (3) DSA (仅用于签名)
       (4) RSA (仅用于签名)
       (7) DSA (自定义用途)
       (8) RSA (自定义用途)
       (9) ECC and ECC
      (10) ECC (sign only)
      (11) ECC (set your own capabilities)
    您的选择？

可能汝和咱的有些不一样(因为 ECC 加密算法在 gpg 2.14 才开始支持).

ECC 又称椭圆曲线密码学,主要优势是在某些情况下
它比其他的方法使用更小的密钥——比如RSA加密算法——提供相当的或更高等级的安全。

不过有些地方还不能用这种新加密算法,这里示范咱就只用默认的 RSA 啦😂

输入密钥种类前面的数字(不带括号),然后确认.

.. code-block:: bash

    RSA 密钥长度应在 1024 位与 4096 位之间。
    您想要用多大的密钥尺寸？(2048)

在这里设置一个密钥长度,默认的长度应该足够安全了.如果有需要,也可以增加.


.. code-block:: bash

    RSA 密钥长度应在 1024 位与 4096 位之间。
    What keysize do you want for the subkey? (2048)

然后为子密钥设置密钥长度.

    是不是想问啥是子密钥 (subkey) ?

    一个密钥对下可以添加不同的子密钥,这样做的好处有啥咧?

    * 区分目的:例如一个子密钥用于加密,另一个子密钥用于电子邮件签名等等......

    * 区分设备:比如某些安全性稍差的设备(例如手机),汝可以只把一对专用的子密钥放到里面.

    * 保护主密钥:如果子密钥泄漏,只要吊销相应的子密钥就好,而主密钥依然安全~

.. code-block:: bash

    请设定这把密钥的有效期限。
         0 = 密钥永不过期
      <n>  = 密钥在 n 天后过期
      <n>w = 密钥在 n 周后过期
      <n>m = 密钥在 n 月后过期
      <n>y = 密钥在 n 年后过期
    密钥的有效期限是？(0)

接下来为密钥设置有效期啦~

不用担心密钥过期啦,因为在密钥过期前汝还是可以用 gpg --edit-key 命令来重新设置有效期呗~

设定完过期时间以后输入 y 确认.


标识密钥对
------------------

.. code-block:: bash

    You need a user ID to identify your key; the software constructs the user ID
    from the Real Name, Comment and Email Address in this form:
        "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

    真实姓名：
    电子邮件地址：
    注释：

在这里用姓名,电子邮件地址和注释标识这把密钥吧~

gpg 会用这些信息生成特定的用户 ID ,以后对密钥进行操作都需要用到用户 ID 呐~

.. code-block:: bash

    您选定了这个用户标识：
        “ThisIsAName (Comment) <mailaddress@domain.tld>”

    更改姓名(N)、注释(C)、电子邮件地址(E)或确定(O)/退出(Q)？

在输入完成之后会有一次确认的机会,觉得没问题就输入 O 继续吧~

接下来会弹出一个对话框,提示汝输入一个密码来保护私钥.至于如何创建一个强密码......

然后.....

    我们需要生成大量的随机字节。这个时候您可以多做些琐事(像是敲打键盘、移动
    鼠标、读写硬盘之类的)，这会让随机数字发生器有更好的机会获得足够的熵数。

于是随便做些啥吧~

完工啦~
-------------------------------

.. code-block:: bash

    gpg: 密钥 A4A7BA4D077C612F 被标记为绝对信任
    gpg: revocation certificate stored as
    '/home/horo/.gnupg/openpgp-revocs.d/2A35F5ED3FB328E39DFADA2AA4A7BA4D077C612F.rev'
    公钥和私钥已经生成并经签名。

    pub   rsa2048 2016-09-29 [SC]
        2A35F5ED3FB328E39DFADA2AA4A7BA4D077C612F
        uid                      ThisIsAName (Comment) <mailaddress@domain.tld>
        sub   rsa2048 2016-09-29 [E]

这个时候就表示密钥生成好啦~

像 A4A7BA4D077C612F 这样的就是汝的用户 ID 啦,有时也会用到下面那个比较长的 ID.

同时会在 .gnupg/openpgp-revocs.d/ 文件夹下生成一份吊销证书,当汝因为某些原因
丢失了私钥或者怀疑密钥被窃时,可以用这个吊销证书来吊销汝的密钥.

这样一对密钥就创建好啦~,下次再细水长流密钥的应用咯 😂
