自定 Pelican 主题 
==================================================

:slug: pelican_theming
:lang: zh
:date: 2016-03-30
:tags: pelican,blog
:series: Pelican 
:desc: 论颜值的重要性 (雾
:issueid: 33

.. PELICAN_BEGIN_SUMMARY

:del:`论颜值的重要性 (雾`

.. PELICAN_END_SUMMARY

谁叫这是个看脸的世界呢~

Pelican 的默认主题的效果是啥, `大概像这个样子 (╯>＿<)╯ ┻━┻  <http://blog.getpelican.com/>`_

觉得不爽,那就改它呗~

首先把默认的 :ruby:`simple|什么都没有` 主题复制出来,接下来就基于它来修改呗~

    # cp 的 -R (--recursive) 参数表示递归的复制文件夹中的文件.
    
    # 如果汝的 Python 安装在不同的目录下,记得改了.
    
    cp -R /usr/lib/python3.5/site-packages/pelican/themes/simple /path/to/your/pelican/
    
不过话说回来为啥不直接看 Pelican 官方的文档咧?

    `Creating themes <http://docs.getpelican.com/en/3.6.3/themes.html>`_
    
.. contents::    

Step 1 : 了解一下 Pelican 主题的结构
--------------------------------------------------

典型的 Pelican 主题大概像这样:


.. code-block:: html

    ├── static                    // 各类静态数据
    │   ├── css                   // 例如 CSS.....
    │   └── images                // 和图像
    └── templates                 // 用来生成页面的模板
        ├── base.html             // 模板的基础
        ├── archives.html         // 文章归档
        ├── period_archives.html  // 分时段的文章归档
        ├── article.html          // 文章
        ├── author.html           // 按作者分类的文章归档
        ├── authors.html          // 作者列表
        ├── categories.html       // 分类列表
        ├── category.html         // 按分类的文章归档
        ├── index.html            // 首页
        ├── page.html             // 页面
        ├── tag.html              // 标签
        ├── pagination.html       // 分页
        └── tags.html             // 标签列表
        
        
如果觉得哪个用不到,直接删掉好了~

另外,像首页啦,分类列表,标签列表这些直接生成一个页面的模板 ( 区分于文章和页面这样的模板 ),如果汝有用到其他的,记得在
:code:`pelican.conf` 里设置:

.. code-block:: python 
    
    # 直接生成的页面的列表,不用加上 .html 的扩展名.
    
    DIRECT_TEMPLATES = (('index', 'archives', 'search'))
    
Step 2 : 修改基础模板 ( base.html )
--------------------------------------------------------

(请允许咱不厌其烦的把 html 代码贴在上面 _(:з」∠)_ ) 

.. code-block:: html

    <!DOCTYPE html>
        <!-- 
        像这样用大括号包围起来的是变量啦,可以来自 pelicanconf.py 
        文章,页面,和分类也提供了一些变量呐~     
        -->
        <html lang="{{ DEFAULT_LANG }}">
        <head>
        <!-- 
        其它模板页面可以扩展或是替换掉 block 中的内容呗~
        -->
        {% block head %}
        <title>{% block title %}{{ SITENAME }}{% endblock title %}</title>
        <meta charset="utf-8" />
        <!-- 
        Python 中的 if 语句,如果后面的变量有内容的话,显示块中的内容呗~
        -->
        {% if FEED_ALL_ATOM %}
        ......
        {% endblock head %}
    </head>

    <body id="index" class="home">
        <header id="banner" class="body">
                <h1><a href="{{ SITEURL }}/">{{ SITENAME }} <strong>{{ SITESUBTITLE }}</strong></a></h1>
        </header><!-- /#banner -->
        <nav id="menu"><ul>
         <!-- 
        Python 中的 for 循环 <(=﹁"﹁=)>
        -->
        {% for title, link in MENUITEMS %}
            <li><a href="{{ link }}">{{ title }}</a></li>
        {% endfor %}
        {% block content %}
        {% endblock %}
        <footer id="contentinfo" class="body">
                <address id="about" class="vcard body">
                Proudly powered by <a href="http://getpelican.com/">Pelican</a>,
                which takes great advantage of <a href="http://python.org">Python</a>.
                </address><!-- /#about -->
        </footer><!-- /#contentinfo -->
    </body>
    </html>

因为 Pelican 用的是 Jinja2 模板引擎嘛,所以 `先去看看 Jinja 的文档嘛~ <http://jinja.pocoo.org/docs/dev/>`_

如果看不懂的话,pia 啦~  (╯・﹏・)╯ ┻━┻ 这里只举一个加入 CSS 的栗子:

首先把 CSS 放到 :code:`static/css` 文件夹里,然后修改 :code:`base.html`

.. code-block:: html

    {% block head %}
        <!-- static 文件夹输出以后就是 /theme 啦~ -->
        <link href="{{ SITEURL }}/theme/css/metro.css" rel="stylesheet">
        
    {% endblock head %}

就这样 ~(>_<~)

Step 3 : 修改子模板
---------------------------------

再举一个文章的栗子 (\´・ω・\`)

.. code-block:: html

    <!-- entends 就是扩展啦,后面是基于的模板页面的名称 -->
    {% extends "base.html" %}
    {% block head %}
    <!-- 用 super () 来包含 block 默认的内容 --> 
    {{ super() }}
    {% for keyword in article.keywords %}
    <meta name="keywords" content="{{keyword}}" />
    {% endfor %}

    {% endblock %}
    {% block content %}
    <!-- 这里加上需要的内容 -->
    {% endfor %}
    </dl>
    {% endblock %}

本来打算贴出模板里能用的变量的，不过咱有了要翻译 Pelican 文档的想法了呐~ （ todo list 要爆炸的节奏 )

