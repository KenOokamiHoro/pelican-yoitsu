发布 Pelican 博客的几种方式
==================================================

:slug: pelican_output
:lang: zh
:date: 2016-03-19
:tags: pelican,blog
:series: Pelican 
:desc: 汝的 Pelican 博客有几篇文章了没？想不想让其他人也看到？
:issueid: 32

.. PELICAN_BEGIN_SUMMARY

汝的 Pelican 博客有几篇文章了没？想不想让其他人也看到？

.. PELICAN_END_SUMMARY

如果汝在一开始使用了 :code:`pelican-quickstart` 来创建汝的博客的话,后面可能会方便点呗~ 
( 因为它帮汝生成了一个 Makefile 呐~ 然而 baka Windows 并不能用 Makefile (ノ=Д=)ノ┻━┻ )

.. contents::

没用 pelican-quickstart ? 可以自己来写一个 Makefile 呗~
------------------------------------------------------------------

样例在这：

.. raw:: html
    
    <script src="https://gist.github.com/KenOokamiHoro/223974ff5884c6eb82a5.js"></script>
    
(这里用到了 Github Gist 嘛,如果看不到的话, `链接在这 <https://gist.github.com/KenOokamiHoro/223974ff5884c6eb82a5>`_ )

对于喜欢自己动手的 :del:`菊苣` 来说, Makefile 里的命令稍加改造就可以直接执行呐~

在下面的例子中，上面是 Make 规则，下面是实际运行的命令呗~

( :code:`/path/to/your/content/` 是汝的内容存放的目录, :code:`path/to/your/settings.py` 是汝设置文件的目录,
记得用汝实际的位置替换呗~ )

    make html
    
----------------------------------------
    
    pelican /path/to/your/content/ -s path/to/your/settings.py
    

首先当然是要生成汝的博客啦~
-------------------------------

如果加上 :code:`-r` 或者 :code:`--autoreload` 参数, Pelican 就会在源文件更改时自动更新输出哟~

如果汝在一开始使用了 :code:`pelican-quickstart` ,会生成一个发布时使用的 :code:`publishconf.py` 文件呐~

(需要时就用它呗)

    make html
    
----------------------------------------
    
    pelican /path/to/your/content/ [-s path/to/your/settings.py]

接下来先在本机上预览一下~
---------------------------------

如果出了错再重新上传一遍岂不是很痛苦 (╯>＿<)╯ ┻━┻

如果汝在一开始使用了 :code:`pelican-quickstart` 会在汝的文件夹下生成一个 :code:`developer_server.sh` 文件,像这样?

.. raw:: html
    
    <script src="https://gist.github.com/KenOokamiHoro/011731f75ee363ef4460.js"></script>
    
(这里用到了 Github Gist 嘛,如果看不到的话, `链接在这 again~ <https://gist.github.com/KenOokamiHoro/011731f75ee363ef4460>`_ )

如果是这样的话让它帮忙就好了啦~

    # 一共有 start | stop | restart 三个选项,该知道是啥了吧 (\´･ω･\`)
    
    sh developer_server.sh start

或者切换到输出的目录,然后直接运行 :code:`pelican.server`

    python -m pelican.server
    
在汝的浏览器上打开 :code:`http://localhost:8000` 看看效果呗~

如果一切 OK 的话,用 :code:`publishconf.py` 生成发布时用的文件呗~

如果汝用自己的服务器?
-----------------------------------

如果汝用自己的服务器的话,修改汝的 Makefile 的这一段:

如果汝在用FTP : ( 不过为啥还在用不安全的 FTP 啊 (╯‵Д′)╯ ┻━┻ )

    FTP_HOST=localhost # 这是主机的域名或者 IP 地址
    
    FTP_USER=anonymous # 这是用户名
    
    FTP_TARGET_DIR=/ # 这是上传到的文件夹

然后安装 lftp ( Makefile 里用的是这个 )

如果汝在用 SFTP (SCP/SSH) : 

    SSH_HOST=localhost # 这是主机的域名或者 IP 地址
    
    SSH_PORT=22 # 如果主机的 SSH 端口不是默认的记得改~
    
    SSH_USER=root # 这是用户名
    
    SSH_TARGET_DIR=/var/www # 这是上传到的文件夹

然后安装 scp ( Makefile 里用的是这个 )    

接下来只要直接 Make 相应的目标就可以啦~


    make ssh_upload
    
----------------------------------------
    
    pelican /path/to/your/content/ -s path/to/your/publishconf.py
    
    scp -P $(SSH_PORT) -r $(OUTPUTDIR)/* $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR)
    
    
--------------------------------------------


    make ftp_upload
    
----------------------------------------
    
    pelican /path/to/your/content/ -s path/to/your/publishconf.py
    
    lftp ftp://$(FTP_USER)@$(FTP_HOST) -e "mirror -R $(OUTPUTDIR) $(FTP_TARGET_DIR) ; quit"
    
如果汝打算用 Github Pages ?
-------------------------------------

:fref:`farseerfc` 写过一篇利用 Travis 生成 Github Pages 的教程:

    `用 Travis-CI 生成 Github Pages 博客  <http://farseerfc.me/travis-push-to-github-pages-blog.html>`_
    

----------------------

要上传到像 Amazon S3 ,Dropbox 一类的地方 , `还是看Pelican官方的文档吧 <http://docs.getpelican.com/en/3.6.3/publish.html>`_


    
