su, sudo, pkexec 和 root 的二三事
==================================================

:slug: su_sudo_and_pkexec
:lang: zh
:date: 2018-08-01
:tags:
:desc: "到底有啥区别  _(:з」∠)_"


.. PELICAN_BEGIN_SUMMARY

到底有啥区别  \_(:з」∠)\_

.. PELICAN_END_SUMMARY

.. contents::

什么是 root 权限？
-----------------------------------------------

这里的 root 权限，实际上指的是名为 root 的账户所具有的权限。“获取 root 权限”，指的实际是获取 root 账户的使用权。
root 是系统中权限最高的用户。和 Windows 的 Administrator 相比，权限不知道高到哪里去了。
（注：在 Windows 中想获取较高的权限，可以使用 NSudo）举个例子：Administrator 是删除不了 explorer.exe 文件的，
而 root 可以删除你当前正在使用的 shell 甚至整个系统 。

在 GNU/Linux 上 切换到 root 用户的方法大有三种：


su - 其实是切换用户 (switch user)
------------------------------------------------

    su [options] [-] [user [argument...]]

su 其实是用来切换用户的啦~ 如果没参数的话，默认是切换到 root 的，所以会询问 root 的密码。

以及 su 默认只改变 HOME 和 SHELL 变量，可以用 --login （或者更短些的 - ）来规避混合环境变量的副作用：

    su -

直到离开 shell 之前都能保持 root 权限。

sudo - 家庭常备（误）
-------------------------------------------------

sudo(substitute user do) 使得系统管理员可以授权特定用户或用户组作为 root 或他用户执行某些（或所有）命令，
同时还能够对命令及其参数提供审核跟踪。

一个典型的 sudo 操作大概像这样：

.. code-block:: bash

    $ sudo pacman -Syu
    # sudo 会请求当前用户的密码
    [sudo] password for horo:
    # 然后就可以用 root 权限执行某些操作啦~
    :: Synchronizing package databases...

不过不是所有能用 sudo 这一个命令的家伙都能获得 root 权限的啦~
sudo 君有一个小名单，只有名单上的人才可以使用 sudo。这个名单就是 /etc/sudoers 。

为了确保正确的更改 /etc/sudoers 文件，通常会使用 visudo 命令。
visudo会锁住 sudoers 文件，保存修改到临时文件，然后检查文件格式，
确保正确后才会覆盖 sudoers 文件。

pkexec - Systemd 全家桶内置（误）
-------------------------------------------------

    pkexec allows an authorized user to execute PROGRAM as another user.
    If PROGRAM is not specified.the default shell will be run.
    If username is not specified, then the program will be executed as
    the administrative super user, root.

pkexec 用法上和 sudo 大概差不多：

.. code-block:: bash

    $ pkexec <command>

不过这个貌似桌面环境里用的比较多，在桌面环境上接下来会弹出一个对话框，
提示汝输入汝自己的密码，大概像这样（GNOME 大概像这样……）：

.. image:: /images/pkexec.png
   :alt: pkexec

所以这仨到底有啥区别还是没搞清楚  \_(:з」∠)\_