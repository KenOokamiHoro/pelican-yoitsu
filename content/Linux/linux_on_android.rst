在 Android 上使用 GNU/Linux 工具 
========================================================================

:slug: linux_on_android
:lang: zh
:date: 2020-04-28
:tags: 
:desc: 在 Android 上使用常用的 GNU/Linux 工具，甚至……

虽然……
---------------------------------------------------------------
虽然汝可能从哪里听说过 Android 是基于 Linux 内核的啦，不过大多数时候汝大概没办法直接把汝爱用的 GNU/Linux 发行版上的
工具直接拿过来用，为啥咧？

* 手机和电脑不是一种 CPU（PC 常见的就是 x86_64 , 手机上比较常见的是 arm64（有时也称作 aarch64）），因此
  两边的二进制文件并不能直接拿来换着用。

* 虽然 Android 用了 Linux 内核，但是和普通的 GNU/Linux 发行版还是有很大的区别的。例如以 bionic 取代glibc
  （C 函数库）、以 Skia 取代 Cairo （用于向量图形绘图）、再以 OpenCORE 取代 FFmpeg（常用于音视频的录影和转换）。
  于是不少依赖它们的软件不好运行。

* 以及随着越来越多的 GNU/Linux 发行版开始用 Systemd 作为初始化程序，很多程序或多或少就和 Systemd 有了些关联（或者依赖）。
  不巧的是……

:del:`所以 Android 应用们不好用了么？`

STAGE 0 - Android 上的终端（模拟器）
---------------------------------------------------------------
如果汝有用过一些需要在电脑上运行一个辅助程序的 Android 应用（像是
`AppOps <https://appops.rikka.app/zh-hans/guide/>`_ 这样的），汝其实已经离 Shell 很近了。

在手机和电脑连接以后，可以运行 adb shell 命令获得一个 shell：

.. code-block:: text

    (PC) $ adb shell
    walleye:/ $

:del:`接下来就可以 cd ls 乱出了……` （啥？） 因为别的汝也干不了啊……

如果汝的手机有 root 权限的话，在 Shell 里运行 su 试试？第一次的话汝的手机上的
root 权限管理程序应该会提示汝授予权限什么的。

要想在手机上试试的话，也有差不多的终端模拟器程序可以选择。
`例如这个？ <https://play.google.com/store/apps/details?id=jackpal.androidterm>`_ 

STAGE 1 - 在 Android 上使用（部份） GNU/Linux 程序
---------------------------------------------------------------

    只有个 shell 哪里够……

正好也有一群人这么想，于是它们把终端模拟器和一些常用的 GNU/Linux 应用程序组合起来，创造出了
`Termux <https://play.google.com/store/apps/details?id=com.termux&hl=zh>`_。

.. image:: /images/linux_on_android/termux_intro.png
    :alt: Termux 的启动界面

（这个在电脑上显示 Android 设备画面的程序是
`scrcpy <https://github.com/Genymobile/scrcpy>`_ 啦~）

这里面提到的 pkg 其实就是有点小包装的 apt 啦，熟悉 Debian / Ubuntu 的话应该不会陌生。

下面的 root / Unstable / x11 是额外的仓库，需要使用要求 root 权限或者图形界面的软件在里面。

* Play 商店上也有几个 Termux 使用的插件，可以实现自定义字体、浮动窗口、快速运行脚本、访问手机内存设备
  等功能。有几个是收费的，不过有别的地方可以免费下载（小声）

* 键盘上面的特殊按键可以通过编辑 ~/.termux/termux.properties 文件中的 extra-keys 属性来修改，
  `特殊键的定义在这里。 <https://wiki.termux.com/wiki/Touch_Keyboard>`_。

* 偷懒的话也可以直接安装一个有这些特殊键的输入法，例如
  `Hacker's Keyboard <https://play.google.com/store/apps/details?id=org.pocketworkstation.pckeyboard&hl=zh>`_

* 当然外接键盘也是可以的。

`以及大概有人踩过了不少的坑的样子…… <https://www.sqlsec.com/2018/05/termux.html>`_ 

STAGE 2 - 在 Android 上运行（部份） GNU/Linux 容器
---------------------------------------------------------------

    :del:`啊 pkg/apt 好不习惯……`

怀念汝习惯的 GNU/Linux 发行版的原汁原味的包管理器和其它工具？可以考虑运行一个容器来解馋（？）

    Linux® 容器是与系统其他部分隔离开的一系列进程。运行这些进程所需的所有文件都由另一个镜像提供，
    这意味着从开发到测试再到生产的整个过程中，Linux 容器都具有可移植性和一致性。

    -- `RedHat : 什么是 Linux 容器？ <https://www.redhat.com/zh/topics/containers/whats-a-linux-container>`_

实现这种操作的关键要素就是 chroot ，它针对正在运作的软件行程和它的子进程，改变它外显的根目录。
在使用 chroot 之后，进程就会认为设置的目录是根目录，就可以进行各种操作啦，例如隔离访问或者修复系统什么的。

当然这操作是要 root 权限的，至于后来有人写出用户空间中的
`proot <https://proot-me.github.io/>`_ ，就是另一个故事啦……

* 手机有 root 权限的话，可以用 chroot 安装一个 GNU/Linux 容器。也有像是
  `Linux Deploy <https://github.com/meefik/linuxdeploy>`_ 这样的应用可以简化这一过程。

* 没有 root 权限的话，借助前面提起的 proot 也能实现差不多的操作。也有像是
  `UserLAnd <https://f-droid.org/zh_Hans/packages/tech.ula/>`_ 和
  `TermuxArch <https://github.com/SDRausty/TermuxArch>`_ 这样的工具帮忙简化这一过程。

`以及大概也有人踩过了不少的坑的样子…… <https://bbs.letitfly.me/d/1086>`_ 

    那如果咱就是想从零（？）开始呢？

:del:`那下次再说吧……`

