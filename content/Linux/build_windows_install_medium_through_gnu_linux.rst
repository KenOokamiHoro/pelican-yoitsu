在 GNU/Linux 下制作 Windows 的安装 USB
==========================================================

:slug: build_windows_install_medium_through_gnu_linux
:lang: zh
:date: 2018-04-10
:tags: Linux, Windows, 操作
:desc: 嗯……
:issueid: 

.. PELICAN_BEGIN_SUMMARY

嗯……

.. PELICAN_END_SUMMARY

.. contents::

UEFI
------------------------

最简单的方法是把 U 盘格式化成 FAT32 文件系统，然后把 ISO 里的文件复制到 U 盘上。
（不是把那一个 ISO 文件复制到 U 盘上啦~）

BIOS
-----------------------

貌似直接 dd 不起作用…… 

有人说 unetbootin 可行，但是咱装好以后一片空白…… 😂

或者开个 Windows 虚拟机试试 rufus 或者巨硬的 Media Creation Tool ？（……）

于是就 ms-sys 了（不是 msys 😂，是 http://ms-sys.sourceforge.net/ ）

* 首先把汝的 U 盘格式化成 MBR 分区表，然后新建一个 NTFS 分区并把它设置成活动分区 （cfdisk 一气呵成（误））

    于是假设它是 /dev/sdb ，如果 mkfs.ntfs 的时候太慢的话可以加个 -Q 试试（

* 然后把 Windows 的安装 ISO 挂载上，里面的文件复制到 U 盘上。

* 把 Windows 7 的 MBR 写进 U 盘里去：

    sudo ms-sys -7 /dev/sdb

* 把 NTFS 的 PBR 写进 U 盘的第一个分区（大概也是唯一一个）里去：

    sudo ms-sys -n /dev/sdb1

大概就可以用啦 😂 :del:`于是咱又水了一次……`

