浅说基于 Linux 内核的操作系统 (6) - 安装 Debian
==========================================================

:slug: linux_install_debian
:lang: zh
:date: 2016-04-19
:tags: Linux
:series: 浅说基于 Linux 内核的操作系统
:desc: 接着咱再来装 Debian  ~(>_<~)
:issueid: 18

.. PELICAN_BEGIN_SUMMARY

接着咱再来装 Debian  ~(>_<~)

.. PELICAN_END_SUMMARY

.. contents::

下载 Debian 
---------------------------------------------
   
:del:`话说 Debian 从官网到 Wiki 都有一种年久失修的气息😂😂`

`这是官方的下载页面 <https://www.debian.org/distrib/>`_

嗯……看不懂的话直接去崔土豪源吧😂😂 

DVD : https://mirrors.ustc.edu.cn/debian-cd/8.4.0/amd64/iso-dvd/

( 不用下载带 Update 的 iso )

Live DVD : https://mirrors.ustc.edu.cn/debian-cd/8.4.0-live/amd64/iso-hybrid/ 

( 后面的 xxxx-desktop 表示的是某种环境的 Live 系统 )


然而咱并没有找到 Debian 的系统需求啊 (╯•̀∧•́)╯ ┻━┻

后面咱就用 DVD 映像啦 ~(>_<~)
  
区分 MBR 和 UEFI 系统 | ω・\`)
-------------------------------------------

MBR 系统启动安装映像之后大概像这样 _(:з」∠)_

.. image:: /images/Installing_Debian/1.png
   :alt: MBR 系统启动之后大概像这样 
      
* Install ： 基于终端的安装界面

* Graphical Install : 基于 GUI 的安装界面 | ω・\`)  

* Advanced Options : 高级选项......

* Help : 帮助

* Install with speech synchesis : 在文字转语音支持下安装 ( 然而估计用不到 😂😂 )

( 对于 Live ISO ,会多一个 "Start Debian Live" 的选项,可以来进行测试 o(\*￣3￣)o

而 UEFI 启动大概像这样 (\´・ω・\`)

.. image:: /images/Installing_Debian/2.png
   :alt: UEFI啦~


那么 Advanced Options 都有哪些咧 ?

.. image:: /images/Installing_Debian/3.png
   :alt: 这是高级选项 _(:з」∠)_
   
* Expert Install : 以 Expert Mode 进行安装 ( :ruby:`专家|啰嗦` 模式 )

* Rescue Mode : 修复当前系统的急救模式
* Automated Install : 自动安装 ( 然而咱没用过 😂😂 )

( 下面三个带 Graphical 的选项就是图形界面下的啦~ )

第一阶段：语言和区域，设置网络和创建用户
----------------------------------------

* 选择语言，在安装程序中选择的语言会是汝安装好的系统的语言呗~

.. image:: /images/Installing_Debian/4.png
   :alt: 选择语言
   
* 例行提示所选语言的翻译不完整  _(:з」∠)_

.. image:: /images/Installing_Debian/5.png
   :alt: 提示翻译不完整……
   
* 选择汝的区域,一般选择汝所在的位置就好啦~

.. image:: /images/Installing_Debian/6.png
   :alt: 选择区域
   
* 配置键盘映射,默认的应该就合适 <(=﹁"﹁=)>

.. image:: /images/Installing_Debian/7.png
   :alt: 键盘映射
   
* 探测网络设备,偶尔会提示缺少某些驱动,先记下来.

    ( VirtualBox 不缺驱动所以没截图 😂😂 )

* 探测连接,没有的直接按取消吧 😂😂

.. image:: /images/Installing_Debian/9.png
   :alt: 探测网络连接
   
* 探测网络连接失败,接着选 "现在不进行网络设置" 然后下一步.

.. image:: /images/Installing_Debian/8.png
   :alt: 探测网络连接失败   _(:з」∠)_

* 设置一个主机名,一般可以随便些~

.. image:: /images/Installing_Debian/10.png
   :alt: 设置主机名
   
* 这里可以选择为 root 用户设置一个密码,也可以不设置

    考虑到安全性还是不要设置的好  <(=﹁"﹁=)>
    
.. image:: /images/Installing_Debian/12.png
   :alt: 可选为 root 用户设置个密码
   
* 接着创建一个用户,因为 root 没有密码,所以这个用户可以通过 sudo 获得 root 权限 ~(>_<~)

.. image:: /images/Installing_Debian/13.png
   :alt: 创建新用户
   
.. image:: /images/Installing_Debian/14.png
   :alt: 创建新用户
   
.. image:: /images/Installing_Debian/15.png
   :alt: 为新用户设置密码
   
然后又到最坑的环节啦 😂😂

第二阶段 : 磁盘分区和安装基本系统 Σ( ° △ °|||)
--------------------------------------------------

* 选择一种分区方式,为了不坑选择手动 ʃ ̂͜•̄ ̱̩ ̄͜►

.. image:: /images/Installing_Debian/16.png
   :alt: 选择一种分区方式
   
* 如果汝计划在一块新硬盘上安装系统的话,先创建一下分区表呗~

    点击设备名称来创建分区表 <(ノ=﹁"﹁=)ノ┻━┻
    
.. image:: /images/Installing_Debian/18.png
   :alt: 分区菜单
   
.. image:: /images/Installing_Debian/19.png
   :alt: 为设备创建分区表
   
* 接下来点击那个新的 "空闲空间" 创建一个新分区呗~

.. image:: /images/Installing_Debian/20.png
   :alt: 创建新分区
   
* 设置新分区的大小.
   
.. image:: /images/Installing_Debian/21.png
   :alt: 新分区的大小
   
* 调整新分区的属性,一般设置挂载点就好 ( :code:`/` 是必须的 (╯ˊ_>ˋ)╯ ┻━┻ )
   
.. image:: /images/Installing_Debian/22.png
   :alt: 编辑分区的详细信息
   
* 然后效果大概像这样 _(:з」∠)_

.. image:: /images/Installing_Debian/23.png
   :alt: 分区的详细信息
   
* 对于 UEFI 系统，还需要一个 EFI 系统分区呗~

.. image:: /images/Installing_Debian/24.png
   :alt: 创建一个 EFI 系统分区 

.. image:: /images/Installing_Debian/25.png
   :alt: 包括 EFI 系统分区的分区表 
   
* 单击 "分区设定结束并将设置写入磁盘" ,这时会提示没有创建交换空间 😂😂

    对于 RAM 小于 1GB 的用户，交换空间通常是推荐的 ( 一般推荐是RAM 的两倍...... )，但是对于拥有大量的物理内存的用户来说是否使用主要看个人口味了 (尽管它对于休眠到硬盘支持是必须的)。
    
    自己看着办呗~ ......
    
    :del:`大土壕 hjc 这样说:`
    
    David Huang 🌚, [16.04.16 04:38]
    有啊

    David Huang 🌚, [16.04.16 04:38]
    能把更冷的内存压缩后放磁盘上，将腾出来的空间用于更热的缓存

    David Huang 🌚, [16.04.16 04:39]
    比如我chrome泄露了50个G的内存

    David Huang 🌚, [16.04.16 04:39]
    我总共有64G内存

    David Huang 🌚, [16.04.16 04:39]
    这个时候系统就会把这50G压缩后放在硬盘上

    David Huang 🌚, [16.04.16 04:40]
    然后这些空间用于缓存

    David Huang 🌚, [16.04.16 04:40]
    明显提高系统运行速度

    David Huang 🌚, [16.04.16 04:40]
    →_→

.. image:: /images/Installing_Debian/26.png
   :alt: 提示没有交换空间 ......
   
* 接着该确认了.

    一旦选了是,就没有回头路了呐~
    
.. image:: /images/Installing_Debian/27.png
   :alt: 确认提交对硬盘分区的更改
   
* 开始安装基本系统,去喝杯咖啡呗~ 😂

   
只有 DVD 安装需要的第三阶段 : 设置软件包管理器
---------------------------------------------------

* 只有在汝有其他的 DVD 映像的时才需要扫描呐~

.. image:: /images/Installing_Debian/28.png
   :alt: 插别的 DVD ?

* 虽然可以在这里选择网络映像，但前提汝要有足够的网速和时间 😂😂

.. image:: /images/Installing_Debian/29.png
   :alt: 使用网络镜像
   
* 要不要参加软件包流行度调查随便啦~
   
.. image:: /images/Installing_Debian/31.png
   :alt: 参加软件包流行度调查?
   
* 选择安装的软件包组，一般就是再选一个桌面环境 😂

.. image:: /images/Installing_Debian/32.png
   :alt: 选择软件包集
  
* 然后接着装（可以去把刚刚泡的咖啡喝了 😂

.. image:: /images/Installing_Debian/33.png
   :alt: 安装软件包
   
   
只有 MBR 安装需要的第三阶段 : 安装 GRUB 启动管理器
---------------------------------------------------

（ 其实 UEFI 安装也有这一步啦。只是不需要汝手动设置啦~ ）

* 要把 GRUB 安装到 MBR 上吗? ( 当然是要啦~ )

.. image:: /images/Installing_Debian/34.png
   :alt: 要把 GRUB 安装到 MBR 上吗?
   
     
* 然后选择要安装到的硬盘上呗~

.. image:: /images/Installing_Debian/35.png
   :alt: 然后选择要安装到的硬盘
   
最后 ......
-----------------------------------------------

* 收工重启 ......

.. image:: /images/Installing_Debian/38.png
   :alt: 重启
   
* 两个选项，分别是启动系统和高级选项 _(:з」∠)_

.. image:: /images/Installing_Debian/39.png
   :alt: GRUB
   
* 高级选项的两个选项，第一和刚刚的第一个一样，第二个是进入恢复模式 (╯T皿T)╯ ┻━┻

.. image:: /images/Installing_Debian/40.png
   :alt: 高级选项
   
* 该登录啦~

.. image:: /images/Installing_Debian/41.png
   :alt: 登录
   
--------------------------------------

:del:`咱要不要再装个 openSUSE 😂😂`
