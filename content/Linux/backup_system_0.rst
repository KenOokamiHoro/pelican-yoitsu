备份 GNU/Linux 操作系统的经（che）验（dan）合集
==========================================================

:slug: backup_system_0.rst
:lang: zh
:date: 2018-12-17
:tags: Arch Linux,btrfs,备份
:desc: 备份 GNU/Linux 操作系统的经（che）验（dan）合集
:issueid:

备份 GNU/Linux 操作系统的经（che）验（dan）合集

.. PELICAN_END_SUMMARY

.. contents::

为啥要备份？
-------------------

* 防震减灾（雾），其实就是防止各种意外情况（例如硬盘坏掉，电脑丢失等）发生时手足无措……

    那备份的硬盘也坏了怎么办（先有鸡还是先有蛋？）

* 方便迁移（例如买了新电脑的时候）

* ……

Point 0: 要备份些啥？
---------------------

一个典型的 GNU/Linux 的根文件系统大概像这个样子：

.. code-block:: text

    lrwxrwxrwx   1 root root     7 Aug 21 22:21 bin -> usr/bin
    drwxr-xr-x   4 root root 16384 Jan  1  1970 boot
    drwxr-xr-x  22 root root  3460 Dec 19 11:27 dev
    drwxr-xr-x   1 root root  3464 Dec 19 14:13 etc
    drwxr-xr-x   1 root root     8 Dec 19 10:55 home
    lrwxrwxrwx   1 root root     7 Aug 21 22:21 lib -> usr/lib
    lrwxrwxrwx   1 root root     7 Aug 21 22:21 lib64 -> usr/lib
    drwxr-xr-x   1 root root   114 Dec 19 14:19 mnt
    drwxr-xr-x   1 root root   138 Dec 17 09:19 opt
    dr-xr-xr-x 298 root root     0 Dec 19 11:26 proc
    drwxr-x---   1 root root   212 Dec 19 14:47 root
    drwxr-xr-x  24 root root   640 Dec 19 14:13 run
    lrwxrwxrwx   1 root root     7 Aug 21 22:21 sbin -> usr/bin
    drwxr-xr-x   1 root root    14 Nov 20 23:34 srv
    dr-xr-xr-x  13 root root     0 Dec 19 11:26 sys
    drwxrwxrwt  15 root root  1460 Dec 19 14:46 tmp
    drwxr-xr-x   1 root root    80 Dec 19 14:13 usr
    drwxr-xr-x   1 root root   116 Dec 19 08:46 var

熟稔 `FHS <https://wiki.linuxfoundation.org/lsb/fhs>`_ 的话应该知道有些目录其实是不在
硬盘上的（例如 /dev , /sys 和 /proc 等等），还有些目录下的文件没什么用（例如 /tmp , /var/tmp 等等）。

那么该备份哪些呢？

Point 1: rsync
---------------------

    rsync 是 Unix 下的一款应用软件，它能同步更新两处计算机的文件与目录，
    并适当利用差分编码以减少数据传输量。

    rsync 中的一项同类软件不常见的重要特性是每个目标的镜像只需发送一次。
    rsync 可以拷贝 ／ 显示目录内容，以及拷贝文件，并可选压缩以及递归拷贝。

既然备份只是把文件从一个地方复制到另一个地方而已，本来只用 cp 说不定也行。
但是为了省事（例如只复制变化的部分），处理各种状况（例如文件属性和权限），
跨设备和网络复制（例如通过 SSH )，就有了 rsync 咯~

rsync 核心的用法和 cp 几乎一样：

    rsync <source> <target>

例如要把 /home 下的内容复制到 /data：

    rsync /home /data

当然为了应对各种情况， rsync 也有不同的参数：

    * --archive / -a ：存档模式，等于 -rlptgoD （这些短参数是啥啦……），
      会保留文件的大多数属性（例如不会留扩展属性、ACL 和硬链接）和符号链接。

        想要保留扩展属性 / ACL 和硬链接的话，可以分别使用 --xattrs (-X) ，
        --acls (-A) 或 --hard-links (-H) 参数。

    * --one-file-system：只备份这一个文件系统的内容，于是就不会备份
      /sys 啊 /proc 啊 /dev 啊 /tmp 这类目录了，当然如果汝和咱一样 /home
      是单独挂载的话也会……

    * --numeric-ids：把用户名/组名处理成数字而不是实际的名称，跨不同的系统备份时
      可能会有用。

    * --verbose：顾 GNU 参数列表思义的 “罗嗦” 模式，会详细输出正在传送的文件的信息。

    * --progress：显示一个传送文件的进度显示。

    * --exclude 和 --exclude-from：指定传送过程中排除哪些文件。 --exclude 用来明确地指出
      排除哪些文件， --exclude-from 是一个包含文件名列表的文件。

        当然和 --exclude 相对的自然有 --include 和 --include-from，用法类似。

    * --dry-run / -n：模拟运行，防止手抖 😂

当然 rsync 的参数远不止这些啦，剩下的自己查手册页（ man rsync ）咯。于是咱自己备份的话通常是这样：

    rsync --archive --acls --xattrs --verbose --progres /source /target

啥，汝问为啥没有 --one-file-system ？那是因为咱一般都会把根分区单独再挂载到别的地方再 rsync。
没有 --exclude 纯粹就是懒了 😂

Point 2: btrfs 的 subvolume
-------------------------------------

btrfs 的子卷（subvolume）看起来和普通的目录无异，但是可以通过不同的挂载参数让它作为单独的分区挂载，
例如挂载 /dev/sda2 上的子卷 ./root 到 /mnt ：

    当然汝也能在 -o 后面加上别的参数啦，例如 --compress=lzo 启用压缩什么的，记得不同的参数用
    逗号分开。

    # mount -o subvol=root /dev/sda2 /mnt

要创建一个子卷的话，可以用 btrfs subvolume create 命令：

    # btrfs subvolume create <name_of_your_subvolume>

要列出子卷的列表的话，可以用 btrfs subvolume list 命令：

    # btrfs subvolume list <path/to/your/volume>

.. code-block:: bash

    root@yoitsu_optiplex /run/media/horo/External
    # btrfs subvolume list .                                             :(
    ID 257 gen 2007 top level 5 path data
    ID 258 gen 2088 top level 5 path local
    ID 294 gen 2016 top level 5 path backup
    ID 295 gen 2012 top level 294 path backup/home_20181031
    ID 328 gen 2086 top level 294 path backup/parabola_root_20181031
    ID 353 gen 1998 top level 294 path backup/home_20181215
    ID 360 gen 2005 top level 294 path backup/parabola_root_20181215
    ID 373 gen 2086 top level 294 path backup/arch_optiplex
    ID 432 gen 2089 top level 5 path temp

建立的子卷可以是某个子卷的快照：

    # btrfs subvolume snapshot [-r] <source> <target>

    -r 参数用来创建只读快照

可以通过 btrfs send 和 btrfs recieve 在两个 btrfs 卷之间传送子卷（或子卷的快照），不过要是
只读的才行：

    # btrfs send <path/to/your/subvolume> | btrfs recieve <path/to/your/save/place>

如果创建快照时忘记设置只读，可以通过 btrfs property 命令设置上（当然也可以取消只读）：

    # btrfs property set -ts <path/to/your/subvolume> ro [true|false]

汝也可以传递增量快照，就像这样：

    # btrfs send <path/to/your/subvolume> -p <path/to/your/parent_subvolume> |
    btrfs recieve <path/to/your/save/place>

不过汝要先把父快照传送到目标才行。

例如建立今天的快照然后以 2018 年 10 月 31 号的快照为基础发送出去：

    date 命令返回今天的日期和时间，也可以指定一个格式字符串来按某个特定的格式输出。

    以 $ 包围的命令会以它的结果填充，例如 echo "root_$(date +"%Y%m%d")" 的
    结果是 20181219 （咱写完这篇文章的时间，汝自己运行的话结果肯定会和咱有变化）。

    # btrfs subvolume snapshot -r root root_$(date +"%Y%m%d")

    # btrfs send root_$(date +"%Y%m%d") -p root_20181031 | btrfs revieve /backup

Point 2: 使用 dm-crypt 加密分区
---------------------------------------

    为啥要加密应该不用再解释了吧，毕竟汝主目录里是不是有不少公开或者隐私的数据是不是？
    例如 cookies 和帐号配置啦，邮件啦，和聊天记录等等。

cryptsetup 是操作 dm-crypt 用的前端，可以这样创建一个加密的设备：

    # cryptsetup luksFormat <设备文件的名称>

如果需要的话，可以在 luksFormat 之前加上一些选项呐：

    +-------------+----------------+------------------+--------------------------------------------+
    | 参数的名称  | 这啥？         | 推荐是多少？     | :del:`一些废话`                            |
    +=============+================+==================+============================================+
    | --cipher    | 加密方式       | aes-xts-plain64  | AES 加密算法搭配 XTS 模式                  |
    +-------------+----------------+------------------+--------------------------------------------+
    | --key-size  | 密钥长度       | 512              |  XTS 模式需要两对密钥，                    |
    |             |                |                  |  每个的长度是256                           |
    +-------------+----------------+------------------+--------------------------------------------+
    | --hash      | 散列算法       | sha512           |                                            |
    +-------------+----------------+------------------+--------------------------------------------+
    | --iter-time | 迭代时间       | >10000           | 单位是毫秒。该值越大，暴力破解越难；       |
    |             |                |                  | 但是相应的汝打开加密分区时就要等一下啦~    |
    +-------------+----------------+------------------+--------------------------------------------+

例如这样？

.. code-block:: bash

    # cryptsetup --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 10000 luksFormat /dev/nvme0n1p1

    WARNING!
    ========
    这将覆盖 /dev/nvme0n1p1 上的数据，该动作不可取消。

    Are you sure? (Type uppercase yes):
    # 上面要打大写的 YES
    # 然后输入两遍加密分区的密码。
    输入密码：
    确认密码：

然后等一下就好咯~

然后用 cryptsetup open 打开分区：

    # cryptsetup open <已经加密的设备文件名称> <映射名称>

解密的分区会映射到 /dev/mapper/<汝设置的映射名称> 呗~

例如咱解密 /dev/nvme0n1p1 ，然后映射到 /dev/mapper/sysroot :

    # cryptsetup open /dev/nvme0n1p1 sysroot

接下来就像操作普通分区一样操作 /dev/mapper/sysroot 就好啦，例如创建文件系统：

    # mkfs.btrfs /dev/mapper/sysroot

----

推荐阅读
---------------------

* `rsync+btrfs+dm-crypt 备份整个系统 - 依云's Blog <https://blog.lilydjwg.me/2013/12/29/rsync-btrfs-dm-crypt-full-backup.42219.html>`_

* `保护数据，用 LUKS 给磁盘全盘加密 | Nova Kwok's Awesome Blog <https://nova.moe/encrypt-disk-with-luks/>`_
