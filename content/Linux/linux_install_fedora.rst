浅说基于 Linux 内核的操作系统 (5) - 安装 Fedora
==========================================================

:slug: linux_install_fedora
:lang: zh
:date: 2016-04-09
:tags: Linux
:series: 浅说基于 Linux 内核的操作系统
:desc: 汝是不是想说'废话少说快教我怎么装 Linux 发行版'啦~
:issueid: 19

.. PELICAN_BEGIN_SUMMARY

汝是不是想说'废话少说快教我怎么装 Linux 发行版'啦~

.. PELICAN_END_SUMMARY

如果汝已经在运行一个 Linux 发行版的话,那么可以跳过接下来的几节.

如果汝有些心虚,想在虚拟机上先尝试一下,可以先去读一下 
`扫盲操作系统虚拟机系列@ 编程随想的博客 <https://program-think.blogspot.com/2012/10/system-vm-0.html#index>`_

如果汝要直接装在电脑上,先做个USB启动盘啦~

( 啥? 不会? `看看这个 ? <https://program-think.blogspot.com/2013/12/create-bootable-usb-stick-from-iso.html>`_ )

.. contents::

下载 Fedora Workstation
---------------------------------------------
   
    
    Fedora Workstation Live 镜像让您可以为自己电脑制作完整的立即可用的 Fedora Workstation 系统介质。您可以使用 Live 镜像测试、体验 Fedora，同时无需改动硬盘内容。当您满意之后，您可以从 Live 镜像安装 Fedora 到您的硬盘。

    要使用该镜像，您需要可创建或刻录 DVD 的驱动器或者至少跟镜像大小一样的 USB 闪存盘。
    
    -- `这是官方的下载页面 <https://getfedora.org/zh_CN/workstation/download/>`_
 
    
Fedora 23 Workstation 系统需求  😂
--------------------------------------------

这是最低需求啦~

    * CPU : 1Ghz 及以上
    * 内存 : 1GB 及以上 ( 如果汝不需要桌面环境可以少些 ~(>_<~) )
    * 硬盘空间 : 10GB 或更多的剩余空间
    
如果汝希望使用硬件加速 3D 图形,汝需要:

    * GMA9xx/GeForce FX5xxx /Radeon 9500 或更新的显卡 _(:з」∠)_ 或者
    
    * 汝的 CPU 支持 SSE2 指令集,这样就可以通过 LLVMpipe 利用 CPU 加速 3D 图形.
    
        看看汝的 :code:`/proc/cpuinfo` 的 code:`flags` 一节里有没有 code:`SSE2` 啦~
        
在 Windows 下检查 ISO 的散列值
---------------------------------------------

1, 从 `https://fedoraproject.org/verify <https://fedoraproject.org/verify>`_ 根据选用的映像下载需要的散列值文件,
并把它们放在一起啦~

2, 打开 Windows PowerShell.切换到放置映像和散列值文件的文件夹.

3, 收集计算散列值需要的信息.

.. code-block:: bash
        
    > $image = "<此处是汝的 ISO 文件的名称>"
    > $checksum_file = "<此处是汝的 ISO 文件的散列值文件名称>"
    > $sha256 = New-Object -TypeName System.Security.Cryptography.sha256CryptoServiceProvider
    > $expected_checksum = ((Get-Content $checksum_file | Select-String -Pattern $image) -split " ")[0].ToLower()
        
4, 计算 ISO 的散列值,放轻松些 ( 可以去煮杯咖啡~ )

.. code-block:: bash
        
    > $download_checksum = [System.BitConverter]::ToString($sha256.ComputeHash([System.IO.File]::ReadAllBytes("$PWD\$image"))).ToLower() -replace '-', ''
        
5, 然后比较~
 
.. code-block:: bash 
    
    > echo "Download Checksum: $download_checksum"
    > echo "Expected Checksum: $expected_checksum"
    > if ( $download_checksum -eq "$expected_checksum" ) {
    echo "Checksum test passed!"
    } else {
    echo "Checksum test failed."
    }
  
区分 MBR 和 UEFI 系统 | ω・\`)
-------------------------------------------

MBR 系统启动安装映像之后大概像这样 _(:з」∠)_

.. image:: /images/Installing_Fedora/1.png
   :alt: MBR 系统启动之后大概像这样 
      
* Start Fedora Live : 启动 Fedora 的 Live 环境,可以试用或是进行安装.

* Troubleshooting : 供排解问题使用的额外选项 | ω・\`)  

* 按下 Tab 键可以在启动前修改内核参数 :del:`( 然而啥是内核参数咧 😂😂 )`

那么 Troubleshooting 都有哪些咧 ?

.. image:: /images/Installing_Fedora/2.png
   :alt: 这是疑难解答选项 _(:з」∠)_
   
* Start Fedora Live in basic graphics mode : 以基本图形 (不启用硬件加速模式) 启动 Fedora 的 Live 环境.

* Test this media & start Fedora Live : 在启动 Fedora 的 Live 环境之前先检查一下安装介质的完整性 Σ( ° △ °|||)

* Run a memory test : 运行内存测试.....

* Boot from local drive : 从本地硬盘启动操作系统.....

而 UEFI 启动大概像这样 (\´・ω・\`)

.. image:: /images/Installing_Fedora/26.png
   :alt: UEFI啦~
   
那还等啥? 直接 "Start Fedora Live" 啦~

.. image:: /images/Installing_Fedora/3.png
   :alt: 准备安装啦~

如果汝选择的是 "Try Fedora" , 从左侧的 :ruby:`Activities|活动` 中可以找到安装程序  (╯￣-￣)╯ ┻━┻

.. image:: /images/Installing_Fedora/4.png
   :alt: 从左侧的 Activities (活动) 中可以找到安装程序 (╯￣-￣)╯ ┻━┻
   
就是这里啦~ (用鼠标往左上角推)

.. image:: /images/Installing_Fedora/5.png
   :alt: Install Fedora to disk 在这里啦~

开始安装 (σ≧∀≦)σ
-------------------------------

首先选择安装时使用的语言啦~

.. image:: /images/Installing_Fedora/6.png
   :alt: 选择安装时使用的语言
   
接下来进入安装程序摘要,一般来说只要选择 "安装位置" 来调整存储设备就好.

.. image:: /images/Installing_Fedora/8.png
   :alt: 安装程序摘要
   
-----------------------

.. alert-info::
    
    咱是在 VirtualBox 中演示的安装,汝的屏幕上显示的内容可能和咱的不一样.
    
    不过还是小心些好~
    
---------------------------

.. image:: /images/Installing_Fedora/9.png
   :alt: "准备存储设备"菜单

先从上方选择汝的硬盘,然后选择 "我要手动配置分区" ( 毕竟汝不大可能在一块全新的硬盘上安装吧~ ),
再点击 "完成" 进入下一步.

.. image:: /images/Installing_Fedora/10.png
   :alt: "准备存储设备"菜单again~

对于 MBR 系统
`````````````````````````````````

至少要有一个根分区 ( :code:`/` ) _(:з」∠)_

点击下面的 + 号图标来创建一个新的分区:

 .. image:: /images/Installing_Fedora/12.png
   :alt: 创建一个新的分区~
   
选择挂载点和容量,然后确定.

对于 UEFI 系统
``````````````````````````````````

除了根分区以外还需要一个 EFI 系统分区~

.. image:: /images/Installing_Fedora/28.png
   :alt: UEFI 分区举例
   
( 咱偷懒用了自动分区 😂😂😂 )

---------

准备好分区后,按 "完成" 提交汝的更改呗~

.. image:: /images/Installing_Fedora/14.png
   :alt: 提交更改


这才是真正的开始安装啦 o(\*≧▽≦)ツ 
---------------------------------------------------------

这时汝会发现最下面的 "开始安装" 可以点击了诶~

.. image:: /images/Installing_Fedora/17.png
    :alt: "开始安装" 可以点击了诶~
    
.. image:: /images/Installing_Fedora/18.png
   :alt: 开始安装

接下来就开始安装啦~ 但是还需要再做两件事 _(:з」∠)_

* 为 root用户设置密码:

    .. alert-success::
    
        root 用户和  Windows 中的  Administrator 账户类似,都具有控制整个系统的权限,
        
        因此汝要设置一个安全的密码并保管好它哟~

.. image:: /images/Installing_Fedora/19.png
   :alt: 设置 root 用户的密码

* 然而由于 root 的权限太大,所以一般不建议用 root 进行日常操作,下一步就是创建一个普通用户,并为它设置一个密码.

.. image:: /images/Installing_Fedora/20.png
   :alt: 创建一个新用户并设置密码

.. alert-info::

    如果汝选择 "将此用户设置为管理员" 那么这个用户就可以通过 :code:`sudo` 命令来获得管理员权限.

    ( 有点像 Windows 里的用户账户控制 😂😂 )

然后稍等一会儿就好啦~

.. image:: /images/Installing_Fedora/22.png
   :alt: 安装完成啦~

重启进入新安装的 Fedora 
-------------------------------------

要从 Live 环境重新启动,点击右上角,再点击电源图标,选择 "重启" _(:з」∠)_

.. image:: /images/Installing_Fedora/23.png
   :alt: 重启

重启以后就会进入到这个 :ruby:`简洁|简陋` 的启动选项界面啦~

一般选第一个 ( 第二个是恢复模式 _(:з」∠)_ )

.. image:: /images/Installing_Fedora/24.png
   :alt: 选择启动选项

然后稍候片刻进入登录画面,用汝刚刚创建的用户和密码登录吧~

.. image:: /images/Installing_Fedora/25.png
   :alt: 登录界面


    
参考资料
-----------------------------

* `Fedora 文档 <https://docs.fedoraproject.org>`_
