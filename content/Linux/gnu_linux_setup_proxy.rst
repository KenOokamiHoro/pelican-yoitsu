GNU/Linux 中为不同的应用程序设置代理
==========================================================

:slug: gnu_linux_setup_proxy
:lang: zh
:date: 2018-04-17
:tags: GNU/Linux, Proxy, 整理
:desc: 方法挺多……

.. PELICAN_BEGIN_SUMMARY

方法挺多……

.. PELICAN_END_SUMMARY

.. contents::

Web 浏览器（例如 Firefox 和 Chromium）
---------------------------------------------------

Firefox 可以在设置中自行设置
（参见 https://support.mozilla.org/en-US/kb/connection-settings-firefox ）。

Chromium 默认会用系统的代理设置，不过可以用一个命令行选项强行在这次会话中使用代理，例如：

    chromium --proxy-server="socks5://127.0.0.1:1080" --host-resolver-rules="MAP * 0.0.0.0 , EXCLUDE localhost"

不过也可以用个代理扩展，例如 `SwitchyOmega <https://github.com/FelisCatus/SwitchyOmega>`_ 

GNOME 3
-----------------

可以从设置-网络-网络代理中设置：

.. image:: /images/GNU_Linux_Proxies/setting_up_proxy_gnome.png
   :alt: GNOME 3 的代理设置

某些其它应用（是哪些 😂）
-------------------------------

（例如 wget 和 curl），会使用形如 "proto_proxy" 的环境变量用来设置代理啦
（当然也有可能是全部大写的），例如：

.. code-block:: bash

    # export var=value
    export http_proxy=http://10.203.0.1:5187/
    export https_proxy=$http_proxy
    export ftp_proxy=$http_proxy
    export rsync_proxy=$http_proxy
    export no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"

就设置成了 10.203.0.1:5187 作为 HTTP 代理，HTTPS、FTP 和 rsync 使用相同的代理。

于是可以把这样的语句写进一个脚本里在需要使用代理的时候 source 一下 😂

不过这样设置的环境变量一切换用户（例如 sudo）以后就没啦…… sudo 的话，可以用 -E 选项保持
环境变量，或者修改 /etc/sudoers 指定保持哪些环境变量：

.. code-block:: text

    Defaults env_keep += "http_proxy https_proxy ftp_proxy no_proxy"

socks5 代理
--------------------------

curl 和 pacman 可以设置 all_proxy 变量直接使用：

    export all_proxy="socks5://your.proxy:1080"

那剩下的怎么办 😂

用 proxychains 让应用使用 socks5 代理
---------------------------------------

字如其名就是个链式代理工具啦 😂 Arch Linux 的话装上 proxychains-ng 就 OK 啦~

然后去修改 /etc/proxychains.conf ，选项有很多，
但是大多数时候只需要把最后一行换成汝自己的 socks5 代理的 IP 地址和端口就行😂

然后就可以像平常（？）一样：

    proxychains -q /path/to/program

有关 proxychains 的更多用法可以去参考它的文档： http://proxychains.sourceforge.net/

用 privoxy 把 socks5 代理转化成 http 代理
----------------------------------------------

    Privoxy是一款不进行网页缓存且自带过滤功能的代理服务器，针对HTTP、HTTPS协议。
    通过其过滤功能，用户可以保护隐私、对网页内容进行过滤、管理Cookie，以及拦阻各种广告等。
    Privoxy可以单机使用，也可以应用到多用户的网络。
    它也可以与其他代理相连（通常与Squid一起使用），更可以突破互联网审查。

    -- https://zh.wikipedia.org/wiki/Privoxy 

同样 Arch Linux 上装上 privoxy 就 OK 😂

然后去修改 /etc/privoxy/config ，选项还是有很多 😂，
但是大多数情况下只需要加上一行：

.. code-block:: text

    # forward-socks5   target_pattern  socks_proxy:port  http_proxy:port
    # target_pattern 是 . 的话表示对所有域名适用
    # http_proxy 是 . 的话表示继续不使用 HTTP 代理转发……
    forward-socks5 . localhost:1080 .

然后启动 privoxy 服务，把 HTTP 代理设置成 localhost:8118 就好 😂

有关 Privoxy 的更多用法可以去参考它的文档： http://www.privoxy.org/user-manual/

