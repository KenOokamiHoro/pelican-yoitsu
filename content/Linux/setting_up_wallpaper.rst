在 在 GNU/Linux 的桌面环境中设置自动切换壁纸 （GNOME 篇） 
========================================================================

:slug: setting_up_wallpapers
:lang: zh
:date: 2018-11-26
:tags: 
:desc: 在 GNU/Linux 的桌面环境中设置壁纸

在 GNU/Linux 的桌面环境中设置壁纸，是怎么样的呢？

.. PELICAN_END_SUMMARY

如何设置壁纸？
-----------------------------

.. image:: /images/setting_up_wallpaper/tweaks.png
   :alt: GNOME Tweaks 设置壁纸

GNOME 的话，可以用 Tweaks 设置。

    汝是不是已经看见了那里设置的壁纸不是图片而是个 XML？😂

或者可以用 `gsettings` 设置：

    gsettings set org.gnome.desktop.background picture-uri 'file:///path/to/my/picture.jpg'

所以那个 XML 是啥？
-------------------------------
打开看一下不就知道了 ？😂

.. code-block:: xml

    <background>
    <starttime>
        <year>2011</year>
        <month>11</month>
        <day>24</day>
        <hour>7</hour>
        <minute>00</minute>
        <second>00</second>
    </starttime>

    <!-- This animation will start at 7 AM. -->

    <!-- We start with sunrise at 7 AM. It will remain up for 1 hour. -->
    <static>
    <duration>3600.0</duration>
    <file>/usr/share/backgrounds/gnome/adwaita-morning.jpg</file>
    </static>

    <!-- Sunrise starts to transition to day at 8 AM. The transition lasts for 5 hours, ending at 1 PM. -->
    <transition type="overlay">
    <duration>18000.0</duration>
    <from>/usr/share/backgrounds/gnome/adwaita-morning.jpg</from>
    <to>/usr/share/backgrounds/gnome/adwaita-day.jpg</to>
    </transition>

    <!-- It's 1 PM, we're showing the day image in full force now, for 5 hours ending at 6 PM. -->
    <static>
    <duration>18000.0</duration>
    <file>/usr/share/backgrounds/gnome/adwaita-day.jpg</file>
    </static>

    <!-- It's 7 PM and it's going to start to get darker. This will transition for 6 hours up until midnight. -->
    <transition type="overlay">
    <duration>21600.0</duration>
    <from>/usr/share/backgrounds/gnome/adwaita-day.jpg</from>
    <to>/usr/share/backgrounds/gnome/adwaita-night.jpg</to>
    </transition>

    <!-- It's midnight. It'll stay dark for 5 hours up until 5 AM. -->
    <static>
    <duration>18000.0</duration>
    <file>/usr/share/backgrounds/gnome/adwaita-night.jpg</file>
    </static>

    <!-- It's 5 AM. We'll start transitioning to sunrise for 2 hours up until 7 AM. -->
    <transition type="overlay">
    <duration>7200.0</duration>
    <from>/usr/share/backgrounds/gnome/adwaita-night.jpg</from>
    <to>/usr/share/backgrounds/gnome/adwaita-morning.jpg</to>
    </transition>
    </background>    

:del:`这文件看起来好简单啊咱就不加注释了……`

于是汝就能自己写一个脚本来生成这样的 XML (例如 https://pastebin.com/019G2rCy )

如果想更懒一些的话可以试试 Shotwell （选择想要的图片然后 File → Set as Desktop Slideshow）

是不是很简单鸭 😂

不是 GNOME ？
-----------------------

KDE Plasma 的话可以参考兔兔的
`[Linux/KDE] 一步步教你给 KDE 用上 macOS 专属动态壁纸
<https://blog.brucezhang.cf/2018/12/07/kde-dynamic-wallpaper.html>`_

虽然例子是 macOS 的那个沙漠，但是汝自己可以用别的啊 😂

其它桌面可以参考布偶君的
`在 GNU/Linux 的桌面环境中设置自动切换壁纸 
<https://shadowrz.github.io/posts.d/2018/12/01/dynamic-wallpaper.html>`_

