浅说基于 Linux 内核的操作系统 (0) - 写在前面
====================================================

:slug: linux_newbie_preface
:lang: zh
:date: 2016-03-18
:tags: Linux,凑数
:series: 浅说基于 Linux 内核的操作系统
:issueid: 25

.. PELICAN_BEGIN_SUMMARY

挖了个新坑,就是这样 _(:з」∠)_

.. PELICAN_END_SUMMARY

.. contents::

挖这个坑的动机
--------------------------

咱这学期以"二周目成员"的形式加入了学院的计算机社团,然后就是"三周目成员"的招募啦~,接着就要开始学习C语言啦,然而配置环境就是个复杂的工程呗.

    "要啥Windows啦,换Linux多好."

    ----咱是这么想的.

然而当他们告诉咱没用过Linux时,😂😂😂(咱想不到该说什么了)

接着咱去看了几本关于Linux的书,发现国内出版的书正交性不够(只针对特定的发行版),国外的书的话咱的同学看着会很痛苦😂

于是咱就有了写这系列文章的动机,然而却一直拖着 (\´･ω･\`) ,直到社长说要让咱做一次基于 Linux 内核的操作系统的讲座  (╯•̀﹏•́)╯ ┻━┻

    「基本上，得记住的文字种类太多了。还有呐，莫名其妙的组合也太多了。虽然人类会说只要照着说话规则写字就好，但是这显然是骗人的呗。」

    ---- 咱的水平明明还没那么高啊~

然后在 :irc:`archlinux-cn` 又受到了调侃  (╯＠_＠)╯ ┻━┻

    ヨイツの賢狼ホロ, [19.03.16 05:20]
    /me 社长叫咱准备一期 Linux 相关的讲座😂😂

    archcnbot, [19.03.16 05:21]
    [KaseiWang] 狼主席什么时候上位啊

    ヨイツの賢狼ホロ, [19.03.16 05:22]
    [In reply to archcnbot]
    咱可不是主席😂😂

    archcnbot, [19.03.16 05:22]
    [Shirasaka-Hazumi] 要上台

    cuihao, [19.03.16 05:22]
    狼主席！

    archcnbot, [19.03.16 05:22]
    (quininer) 狼主席！

    StarDuster, [19.03.16 05:22]
    狼主席

    archcnbot, [19.03.16 05:22]
    [Shirasaka-Hazumi] Chair Lang

    archcnbot, [19.03.16 05:23]
    [felixonmars] 狼主席什么时候上位啊

    archcnbot, [19.03.16 05:23]
    [felixonmars] 我突然发现

    ヨイツの賢狼ホロ, [19.03.16 05:23]
    😂😂😂

    archcnbot, [19.03.16 05:23]
    (quininer) 突然发现

    archcnbot, [19.03.16 05:23]
    [felixonmars] 有个很厉害的搜索引擎，叫狼内存

    archcnbot, [19.03.16 05:24]
    (quininer) 'google 狼内存

    archcnbot, [19.03.16 05:24]
    [varia] quininer: Wolfram 中文名| Mathematica小组| 果壳网科技有意思 [ http://www.guokr.com/post/653834/  ]

    archcnbot, [19.03.16 05:24]
    [Lisa] varia: ⇪网页标题: Wolfram 中文名 | Mathematica小组 | 果壳网 科技有意思

    StarDuster, [19.03.16 05:24]
    噗

咱参考了哪些内容?
----------------------------

咱参考了这些内容,先道个谢~

* `编程随想的博客 <https://program-think.blogspot.com>`_ (他开了个头)
* `鸟哥的 Linux 私房菜 <http://linux.vbird.org/>`_ (写的比较透彻,值得一读)
* `笨兔兔的故事@Ubuntu中文论坛 <http://forum.ubuntu.org.cn/viewtopic.php?f=112&t=162040>`_  (基于Ubuntu8.04完成的一篇故事,咱会说是卖萌向?)

----

既然是 "写在前面" 嘛,就不能说的太多 :del:`其实是还没想好怎么写~` , 那么下次再见了😂😂
