PackageKit：统一（？）GNU/Linux 发行版包管理器操作的前端
========================================================================

:slug: packagekit
:lang: zh
:date: 2019-04-03
:tags: package, command
:desc:

如果汝是 GNU/Linux 爱好者的话，一定有自己喜欢的 GNU/Linux 发行版了吧。
对那个发行版使用的软件包管理器的操作大概也已经谙熟于心了呗~

直到汝有时需要摆弄别的发行版的时候：

.. code-block:: bash

    horo@debian:~ $ sudo pacman -Syu
    sudo: pacman: command not found

emmm......

虽然 pacman 的话，有 `pacapt <https://github.com/icy/pacapt>`_ 这样的工具来包装其它
软件包管理器的操作。但如果汝不适合这种状况呢（例如常用 Ubuntu ？:del:`（打死……）` ）？

PackageKit 出手相助~ 等等那是？
-------------------------------------------------

    PackageKit is a system designed to make installing and updating 
    software on your computer easier. 
    
    The primary design goal is to unify all the software graphical tools
    used in different distributions, 
    and use some of the latest technology like PolicyKit. 


    -- https://www.freedesktop.org/software/PackageKit/pk-intro.html 

PackageKit 是一个在不同的软件包管理器中统一操作的高阶前端（比 apt / dnf 一类的更高阶？），
当然没有打算取代各位的意思啦（笑）。

和 gcc 类似， PackageKit 通常也是由若干个前端和若干个后端组成的。

* 后端通过一系列抽象工具和 packagekitd / libpackagekit 交互。
* 前端为用户提供界面。例如 PackageKit 自己提供的 pkcon。

所以它怎么用？
-------------------------------------------------

基本操作和普通的软件包管理器差不多：

.. code-block:: bash

    # pkcon install <package>
    # 如果汝的账户具有管理员权限（在大多数桌面环境中认为 wheel 组的成员具有管理员权限），
    # 安装时会询问汝的密码（通过 Polkit 完成）。
    $ pkcon install tk
    Resolving                     [=========================]         
    Finished                      [                         ] (0%)  
    The following packages have to be updated:
    tk-8.6.9-2.x86_64	A windowing toolkit for use with tcl
    Proceed with changes? [N/y] 
    Testing changes               [=========================]         
    Installing                    [=========================]         
    Installing updates            [=========================]         
    Finished                      [=========================]  
    # pkcon search <keyword>
    $ pkcon search fcitx
    Searching by details          [=========================]         
    Querying                      [=========================]         
    Finished                      [=========================]         
    Installed   	fcitx-4.2.9.6-1.x86_64 (installed)                          	Flexible Context-aware Input Tool with eXtension
    Available   	fcitx-anthy-0.2.3-1.x86_64 (community)                      	Fcitx Wrapper for anthy.
    Available   	fcitx-chewing-0.2.3-2.x86_64 (community)                    	Fcitx Wrapper for chewing
    Available   	fcitx-chewing-git-0.2.3.r4.ge4bb273-4.x86_64 (archlinuxcn)  	Fcitx Wrapper for chewing
    Available   	fcitx-cloudpinyin-0.3.6-2.x86_64 (community)                	Standalone module for fcitx that uses pinyin API on the internet to provide additional input candidates
    Installed   	fcitx-configtool-0.4.10-3.x86_64 (installed)                	GTK based config tool for Fcitx
    Available   	fcitx-googlepinyin-0.1.6-6.x86_64 (community)               	Fcitx Wrapper for googlepinyin
    Installed   	fcitx-gtk2-4.2.9.6-1.x86_64 (installed)                     	GTK2 IM Module for fcitx
    Installed   	fcitx-gtk3-4.2.9.6-1.x86_64 (installed)                     	GTK3 IM Module for fcitx
    ......
    # pkcon get-updates 可以检查更新
    $ pkcon get-updates
    Getting updates               [=========================]         
    Downloading list of packages  [=========================]             
    Finished                      [=========================]         
    Normal      	adwaita-icon-theme-3.32.0-1.any (extra)                     	GNOME standard icons
    Normal      	apache-2.4.39-1.x86_64 (extra)                              	A high performance Unix-based HTTP server
    Normal      	archlinux-appstream-data-20190320-1.any (extra)             	Arch Linux application database for AppStream-based software centers
    Normal      	archlinuxcn-keyring-20190315-2.any (archlinuxcn)            	Arch Linux CN PGP keyring
    Normal      	babl-0.1.62-1.x86_64 (extra)                                	Dynamic, any to any, pixel format conversion library
    ......
    # pkcon refresh 可以更新软件仓库列表。
    $ pkcon refresh
    # pkcon backend-details 可以查询 PackageKit 使用的后端的详细信息。
    $ pkcon backend-details
    Name:		alpm
    Description:	alpm
    Author:	Aleix Pol i Gonzàlez <aleixpol@kde.org>, Fabien Bourigault <bourigaultfabien@gmail.com>, Jonathan Conder <jonno.conder@gmail.com>

其它 pkcon 的命令可以通过查询手册页了解（pkcon(1)）

那有没有图形前端？
--------------------------------------

当然有啦，例如 gnome-packagekit 和 apper， 它们通过 PackageKit 提供了安装，查找和卸载软件包的功能：

.. image:: https://upload.wikimedia.org/wikipedia/commons/6/6f/Gnome-packagekit_3.22_on_GNOME_Shell_3.22_--_2016%2C_09.png
    :alt: gnome-packagekit 的界面

.. image:: https://upload.wikimedia.org/wikipedia/commons/c/c9/Kpackagekit.png
    :alt: apper 的界面

如果汝更中意类似应用商店的体验， GNOME 有 GNOME Software， KDE Plasma 有 Discover：

.. image:: https://wiki.gnome.org/Apps/Software?action=AttachFile&do=get&target=gnome-software.png
    :alt: GMOME Software

.. image:: https://userbase.kde.org/images.userbase/8/8c/Discoverfront.png
    :alt: Discover

至于怎么用请参考它们和汝正在使用的 GNU/Linux 发行版的文档 :del:`（懒了……）`

PackageKit 不是什么？
-------------------------------------------------

PackageKit 不能代替汝正在使用的软件包管理器（毕竟没了它们做后端这家伙啥也干不了）

emmm......

