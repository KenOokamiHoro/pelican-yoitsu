使用 SSH 远程连接运行 GNU/Linux 的计算机
==========================================================

:slug: connect_through_ssh
:lang: zh
:date: 2017-04-23
:tags: Linux, SSH, 操作
:series: 浅说基于 Linux 内核的操作系统
:desc: 约等于 Windows 的远程桌面……
:issueid: 

.. PELICAN_BEGIN_SUMMARY

约等于 Windows 的远程桌面……

.. PELICAN_END_SUMMARY

.. contents::

本文里用到的是 GNU/Linux 发行版一般都会有的 ssh 命令啦~

SSH 是啥？
---------------------------------------------
   
    Secure Shell（縮寫为SSH），由IETF的網路工作小組（Network Working Group）所制定；SSH為一项建立在应用层和传输层基础上的安全协议，为计算机上的Shell（壳层）提供安全的传输和使用环境。

    而SSH是目前较可靠，專为远程登录会话和其他网络服务提供安全性的协议。利用SSH协议可以有效防止远程管理过程中的信息泄露问题。透過SSH可以對所有传输的数据进行加密，也能够防止DNS欺骗和IP欺骗。

    SSH之另一項優點為其传输的数据可以是经过压缩的，所以可以加快传输的速度。SSH有很多功能，它既可以代替Telnet，又可以为FTP、POP、甚至为PPP提供一个安全的「通道」。

当然现在用的更多的是 SSH 的一种叫做 OpenSSH 的自由实现啦~

做点准备工作
---------------------------------------------

首先要把 openssh 装上啦，具体看发行版了呗（比如有些是把客户端和服务端拆分成 
openssh-client 和 openssh-server 一类的，有的就直接是一个大包 openssh 了呗~ ）

然后服务器上生成一下主机密钥咯，这个操作大概启动 sshd （ssh daemon 的缩写？）服务
时多半就会自动完成了吧…… 大概像这样？

    有 Systemd 的发行版：

    # systemctl start sshd

    有 sysVinit 或者 upStart 的发行版：

    # /etc/init.d/sshd start 

然后记下来服务器的 IP 地址咯~

最简单的连接
----------------------------------------

    ssh [<用户名>@]<主机名或者IP地址>

如果没有用户名的话，就会用汝当前的用户名作为连接到服务器时使用的用户名咯~

    下面的例子里咱其实是自己连自己了……

    $ ssh horo@localhost

第一次连接时大概像这样：

.. code-block:: bash

    The authenticity of host 'localhost (::1)' can't be established.
    ECDSA key fingerprint is SHA256:ZrEikKJj6wqLk8Cqgs6JWXGX3FgS2iQI6aK73GTCHVk.
    Are you sure you want to continue connecting (yes/no)?

大概的意思就是“咱好像不记得汝要去的这个地方呐~”，毕竟是第一次连接嘛。

    如果汝在连接其它人的服务器的话，可以让他/她/它把服务器的指纹发来确认一下。

如果确认了以后，就输入 yes 让 ssh 客户端记下来。

然后：

.. code-block:: bash

    Warning: Permanently added 'localhost' (ECDSA) to the list of known hosts.
    horo@localhost's password:

这里输入密码。记得输入密码时就是什么都没有，不用担心键盘坏了啦~

.. code-block:: bash

    Last login: Thu Mar  9 23:00:10 2017
    [horo@Yoitsu_SurfaceBook horo] $

这样大概就是登录成功了呗，如果工作做完了以后，记得关闭连接呐~

.. code-block:: bash

    [horo@Yoitsu_SurfaceBook horo] $ exit
    Connection to localhost closed.


使用密钥连接
--------------------------

为啥要用密钥登录？其实咱也说不清楚……

    大概是因为不容易泄漏和不容易暴力破解？

    或者上 `Stack Exchange <https://security.stackexchange.com/questions/69407/why-is-using-an-ssh-key-more-secure-than-using-passwords>`_ 问问？

首先生成一对密钥：

    $ ssh-keygen -t rsa -b 4096

    其中的 -t rsa 表示加密算法的类型是 RSA
    其中的 -b 4096 表示密钥是 4096 bit
    
    注意不同加密算法的位数是没有可比性的啦~
    对于 RSA 加密算法而言， >2048 bit 应该足够安全了呗~ 
    （然而默认是 1024 bit 的，只好手动加点……）

整个过程大概像这样：

.. code-block:: bash

    [horo@Yoitsu_SurfaceBook ~]$ ssh-keygen -t rsa
    Generating public/private rsa key pair.
    # 密钥存在哪？
    Enter file in which to save the key (/home/horo/.ssh/id_rsa): 
    # 为私钥设置一个密码
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in /home/horo/.ssh/id_rsa.
    Your public key has been saved in /home/horo/.ssh/id_rsa.pub.
    The key fingerprint is:
    # 这是密钥的指纹啦~
    SHA256:nySil+IhkH0yDg1eYFIOdY8Zq7j3vxo6Wlab9znx17Q horo@Yoitsu_SurfaceBook
    The key's randomart image is:
    +---[RSA 2048]----+
    |o=o o            |
    |oo.. *           |
    |. ..+ .          |
    |.*..             |
    |=.* o . S .      |
    | = = + o.+ .  .  |
    |. * B +  oo  o . |
    | +.= * .... . E  |
    |....+oo.o. .     |
    +----[SHA256]-----+

把这对密钥的公钥（ /home/horo/.ssh/id_rsa.pub ）添加到 authorized_keys 里：

    $ cat /home/horo/.ssh/id_rsa.pub > /home/horo/.ssh/authorized_keys

    因为 sshd 默认会从 authorized_keys 里寻找接受哪些密钥连接啦~

把私钥 （/home/horo/.ssh/id_rsa ）复制到汝自己的电脑上咯~

然后连接服务器时，用 -i 参数指定使用哪一个私钥文件：

    $ ssh [<用户名>@]<主机名或者IP地址> -i <私钥文件的路径>

如果汝遇到了这样的错误：

.. code-block:: text

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    Permissions 0755 for '/home/horo/.ssh/id_rsa' are too open.
    It is required that your private key files are NOT accessible by others.
    This private key will be ignored.
    Load key "/home/horo/.ssh/id_rsa": bad permissions

这表示汝的私钥的权限设置的不对啦 >_< 

对于私钥文件这种只需要读取的文件来说，自己能读就足够啦~

    $ chmod 400 <私钥文件的路径>

    类 Unix 系统的权限位大多数都是像这样的八进制数啦，三位从左向右
    分别表示文件的所有者，所属于的组，和其它人分别拥有的权限。

    而其中 4 表示读，2表示写，1表示执行啦~

    所以 400 = 自己只能读，所属的组和其它人看都不能看！

    关于文件权限的更多内容，就先从维基百科开始咯~：

    https://en.wikipedia.org/wiki/File_system_permissions

    而保护私钥文件的措施，推荐这篇文章：

    https://martin.kleppmann.com/2013/05/24/improving-security-of-ssh-private-keys.html

如果汝设置了私钥密码的话，需要在连接时输入私钥的密码（而不再是汝用户的密码了呐~）

如果可以成功登录了的话，就可以关掉密码登录了呗：

    用汝喜欢的文字编辑器（和 root 权限）打开 /etc/ssh/sshd_config 文件，
    找到 PasswordAuthentication 这一行，把后面的 yes 改成 no 。
    
    如果没有的话就自己加一行 PasswordAuthentication no 咯~

顺便可以禁止用 root 登录：

    找到 PermitRootLogin 这一行，把后面的 yes 改成 no 。
    
    如果没有的话就自己加一行 PermitRootLogin no 咯~

然后保存，记得需要重新启动 sshd 服务才能生效咯~

用不同的端口连接
----------------------------

可以稍微防止被扫描到？

    用汝喜欢的文字编辑器（和 root 权限）打开 /etc/ssh/sshd_config 文件，
    找到 #Port 22 这一行，去掉前面的 # 号，然后自己设定一个数。

    保存，记得需要重新启动 sshd 服务才能生效咯~

    另外，大多数配置文件中 # 开头的行都是为了方便记忆和理解写下的注释，
    虽然程序不会用到，但是有时也很有用呐~

如果设置了不同的端口，连接时加上 -p 选项：

    $ ssh [<用户名>@]<主机名或者IP地址> -i <私钥文件的路径> -p <汝所设置的端口>

其实 ssh 的选项很多啦（不过咱就会这几个……），其它的选项怎么用，
汝可以 man 一下咯~

    $ man ssh

本地写点配置文件方便连接
---------------------------------

随着命令越来越复杂，写的也越来越长，对记忆力也是个考验了呐~

所以 ssh 客户端可以通过配置文件来简化一些操作呐~

用汝喜欢的文字编辑器打开汝主文件夹下的 .ssh/config 文件，
（如果没有的话就自己建立一个咯~

然后每一个服务器这样写：

.. code-block:: text

    Host <这里可以起一个名字，一会儿会用到>
        HostName                  <服务器的主机名或 IP 地址>
        Port                      <端口>
        User                      <用户名>
        PreferredAuthentications  publickey
        # 偏好的验证方法，这里选择公钥验证
        IdentityFile              <汝的私钥文件路径>

然后汝可以通过 ssh <刚刚设置的别名> 来连接了哦~

有时可能……
-------------------------------------
有时汝可能会遇到这样的错误呗~

.. code-block:: text

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
    Someone could be eavesdropping on you right now (man-in-the-middle attack)!
    It is also possible that a host key has just been changed.
    The fingerprint for the ECDSA key sent by the remote host is
    SHA256:S8zGzMsXj0FbOyrTM9OTalx8pbddwmMdxeVXaKQaaA0.
    Please contact your system administrator.
    Add correct host key in /home/horo/.ssh/known_hosts to get rid of this message.
    Offending ECDSA key in /home/horo/.ssh/known_hosts:2
    ECDSA host key for localhost has changed and you have requested strict checking.
    Host key verification failed.

 这表示服务器发送来的主机密钥和汝 known_hosts 中的不相同呗~
 有可能是遇到了中间人攻击，或者只是汝重装了系统或者重新生成了主机密钥而已……

 如果汝在连接其它人的服务器的话，最好还是确认一下为好~

 要真的是汝重装了系统了的话，就把新的主机密钥记下来呗~找到 known_hosts 对应的那一行修改一下就好了啦~



最后……
----------------------------------

其实保护服务器只靠这些还是不够啦~，可以考虑使用一些防暴力攻击的软件，或者防火墙
（例如 ufw ），当然上网搜索一些前人的姿势也大有裨益咯~



        