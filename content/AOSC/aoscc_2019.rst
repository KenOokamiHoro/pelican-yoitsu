AOSCC 2019 游记
=========================================

:slug: aoscc_2019
:lang: zh
:date: 2019-07-15
:tags: 社区,Trip
:series: AOSCC 游记

“安徽合肥，安徽合肥，AOSCC 又倒闭了……”

“安徽同志们终于来到了安徽（x）”

……

Day -2 - 为啥咱没写 2017 ？
-------------------------------------------------
因为咱没去鸭（x），可以去它们的网站上阅读总结：
https://aosc.io/news/5959-aoscc-2017-re-cap

Day -1 - 为啥咱没写 2018 ？
--------------------------------------------------
还是因为咱没去呀（xx），而且那年因为某些不可抗力原因没有开成线下活动。
还是可以去它们的网站上阅读检讨： 
https://aosc.io/news/4606-aoscc-2018

Day 1 - Zzzz......
--------------------------------------------------
（因为刚坐了一夜的火车就直奔会场了，
所以没提到的部分……

* 要么是睡过去了，
* 要么是没听懂去闲逛了没留下照片
* 要么是当事人不愿意露脸（？）
* 要么是听得投入没拍照……

😂）

以及所有演讲的 Slide 可以在 `AOSC OS 的仓库服务器 <https://repo.aosc.io/aosc-documentation/aoscc-2019/>`_
上找到。

:del:`Lecture 催眠力 +100%`

.. image:: /images/AOSCC/2019/0.jpg
    :alt: 祖传（？）易拉宝上线（雾）

祖传（？）易拉宝上线（雾）

.. image:: /images/AOSCC/2019/1.jpg
    :alt: AOSC Retro 展示设备

运行为旧硬件特化的 AOSC OS 的一些设备（PineBook（并不是 AOSC OS Retro 展示设备），PowerBook G4， iBook G3 和
Sony VAIO PCG-C1VN）。

    “AOSC OS，关爱您、您的开发板、您的谜之处理器和您的史前遗产”—— imi415

.. image:: /images/AOSCC/2019/2.jpg
    :alt: AOSC OS 展示设备

与之相对的就是一些运行 AOSC OS 的“现代”设备啦~ 
    
    :del:`好想上安徽女同志网啊……`

.. image:: /images/AOSCC/2019/3.jpg
    :alt: 贴纸

今年的贴纸， :del:`在成为造梗发行版的路上越走越远……`

可以在 `AOSCC 的仓库 <https://github.com/AOSC-Dev/aoscc/tree/master/2019/stickers>`_ 下载到
每一个贴纸的源代码（？）

.. image:: /images/AOSCC/2019/4.jpg
    :alt: 直播准备中

直播准备中，:del:`“虽然不知道为什么每一次 AOSCC 都能遇上网络问题……”`

.. image:: /images/AOSCC/2019/5.jpg
    :alt: 果冻之谢罪

首先是来自 :del:`特首果冻 Jelly` Mingcong Bai 的谢罪（雾），解释了为啥 AOSCC 2018 
迫不得已（？）搞成了线上活动，还有最近两年的成果什么的。

.. image:: /images/AOSCC/2019/6.png
    :alt: 常联系啊

“常联系，啊” -- Mingcong Bai

.. image:: /images/AOSCC/2019/7.jpg
    :alt: Junde Yhi 

Junde Yhi 分享他编译和在 X200 上安装 Libreboot 的经验和心得。

“Libreboot 的构建工具好难用的……”

.. image:: /images/AOSCC/2019/8.jpg
    :alt: Play with OriginCode

见到 :del:`陈先生` 橙喵 OriginCode 啦，开心 ~ 还互相交（mai）流（ruo）了下……

.. image:: /images/AOSCC/2019/9.jpg
    :alt: AOSC OS 7 -- Gumblex

顺便提一下，AOSC 下一个 Release（？） 的 Codename 是社区开发者之一的昵称 Gumblex 哦~

(虽然当事人当时并不在场）以及并没有在去哪里吃饭这个问题上达成一致。

    チェン | 😸 | 🥬 | ExOrigin v15.8, [11.07.19 18:56]
    
    話說午餐怎麼解決喵（

    
    Mingcong Bai, [11.07.19 18:57]
    
    [In reply to チェン | 😸 | 🥬 | ExOrigin v15.8]
    
    饿着

:del:`没有……没有……没有……通过!`

Day 2
-------------------------------------------------

.. image:: /images/AOSCC/2019/10.jpg
    :alt: @Gumblex

----

.. image:: /images/AOSCC/2019/11.jpg
    :alt: @Gumblex

新 Codename 登场！ 😂

Gumblex 介绍了由他制作的 AOSC Packages 网站的功能，和
AOSC OS 软件包的质量检测和保障等等。

.. image:: /images/AOSCC/2019/26.jpg
    :alt: @Gumblex

刚将军与刚将军（

.. image:: /images/AOSCC/2019/12.jpg
    :alt: PowerBook G4

.. image:: /images/AOSCC/2019/13.jpg
    :alt: PowerBook G4

正在准备今天抽奖的奖品——PowerBook G4。

.. image:: /images/AOSCC/2019/14.jpg
    :alt: Apple Pencil

哇，有生之年第一次见到充电中的 Apple Pencil 耶（x

.. image:: /images/AOSCC/2019/15.jpg
    :alt: 胶片相机

正在用胶片相机拍照的……

.. image:: /images/AOSCC/2019/16.jpg
    :alt: ThinkPads

AOSC ThinkPad 用户组再聚首（请无视旁边的 PineBook （x））

.. image:: /images/AOSCC/2019/17.jpg
    :alt: KexyBiscuit

Kexy Biscuit 给 AOSC 带来了 以敏捷流程优化社区开发工作的尝试的建议。

Day 3
-------------------------------------------------

.. image:: /images/AOSCC/2019/18.jpg
    :alt: Flashing coreboot

现场拆机刷 coreboot 中……

.. image:: /images/AOSCC/2019/19.jpg
    :alt: Junde Yhi x2 

又是 Junde Yhi，先是因为各种咕咕咕了的 Installer :del:`谢罪` 检讨，
然后介绍了新 AOSC DeployKit 的计划。

.. image:: /images/AOSCC/2019/20.jpg
    :alt: Junde Yhi x2 

咕咕咕预定（x

.. image:: /images/AOSCC/2019/21.jpg
    :alt: Mingcong Bai x2 

特首的大抄（雾）

.. image:: /images/AOSCC/2019/22.jpg
    :alt: meet cuihao

另类面基证明（？）

.. image:: /images/AOSCC/2019/23.jpg
    :alt: USTC Mirrors

作为东道主（？）的 USTC 也有人来分享 USTC Mirrors 的心路历程。

.. image:: /images/AOSCC/2019/24.jpg
    :alt: Playing VOEZ

AOSC 人均单手神仙 tkpl（太可怕了）

.. image:: /images/AOSCC/2019/25.jpg
    :alt: confusing

迷惑行为大赏之保健粉笔（

    不过还真的有中药加石膏制成的保健粉笔 😂

    `CN1071122A 一种药物保健粉笔的制作方法
    <https://patents.google.com/patent/CN1071122A/zh>`_

Extras 
-------------------------------------------------

.. image:: /images/AOSCC/2019/-1.jpg
    :alt: 江镇包子铺

主 谓 宾 （

.. image:: /images/AOSCC/2019/-2.jpg
    :alt: 欢迎饮品

要素察觉（

.. image:: /images/AOSCC/2019/-4.jpg
    :alt: 红茶

要素察觉x2（

.. image:: /images/AOSCC/2019/-5.jpg
    :alt: emmm

能走上 （？） 19 楼塞下这个也是挺不容易的啦 😂

One More Extras
-------------------------------------------------

.. code-block:: text

    Junde Yhi, [14.07.19 20:21]
    [In reply to アカネチャン]
    您打音游的照片要放到新闻里去

    アカネチャン, [14.07.19 20:21]
    [In reply to Junde Yhi]
    这信息量太大了吧（

    Junde Yhi, [14.07.19 20:22]
    我要向各位学习除下落式以外的音游

    ., [14.07.19 20:23]
    [In reply to Junde Yhi]
    向音游导师 @Kotonoha_Akane 学习

    アカネチャン, [14.07.19 20:24]
    导（gao）师（shi）

    ., [14.07.19 20:24]
    果冻说的要是我是老师一个粉笔丢过去

    アカネチャン, [14.07.19 20:24]
    （

    アカネチャン, [14.07.19 20:25]
    然而它不是）

    アカネチャン, [14.07.19 20:25]
    所以我逃过一劫？😂

    Mingcong Bai, [14.07.19 20:25]
    （

    liushuyu, [14.07.19 20:27]
    别怕，是 保健粉笔（

    Xiaoxing Ye, [14.07.19 20:28]
    昏睡粉笔

    アカネチャン, [14.07.19 20:28]
    某种意义上真的会昏睡（

----

.. code-block:: text

    Neruthes AFB3373F5200DF38, [13.07.19 22:01]
    那么有人要来开夜谈会吗

    Neruthes AFB3373F5200DF38, [13.07.19 22:01]
    欢迎来 2013

    Neruthes AFB3373F5200DF38, [13.07.19 22:12]
    cc @Staph @JeffBai etc

    Mingcong Bai, [13.07.19 22:13]
    Nope (

    Neruthes AFB3373F5200DF38, [13.07.19 22:21]
    没有，通过！

    Neruthes AFB3373F5200DF38, [13.07.19 22:21]
    去做大保健了

所以今年没有钦定 AOSCC 2020 的位置鸭（咕咕咕）

