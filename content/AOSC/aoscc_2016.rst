AOSCC 2016 游记 - 第一天
=========================================

:slug: aoscc_2016
:lang: zh
:date: 2016-07-22   
:tags: 社区,Trip
:series: AOSCC 游记
:issueid: 1

.. PELICAN_BEGIN_SUMMARY

AOSCC 2016 围观日记 （误

.. PELICAN_END_SUMMARY

去 AOSCC 会场围观了下，然后拖延症又发作了😂😂

.. contents::

AOSC(C) 是啥？
-----------------------------------------

AOSC ,全称 :del:`Always Overuse Shell Community` Anthon Open Source Community ,安同开源社区。
是一个主要学生组成的开源社区，主要致力于 AOSC OS 的开发工作。

AOSC OS 是安同开源社区的主要项目，它是一个“白”手起家（？），采用 dpkg 作为软件包管理器的 Linux 发行版，
按照 :fref:`jeffbai`: 可爱的特首(误) Jelly Bai 的想法：

    “在我们的发行版真的拿得出手前（希望至少能和 Arch 比比整洁度），真不希望我们像 Deepin 一样被捧来捧去......”

而后面加个 C (Conference) 就是一年一度的见面会啦~ (今年好像是第二届?

然后作为在其间打酱油的咱怎么会想到去参加这个呢? (难道是因为被调侃了?

    liushuyu 🐟, [09.07.16 17:52]
    萌狼跟谁住啊（

    ヨイツの賢狼ホロ 😋(*), [09.07.16 17:55]
    🌚

    Cheng Cao, [09.07.16 17:57]
    [In reply to liushuyu 🐟]
    会发生什么

    ヨイツの賢狼ホロ 😋(*), [09.07.16 18:03]
    [In reply to Cheng Cao]
    😋

    liushuyu 🐟, [09.07.16 18:05]
    [In reply to Cheng Cao]
    旭日初升，恶臭四溢。AOSCC 与会人员们寻着气味而走，赫然发现【xxx】身首异处，躯体被撕成了碎片。这回糟了！

    KayMW | Syntax-Breaker | 不想猎取灵魂的咸鱼不是好灵魂画手 | 0x547E5906116A1966, [09.07.16 18:17]
    [In reply to Cheng Cao]
    会被吃

    KayMW | Syntax-Breaker | 不想猎取灵魂的咸鱼不是好灵魂画手 | 0x547E5906116A1966, [09.07.16 18:17]
    [In reply to liushuyu 🐟]
    萌狼：“下一个吃谁呢~”

    ヨイツの賢狼ホロ 😋(*), [09.07.16 18:18]
    😂

:del:`请听题:上面要补多少个括号?` 😂😂

第一天......
---------------------
多图杀猫+原谅咱的渣照相技术😂

.. image:: /images/AOSCC/Day1/1.jpg
    :alt: 前奏:当地桥好像很多的样子......

(当地桥好像很多的样子~)

.. image:: /images/AOSCC/Day1/4.jpg
    :alt: 前奏:这是啥?

(当心电离辐射\.\.\.\.\.\.\#滑稽)

.. image:: /images/AOSCC/Day1/6.jpg
    :alt: 前奏:导向牌

(简朴的导向标志)

:del:`AOSCC 与会人员们聚集在会场 —— 大家都如释重负，因为昨晚似乎无人受袭。`

.. image:: /images/AOSCC/Day1/7.jpg
    :alt: 正在打游戏的 A2 ww~

.. image:: /images/AOSCC/Day1/8.jpg
    :alt: 正在打游戏的 A2 ww~

在白特首来之前先来围观下 A2 ~

.. image:: /images/AOSCC/Day1/9.jpg
    :alt: 维他柠檬茶

夭寿辣,会场聚众吸毒啦😂😂

.. image:: /images/AOSCC/Day1/10.jpg
    :alt: locale-gen

现场生成 locale 中(貌似 AOSC 没给 /etc/locale.gen 上注释😂

.. image:: /images/AOSCC/Day1/11.jpg
    :alt: ThinkPads

ThinkPad 信仰充值中心一分部 😂 (画面中间的是果冻特首的 W541 ,旁边的是 A2 的 X1 Carbon)

.. image:: /images/AOSCC/Day1/13.jpg
    :alt: Longson

还有特首的龙芯笔记本~

.. image:: /images/AOSCC/Day1/14.jpg
    :alt: WhiteBoard

Hmm\.\.\.清真猪肉公共许可证, :del:`嗯可以,这很清真`

.. image:: /images/AOSCC/Day1/15.jpg
    :alt: Raspberrypi

现场装 X 中的树莓派 (然而不久有人把线碰掉然后就关机了, :del:`毕竟出来混迟早是要还的嘛` 😂)

.. image:: /images/AOSCC/Day1/19.jpg
    :alt: AOSCC17

.. image:: /images/AOSCC/Day1/20.jpg
    :alt: AOSCC201

AOSCC 2017,下一站广州!😋

.. image:: /images/AOSCC/Day1/22.jpg
    :alt: AOSCC Core 4

AOSCC 2016 与会者选出的 AOSC OS Core 4 的开发代号是 "Duang-Duang"（努力保持严肃……）；

:del:`夜幕降临，人们都活在恐惧中，彻夜难眠。这漫长的夜晚竟然有几个小时！` 😂😂

Extra
---------------------

.. image:: /images/AOSCC/Day1/18.jpg
    :alt: Naive Blue

😂😂