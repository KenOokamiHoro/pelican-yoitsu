用 dm-crypt 加密一下 Parabola 
==========================================================

:slug: encrypt_parabola_with_dm_crypt
:lang: zh
:date: 2017-04-17
:tags: 加密,dm-crypt
:desc: 用 dm-crypt 加密一下 Parabola

.. PELICAN_BEGIN_SUMMARY

用 dm-crypt 加密一下 btrfs 上的 Parabola…… 

.. PELICAN_END_SUMMARY

:del:`纯粹是闲的无聊……`

.. contents::

为啥要加密？
-----------------------------------------

简单说：文件加密可以防范失窃，保护隐私or备份数据，和提防某些人……

复杂点的话……
`看看这个咯~ <https://program-think.blogspot.com/2011/05/file-encryption-overview.html>`_

为啥是 dm-crypt?
-----------------------------------------

因为是 Linux 内核内置哒~ （好没有说服力的借口呐）

加密开始前的准备工作
-----------------------------------------

* 准备一个外部硬盘（放加密过程中备份的系统啦）

* 一个 Live USB （因为中间要格掉原来的系统……）

* 然后备份一下系统。

    btrfs 的话可以用发送快照的方式呗~

    * 首先创建一个只读快照：

        # btrfs subvolume snapshot -r <源位置> <目标位置>

        例如 btrfs subvolume snapshot -r / /root_backup 就会在 /root_backup 新建一个 / 的快照~

    * 然后发送出去：

        # btrfs send <快照名称> | btrfs receive <目标位置>

        例如咱把一个硬盘挂载到了 /mnt 的话：

        # btrfs send /root_backup | btrfs receive /mnt/

    不是 btrfs 的话就自行发挥想象力咯~ （比如 
    `和咱以前一样用 rsync </arch-linux/get_surface_book.html>`_ ~ ）


创建加密分区 
---------------------------------------

如果汝一开始不是安装了整个 core 组的话，先把 cryptsetup 装上咯~ _(:з」∠)_

然后创建加密分区：

    # cryptsetup luksFormat <设备文件的名称>

如果需要的话，可以在 luksFormat 之前加上一些选项呐：

    +-------------+----------------+------------------+--------------------------------------------+
    | 参数的名称  | 这啥？         | 推荐是多少？     | :del:`一些废话`                            | 
    +=============+================+==================+============================================+
    | --cipher    | 加密方式       | aes-xts-plain64  | AES 加密算法搭配 XTS 模式                  |
    +-------------+----------------+------------------+--------------------------------------------+
    | --key-size  | 密钥长度       | 512              |  XTS 模式需要两对密钥，                    |
    |             |                |                  |  每个的长度是256                           |
    +-------------+----------------+------------------+--------------------------------------------+
    | --hash      | 散列算法       | sha512           |                                            |
    +-------------+----------------+------------------+--------------------------------------------+
    | --iter-time | 迭代时间       | >10000           | 单位是毫秒。该值越大，暴力破解越难；       |
    |             |                |                  | 但是相应的汝打开加密分区时就要等一下啦~    |
    +-------------+----------------+------------------+--------------------------------------------+

例如这样？

.. code-block:: bash

    # cryptsetup --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 10000 luksFormat /dev/nvme0n1p1

    WARNING!
    ========
    这将覆盖 /dev/nvme0n1p1 上的数据，该动作不可取消。

    Are you sure? (Type uppercase yes): 
    # 上面要打大写的 YES
    # 然后输入两遍加密分区的密码。
    输入密码：
    确认密码：

然后等一下就好咯~

打开加密分区
------------------------------------------

    # cryptsetup open <已经加密的设备文件名称> <映射名称>

解密的分区会映射到 /dev/mapper/<汝设置的映射名称> 呗~

例如咱解密 /dev/nvme0n1p1 ，然后映射到 /dev/mapper/sysroot :

    # cryptsetup open /dev/nvme0n1p1 sysroot

接下来就像操作普通分区一样操作 /dev/mapper/sysroot 就好啦~

然后把刚刚备份好的系统再搬回来 _(:з」∠)_

    如果汝也是用 btrfs send receive 搬回来的，别忘了让快照可写:

        # btrfs property set -ts <快照的位置> ro false



重新设置一下系统 ~
--------------------------------------------

相比与原来的系统，现在的变化就是分区和启动方式改变了呐~

下面咱就假设它在 /mnt 啦 ~

``````````````````
fstab + chroot
``````````````````

首先重新生成一下 fstab ：

    # genfstab -U -p /mnt >> /mnt/etc/fstab

然后打开它，检查一下有没有问题 ( ⚆ _ ⚆ )

接着 chroot 进去：

    # arch-chroot /mnt

````````````````````
mkinitcpio
````````````````````

因为加密了嘛，所以要让内核知道怎么解开咯~ （就是带上 encrypt HOOK 啦……

打开 /etc/mkinitcpio.conf ：

    * 为了用键盘加上 keyboard

    * 如果汝有用 systemd hooks 的话，把 sd-encrypt 添加到 block 和 filesystems 之间咯，像这样：

        HOOKS="base systemd autodetect keyboard modconf block sd-encrypt filesystems fsck"

    * 不是的话加上 encrypt 咯~

        HOOKS="base udev autodetect keyboard consolefont modconf block encrypt filesystems fsck"

最后重新生成一下 initramfs ：

    # mkinitcpio -p linux

如果汝在用其它内核的话，用内核的名称（例如 linux-mainline ) 换掉 linux 呐~

`````````````````````````
内核参数 （非 sysytemd ）
`````````````````````````

    如果汝用了 sd-encrypt HOOK，跳过看下面的 >_< 

这时就需要 cryptdevices 参数啦~，大概这么写:

    cryptdevices=<加密的分区>:<解密后使用的映射>

例如咱解密 /dev/nvme0n1p1 ，然后映射到 /dev/mapper/sysroot 的话：

    cryptdevices=/dev/nvme0n1p1:sysroot

如果有不止一个加密分区的话，就挨个写，然后用分号分开，就像这样：

    cryptdevices=/dev/nvme0n1p1:sysroot;/dev/nvme0n1p2:sysext

如果汝是手动设置的 root 参数的话，记得换成刚刚写的映射位置:

    root=/dev/mapper/sysroot

`````````````````````````
内核参数 （ sysytemd ）
`````````````````````````

    用了 sd-encrypt HOOK 来看啦~

首先汝需要加密分区的 UUID，lsblk -f 看一下就好啦~

然后写一个 luks.name 参数:

    luks.name=<加密的分区的UUID>=<解密后使用的映射>

像这样？

    luks.name=160577ec-674c-4792-bdf5-3f4edd4aa00c=sysroot

如果汝是手动设置的 root 参数的话，记得换成刚刚写的映射位置:

    root=/dev/mapper/sysroot

`````````````````````````````
bootloaders
`````````````````````````````

GRUB 的话可以把刚刚的内核参数加到 /etc/default/grub 的 GRUB_CMDLINE_LINUX 参数里，
然后重新生成下配置文件咯

systemd-boot 的话，把刚刚写的内核参数加进 conf 文件里的 options 参数咯~

收尾工作
-----------------------------

 卸载然后关闭加密分区：

    # umount -R <某个挂载点>

    # cryptsetup close <映射名称>

如果需要的话，擦除一下刚刚用的备份分区 ( ⚆ _ ⚆ )

`````

然后重新启动就好啦~
