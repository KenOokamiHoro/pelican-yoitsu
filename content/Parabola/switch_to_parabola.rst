换到了 Parabola GNU/Linux Libre
==========================================================

:slug: switch_to_parabola
:lang: zh
:date: 2017-04-06
:tags: Arch Linux,GNU,凑数
:desc: 在自由之路越走越远……

.. PELICAN_BEGIN_SUMMARY

在 :del:`清真` 自由之路越走越远……

.. PELICAN_END_SUMMARY

.. contents::

什么是 Parabola GNU/Linux Libre？
-----------------------------------------

    Parabola GNU/Linux-libre 是一个基于 Arch Linux 的自由软件项目，旨在提供完全自由的 GNU/Linux 发行版，并针对 i686，x86_64, ARM 三种 CPU 进行了优化。 Parabola 旨在保持包管理工具的简单化。其主要目的是为了让用户完全掌控自己的系統以及100%的自由软件。 作为一个完全自由的发行版，Parabola GNU/Linux-libre 被列在自由软件基金会上。

    Parabola 发展的重点是简洁，优雅，源代码的正确性和最前沿的自由软件之间的平衡。

    它轻量简约的设计使它易于扩展，可以根据自己的需求来塑造系统。 

就是自由化的 Arch Linux 咯~

怎么换？
-----------------------------------------

https://wiki.parabola.nu/Migration_from_Arch_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)

Wiki 写的挺详细的 （咱顺手糊了翻译…… 😂

然后呢？ 😂
-----------------------------------------

* 只比 Arch Linux 少了个无线网卡诶（😂其实本来 Arch 就没剩下多少……，找个 USB 无线网卡插上凑合一下……

* 不少官方源的包和 `your-freedom <https://www.parabola.nu/packages/libre/any/your-freedom/>`_ 冲突， :del:`被批判了一番……` ，然后被卸载了。

* Firefox 和 ThunderBird 因为 Mozilla 的商标问题换成了 Iceweasel 和 IceDove ，然而 Firefox 那蜜汁 Emoji 显示…… 😂

:del:`没错又水了篇文章……`
------------------------------------

嗯……😂😂😂




 

