MediaWiki 网站搬家记
=========================================

:slug: mediawiki_move_note
:lang: zh
:date: 2016-05-30
:tags: mediawiki,notes
:series: MediaWiki
:desc: 如何把 MediaWiki 网站搬到另外一个服务器上?
:issueid: 26

.. PELICAN_BEGIN_SUMMARY

如何把 MediaWiki 网站搬到另外一个服务器上?

:del:`这篇纯粹是来凑数的2333~`

.. PELICAN_END_SUMMARY

.. contents::

第一步:备份数据库 ......
--------------------------------

说实话咱只用过 MySQL/MariaDB 😂,所以 :code:`mysqldump` 解决 😂

.. code-block:: bash

    # 这里用了一个重定向标准输出到某个文件
    # mysqldump [连接数据库的各种选项,例如用户名啥的] [数据库名称] > somefile.sql 
    $ mysqldump -u wikiuser -p somedb > somefile.sql

然后输入密码,稍等片刻就会发现汝的当前目录下多了个文件 (例如 somefile.sql ),这就是导出的数据库啦~

第二步:传输文件
--------------------------------

假设汝的 MediaWiki 安装在 :code:`/path/to/mediawiki` 😂

如果汝能物理访问汝的两台服务器,
那就直接把 :code:`/path/to/mediawiki` 和上一步备份的 SQL 转储复制过去不就好啦~  (╯・ω・)╯ ┻━┻ 

或者如果汝能用 SSH 连接两台服务器的话,可以试试这个:

.. code-block:: bash

    tar czf - /path/to/mediawiki | ssh username@server tar xzf - -C /path/to/mediawiki
    
这一行命令做了这些:

* 把 /path/to/mediawiki 打包成归档文件,不过输出到标准输出. 
    :del:`(千万别说汝不知道啥叫管道以及管道怎么用)` 😂
* 通过 ssh 连接到目标服务器.后面是连接到服务器上以后执行的命令.
* 后面的 tar 命令解开一个归档文件,中间孤独的 hyphen ("-") 表示要解开的文件来自标准输入. :code:`-C` 参数用于改变解开到的目录.

别忘了把转储的数据库文件也 copy 过去😂

然后输入密码,稍等片刻 again ......

如果不行的话,那就传统的 FTP Copy 过去,就是可能慢点 😂

第三步: 还原数据库备份
-------------------------------------

mysql (MariaDB 也是这个命令):

.. code-block:: bash

    # 这里用了一个重定向某个文件到标准输入
    # mysql -u [用户名] -p [数据库名] < somefile.sql
    $ mysql -u wikiuser -p somedb < somefile.sql

一些收尾工作
-------------------------------

* 数据库连接信息变了就去 :code:`LocalSettings.php` 里改😏

* 连外网的就去改 DNS 😂

好了大概就是这个 :del:`鬼` 样子😂😂
