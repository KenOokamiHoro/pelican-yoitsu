Sailfish OS 移植中（1）- hybris-hal
==================================================

:slug: porting_sailfishos_1
:lang: zh 
:date: 2018-06-07
:series: Sailfish OS 移植中
:tags: Sailfish OS
:desc: "HAL  _(:з」∠)_"

.. PELICAN_BEGIN_SUMMARY

Android HAL  \_(:з」∠)\_

.. PELICAN_END_SUMMARY

从这个阶段开始汝可能会遇到各种神奇的问题 😂，要是遇到了啥问题的话，不妨到这俩地方来问一问：

* `Telegram 上的 Jolla-Sailfish OS 中文交流群 <https://t.me/jollacn>`_
* irc.freenode.net 上的 #sailfishos-porters （记得看 topic）

.. contents:: 

取得挂载点信息
-------------------------

看 `HADK 文档里 <https://sailfishos.org/wp-content/uploads/2017/09/SailfishOS-HardwareAdaptationDevelopmentKit-2.0.1.pdf#section.5.3>`_ 讲的：

    Systemd：怪我咯 😂

打开 hybris/hybris-boot/fixup-mountpoints ，大概像这样：

.. code-block:: bash

    #!/bin/sh
    # Fix up mount points device node names.
    #
    # /data needs to be mounted in initrd, but there is no udev that early,
    # which means there is no /dev/block/platform/*/by-name/* (or bootdevice).
    # This file is a map from the "by-name" path to /dev/mmcblkMpN.
    # It also serves dhd to convert fstab and *.rc mount entries to systemd units,
    # because at that boot stage there is still no udev "by-name" paths, even when
    # systemd starts to execut the "*.mount" units.

    DEVICE=$1
    shift

    echo "Fixing mount-points for device $DEVICE"

    case "$DEVICE" in
        # 中间省去若干行 ……
        "shamu")
            sed -i \
                -e 's block/platform/msm_sdcc.1/by-name/aboot mmcblk0p7 ' \
                -e 's block/platform/msm_sdcc.1/by-name/abootBackup mmcblk0p13 ' \
                -e 's block/platform/msm_sdcc.1/by-name/boot mmcblk0p37 ' \
                -e 's block/platform/msm_sdcc.1/by-name/cache mmcblk0p38 ' \
                -e 's block/platform/msm_sdcc.1/by-name/cid mmcblk0p29 ' \
                -e 's block/platform/msm_sdcc.1/by-name/ddr mmcblk0p6 ' \
                -e 's block/platform/msm_sdcc.1/by-name/frp mmcblk0p18 ' \
                -e 's block/platform/msm_sdcc.1/by-name/keystore mmcblk0p24 ' \
                -e 's block/platform/msm_sdcc.1/by-name/kpan mmcblk0p36 ' \
                -e 's block/platform/msm_sdcc.1/by-name/logo mmcblk0p30 ' \
                -e 's block/platform/msm_sdcc.1/by-name/logs mmcblk0p25 ' \
                -e 's block/platform/msm_sdcc.1/by-name/mdm1dhob mmcblk0p28 ' \
                -e 's block/platform/msm_sdcc.1/by-name/mdm1hob mmcblk0p27 ' \
                -e 's block/platform/msm_sdcc.1/by-name/mdm1m9kefs1 mmcblk0p19 ' \
                -e 's block/platform/msm_sdcc.1/by-name/mdm1m9kefs2 mmcblk0p20 ' \
                -e 's block/platform/msm_sdcc.1/by-name/mdm1m9kefs3 mmcblk0p21 ' \
                -e 's block/platform/msm_sdcc.1/by-name/mdm1m9kefsc mmcblk0p33 ' \
                -e 's block/platform/msm_sdcc.1/by-name/metadata mmcblk0p2 ' \
                -e 's block/platform/msm_sdcc.1/by-name/misc mmcblk0p31 ' \
                -e 's block/platform/msm_sdcc.1/by-name/modem mmcblk0p1 ' \
                -e 's block/platform/msm_sdcc.1/by-name/oem mmcblk0p39 ' \
                -e 's block/platform/msm_sdcc.1/by-name/padA mmcblk0p11 ' \
                -e 's block/platform/msm_sdcc.1/by-name/padB mmcblk0p22 ' \
                -e 's block/platform/msm_sdcc.1/by-name/padC mmcblk0p40 ' \
                -e 's block/platform/msm_sdcc.1/by-name/padD mmcblk0p32 ' \
                -e 's block/platform/msm_sdcc.1/by-name/persist mmcblk0p26 ' \
                -e 's block/platform/msm_sdcc.1/by-name/recovery mmcblk0p35 ' \
                -e 's block/platform/msm_sdcc.1/by-name/rpm mmcblk0p8 ' \
                -e 's block/platform/msm_sdcc.1/by-name/rpmBackup mmcblk0p14 ' \
                -e 's block/platform/msm_sdcc.1/by-name/sbl1 mmcblk0p3 ' \
                -e 's block/platform/msm_sdcc.1/by-name/sbl1bak mmcblk0p12 ' \
                -e 's block/platform/msm_sdcc.1/by-name/sdi mmcblk0p4 ' \
                -e 's block/platform/msm_sdcc.1/by-name/sec mmcblk0p5 ' \
                -e 's block/platform/msm_sdcc.1/by-name/sp mmcblk0p23 ' \
                -e 's block/platform/msm_sdcc.1/by-name/ssd mmcblk0p34 ' \
                -e 's block/platform/msm_sdcc.1/by-name/system mmcblk0p41 ' \
                -e 's block/platform/msm_sdcc.1/by-name/tz mmcblk0p10 ' \
                -e 's block/platform/msm_sdcc.1/by-name/tzBackup mmcblk0p16 ' \
                -e 's block/platform/msm_sdcc.1/by-name/userdata mmcblk0p42 ' \
                -e 's block/platform/msm_sdcc.1/by-name/utags mmcblk0p9 ' \
                -e 's block/platform/msm_sdcc.1/by-name/utagsBackup mmcblk0p15 ' \
                -e 's block/platform/msm_sdcc.1/by-name/versions mmcblk0p17 ' \
                "$@"
            ;;
        *)
            cat <<EOF

    ****************************************************************
    ****************************************************************
    ERROR: $DEVICE does not have mountpoint fixup data - see
        Sailfish OS HADK for details on how to fix this.
    ****************************************************************
    ****************************************************************

    EOF
            exit 1
            ;;
    esac

大概就是：

.. code-block:: bash

    "{{汝设备的代号}}")
        sed -i \
            -e 's block/xxx xxx ' \
            ......
            "$@"
        ;;

这个样子 😂

那么中间一大把 -e 怎么生成呢？

把汝的手机接到电脑上，运行一下 adb ：

.. code-block:: bash

    $ adb shell
    $ shamu:/ $ ls -l /dev/block/platform/ 
    # 在这里按一下 TAB 补全一下：
    $ shamu:/ $ ls -l /dev/block/platform/msm_sdcc.1/by-name/                                                            
    total 0
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 aboot -> /dev/block/mmcblk0p7
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 abootBackup -> /dev/block/mmcblk0p13
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 boot -> /dev/block/mmcblk0p37
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 cache -> /dev/block/mmcblk0p38
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 cid -> /dev/block/mmcblk0p29
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 ddr -> /dev/block/mmcblk0p6
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 frp -> /dev/block/mmcblk0p18
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 keystore -> /dev/block/mmcblk0p24
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 kpan -> /dev/block/mmcblk0p36
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 logo -> /dev/block/mmcblk0p30
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 logs -> /dev/block/mmcblk0p25
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 mdm1dhob -> /dev/block/mmcblk0p28
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 mdm1hob -> /dev/block/mmcblk0p27
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 mdm1m9kefs1 -> /dev/block/mmcblk0p19
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 mdm1m9kefs2 -> /dev/block/mmcblk0p20
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 mdm1m9kefs3 -> /dev/block/mmcblk0p21
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 mdm1m9kefsc -> /dev/block/mmcblk0p33
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 metadata -> /dev/block/mmcblk0p2
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 misc -> /dev/block/mmcblk0p31
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 modem -> /dev/block/mmcblk0p1
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 oem -> /dev/block/mmcblk0p39
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 padA -> /dev/block/mmcblk0p11
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 padB -> /dev/block/mmcblk0p22
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 padC -> /dev/block/mmcblk0p40
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 padD -> /dev/block/mmcblk0p32
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 persist -> /dev/block/mmcblk0p26
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 recovery -> /dev/block/mmcblk0p35
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 rpm -> /dev/block/mmcblk0p8
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 rpmBackup -> /dev/block/mmcblk0p14
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 sbl1 -> /dev/block/mmcblk0p3
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 sbl1bak -> /dev/block/mmcblk0p12
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 sdi -> /dev/block/mmcblk0p4
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 sec -> /dev/block/mmcblk0p5
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 sp -> /dev/block/mmcblk0p23
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 ssd -> /dev/block/mmcblk0p34
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 system -> /dev/block/mmcblk0p41
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 tz -> /dev/block/mmcblk0p10
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 tzBackup -> /dev/block/mmcblk0p16
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 userdata -> /dev/block/mmcblk0p42
    lrwxrwxrwx 1 root root 20 1970-02-24 06:55 utags -> /dev/block/mmcblk0p9
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 utagsBackup -> /dev/block/mmcblk0p15
    lrwxrwxrwx 1 root root 21 1970-02-24 06:55 versions -> /dev/block/mmcblk0p17

以第一行为例，大概可以这样写：

    -e 's block/platform/msm_sdcc.1/by-name/aboot mmcblk0p7 ' \

以此类推，可以考虑写个脚本简化这一过程，不过记住最后的反斜线后面应该是啥都没有的 😂
（说多了都是眼泪啊……）

编译 hybris-hal 
--------------------------

设置相应的环境变量：

.. code-block:: bash

    # 就是汝上一节写的那个啦
    $ source ~/.hadk.env 
    $ cd $ANDROID_ROOT
    # 换 python2
    $ source venv/bin/activate
    # 如果硬盘够用的话，可以考虑用 ccache 缓存中间文件加速编译
    $ export USE_CCACHE=1
    # 设置编译 Android 的环境变量
    $ source build/envsetup.sh
    $ export LC_ALL=C
    $ breakfast $DEVICE

然后开始编译：

.. code-block:: bash

    # 和其它编译类似，可以通过 -j 并行编译提高速度。
    $ make -j4 hybris-hal

检查内核配置是否符合 hybris 的需要：

.. code-block:: bash

    $ hybris/mer-kernel-check/mer_verify_kernel_config ./out/target/product/$DEVICE/obj/KERNEL_OBJ/.config

    WARNING: CONFIG_CGROUP_MEM_RES_CTLR_KMEM is invalid
    It is unset
    Allowed values : y, !
    Comment says: systemd (optional): http://0pointer.de/blog/projects/cgroups-vs-cgroups.html

    WARNING: CONFIG_SECURITY_SELINUX_BOOTPARAM is invalid
    It is unset
    Allowed values : y, !
    Comment says: Required by hybris, SELinux needs to be disabled. 
    Leave as not set, if you have unset AUDIT (read more about the CONFIG_AUDIT flag)

    WARNING: CONFIG_CGROUP_MEM_RES_CTLR_SWAP is invalid
    It is unset
    Allowed values : y, !
    Comment says: systemd (optional): http://0pointer.de/blog/projects/cgroups-vs-cgroups.html

    WARNING: CONFIG_CGROUP_MEM_RES_CTLR is invalid
    It is unset
    Allowed values : y, !
    Comment says: systemd (optional): http://0pointer.de/blog/projects/cgroups-vs-cgroups.html

    WARNING: CONFIG_FW_LOADER_USER_HELPER is invalid
    Value is: y
    Allowed values : n, !
    Comment says: it's actually needed by some Lollipop based devices; 
    systemd(optional): http://cgit.freedesktop.org/systemd/systemd/commit/README?id=713bc0cfa477ca1df8769041cb3dbc83c10eace2

可（ken）能（ding）会出一些 ERROR 和 WARNING ，按提示去改汝的 defconfig 去吧……

    啥？汝不知道汝的 defconfig 是哪个？这里有几种确定的方法：

    * 看  $ANDROID_ROOT/device/$VENDOR/\*/BoardConfig\*.mk 里的 TARGET_KERNEL_CONFIG
    * 或者去 kernel/arch/arm/configs 里按 codename 瞅瞅？ 😂

编辑完以后 make -j4 hybris-boot 重新生成内核再来一遍。
以及因为内核配置可能有依赖关系和这个检测脚本有点老的缘故，有时汝改了 defconfig 警告还是有 😂，
只要没有 ERROR 多半就 OK 😂

如果已经尽力消除所有 ERROR 和大多数 or 全部 WARNING以后，运行 make hybris-recovery 生成恢复内核。


------------------


未完待续 \*2 😂