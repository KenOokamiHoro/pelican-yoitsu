Sailfish OS 移植中（0）- 挖坑
==================================================


:slug: porting_sailfishos_0 
:lang: zh 
:date: 2018-06-05
:series: Sailfish OS 移植中
:tags: Sailfish OS
:desc: "心血来潮，能不能成功还是个未知数  _(:з」∠)_"
:modified: 2018-08-01

.. PELICAN_BEGIN_SUMMARY

心血来潮，能不能成功还是个未知数  _(:з」∠)_

.. PELICAN_END_SUMMARY

.. contents:: 

那么问题来了……
--------------------------

    Sailfish OS 是啥来着？

（此处应有斜眼）

先决条件
-------------------------

* 一部支持 Cyanogenmod/LineageOS 的手机
* 一台电脑，至少要有 4 GiB 内存和 16 GiB 硬盘的电脑
    * 如果用 ccache 的话需要更多硬盘
    * 当然是多多益善啦~
* `SailfishOS-HardwareAdaptationDevelopmentKit，官方的移植文档 <https://sailfishos.org/wp-content/uploads/2017/09/SailfishOS-HardwareAdaptationDevelopmentKit-2.0.1.pdf>`_
* 一个网速 OK 的梯子

事前准备
--------------------------

    咱是在 Parabola GNU/Linux Libre 上编译的，于是 Arch GNU/Linux 应该也可以用。
    其它发行版就自行按图索骥好了 😂

安装编译 LineageOS 需要的软件
（因为现在移植到 Android 手机上的 Sailfish OS 是在 Android 上用 hybris 跑起来一个 GNU/Linux 的
用户空间嘛）， AUR 上有个 lineageos-devel 的 metapackage。

    可以参考 https://wiki.archlinux.org/index.php/Android#Building

因为要拖源代码回来，所以装个 git 和 repo ，然后设置一下汝的名称和邮箱（如果汝以前用过 Git 的话可以跳过这一步）：

    git config --global user.name "Your Name"

    git config --global user.email "you@example.com"

设置相应的环境变量
--------------------------

.. code-block:: bash

    # ~/.hadk.env
    # Mer SDK 的位置
    export MER_ROOT=/srv/mer
    export PLATFORM_SDK_ROOT=$MER_ROOT
    # 移植到的目标手机的厂商和代号，可以去 LineageOS Wiki 上找到
    export VENDOR="moto"
    export DEVICE="shamu"
    # 移植的架构，和手机有关
    export PORT_ARCH="armv7hl"

    # 要移植的 hybris 和 Sailfish OS 版本
    export ROOMSERVICE_BRANCHES="cm-14.1"
    export HYBRIS_BRANCH="hybris-14.1"
    export SAILFISH_VERSION=2.1.4.13

    # 源代码放在哪
    export ANDROID_ROOT=/home/repo/libhybris



下载源代码
------------------------------

初始化仓库：

.. code-block:: bash

    $ mkdir -p $ANDROID_ROOT
    $ cd $ANDROID_ROOT
    $ repo init -u git://github.com/mer-hybris/android.git -b $HYBRIS_BRANCH
    
设置本地清单，用汝喜欢的文字编辑器打开 $ANDROID_ROOT/.repo/local_manifests 
（需要的话就新建一个），然后建立一个 {{汝设备的代号}}.xml ，加入适当的 repo :

* 至少需要 Device 和 kernel ，在 Github 上搜索一下 “android_[device,kernel]_$VENDOR_$DEVICE”看看？

例如咱的 shamu ：

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8"?>
    <manifest>
        <project path="device/moto/shamu" remote="private" name="KenOokamihoro/android_device_moto_shamu" 
        revision="cm-14.1" />
        <project path="kernel/moto/shamu" remote="private" name="KenOokamiHoro/android_kernel_moto_shamu" 
        revision="cm-14.1" />
        <project name="TheMuppets/proprietary_vendor_motorola" path="vendor/motorola" revision="cm-14.1"  />
        <project path="rpm/" name="KenOokamiHoro/droid-hal-shamu" revision="master" />
        <project path="hybris/droid-configs" name="KenOokamiHoro/droid-config-shamu" revision="master" />
        <project path="hybris/droid-hal-version-shamu" name="KenOokamiHoro/droid-hal-version-shamu" 
        revision="master" 
        />
    </manifest>

然后同步一下代码：

.. code-block:: bash

    $ repo sync --fetch-submodules

* 同步可能需要很长的时间，甚至有可能会失败，失败的话就多试几次 😂

设置一个 Python2 虚拟环境
----------------------------------

    因为 Android 这套工具还认为 python 是 python2 😂

用汝喜欢的虚拟环境工具就 Ok 啦，例如 python2-virtualenv ：

.. code-block:: bash

    $ virtualenv2 /path/to/your/virtualenv
    # 以后要用的话：
    $ source /path/to/your/virtualenv/bin/activate



安装 Platform SDK 
----------------------------------

    从官方文档里抄的 😂

.. code-block:: bash

    # 下载并解压 SDK 的 chroot
    export PLATFORM_SDK_ROOT=/srv/mer
    curl -k -O http://releases.sailfishos.org/sdk/installers/latest/Jolla-latest-SailfishOS_Platform_SDK_Chroot-i486.tar.bz2 ;
    sudo mkdir -p $PLATFORM_SDK_ROOT/sdks/sfossdk ;
    sudo tar --numeric-owner -p -xjf Jolla-latest-SailfishOS_Platform_SDK_Chroot-i486.tar.bz2 -C $PLATFORM_SDK_ROOT/sdks/sfossdk  ;
    # 为 .bashrc 设置相应的 PS1 ，alias 和环境变量 （其它 Shell 用户请自行依样画葫芦）
    echo "export PLATFORM_SDK_ROOT=$PLATFORM_SDK_ROOT" >> ~/.bashrc
    echo 'alias sfossdk=$PLATFORM_SDK_ROOT/sdks/sfossdk/mer-sdk-chroot' >> ~/.bashrc ; exec bash ;
    echo 'PS1="PlatformSDK $PS1"' > ~/.mersdk.profile ;
    echo '[ -d /etc/bash_completion.d ] && for i in /etc/bash_completion.d/*;do . $i;done'  >> ~/.mersdk.profile ;
    # 进去 😂
    sfossdk

下载工具：

.. code-block:: bash

    # 安装工具链
    sudo zypper in android-tools-hadk tar
    # 安装通用工具链
    # sdk-manage target install <name> <url> --tooling <name> --toolchain <name>
    sdk-manage target install SailfishOS-latest-armv7hl http://releases.sailfishos.org/sdk/latest/Jolla-latest-Sailfish_SDK_Target-armv7hl.tar.bz2 \
     --tooling SailfishOS-latest --tooling-url http://releases.sailfishos.org/sdk/latest/Jolla-latest-Sailfish_SDK_Tooling-i486.tar.bz2
    # 安装汝设备使用的工具链
    # target 的名称命名为 $VENDOR-$DEVICE-$PORT_ARCH，tooling 的名称则是 $VENDOR-$DEVICE
    sdk-manage target install moto-shamu-armv7hl http://releases.sailfishos.org/sdk/latest/Jolla-latest-Sailfish_SDK_Target-armv7hl.tar.bz2 --tooling moto-shamu --tooling-url http://releases.sailfishos.org/sdk/latest/Jolla-latest-Sailfish_SDK_Tooling-i486.tar.bz2

    # 更新 chroot
    # 更新到最新的版本 （写这篇文章的时候最新的是 2.2.0.29）
    sudo ssu re 2.2.0.29
    sudo zypper ref
    sudo zypper dup

    
    
---------------------

未完待续 😂