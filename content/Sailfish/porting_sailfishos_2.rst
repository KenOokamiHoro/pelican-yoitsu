Sailfish OS 移植中（2）- droid-hal 和根文件系统
==================================================

:slug: porting_sailfishos_2
:lang: zh 
:date: 2018-09-01
:series: Sailfish OS 移植中
:tags: Sailfish OS
:desc: "Droid HAL，对应 HADK 文档的第七，第八和第十章  _(:з」∠)_"

.. PELICAN_BEGIN_SUMMARY

Droid HAL，对应 HADK 文档的第七，第八和第十章  \_(:з」∠)\_

.. PELICAN_END_SUMMARY


.. contents:: 

为新设备生成必要的模板
------------------------------------

进入 Platform SDK （sfossdk），引入需要的环境变量（如果汝没有写进 bash_profile 或 bashrc 的话）。

接下来就是依样画葫芦啦 😂

.. code-block:: bash

    PLATFORM_SDK $ 

    cd $ANDROID_ROOT
    mkdir rpm
    cd rpm
    git init
    # 引入必要的 submodules （例如这里的 dhd）
    git submodule add https://github.com/mer-hybris/droid-hal-device dhd
    # 必要的替换成汝的手机型号和品牌
    sed -e "s/@DEVICE@/shamu/" \
    -e "s/@VENDOR@/moto/" \
    -e "s/@DEVICE_PRETTY@/Nexus 6/" \
    -e "s/@VENDOR_PRETTY@/Motorola/" \
    dhd/droid-hal-@DEVICE@.spec.template > droid-hal-shamu.spec
    # 然后提交到汝自己对应的 git 仓库中
    # 最好提前检查一下新生成的文件中的内容
    git add .
    git commit -m "[dhd] Initial content"
    # 如果汝偏好使用 SSH ，换成 git@github.com:myname/your_repo_name
    # 例如 git@github.com:myname/droid-hal-shamu
    # 以及别忘了把 myname 换成汝自己的用户名，下同……
    git remote add myname https://github.com/myname/droid-hal-shamu.git
    git push myname master
    cd -
    # 下面的其实也差不多 😂
    mkdir -p hybris/droid-configs
    cd hybris/droid-configs
    git init
    git submodule add https://github.com/mer-hybris/droid-hal-configs \
    droid-configs-device
    mkdir rpm
    sed -e "s/@DEVICE@/shamu/" \
    -e "s/@VENDOR@/moto/" \
    -e "s/@DEVICE_PRETTY@/Nexus 6/" \
    -e "s/@VENDOR_PRETTY@/Motorola/" \
    droid-configs-device/droid-config-@DEVICE@.spec.template > \
    rpm/droid-config-shamu.spec
    # Please review rpm/droid-config-shamu.spec before committing!
    git add .
    git commit -m "[dcd] Initial content"
    # Create this repository under your GitHub home
    git remote add myname https://github.com/myname/droid-config-shamu.git
    git push myname master
    cd -
    rpm/dhd/helpers/add_new_device.sh
    #  On Nexus 6 the output of the last command is:
    #  Creating the following nodes:
    #   sparse/
    #   patterns/
    #   patterns/jolla-configuration-shamu.yaml
    #   patterns/jolla-hw-adaptation-shamu.yaml
    cd hybris/droid-configs
    COMPOSITOR_CFGS=sparse/var/lib/environment/compositor
    mkdir -p $COMPOSITOR_CFGS

把手机连接到电脑上，从日志输出中找到汝的触屏是哪一个设备（例如 /dev/input/event0），
然后新建一个 $COMPOSITOR_CFGS/droid-hal-device.conf ：

.. code-block:: conf

    # Config for $VENDOR/$DEVICE
    EGL_PLATFORM=hwcomposer
    QT_QPA_PLATFORM=hwcomposer
    # 用汝获得的 /dev/input/event 替换 /dev/input/event0，还有记得这是一行
    LIPSTICK_OPTIONS=-plugin evdevtouch:/dev/input/event0 -plugin evdevkeyboard:keymap=/usr/share/qt5/keymaps/droid.qmap

然后继续建立 Git 仓库：

.. code-block:: bash

    git add .
    git commit -m "[dcd] Patterns and compositor config"
    git push myname master
    cd -
    mkdir -p hybris/droid-hal-version-shamu
    cd hybris/droid-hal-version-shamu
    git init
    git submodule add https://github.com/mer-hybris/droid-hal-version
    mkdir rpm
    sed -e "s/@DEVICE@/shamu/" \
    -e "s/@VENDOR@/moto/" \
    -e "s/@DEVICE_PRETTY@/Nexus 6/" \
    -e "s/@VENDOR_PRETTY@/Motorola/" \
    droid-hal-version/droid-hal-version-@DEVICE@.spec.template > \
    rpm/droid-hal-version-shamu.spec
    # Please review rpm/droid-hal-version-shamu.spec before committing!
    git add .
    git commit -m "[dvd] Initial content"
    # Create this repository under your GitHub home
    git remote add myname \
    https://github.com/myname/droid-hal-version-shamu.git
    git push myname master

全部完成以后，别忘了修改汝自己的  manifest 文件，加上相应的仓库：

.. code-block:: xml

    <project path="rpm/" 
        name="myname/droid-hal-hammerhead" revision="master" />
    <project path="hybris/droid-configs"
        name="myname/droid-config-hammerhead" revision="master" />
    <project path="hybris/droid-hal-version-hammerhead"
        name="myname/droid-hal-version-hammerhead" revision="master" />

等汝的移植工作完成而且可用的时候，可以去向官方的 manifest 仓库提交一个汝的设备的 Pull Request \:-)


打包 droid-hal-device
-------------------------------

其实只有一步：

    rpm/dhd/helpers/build_packages.sh

以后要是汝改了什么地方的话，记得再运行一次就 OK 啦~

如果汝遇到了 Installed  (but  unpackaged)  file(s)  found 错误的话，记下不在包中的文件，
把文件名添加到  rpm/droid-hal-$DEVICE.spec 的 %include 之前，例如：

.. code-block:: text

    %define straggler_files \
    /init.mmi.boot.sh \
    /init.mmi.touch.sh \
    /init.qcom.ssr.sh \
    /selinux_version \
    /service_contexts \
    %{nil}

然后在 droid-configs/patterns/jolla-hw-adaptation-$DEVICE.yaml 中加上 "- droid-hal-$DEVICE-detritus"。

再次打包就好。

打包 Sailfish OS 的根文件系统
-----------------------------------------------------

生成 KickStart 文件：

.. code-block:: bash

    rpm2cpio droid-local-repo/$DEVICE/droid-configs/droid-config-$DEVICE-ssu-kickstarts-1-1.armv7hl.rpm | cpio -idmv

    HA_REPO="repo --name=adaptation-community-common-$DEVICE-@RELEASE@"
    HA_DEV="repo --name=adaptation-community-$DEVICE-@RELEASE@"
    KS="Jolla-@RELEASE@-$DEVICE-@ARCH@.ks"
    sed \
        "/$HA_REPO/i$HA_DEV --baseurl=file:\/\/$ANDROID_ROOT\/droid-local-repo\/$DEVICE" \
        $ANDROID_ROOT/usr/share/kickstarts/$KS \
        > $KS

使用 MIC 生成安装包：

.. code-block:: bash

    # 选择一个版本，例如写这篇文章时最新的 2.2.0.29
    RELEASE=2.2.0.29
    # 设置一个自定义名称，用来区分汝自己的不同版本，不过不能用 "."。
    EXTRA_NAME=-my1
    # 生成需要的 patterns 。
    # 最后一定会抛出一个 AttributeError: "'NoneType' object has no attribute 'px_proxy_fa.. 
    # 异常，可以安全的忽略掉。
    hybris/droid-configs/droid-configs-device/helpers/process_patterns.sh
    # 用 Mic 生成根文件系统安装包
    sudo mic create fs --arch=$PORT_ARCH \
        --tokenmap=ARCH:$PORT_ARCH,RELEASE:$RELEASE,EXTRA_NAME:$EXTRA_NAME \
        --record-pkgs=name,url \
        --outdir=sfe-$DEVICE-$RELEASE$EXTRA_NAME \
        --pack-to=sfe-$DEVICE-$RELEASE$EXTRA_NAME.tar.bz2 \
        $ANDROID_ROOT/Jolla-@RELEASE@-$DEVICE-@ARCH@.ks

如果一切 OK 的话，汝就会在汝的 $ANDROID_ROOT 文件夹下发现一个 sfe-$DEVICE-$RELEASE$EXTRA_NAME
文件夹里面放着一个 sfe-$DEVICE-$RELEASE$EXTRA_NAME.zip ，这就是汝的根文件系统的刷机包咯~

刷入测试
--------------------------

进入汝手机的第三方 Recovery （例如 TWRP），把汝 hybris 对应的 LineageOS 的底包
和刚刚生成的根文件系统复制到汝的手机上并安装。或者 adb sideload 也行。

* 如果一切都 OK 的话，汝应该能见到 Sailfish OS 的开机画面，试着完成设置看看吧~
* 要是不行的话，在看看 `HADK FAQ <https://wiki.merproject.org/wiki/Adaptations/faq-hadk>`_ 找找问题，
  或者上 IRC 问一下其他人吧 \_(:з」∠)\_

-------------------------

在启动早期和手机建立连接
------------------------------

如果手机和电脑相连以及 hybris 能成功加载的话，汝的电脑上应该会多出一个网络适配器，
和 Android 启动 USB 网络共享类似。 GNU/Linux 的话可以通过 ip 命令确认。

可以用 telnet 连接到早期启动阶段的手机：

    telnet 192.168.2.15 

如果连接不上，可能是 Sailfish OS 已经在加载但是显示不出界面。
这个时候 telnet 的端口是 2323。

连接上以后，可以用 devel-su 命令进入 root shell：

    devel-su

然后就可以通过 dmesg 或者 journalctl 等命令查看日志啦，在调试的时候可能会派上用场。

如果反复重新启动
--------------------------

反复重新启动有这么几种情况：

* 启动后立刻重启（或进入 Recovery 模式）：大概是 SELinux 的问题 \_(:з」∠)\_
    
    所有基于 CyanogenMod 11 以后（Android 4.4）的移植都必须停用 SELinux。
    尝试在编译内核的选项中停用 SELinux ，或者设置 CONFIG_SECURITY_SELINUX_BOOTPARAM=y ，
    然后在启动参数中加上 selinux=0 （启动参数通常在 $ANDROID_ROOT/device/$VENDOR/*/BoardConfig*.mk 
    中的 BOARD_KERNEL_CMDLINE 中）

* 启动后数分钟后重启：尝试停用 ofono 服务，启动时尽快通过 telnet 进入 root shell：

    ln -s /dev/null /etc/systemd/system/ofono.service

* ……

