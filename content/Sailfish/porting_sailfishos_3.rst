Sailfish OS 移植中（3）- 更新和各种杂务续篇
==================================================

:slug: porting_sailfishos_3
:lang: zh 
:date: 2019-01-19
:series: Sailfish OS 移植中
:tags: Sailfish OS
:desc: "如果 Target 更新了该怎么办？ _(:з」∠)_"

如果 Target 更新了该怎么办？ _(:з」∠)_

.. contents:: 

咱该怎么知道 Target 更新了呢？
------------------------------------

https://releases.sailfishos.org/sdk/targets/ 有各个版本 SDK 的列表。

更新 Target SDK 
-------------------------------------

在 Platform SDK Shell 里运行：

.. code-block:: bash

    # sdk-assistant 方式
    $ sdk-assistant update <target-name>
    # sdk-manage 方式
    $ sdk-manage tooling update <name>
    $ sdk-manage target update <name>

:del:`当然要是布星的话把 /srv/mer 删了重新来一遍也行啊 ……`

更新源代码和 submodules
-------------------------------------

切换到 libybris 的目录，然后运行 :code:`repo sync --fetch-submodules` 。

* 记得先提交汝的修改。

还有一些 submodule 需要更新：

* rpm/dhd
* hybris/droid-configs/droid-configs-device
* hybris/droid-hal-version-$VENDOR-$device/droid-hal-version

在这些目录里运行：

.. code-block:: bash

    # git remote -v 获得远程分支
    $ git remote -v
    github	https://github.com/mer-hybris/droid-hal-configs (fetch)
    github	https://github.com/mer-hybris/droid-hal-configs (push)

    # 拉取远程的源代码
    $ git fetch github
    $ git pull github master

然后把 hybris-boot 和 droid-hal 重新打包一下就好（忘了怎么搞？
去翻翻以前的文章或者文档吧 😂）

跳过教程
------------------------------------
每次刷机都要重新设置和看一遍教程很烦？ 在教程画面从左上角顺时针点按屏幕即可跳过（大嘘

hybris-15.1 移植的相关事项
-------------------------------------

在编译 hybris-hal 之前：

.. code-block:: bash

    cd $ANDROID_ROOT
    mkdir -p hybris/mw
    cd hybris/mw
    git clone --recurse-submodules https://github.com/mer-hybris/libhybris.git -b android8-initial

以及不要在 build_package.sh 时编译 libhybris ，而是先手动单独编译：

.. code-block:: bash

    rpm/dhd/helper/build_packages.sh  -b hybris/mw/libhybris/

然后再编译剩下的包（记得跳过 libhybris）。

   When running build_package.sh always skip building libhybris and instead run in PLATFORM_SDK

接着把 https://github.com/mer-hybris/droid-config-sony-nile/tree/master/sparse/usr/libexec/droid-hybris/system/etc/init 
复制到汝的 config repo 然后运行：

.. code-block:: bash

    build_packages.sh -c

通过 adb 解压 tarball 和刷写内核映像
-------------------------------------------------

有时可能用得上？（例如不想整个重新来过的时候）

.. code-block:: bash

    # adb push <汝本地的文件路径> <手机的文件路径>
    $ adb push your_sailfishos_tarball.tar.gz /sdcard/
    $ adb push out/target/product/$DEVICE/hybris-boot.img /sdcard/
    adb shell
    # 如果汝是在 TWRP 里运行的话默认就是 root 了，于是不需要 su 这一步
    # （但是有可能需要先把 /data 挂载上）
    su
    # 解开 tarball
    mkdir -p /data/.stowaways/sailfishos
    tar --numeric-owner -xvzf /sdcard/our_sailfishos_tarball.tar.gz \
    -C /data/.stowaways/sailfishos
    # 汝可能需要自己确定一下汝手机 boot 分区在哪个设备上
    dd if=/sdcard/hybris-boot.img of=/dev/block/platform/msm_sdcc.1/by-name/boot

在 Android 中操作 Sailfish OS 的 chroot
-------------------------------------------------
前提是汝的 /data 没有加密（不然是启动不了 Sailfish OS 的）

.. code-block:: bash 

    $ adb shell
    # 这里只是表示进了 Android 的 shell 😂
    (android) $ su
    # 挂载各种虚拟文件系统
    (android) # mount -o bind /dev /data/.stowaways/sailfishos/dev
    (android) # mount -o bind /proc /data/.stowaways/sailfishos/proc
    (android) # mount -o bind /sys /data/.stowaways/sailfishos/sys
    # chroot 进去
    (android) # chroot /data/.stowaways/sailfishos/ /bin/su -
    # 生成 /etc/resolv.conf ，然后就能用 Android 的网络上网了。
    (sailfish) # echo "nameserver 8.8.8.8" >/etc/resolv.conf