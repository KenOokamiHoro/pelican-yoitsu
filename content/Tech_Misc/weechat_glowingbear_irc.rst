Weechat + Glowing Bear 打造一个网页版 IRC 
====================================================

:slug: weechat_glowingbear_irc
:lang: zh
:date: 2016-03-27
:tags: notes,software,irc,凑数
:desc: Weechat + Glowing Bear 打造一个网页版 IRC ,这样在哪里都可以聊天了呐~
:issueid: 36


.. PELICAN_BEGIN_SUMMARY

Weechat + Glowing Bear 打造一个网页版 IRC ,这样在哪里都可以聊天了呐~

.. PELICAN_END_SUMMARY

.. contents::

为啥要自己搭建一个网页版 IRC 咧?
-------------------------------------

* 不是啥设备都有聊天软件客户端呐~ (比如 PS Vita )  (╯＠Д＠)╯ ┻━┻

* `freenode 的 WebIRC <https://webchat.freenode.net>`_ 很不错,但是验证码是 Google 的,于是又引出了怎么翻墙的问题 ~

为啥是 WeeChat ?
--------------------------------------

多半是 :fref:`farseerfc` 的安利呗~

Prolgue: 安装 weechat 
----------------------------------------------

weechat 在好几个平台上都有二进制包啦~

* Arch Linux: pacman -S weechat

* Cygwin (Windows): select WeeChat packages in setup.exe

* Debian 及其衍生发行版: apt-get install weechat-curses

* Fedora Core: dnf install weechat

* Gentoo: emerge weechat

* Mandriva/RedHat (或其他基于 RPM 包管理器的发行版): rpm -i /path/to/weechat-x.y.z-1.i386.rpm

* openSUSE: zypper in weechat

* Sourcemage: cast weechat

* OS X + Homebrew :  brew install weechat --with-python --with-perl

如果需要编译的话 , `去看 weechat 的文档啦~ <https://weechat.org/files/doc/stable/weechat_user.en.html#source_package>`_


Act 0 : 准备 SSL 证书
-------------------------------------------------

首先运行一次 weechat 来生成需要的文件,然后输入 :code:`/quit` 来退出.

    $ weechat && mkdir -p ~/.weechat/ssl

.. alert-info::

    如果汝有自己的域名的话,为何不试试 `Let's Encrypt <https://letsencrypt.org/>`_ ?
    
    如果没有的话,那就只好自己签名一个证书啦 _(:з」∠)_
    

* 如果汝自己有证书:

    把证书和私钥保存成一个文件:
    
    $ cat /path/to/your/cert /path/to/your/privkey >> ~/.weechat/ssl/relay.pem
    
* 如果没有域名的话,只好自己生成一个证书啦  (╯´ー`)╯ ┻━┻

    $ cd ~/.weechat/ssl

    $ openssl req -nodes -newkey rsa:2048 -keyout relay.pem -x509 -days 365 -out relay.pem
    
* 接下来运行一下 weechat 来设置一下 relay:

    # 咱不知道怎么把 weechat 设置成服务,于是就用 screen 了 😂😂
    
    $ screen weechat
    
    # 加载证书和私钥
    
    /relay sslcertkey
    
    # 为 relay 设置一个密码 ( 用实际的密码替换 "mypassword" )
    
    /set relay.network.password "mypassword"
    
    # 打开中继
    
    /relay add ssl.weechat 9001
    
    然后可以用 Ctrl + a + d 断开 screen 了 (\´・ω・\`)
    
Act 1 : 连接到 relay 
--------------------------------------------------------

如果汝不想自己搭建的话,可以用 glowing-bear 自己的呗~

`链接在这: https://www.glowing-bear.org/ <https://www.glowing-bear.org/>`_

按照上面的指南来连接吧~

如果汝要自己搭建一个的话,首先需要一个 Web 服务器啦~ ( Apache 和 Nginx 都不错,ArchWiki 上能找到文档~)

然后从
 `glowing-bear 的 Github 项目页面上参考上面的提示把 glowing-bear 下载到本地呐~ <https://github.com/glowing-bear/glowing-bear>`_
 
 就是这样?
 
常用操作一览
--------------------------
 
* 指定一个服务器:

    /server add <名称>/<端口> <irc服务器的域名> [ -ssl ( 启用SSL ) ] [ -autoconnect 在启动时自动连接 ]
 
    /server add freenode chat.freenode.net/6697 -ssl -autoconnect
     
* 连接到服务器:
    
    /connect <名称>

    /connect freenode
 
* 修改昵称

    /nick <新昵称>
    
* 加入一个频道

    /join <频道名称>
    
--------------------------

(\´・ω・\`)
-----------------------------
