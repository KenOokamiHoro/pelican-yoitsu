把电脑上的 Shadowsocks 配置分享给手机使用的一点思路
================================================================

:slug: convert_ss_config_to_link
:lang: zh
:date: 2018-05-09
:tags: Python, software, Shadowsocks
:desc: 限于 shadowsocks-libev 😂

.. PELICAN_BEGIN_SUMMARY

:del:`限于 shadowsocks-libev` 😂

.. PELICAN_END_SUMMARY

问题
------------------

手机上的 Shadowsocks 客户端只能从 ss:// 开头的链接读取配置，
并不能直接用 libev 的配置文件 😂

Windows
-------------------
Windows 的 `Shadowsocks-windows <https://github.com/shadowsocks/shadowsocks-windows>`_ 是可以生成
ss:// 的链接的，还可以生成二维码，于是不在本文的讨论范围内 （雾

生成链接
-----------------------

一个普通的 Shadowsocks-libev 的配置文件大概长这样：

.. code-block:: json

    {
    "server":"server_address",
    "server_port":9999,
    "local_address": "127.0.0.1",
    "local_port":8888,
    "password":"some_password",
    "timeout":300,
    "method":"aes-128-gcm",
    }

在 Python 里可以用 json.read() 把 JSON 文件转换成一个字典：

.. code-block:: python

    # 偷点懒不单独写一行开文件啦……
    import json
    config = json.load(open("/path/to/config.json"))

一个普通的 ss:// 链接大概像这样

    ss://{base64编码过的加密方式和密码，中间用冒号分开}@服务器:端口?插件;插件选项#配置名称

如果服务器是个 IPv6 地址，那么两边包上中括号……

对一个字符串直接 encode() 就有二进制流，反过来对着二进制流 decode() 就可以有字符串（感觉这说法好不正经 😂)

以及可以用 base64 模块对二进制流进行 base64 编码和解码：

.. code-block:: python

    import base64
    encoded = base64.encodebytes("some_string".encode())

当然出来的也是个二进制流，需要的话可以再整成字符串，以及消除空白 😂

.. code-block:: python

    encoded_string = encoded.decode().strip()

其它的选项从配置文件里直接读就好，配置名称需要的话就写一个。

生成二维码
---------------------------

方法有很多 😂 例如用 pypi 里的 qrcode：

.. code-block:: python

    import qrcode
    img = qrcode.make("some_text")

需要的话可以把这个图像保存到某个地方，然后打开它：

.. code-block:: python

    img.save("/path/to/images")

要在 Python 里运行其它程序？最简单的方法是 os.system() 😂 稍微复杂一点的可以试试 Subprocess：

.. code-block:: python

    import os
    import subprocess

    os.system("xdg-open /path/to/images")
    subprocess.Call(["xdg-open","/path/to/images"])

这样就会用默认的图片查看器打开刚生成的二维码啦，用汝的手机扫一下看看？ 😂

:del:`水文 +1……`
