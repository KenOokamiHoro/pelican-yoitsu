扫盲 XMPP(Jabber) - 一个自由开放的即时消息协议
====================================================

:slug: using_xmpp_for_chats
:lang: zh
:date: 2018-09-28
:tags:
:desc: 这标题的命名方式哪里眼熟……
:issueid: 

.. PELICAN_BEGIN_SUMMARY

这标题的命名方式哪里眼熟……

.. PELICAN_END_SUMMARY

.. contents::

想和谁在网上聊天的话，可以用的 IM 软件有不少，当然槽点也很多（这家伙要求真多.png）：

* WhatsApp , Line 之流： 从客户端到协议都是私有的，以及似乎有向审查屈服的倾向？

* Telegram ，服务端是私有的，官方客户端的源代码经常咕咕咕……

* Signal 和 Wire ，虽然服务端和客户端都是自由的，但是有人成功运行起来自己的服务端嘛 🤔

……

于是是时候试试看 XMPP 啦（编不下去了.webp.png 😂）

XMPP 是啥？
------------------------------------

可扩展消息与存在协议（XMPP，全称为Extensible Messaging and Presence Protocol，前称Jabber）
是一种以XML为基础的开放式即时通信协议，是经由互联网工程工作小组（IETF）通过的互联网标准。

Jabber是一个开放源代码形式组织产生的网络即时通信协议。XMPP原本是为即时通讯而量身定制，
但由于XML Stanza本身是XML元素，在基于XML灵活发展的特性下，使得XMPP也可以适用其他方面，
已经得到了IETF的批准。XMPP与IMPP、PRIM、SIP（SIMPLE）合称四大IM协议主流，在此4大协议中，XMPP是最灵活的。 

（从维基百科抄的（雾））

有不少聊天软件是 XMPP 的实现，例如 Google Talk 和 Facebook Messenger，以及 AOL 和 一些网络游戏的聊天等。

因为传送的是 XML 嘛，所以 XMPP 协议本身十分灵活，不过传送二进制文件来就稍微有些困难了（不过有不同的扩展
可以提供文件上传等功能）。

和电子邮件一样， XMPP 也是非中心化的协议，可以均衡负载和对抗封锁。当然有可以多处登录啦， XMPP 通过优先级来
区分不同的设备，优先级高的设备会收到相应的消息（有人认为这是个缺点，于是有了 MAM 在不同的设备间同步消息）。

XMPP/Jabber实现多人聊天（MUC，Multiply User Chat）是采用聊天室的方式实现的，形如 room@domain.tld 的形式，
用户可以添加这个聊天室进行群聊，不过介于不少客户端不支持群聊加密，于是 XMPP 更多的还是以私聊为主。

注册一个 XMPP/Jabber 帐号
----------------------------------

因为 XMPP 是个自由开放的协议啦，所以网上有很多公开的 XMPP 服务，
`这里 <https://www.jabberes.org/servers/servers_by_proxy_bytestreams.html>`_ 有一个大而全的列表。
但是如果汝只是想找个速度合适的服务器的话，BLUG 整理出了一个
`经过测试的部分 XMPP 服务器清单 <https://beijinglug.club/wiki/doku.php?id=projects:xmpp>`_ 。

可以看到大多数的 XMPP 服务器对注册没有任何要求，只需要汝选择一个 Jabber ID 然后输入密码就 OK 啦，
当然这么做的后果之一就是忘记密码就不能找回了，所以创建一个强密码并记住它特别重要。

除了在网页上注册以外，不少服务器也支持在客户端上注册，所以……

安装一个 XMPP 客户端
---------------------------------

不同平台上可以推荐的客户端有很多：

* 比如跨平台的 Pidgin （顺便还可以跨协议，但是 Pidgin 支持 XMPP 是挺优秀的）。 Windows
  可以从 https://pidgin.im/download/ 下载安装包，GNU/Linux 可以从包管理器安装，或者下载源代码编译

* macOS 用户可以安装 Adium，可以从 https://adium.im 下载。

* iOS 用户推荐 ChatSecure，可以从 App Store 下载安装。

* Android 用户推荐 `Xabber <https://f-droid.org/en/packages/com.xabber.androiddev/>`_ 和 
  `Conversations <https://f-droid.org/en/packages/eu.siacs.conversations/>`_ 前者支持 XMPP 中常用的 OTR 端到端加密，
  后者支持 MUC 和新的 OMEMO 和 openPGP 加密。

每一个 XMPP 客户端的登录和注册方法都不尽相同，但都比较简单。这里就偷个懒不指导啦 😂

和好友聊天或进行群聊
-----------------------------------

添加汝好友或群聊的 Jabber ID 就 OK 啦~ 

.. image:: /images/XMPP/buddies.png 
    :alt: 大概长这样

加密聊天
----------------------------------

    主要说的是端到端加密，毕竟 8102 年了，传输加密已经是刚需了吧 😂

目前 XMPP 中常用的端到端加密方式有三种 ，都是通过非对称加密实现的：

* `OTR（Off-the-Record Messaging，不留记录即时通讯） <https://otr.cypherpunks.ca/>`_ 是
  XMPP 中最常用的端到端加密方式，被大多数客户端所支持。加密迅速而且前向安全。
  缺点是更换客户端或重新登录时 OTR 指纹会发生变化，
  可能会无法保证消息身份的一致性而需要重新验证。以及对文件的加密尚未标准化（不同客户端加密的文件
  可能无法相互存取，因此习惯是发送文件的链接），另外称为下一代 OTR 的 `OTRv4 <https://github.com/otrv4/otrv4>`_ 正在开发中。

.. image:: /images/XMPP/pidgin_otr.png 
    :alt: Pidgin 通过插件可以支持 OTR

Pidgin 通过插件可以支持 OTR，上面介绍的客户端中除了 Conversations 都支持 OTR。

* `OMEMO（OMEMO Multi-End Message and Object Encryption，OMEMO 多终端消息和对象加密） <https://conversations.im/omemo/>`_ 是
  一种新型的端到端加密协议，可以跨设备同步加密的消息记录和文件，验证起来也比较简单（ :del:`因为都盲目信任了？` ）。
  不过除了 Conversations 以外好像都没支持好的样子……

* openPGP 也能用于 XMPP 的端到端加密，不过毕竟是面向邮件的非对称加密，客户端不一定都支持
  （Gajim、Psi+ 和 mcabber 支持）速度可能没那么快，还有可能把服务器娘累坏（雾）。

一些注意事项？
----------------------------------

* 因为大多数 XMPP 服务没有提供找回密码的功能，** 所以所以创建一个强密码并记住它！**

* 传输加密是必须的，端到端加密最好也用上。

* 端到端加密最好验证下双方的身份，除了传统的指纹验证以外 OTR 还可以通过共享秘密和问答验证。

* 只使用自由开源的客户端， :del:`这个不解释。`

自己搭建 XMPP 服务器
-----------------------------------

好啊好啊，这就是非中心化服务的优势之一呢~

比较推荐的是 `Prosody <https://prosody.im/>`_ 和 `ejabberd <https://www.ejabberd.im/>`_ ，
前者轻量，配置简单。后者功能丰富，扩展性强。
`咱就用 Prosody 搭了一个 </arch-linux/prosody_on_archlinux.html>`_

读点别的？
-----------------------------------

https://tonghuix.io/2015/03/xmpp-chat/

⇪网页标题：弃用QQ和微信！全面转向基于XMPP(Jabber)的即时聊天

这篇文章简单的介绍了下 XMPP 的基本特点，也推荐了些不同平台的客户端。

https://beijinglug.club/wiki/lib/exe/fetch.php?media=xmpp-guide.pdf

⇪网页标题：XMPP(Jabber) 聊天快速指南

由北京 GNU/Linux 用户组（BLUG）成员编写的 XMPP 入门指南，图文并茂值得一读（？）

https://hardenedlinux.github.io/cryptography/2018/02/01/pidgin_xmpp_otr_debian.html

⇪网页标题：Debian/Ubuntu 用户使用基于 XMPP 即时通信协议的 OTR 保护隐私的标准化部署流程

介绍了 OTR 的原理， Debian 上使用 Pidgin 进行 OTR 聊天的流程，以及验证身份的一些技巧。