常见的双因素验证类型
====================================================

:slug: common_2fa_auth
:lang: zh
:date: 2017-10-02
:tags: software,translation 
:desc: 这就是咱鸽了两个月的理由 ？😂
:issueid: 

.. PELICAN_BEGIN_SUMMARY

:del:`这就是咱鸽了两个月的理由 ？😂`

.. PELICAN_END_SUMMARY

.. contents::

Two-factor authentication (or 2FA) is one of the biggest-bang-for-your-buck 
ways to improve the security of your online accounts. 
Luckily, it's becoming much more common across the web. With often just a few 
clicks in a given account's settings, 
2FA adds an extra layer of security to your online accounts on top of your password.

双因素认证（Two-factor authentication，有时也缩写成 2FA）是保护汝在线账户安全的有力工具之一。
值得高兴的是，越来越多的服务和网站开始支持这一功能，设置起来也越来越简单。
（大多数时候只要在网站的账户设置页面上按步骤点击几下鼠标敲击几下键盘就好啦）
双因素认证在汝的密码之上对账户增加了另一层保护。

In addition to requesting something you know to log in (in this case, your password), 
an account protected with 2FA will also request information from something you have 
(usually your phone or a special USB security key). 
Once you put in your password, you'll grab a code from a text or app on your phone or 
plug in your security key before you are allowed to log in. 
Some platforms call 2FA different things—Multi-Factor Authentication (MFA), 
Two Step Verification (2SV), or Login Approvals—but no matter the name, the idea is the same: 
Even if someone gets your password, they won't be able to access your accounts 
unless they also have your phone or security key.

如果汝启用了双因素认证，在一般的登录工作完成以后（在这个环境下是汝输入完密码以后）
会要求其它的信息（通常在汝的手机上，或者是一个特殊的 USB 安全密钥）。
然后汝可能就会从手机短信或者应用上获得一串代码（或者插入安全密钥）来完成这一登录过程。
不同的地方可能对双因素认证有不同的称呼（汝可能听说过多因素认证，两步认证或是登录批准一类的词了呐~）
但不同名字下的目的却是相同的：即使有人得到了汝的密码，没有汝的手机或者安全密钥也是无法登录的呗~

There are four main types of 2FA in common use by consumer websites,
and it's useful to know the differences. Some sites offer only one option;
other sites offer a few different options. We recommend checking twofactorauth.org 
to find out which sites support 2FA and how, 
and turning on 2FA for as many of your online accounts as possible. 
For more visual learners, this infographic from Access Now offers additional information.

这里举出了常见的四种双因素认证的例子，知道它们的区别还是有些用处的啦~
有些网站只提供一种选项，有些会提供好几种。汝如果可以上 https://twofactorauth.org 
确认一下哪些网站提供双因素认证，而且尽可能的在汝使用的网站上打开双因素认证的话最好啦~
不够生动？`Access Now <https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png>`_
有一张介绍各种双因素验证方式优劣的图，汝最好也去看看呗~

Finally, the extra layer of protection from 2FA doesn't mean you should use a weak password.
Always make unique, strong passwords for each of your accounts, 
and then put 2FA on top of those for even better log-in security.

最后，不要认为有了双因素验证的保护就可以用个弱密码啦 😡 ,还是和平常教导的一样，
为汝的每一个账户使用一个独立而且安全性足够强的密码，然后使用双因素认证多添一层保护咯~


SMS 2FA (基于短信的双因素认证)
-----------------------------------

When you enable a site's SMS 2FA option, you'll often be asked to provide a phone number. 
Next time you log in with your username and password, you'll also be asked to enter a short code 
(typically 5-6 digits) that gets texted to your phone. This is a very popular option for sites to implement,
since many people have an SMS-capable phone number and it doesn't require installing an app. 
It provides a significant step up in account security relative to just a username and password.

汝启用这样的双因素验证方式时便会像网站提供一个手机号码。
下次登录的时候除了用户名和密码以外汝还会要求输入一个从手机收到的代码（一般是五六个数字）。
这是种很常见的双因素验证的方法，因为大多数人的手机都能收短信（雾），而且实现起来也容易。
关键是效果显著啊（看起来比只输入用户名和密码安全）。

There are some disadvantages, however. Some people may not be comfortable giving their phone number—a piece of 
potentially identifying information—to a given website or platform. Even worse, 
some websites, once they have your phone number for 2FA purposes, will use it for other purposes, 
like targeted advertising, conversion tracking, and password resets. 
Allowing password resets based on a phone number provided for 2FA is an especially egregious problem, 
because it means attackers using phone number takeovers could 
get access to your account without even knowing your password.

当然这种方法的缺点也是有的：

* 有些人不想泄露自己的手机号（或者其他可能关联到自己身份的物件）给网站
* 现在网站有了汝的手机号，怎么利用就看它们的人品了（例如可能会用来点对点投放广告……）
* 如果这种方法能重置密码的话，攻击者只需要拿到汝的手机就能在不知道汝的账户的情况下重置密码然后控制汝的账户啦~ 

    详情可以看 `这篇 <https://www.theregister.co.uk/2017/07/10/att_falls_for_hacker_tricks/>`_ 和
    `另一篇 <https://blog.cloudflare.com/the-four-critical-security-flaws-that-resulte/>`_ 报道。

Further, you can't log in with SMS 2FA if your phone is dead or can't connect to a mobile network. 
This can especially be a problem when travelling abroad. Also, 
it's often possible for an attacker to trick your phone company into assigning your phone number to a different SIM card, 
allowing them to receive your 2FA codes. Flaws in the SS7 telephony protocol can allow the same thing. 
Note that both of these attacks only reduce the security of your account to the security of your password.

再多想点，要是汝的手机坏了或者收不到信号（例如汝正在旅行）就收不到验证码了诶。
有小心机的攻击者可以用手段骗过运营商把汝的手机号转移给他，更高明的话可以
`利用通信协议的漏洞 <https://www.wired.com/2017/05/fix-ss7-two-factor-authentication-bank-accounts/>`_ 。
这样汝的验证码就无声无息的被它们收到了哦~ 

Authenticator App / TOTP 2FA （验证器应用 / 基于时间的一次性密码）
--------------------------------------------------------------------------------------------

Another phone-based option for 2FA is to use an application that generates codes locally based on a secret key. 
Google Authenticator is a very popular application for this; FreeOTP is a free software alternative. 
The underlying technology for this style of 2FA is called Time-Based One Time Password (TOTP), 
and is part of the Open Authentication (OATH) architecture 
(not to be confused with OAuth, the technology behind "Log in with Facebook" and "Log in with Twitter" buttons).

另外一种基于手机的验证就是生成验证代码的应用啦，例如 Google Authenticator （相当流行） 或者 FreeOTP （一个自由软件实现）。
驱动这一种双因素认证的技术是“基于时间的一次性代码”（Time-Based One Time Password ，缩写成TOTP），它也是开放认证
（ Open Authentication）结构的一部分。

    不过别和 OAUTH 搞混啦（就是网站上那些 “使用 Facebook 登录” “使用 Twitter 登录” 按钮背后的技术）

If a site offers this style of 2FA, it will show you a QR code containing the secret key. 
You can scan that QR code into your application. 
If you have multiple phones you can scan it multiple times;
you can also save the image to a safe place or print it out if you need a backup. 
Once you've scanned such a QR code, your application will produce a new 6-digit code every 30 seconds. 
Similar to SMS 2FA, you'll have to enter one of these codes in addition to your username and password in order to log in.

如果汝正在使用的网站提供这种验证方法的话，汝会按要求扫描网站提供的二维码
（或者在生成器应用中输入一个密钥，其实二维码的内容就是这个密钥）。
当然汝也可以在汝的好几个不同的手机上重复同样的操作，也可以把二维码或者密钥打印出来放在个安全的地方备用。
汝的生成器应用会每隔一段时间提供一个新的代码（比较常见的是30秒？），和基于短信的
双因素认证类似，汝会在输入完用户名和密码后输入生成器生成的代码。

This style of 2FA improves on SMS 2FA because you can use it 
even when your phone is not connected to a mobile network, 
and because the secret key is stored physically on your phone. 
If someone redirects your phone number to their own phone, 
they still won't be able to get your 2FA codes. It also has some disadvantages: 
If your phone dies or gets stolen, 
and you don't have printed backup codes or a saved copy of the original QR code, 
you can lose access to your account. For this reason, 
many sites will encourage you to enable SMS 2FA as a backup. Also, 
if you log in frequently on different computers, 
it can be inconvenient to unlock your phone, open an app, and type in the code each time.

基于时间的一次性密码比基于短信的双因素认证好的一点是就算汝的手机没有连接到网络也可以使用。
而且因为密钥存储在你的手机上，所以得到了汝的手机号码的人也没法获得汝的验证代码。
当然缺点也不是没有：

* 如果汝的手机出了点状况（例如坏了，丢了等等），汝就没办法获得验证代码啦…… 所以不少网站会建议汝添加一个手机号码备用。
* 另外如果汝经常在不同的电脑上登录的话，打开手机-解锁-打开验证器应用这一串过程可能会觉得有点麻烦(lll￢ω￢)

Push-based 2FA （基于推送通知的双因素认证）
--------------------------------------------

Some systems, like Duo Push and Apple's Trusted Devices method,
can send a prompt to one of your devices during login. 
This prompt will indicate that someone (possibly you) is trying to log in,
and an estimated location for the login attempt. You can then approve or deny the attempt.

Duo Push 和 Apple 的“信任的设备”功能可以在登录时像汝的设备发送一条提示，
提示有人（当然可能是汝自己啦）在尝试登录。同时会提供一个估计的地址。汝可以选择批准或拒绝登录。

This style of 2FA improves on authenticator apps in two ways: 
Acknowledging the prompt is slightly more convenient than typing in a code, 
and it is somewhat more resistant to phishing. With SMS and authenticator apps,
a phishing site can simply ask for your code in addition to your password,
and pass that code along to the legitimate site when logging in as you. 
Because push-based 2FA generally displays an estimated location based 
on the IP address from which a login was originated, and most phishing 
attacks don't happen to be operated from the same IP address ranges as their victims,
you may be able to spot a phishing attack in progress by noticing that 
the estimated location differs from your actual location. However, 
this requires that you pay close attention to a subtle security indicator.
And since location is only estimated, it's tempting to ignore any anomalies. 
So the additional phishing protection provided by push-based 2FA is limited.

相对于基于时间的一次性密码，这种方式的优点有：
* 点个通知比敲几个数字或者字母少动手（
* 一定程度上可以防范一定程度高深的钓鱼网站（雾

手法高明的钓鱼网站可以画一个和真网站一样的输入验证码的界面迷惑用户，再坑点还能
把登录信息提交到真网站并把受害者重定向到真网站上去（嘿嘿嘿……

不过由于推送通知一般会包含登录地点的估计地址（通常通过 IP 地址估计），
而大多数情况下钓鱼网站不会和用户是一个 IP 地址，因此可能会被识别出来。
然而地址是估计的嘛所以也没有多少保护力 (╯‵□′)╯︵┻━┻

Disadvantages of push-based 2FA: 
It's not standardized, so you can't choose from a variety of authenticator apps,
and can't consolidate all your push-based credentials in a single app. 
Also, it requires a working data connection on your phone, 
while Authenticator apps don't require any connection, and SMS can work on an SMS-only phone plane 
(or in poor signal areas).

再相对的这个也有缺点：
* 标准还没统一，这就意味着汝没得选替代应用，也没法让多个账户使用一个应用。
* 需要汝的手机有网络连接(lll￢ω￢)，然而 SMS 可以在没有手机网络连接的地方工作（验证器的应用连网络都不要）……

FIDO U2F / Security Keys （FIDO 通用双重因素 / 安全密钥）
------------------------------------------------------------------

Universal Second Factor (U2F) is a relatively new style of 2FA,
typically using small USB, NFC or Bluetooth Low Energy (BTLE) devices often called "security keys."
To set it up on a site, you register your U2F device. On subsequent logins, 
the site will prompt you to connect your device and tap it to allow the login.

通用双重因素（U2F）是一种新兴的双因素验证方法。常见的是 USB，NFC 或者蓝牙低功耗设备。（有时它们也被叫做“安全密钥）
汝在网站上注册了设备以后，下次登录时汝会被要求连接汝的设备然后按下上面的按钮来允许登录。

Like push-based 2FA, this means you don't have to type any codes. Under the hood, the U2F device recognizes the site you are on and responds with a code (a signed challenge) that is specific to that site. This means that U2F has a very important advantage over the other 2FA methods: It is actually phishing-proof, because the browser includes the site name when talking to the U2F device, and the U2F device won't respond to sites it hasn't been registered to. U2F is also well-designed from a privacy perspective: You can use the same U2F device on multiple sites, but you have a different identity with each site, so they can't use a single unique device identity for tracking.

和基于推送通知的双因素认证一样，汝不用多输入些啥。在幕后，汝的 U2F 设备记得汝注册的网站并与它交涉（？）。
这就是 U2F 的一个显著优点，它能更有效的防御钓鱼攻击（因为它不认识钓鱼网站，因此拒绝沟通(lll￢ω￢)）。
U2F 也可以做到保护隐私：汝可以一个 U2F 设备打天下，但是需要的话也可以买好几个各司其职 *★,°*:.☆(￣▽￣):*.°★* 。

The main downsides of U2F are browser support, mobile support, and cost. 
Right now only Chrome supports U2F, though Firefox is working on an implementation. 
The W3C is working on further standardizing the U2F protocol for the web, 
which should lead to further adoption. Additionally, mobile support is challenging, because most U2F devices use USB.

摆在 U2F 面前的主要障碍就是浏览器和移动设备的支持以及花费。
目前只有 Chrome 支持 U2F （不过 Firefox 也在努力赶上啦）。
另外 W3C 也在加紧标准化 U2F 协议咯~ 
不过手机上好像还是很艰难的 o(><；)oo（因为大多数都是 USB 设备？）

There are a handful of U2F devices that work with mobile phones over NFC and BTLE. 
NFC is supported only on Android. On iOS, 
Apple does not currently allow apps to interact with the NFC hardware, 
which prevents effective use of NFC U2F. 
BTLE is much less desirable because a BTLE U2F device requires a battery, 
and the pairing experience is less intuitive that tapping an NFC device.
However, poor mobile support doesn't mean that using U2F prevents you from logging in on mobile. 
Most sites that support U2F also support TOTP and backup codes. 
You can log in once on your mobile device using one of those options, 
while using your phishing-proof U2F device for logins on the desktop. 
This is particularly effective for mobile sites and apps that only require you to log in once, 
and keep you logged in.

有些 U2F 设备通过 NFC 或者 蓝牙低功耗一类的技术支持移动设备（但是 Apple 一直不给应用用 NFC 所以……），
不过这并不能阻止咱们用 U2F （笑），因为提供 U2F 登录的网站大多数也会同时提供 TOTP 或者备份代码验证。
而且大多数移动应用只需要登陆一次就可以在手机上记住登录状态，
所以汝大可以在手机上用传统的方式登录，而在电脑上照插不误（(lll￢ω￢)）。

Lastly, most other 2FA methods are free, assuming you already have a smartphone. Most U2F devices cost money. Brad Hill has put together a review of various U2F devices, which generally cost USD $10-$20. GitHub has written a free, software-based U2F authenticator for macOS, but using this as your only U2F device would mean that losing your laptop could result in losing access to your account.

最后，如果假设汝有一部智能手机的话，其它的双因素验证方法可以说没有额外开销。
但是 U2F 要稍微的投资一下（大概在 10-20 $ 之间？），
`Brad Hill 有一篇评测不同 U2F 设备的文章值得一读 <https://github.com/hillbrad/U2FReviews/blob/master/README.md>`_ 。
如果汝在使用 MacOS 的话，
可以考虑试试 `GitHub 编写的软 U2F 应用 <https://github.com/github/SoftU2F>`_ 工具，
不过还是不要把鸡蛋放在一个篮子里就是啦~


Bonus: Backup Codes （追加提示：备份代码 / 恢复代码）
--------------------------------------------------------

Sites will often give you a set of ten backup codes to print out 
and use in case your phone is dead or you lose your security key. 
Hard-copy backup codes are also useful when traveling, 
or in other situations where your phone may not have signal or reliable charging. 
No matter which 2FA method you decide is right for you,
it's a good idea to keep these backup codes in a safe place to make 
sure you don't get locked out of your account when you need them.

在完成双因素验证设置时网站会提供一些备份代码给汝呗，一般是十个一组。
如果汝的手机或者安全密钥出现了一些情况（例如坏了，没有信号，或者丢了 😂）
或者没这么坏（例如只是汝出门旅行手机没电 or 没信号时……），就到了这些备份代码发挥自己的
价值的时候啦（雾）。
不过不管汝选择的是哪一种双因素验证方式，务必像对待汝的密码一样对待这些备份代码啦~
把它们放到一个只有汝知道又方便取用的地方吧。
（期望不要用到就是了）

----

    翻译自 `电子前哨基金会 <https://www.eff.org>`_ 的
    `A Guide to Common Types of Two-Factor Authentication on the Web <https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web>`_ ,
    以 `知识共享 署名 3.0 US 协议 <https://creativecommons.org/licenses/by/3.0/us/>`_ 授权。

    电子前哨基金会是一个维护互联网上的公民自由的民权组织，汝可以通过 `捐款 <https://supporters.eff.org/donate>`_,
    `成为会员 <https://supporters.eff.org/join>`_ ， 或者 `更多其它途径 <https://www.eff.org/helpout>`_ 支持他们的工作咯~