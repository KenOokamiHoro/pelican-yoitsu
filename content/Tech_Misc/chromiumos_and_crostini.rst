Chromium OS 和 Crostini 初体验
====================================================

:slug: chromiumos_and_crostini
:lang: zh
:date: 2020-05-23
:desc: 因为买不到 Chromebook ？

.. PELICAN_BEGIN_SUMMARY

:del:`日常动机不明瞎折腾……`

.. PELICAN_END_SUMMARY

.. contents::

所以 Chromium OS 是啥？
----------------------------------------------------------------------

    Chromium OS是Google Chrome OS的开放源代码开发版本。
    自2009年11月19日开始，Chrome OS以Chromium OS为名陆续发布其开发源代码，
    并在遵守着BSD授权条款不断有新版本发布，并试图能够提供绝大多数长时间浏览
    万维网的用户一个快速、方便且安全的操作系统。

    历史上Chromium OS曾经整体是创建在以Linux核心为主的Ubuntu
    4.10版本上，而操作系统的的软件包管理系统则是使用Gentoo Linux的Portage。
    但是现在实际上只是单纯利用了Gentoo Linux 的 Portage 而独立编译出来的特制化 
    GNU/Linux 操作系统，而这个系统本身也与 Gentoo Linux 无关。

    `Wikipedia: Chromium OS <https://zh.wikipedia.org/zh-cn/Chromium_OS>`_

:del:`愿意的话当作一个核心是 Chromium 浏览器的操作系统也行……`

.. image:: /images/chromiumos/demo.png
   :alt: Chromium OS 大概像这个样子

于是 Chromium OS 大概像这个样子（嗯？）

先有装到硬盘上的 Chromium OS 
----------------------------------------------------------------------

要安装 Chromium OS 的话，
`可以参照官方的说明自己编译 <https://www.chromium.org/chromium-os/build>`_ 。
或者也可以捡现成的，例如 https://chromium.arnoldthebat.co.uk/index.php

下载来是一个 raw img 映像，用 Rufus / Etcher 甚至 dd 啥的找个 U 盘写进去，
再从 U 盘启动电脑就能试用啦……

:del:`如果汝和咱一样是捡现成的然后还` 想要安装的话，按下 Ctrl + Alt + F2 进入 Developer shell，以 chronos 用户登录。
运行下面的命令就好啦（然而这会把汝选择的硬盘上的所有内容抹掉，小心操作）

    sudo /usr/sbin/chromeos-install --dst /dev/sda

当汝看到 "Please shutdown, remove the USB device, cross your fingers, and reboot."
这一句时就表示安装成功啦，可以重新启动然后拔掉 U 盘啦……

企鹅开采铬铁矿 - Chromium OS 中的 GNU/Linux 容器
----------------------------------------------------------------------

Crostini 是 Google的总括术语，用于使 GNU/Linux 应用程序支持易于使用，
并与Chrome操作系统很好地集成。

`Google 官方的配置指南在这里。 <https://support.google.com/chromebook/answer/9145439>`_ 

要启用 Linux （Beta 版）的话，首先要从设置里把这个选项打开，启动的时候
会下载一定量的文件。

.. image:: /images/chromiumos/crostini_init.png
   :alt: 启动 Chromium OS 中的 Crostini

配置完成之后起动器中会增加一个终端应用，日后安装的有 Desktop Entry 的 GNU/Linux 应用
会自动添加到那个 “Linux 应用” 文件夹里。

.. image:: /images/chromiumos/crostini_launcher.png
   :alt: 启动 Chromium OS 中的 Crostini

刚刚启动的地方也能共享文件夹和 USB 设备给容器：

.. image:: /images/chromiumos/crostini_settings.png
   :alt: 容器设置

不过当然缺少的功能还是有的
（ `依官方的说法的话 <https://chromium.googlesource.com/chromiumos/docs/+/master/containers_and_vms.md#Missing-Features>`_ ）：

* 除了 USB 以外的外部设备
* 图形加速
* 硬件加速解码
* 和输入法（虽然能通过某些奇怪的技巧绕过）

因为咱把容器换成 Arch 啦，于是……

在默认的 Debian 容器上安装中文输入法的话可以
`参考这篇文章 <https://noob.tw/chromeos-vscode/>`_ 

替换 Crostini 的容器
----------------------------------------------------------------------
因为 Crostini 其实就是一个定制的带 LXC/LXD 的容器（大概吧，要是咱说
错了的话麻烦给咱一个改正的机会……），要把默认的 Debian 容器换掉也是比较容易的（嗯？）

想换成 Fedora 的话 `可以参考这篇文章。 <https://asaba.sakuragawa.moe/2019/06/%E5%9C%9F%E8%A3%BD%E9%9B%99%E7%B3%BB%E7%B5%B1-chromebook-%E4%BD%BF%E7%94%A8%E9%AB%94%E9%A9%97%E8%88%87%E8%B8%A9%E5%9D%91/>`_ 

想和咱一样换成 Arch 的话可以 `参考 ArchWiki 的那篇文章。 <https://wiki.archlinux.org/index.php/Chrome_OS_devices/Crostini>`_
具体的操作大致如下：

按 Ctrl + Alt + T 打开 Crosh （Chromium OS 在 Chromium 里的 shell），
然后：

    vmc container termina arch https://us.images.linuxcontainers.org archlinux/current

这个命令运行完后肯定会因为某个错误失败：

.. code-block:: text

    Error: routine at frontends/vmc.rs:403 `container_setup_user(vm_name,
    user_id_hash,container_name,username)` failed: timeout while waiting 
    for signal
    
不要方（慌？）。在刚才的 crosh 里打开 Crostini 的容器里的 Shell ：

.. code-block:: bash
    
    crosh> vsh termina
    (termina) chronos@localhost ~ $ 

列出容器列表（可能需要几分钟才能看到刚建立的容器）

    lxc list

启动新建立的 Arch Linux 容器，以及在容器里打开一个 Shell：

    lxc start arch

    lxc exec arch -- bash

在 Arch Linux 容器的 Shell 里需要完成这些操作：

* 创建用户（UID 1000 那位），以及修改 sudoers 配置（记得用 visudo）。

* 为了和 Chroium OS 的集成，安装 wayland xorg-server-xwayland 包，以及从
  AUR 安装  cros-container-guest-tools-git 。记得启动相应的服务。

.. code-block:: bash

  $ systemctl --user enable --now sommelier@0 sommelier-x@0 sommelier@1 sommelier-x@1

* 可以的话装些别的喜欢的，像是中文字体或者编译工具什么的。（就像新装一个 Arch 一样啦）

最后在 Termina 的 Shell 里把新建的容器的名称改回 penguin (lxc rename arch penguin)，
再重新启动容器。应该就能在 Chromium OS 起动器的终端里启动 Arch Linux 容器啦。

-----------------------------------------------
利用奇怪的技巧增加输入法支持（fcitx 啦）
-----------------------------------------------

首先把 fcitx 、相应的输入法模块（偷懒的话就 fcitx-im)和输入法（例如 fcitx-rime)
装上。然后用配置工具（例如 fcitx-configtool）或者编辑配置文件的方法设置好输入方案。
（以及记得换个组合键，原来的 Ctrl + Space 已经被 Chromium 的输入法切换抢了狸……）

然后编辑 /etc/systemd/user/cros-garcon.service.d/cros-garcon-override.conf 
文件，加上相应的环境变量：

.. code-block:: text

    Environment="GTK_IM_MODULE=fcitx"
    Environment="QT_IM_MODULE=fcitx"
    Environment="XMODIFIERS=@im=fcitx"

编辑 ~/.sommelierrc ，让容器在启动时运行 fcitx：

.. code-block:: text

    /usr/bin/fcitx-autostart

然后重新启动一下容器，是不是能在 Linux 应用里输入中文啦？

.. image:: /images/chromiumos/crostini_ime.png
   :alt: 就像这样

但是不知道为啥 Telegram Desktop 还是不吃这一套（emmmm）

-----------------------------------------------
从 Chromium 访问容器的服务
-----------------------------------------------
如果容器里的服务监听了 0.0.0.0 的话，在 Chromium 里可以通过
访问 penguin.linux.test 访问到。

.. image:: /images/chromiumos/crostini_remote.png
   :alt: 就像这样

没错这篇文章就是在这台 ”Chromebook“ 上写出来的……

蓝色洗彩色（大雾） - 为 Chromium OS 加入 Chrome OS 功能
----------------------------------------------------------------------

和 Chromium OS 相比，Chrome OS 有 Google Play 支持，可以运行 Play 商店上的应用。
以及可以使用 Google 账户连接 Android 设备完成一些像是通过手机收发短信或者 Chromebook 
打开手机热点之类的操作。

汝要是来了兴趣的话，
:del:`那直接去买台 Chromebook 啊……`  （笑）

如果只是想拿来尝试一下的话，有个名字叫做
`Chromefy (也叫 Project Croissant) <https://github.com/imperador/chromefy>`_ 
的项目可以把 Chrome OS 的组件塞进一个 Chromium OS 映像里面。具体的操作方法去看他们的
README 啊 ……

咱是试过一次之后发现运行 Android 应用时 Core m5 不太能 Hold 的住就回去 Chromium OS
啦。
