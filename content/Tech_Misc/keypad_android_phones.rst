带键盘的 Android 手机和多亲 F21 Pro 折腾记
====================================================

:slug: keypad_android_phones
:lang: zh
:date: 2021-10-28
:tags: Android
:desc: 因为人老了就喜欢怀旧？
:issueid: 

.. PELICAN_BEGIN_SUMMARY

:del:`因为人（？）老了就喜欢怀旧？`  😂

.. PELICAN_END_SUMMARY

对咱这个高不成低不就不知道算不算老人的家伙来说呢，能够拿来怀旧的应该会有那些“古老”的
手机游戏了。（也没有多么老啦，就是差不多十年前那些 Java 手机游戏了，好像叫啥 J2ME？
算了不管了后面就都叫 Java 了，希望大家不会和现在还在的那个 Java 搞混。）

虽然现在也有像是 `J2ME-Loader <https://github.com/nikita36078/J2ME-Loader>`_ 
这种 Android 上也可以用的 Java 模拟器了，但是在触摸屏上按虚拟按键完全没有感觉嘛……
所以咱也开始物色有键盘的 Android 手机了。

    好啦咱知道去收购一些老的手机也是可行的解决方案，那下面的内容就完全没有必要写了。
    :del:`是不是咱可以鸽了 （x）`

举几个键盘 Android 手机的栗子
-------------------------------------------------------------

虽然 Android 的操作更多的是以触屏为主，但是也不是没有有物理键盘的机型啦。
比如摩托罗拉里程碑之类的，但是那个好像太古老了点……

最近几年最能拿出来举的例子大概就是黑莓了，例如最后一部亲自操刀的 Priv 和后面授权 TCL 
设计生产的 KeyOne / Key2 。:del:`不过 TCL 也在 2020 年撒手不干了就是...` 

除了黑莓以外，也有几家公司设计过带物理键盘的 Android 手机，例如
`Unhertz Titan <https://www.unihertz.com/products/titan>`_ 和 
`F(x)tec <https://www.fxtec.com/pro1x>`_ 。

不过上面这些还是有一个微妙的问题，它们都是传统的 QWERTY 键盘布局，所以按键普遍的都比较小。
方向键也都在一些奇妙的地方或者干脆就没了。虽然大部分 Java 手机游戏都能用数字键代替方向键来着，
等等，这些手机上的数字键基本也要靠 Fn 之类的组合键触发出来啊……

以及它们的性价比也不怎么漂亮。（家境贫寒.jpg）

后来上网瞎逛的时候就看到了标题说的那个
`多亲 F21 Pro <https://item.jd.com/100024191248.html#none>`_ 。
是常见的 12 键键盘布局，Helio A20 的性能虽然不强，但是满足咱这需求应该是够用的，
于是就买了一台 4G+64G 版本。

    在咱开始写这篇文章的时候，才发现还有一个 F21 Pro+，平台换成了紫光展锐 T310。
    同时可以选择学生版和标准版（标准版可以自由安装应用，学生版不行，F21 Pro 也不行），
    不过只有 3G+32G 版本可选，如果汝不想像后面那样如此麻烦的话，去买那个就行。
    3+32 应该也足够用了，除非汝往里面装了几万个游戏那样的（笑）。

借应用市场刀装第三方应用
-----------------------------------------------------------
既然这都拿“学生手机”作为卖点了嘛，那自然不能自己安装应用就是设计使然而不是 Bug 咯。
这个系统貌似是魔改了系统的软件包安装程序，如果包名不在白名单里就会提示
“禁止安装第三方应用，请去应用市场安装”。 多亲似乎也从 2 Pro 那时学来了不少的反制措施，
例如封掉了 adb 安装，提前占了设备管理员应用的位置之类的。

不过后来有人发现系统自带的应用市场其实是能安装其它的应用的，只要把正在下载中的安装包换成汝想要的软件就行了。
就算后来的系统更新封堵了一阵子，也又被发现只要把旧版的应用市场装上就能绕过了……

也有人写出了能半自动操作的工具了，例如 
`MlgmXyysd/k61v1injector <https://github.com/MlgmXyysd/k61v1injector>`_ 。
但是咱比较懒（上面那个依赖 PHP），所以就手动操作一把了。

* 在 F21 Pro 上启动开发者选项里的 USB 调试。和普通的 Android 手机一样的方法咱就不用再啰嗦了吧？
* 接上电脑，在手机上授权电脑，然后打开一个终端。
* 清除手机上应用市场的数据。（这是为了接下来偷懒方便（x））
* 打开手机的应用市场，下载一个软件。考虑到下载的时间因素，建议选一个稍微大一点的软件，
  或者限制一下手机的网速也是可行的。

然后在终端上输入（汝应该知道要把哪里换成汝自己要安装的 APK 文件的路径，对吧？）：

.. code-block:: bash

    # $() 可以把括号内的命令的结果替换到上层的输入里。
    # 例如这里的 adb shell ls /sdcard/Android/data/com.duoqin.promarket/files/Download/
    # 会列出这个目录（应用市场的下载目录）里的文件列表。
    # 如果汝刚刚是清除了应用数据再下载的话，这里就只有汝当前正在下载的应用了。
    # 于是瞒天过海大法发动（x）

    $ adb push /path/to/your/apk \
    /sdcard/Android/data/com.duoqin.promarket/files/Download/$(adb shell ls /sdcard/Android/data/com.duoqin.promarket/files/Download/)

不过这种方法唯一的缺陷就是，应用更新以后汝还是要这么来一遍，所以还是要研究一下怎么把这破烂限制解掉。
:del:`奈何都喜欢藏着掖着……`

.. image:: /images/Android/f21pro_aida64.png
   :alt: 安装第三方应用演示

效果大概就是这个样子。于是汝也可以把刚刚咱说的那个 Java 模拟器装上，再找来几个老游戏的 JAR。
就可以愉快的玩耍啦。

.. image:: /images/Android/f21pro_j2meloader.png
   :alt: f21pro_j2meloader

因为 F21 Pro 的屏幕分辨率是 480x640 ，正好是以前最常见的 240x320 的两倍。
所以在模拟器里设置好全屏显示，再隐藏掉虚拟键盘以后的感觉就和以前差不多了。
至于按键手感嘛……这个只能说因人而异了。

.. image:: /images/Android/f21pro_play.jpeg
   :alt: f21pro_play



其它的小吐槽
-----------------------------------------------------------

* 如今很多 App 都不会给键盘做优化了啊，还好 F21 Pro 有触摸屏，用起来不会像之前流行
  的 C558 那样麻烦。
* 以及该卡的国产“小而美”还是卡，刚装完能吃掉接近 1G 储存空间可还行……
* fastboot flashing unlock 对这个是可用的，但是这个手机没有音量键没法确认啊 😂

以及这里面手机的截图其实是截取的电脑用 scrcpy 连接到手机的画面，就这样吧。 