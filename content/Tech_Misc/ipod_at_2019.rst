iPod -- 在 2019 年
===========================================================

:slug: ipod_at_2019
:lang: zh
:date: 2019-07-19
:tags: Software,iPod,Rockbox 
:desc: 为什么汝还会在 2019 年使用一台 iPod ?

现在汝会用什么听音乐？装有 Soptify / Google Play Music / Apple Music
等有串流播放应用的手机应该是大多数。但是咱偏不想这么搞：

* “使用其他的例如手机这样的「智能设备」，容易分散做一件事情的注意力” 
  -- `Miracle Milk <https://zhuanlan.zhihu.com/p/71851932>`_

例如用手机听音乐的时候，通知会降低媒体音量，特别是群里灌水时那此起彼伏的
通知声一响，基本就听不见音乐了（雾）。所以有必要单独整个播放音乐的设备。

* 串流音乐服务都不太好用，而且还有地区墙（某些歌只有某些地方能听到）

* 手机的存储空间不够大，特别是现在无损（？）音乐越来越普及的时候。
  （手持 256G 闪存甚至更大容量手机的土豪请无视这一句）

那为什么是 iPod 呢？其实很大一部分原因是身边朋友首先推荐的是 iPod ……

在 9102 年买一台 iPod 
-------------------------------------------------------------

iPod 其实是 Apple 出品的一系列音乐播放器，大致有这么几个产品线：

（iPod Shuffle 5th Gen、iPod Nano 6th Gen、iPod Classic、iPod Touch）

* iPod Classic 系列
  （但只有第六代（也是 Classic 最后一代被称作 “iPod Classic”）
  ，使用 1.8 寸微型硬盘作为存储介质。

* iPod Mini 系列，比 iPod Classic 略小，使用 1 寸微型硬盘作为存储介质。
  不过很短命的只发表了两代就被 iPod Nano 取代了。

* iPod Nano 系列，比 iPod Mini 更小更薄，使用大小不等的闪存做为存储介质，后期型号
  还加入了触摸屏支持。

* iPod Shuffle 系列，和 Nano 系列的区别就是没有屏幕，而且容量都偏小。

以上系列均已停产（x），现在 Apple 还在销售的就是以 iOS 为基础的 iPod Touch 系列了……

咱是捡了一台二手的 iPod Video （iPod Classic 系列的第五代），至于为啥是这个待会儿会讲。

iTunes or Rockbox ？
-------------------------------------------------

既然是苹果家的设备， 通过 iTunes 同步就是基本的操作啦
（虽然 macOS Catalina 里 iTunes 消失了，原有的功能被 Finder 和新的若干个应用取代），
但是……

* iTunes 不支持很多常用的音乐文件格式，例如 FLAC / CUE 啥的，
  虽然能用一些工具转换但是还是好麻烦的样子……

* 通过 macOS 格式化的 iPod 不能在 Windows 上使用，反之亦然。

* ……

如果汝已经是 App$e 全家桶的受害者，或者 iTunes Store 里买了很多音乐的话，那倒是可以接着用。
:del:`但是咱不是，` 所以碰巧听说了一个名为 Rockbox 的固件。

    Rockbox is a free replacement firmware for digital music players. 

    -- https://www.rockbox.org/

:del:`虽然官网一副上个世纪的模样但其实还在更新……`

在 `Rockbox 支持的 Apple 设备中 <https://www.rockbox.org/manual.shtml>`_ ,iPod Video
大概是最新（？）的了（iPod Classic 的支持在 7/19 还是 unstable……）。

所以就是它啦（大嘘）

安装 Rockbox
-------------------------------------------------

其实挺简单的， Windows / macOS / GNU/Linux 上都有 
`Rockbox Utility <https://www.rockbox.org/download/>`_ ，
下载安装然后按着提示操作就 OK 😂 

    不过需要 Windows 格式的 iPod ，如果汝手上的 iPod 是
    macOS 格式的话需要用 Windows 的 iTunes 格式化一下，或者
    `参考 Rockbox Wiki <https://www.rockbox.org/wiki/IpodConversionToFAT32>`_
    手动格式化一下。

安装完之后通常会自动重启进入 Rockbox ，如果在 Hold 开关锁定的情况下开机
就会进入原来的系统。

一些魔改（？）思路
-------------------------------------------------

iPod Video 使用的是 ZIF 接口的微型硬盘（说到底还是个 HDD），
汝甚至能有机会听到磁头在 iPod 里旋转的声音（x

所以现在流行起来了给 iPod 改存储，常见的方案有两种：

* 改成 ZIF 接口的 SSD，如果是厚机（60-80G 版，或是自己换个厚壳）换上半高
  SSD 以后还有机会改大电池。就是这种接口的 SSD 不太好找（淘宝上貌似某国产品牌居多？），
  :del:`而且还贵（120G 的能卖到 500￥ 信不信？）`

* 通过 ZIF 转 CF 卡接口上 CF 卡，优点是便宜（如果用 microSD 卡转 CF 卡套说不定更便宜），
  而且据说比 SSD 省电？（然后翻了两次车的某狼表示 emmm……）

具体的操作方法呢搜索一下大概就能找到，如果汝也和咱一样是 iPod Video 用户的话可以
从这几篇文章开始：

* `iPod 5th Generation (Video) Hard Drive Replacement with a CF or SDHC/SDXC Memory Card
  <https://www.ifixit.com/Guide/iPod+5th+Generation+(Video)+Hard+Drive+Replacement+with+a+CF+or+SDHC-SDXC+Memory+Card/7492>`_ 

* `ipod video 5.5g 升级记 + how to build rockbox
  <https://medium.com/@moreless/ipod-video-5-5g-%E5%8D%87%E7%BA%A7%E8%AE%B0-how-to-build-rockbox-b94dfb34fe30>`_

At last ......
-------------------------------------------------

.. image:: /images/ipod.jpg
   :alt: My iPod

啊有人觉得 unifont 很难看？
`自己编译一个别的就好啦（？） <https://zhuanlan.zhihu.com/p/70392435>`_

----

感谢 `好耶实验室的 Miracle Milk <https://zhuanlan.zhihu.com/eatradish>`_ 的各种想法和在咱
买 iPod 到改机上提供的各种建议和帮助。

