火狐娘兼职 RSS 阅读器~
====================================================

:slug: firefox_rss_reader
:lang: zh
:date: 2016-04-28
:updated: 2019-04-20
:tags: software,rss,凑数
:desc: 火狐娘也可以兼职做 RSS 阅读器呐~
:issueid: 34

.. PELICAN_BEGIN_SUMMARY

:del:`火狐娘也可以兼职做 RSS 阅读器呐~`

Firefox 67 已经砍掉 RSS 的支持啦，所以这篇文章就没用了……

.. PELICAN_END_SUMMARY

.. contents::

啥？ 又来问啥是 RSS ??
---------------------------

如果连 RSS 是啥都不知道那这篇文章就不用看了 (╯>_<)╯ ┻━┻ 

    「自知之明者，谓之贤也。」
    

让火狐娘兼职 RSS 阅读器的好处和问题是啥咧?
--------------------------------------------------------

* 好处:又可以少开一个应用啦~ ( 不过对于用 Owncloud News 的咱来说好像没啥区别😂😂

* 问题:好像没有大多数 RSS 阅读器那样把所有的文章安排在一个流里的功能 _(:з」∠)_

咱们来开始呗~
------------------

首先打开汝的 Firefox 😂😂 , 在右边的长条菜单里找到那个孤零零 (?) 的 "定制" 选项.

.. image:: /images/Firefox_rss/1.png
   :alt: 定制在这~
   
在左边就可以看到"订阅"按钮啦,把它拉到工具栏或者菜单里呗~ :del:`(订阅:我在这快来拉我~)`

.. image:: /images/Firefox_rss/2.png
   :alt: 订阅:我在这快来拉我~

然后打开一个博客 ( 例如 百合仙子 :fref:`lilydjwg` 的 `https://blog.lilydjwg.me <https://blog.lilydjwg.me>`_ )

.. image:: /images/Firefox_rss/4.png
   :alt: 举一个栗子~

如果这个网站有 RSS 的话 ， RSS 那个图标就会亮起来，就像上面那样~

-------------------------

    那么这个具体是咋实现的呢？ 原理就在于这段 HTML 代码上~
    
    .. code-block:: html
    
        <link href="https://blog.lilydjwg.me/posts.rss" rel="alternate" title="Blog RSS" type="application/rss+xml" />
        
    这段链接到了一个 MIME 类型是 application/rss+xml 的文件呐~ 所以火狐娘看到这个就知道这个网站有 RSS 可以看了呢~
    
    除了 RSS 以外火狐娘还认得 :ruby:`Atom Syndication Format|Atom供稿格式` 的标准哟~ ( 虽然也是 RSS 阅读器们的必修课 ~(>_<~) ), 像这样:
    
    .. code-block:: html
    
        <link href="/feeds/all.atom.xml" type="application/atom+xml" rel="alternate" title="约伊兹的萌狼乡手札 Full Atom Feed" />
        
如果有时 RSS 源不止一个,就挑一个呗~ ( 比如第一个 )

.. image:: /images/Firefox_rss/5.png
   :alt: 有时 RSS 源不止一个......

然后汝就会打开一个这样的页面呗~ 其实还可以支持其它方式 ( 例如 Owncloud 新闻阅读器 ) 订阅收取点,不过这次咱就用火狐娘的固有技能 "实时书签" 呗~

.. image:: /images/Firefox_rss/6.png
   :alt: FireFox 解析的 RSS 页面~
   
--------

.. image:: /images/Firefox_rss/7.png
   :alt: 使用 "实时书签" 订阅收取点 
   
实时书签大概像这样,如果汝订阅的网站更新了的话文件夹和每篇文章旁边的 RSS 图标会亮起来的啦~
   
.. image:: /images/Firefox_rss/8.png
   :alt: 实时书签长这样~
   
当然如果汝习惯 RSS 那种文章流,把 RSS 页面加为书签呗~

.. image:: /images/Firefox_rss/9.png
   :alt: 把 RSS 页面加为书签呗~
   
   