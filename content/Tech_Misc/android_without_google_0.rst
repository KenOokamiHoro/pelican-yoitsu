离开 Google 的 Android 之路
===========================================================

:slug: android_without_google_0
:lang: zh
:date: 2019_04_20
:tags: Software,Android,microG 
:desc: 离开 Google 的 Android …… 会是什么样子的呢？
:issueid: 

好聚好散？
------------------------------------------------------------

要说现在流行的移动设备操作系统的话肯定有 Android 的一席之地，Google 在 AOSP 下
开放了 Android 的大部分源代码，吸引了众多硬件生产商和开发者。

如果汝是 Android 用户的话，有没有做过这些事情了呢？

* 获得手机的 Root 权限（从 Zergrush 到 SuperSU 再到 Magisk？）
* 安装第三方 Recovery 和 ROM（甚至自己移植流行的 ROM 到自己的手机上，从此
  踏上了一条不归路……）
* 各种方式修改系统（修改系统分区， Xposed 和 Magisk 等等）
* 刷写或自制定制内核。
* ……

然而时过境迁， Google 已经不是以前那个不做恶的 Google 了，
Stallman 也撰文声讨过： https://stallman.org/google.html

所以 Google 使了哪些坏？
-----------------------------------------------------------
罄竹难书啊（大嘘）

* Google 自己开发了一套私有的称作 Google Mobile Services 的软件包，包含了大多数
  的 Google 应用，也有迹象表示 Google 正在把越来越多 AOSP 里的东西塞进 GMS 里。
* Google 网站越来越依赖非自由的 JavaScript （虽然这大概是现在的趋势吧，唉……），
  甚至登录和注册都需要。
* 任何大体量的公司都容易被用于大规模监控， Google 也不例外。（人家也是要恰饭的不是？）
* `有新闻报道称 <https://www.theguardian.com/technology/2016/nov/17/google-suspends-customer-accounts-for-reselling-pixel-phones>`_ 
  Google 终止了转卖 Pixel 手机的用户的 Google 账户。（服务的使用条款能扩大到物理设备么）
* Google 愈发向审查妥协，例如 `不再支持域前置 <https://www.theverge.com/2018/5/1/17308508/amazon-web-services-signal-domain-fronting-ban-response>`_ ，
  `同意为部分政权提供审查支援 <http://www.theguardian.com/world/2016/jan/19/pakistans-youtube-ban-lifted-as-government-gets-say-over-content>`_
  等等。

所以是时候放弃 Google 了（雾）

    等等我们一直就没有用 Google 啊 -- 来自地球外的某处

开始之前？
-----------------------------------------------------------
开始之前的话，汝最好准备一部合适的 Android 手机，那么什么样叫做合适呢？

* 最好有流行的第三方 Android ROM 适配（例如 LineageOS 等等）

    * 能解锁 Bootloader 大概是这个的充分条件。
    * 因为第三方 ROM 一般都不含 Gapps，所以适合用作基底。
    * 所以内置 GMS 的 ROM （例如 Pixel Experience） 不在推荐范围内。
    * 特别的，虽然 /e/ 在做的事情和咱现在做的事情类似，但是有不少人不推荐，
      例如 https://ewwlo.xyz/evil 

* 至于哪些手机解锁 Bootloader 的难度，咱踩过的坑（？）有：

    * 早些年的不少手机是没有锁的，不在意性能的话可以考虑。（例如以前的小米和三星？）
    * 能通过一条命令（ `fastboot flashing unlock` ）解锁的设备
      非常值得推荐，例如 Nexus/Pixel 和 OnePlus。
    * 像 Moto / LG / HTC 之类的主流厂商会要求汝去它们的网站上
      提供一些信息生成一些解锁代码，有一点点麻烦，但又不是不能用~
    * 有些厂商解锁会失去一些功能（例如 SONY，解锁会使 TA 分区无法访问，典型
      的副作用就是相机退化），如果汝不是很需要那些功能的话再考虑。
    * 有些厂商虽然允许解锁，但解锁条件过于刁钻（没错说的就是现在的小米）。
      只有在汝能忍受那些恶心的条件的情况下才能考虑。
    * 有些厂商不给官方解锁，但奈何不了有些野路子（例如 HMD 的 Nokia），虽然
      算作能解锁，但因为不容易的缘故，大概没有多少人为它开发。
    * 不能解锁的厂商当然不要考虑啦。 :del:`（蓝绿海军警告，以上）`

在这篇和接下来的文章（如果有的话，咕咕咕）里咱就拿一台 OnePlus 3 作为演示机咯~ 

然后备份好所有的数据（这不是废话么）

microG - 自由的 Google Service Framework 实现
---------------------------------------------------------------------

    A free-as-in-freedom re-implementation of Google’s proprietary
    Android user space apps and libraries.

microG 是一组代替 Google 私有的 Android 程序和库的程序和库的合称。
像 Google 之类的服务大概很难一下完全放弃的， 那就慢慢来好了……

然而不少应用只认 Google 的 Google Play Service 的签名，所以咱们还需要摆弄一下系统
让它把 microG 的签名当作 Google 的。放在一起的话方法有很多：

* 安装内置了 microG 的 ROM，例如 `LineageOS for microG <https://lineage.microg.org/>`_ 
  和 `Bliss <https://blissroms.com/>`_ 。
* 修改系统欺骗签名，例如通过 Xposed ，可以参考 `MicroG安装与配置 | 蠢黑通行 <https://www.blackyau.cc/4.html>`_
* 也有人做出了 microG / F-Droid 和 欺骗内核签名等功能的整合包，例如 NanoMod：
  https://forum.xda-developers.com/apps/magisk/module-nanomod-5-0-20170405-microg-t3584928

在经历了各种方法把 microG 安装上以后，可以通过 microG 自己的
Self Check 确认 microG 的工作状态。

.. image:: /images/Android/microg_check.png
    :alt: MicroG 的自检

接下来像普通的方法一样添加 Google 账户啦，不过咱用的时候有注意到几个问题：

* 不能添加 G Suite 账户（？）
* 咱不打开 microG 里的 Google Device Registration 的话没法保存 Google 账户的信息。

:del:`又不是不能用*2`

如果汝哪一天下定决心完全告别 Google 的话，只要把这些东西卸载掉就 OK（大嘘）

Yalp Store - Google Play 商店的替代品
-----------------------------------------------------------

    Yalp Store lets you download apps from Google Play Store as apk files. 
    It can search for updates of installed apps and lets you search for other apps.

Yalp Store 可以直接下载 Play 商店上的应用，如果汝在里面登录了汝自己的 Google 账号，
可以下载汝购买的付费应用。

（虽然 Yalp Store 的开发者说这么干其实会违反 Google Play 商店的 TOS 因此有帐号被关闭
的风险，但是 Yalp Store 内置的 Google 帐号还存活，也没有听说过谁因为用了它被封号……）

可以在 F-Droid 上找到 Yalp Store ，或者 `去它们的 Github 上下载 <https://github.com/yeriomin/YalpStore/releases>`_ ，
Magisk 上有 Yalp Store 安装为系统应用的模组包。

F-Droid - 自由的 Android 应用程序仓库
-----------------------------------------------------------

    F-Droid is an installable catalogue of FOSS (Free and Open Source Software) 
    applications for the Android platform. The client makes it easy to browse,
    install, and keep track of updates on your device.

F-Droid 是一个应用目录应用，很像 Play 商店对不对？不过这里面收录的全是自由软件
（至少客户端是，虽然某些软件会有一些诸如会推广非自由服务的“反功能”）。
通过 F-Droid Priviliged Extension 可以做到自动安装应用等额外功能。

可以从这里下载 F-Droid ：https://f-droid.org/en/ ，
Magisk 仓库中有  F-Droid Priviliged Extension 模块，NanoMod 也提供了 F-Droid 模组。

Guardian Project - 保护手机用户的隐私和自由
------------------------------------------------------------

    While we think that a secure, privacy-enhanced mobile phone is a good thing
    for just about anybody going about their daily lives, we like to also consider the extreme cases,
    where this technology might change the course of someones life.

Guardian Project 是一群开发保护手机用户隐私应用的开发者，最出名的作品是
Orbot 和 Orfox ,将 Tor 带到了 Android 的世界中。

除了 Play 商店以外，汝也可以添加它们的 F-Droid 仓库： https://guardianproject.info/fdroid/ 

旅途愉快 :-)