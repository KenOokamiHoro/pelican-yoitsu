编译 OpenWrt 和加入 Alljoyn Framework 支持
====================================================

:slug: compile_openwrt_and_add_alljoyn_framwork
:lang: zh
:date: 2016-12-13
:tags: software,openwrt
:desc: 编译 OpenWrt 和加入 Alljoyn Framework 支持（纯粹是为了完成任务……）😂
:issueid: 54

.. PELICAN_BEGIN_SUMMARY

编译 OpenWrt 和加入 Alljoyn Framework 支持（纯粹是为了完成任务……）😂

.. PELICAN_END_SUMMARY

.. contents::

OpenWrt 和 Alljoyn 分别是啥？
---------------------------------------

    OpenWrt是适合于嵌入式设备的一个Linux发行版。

    相对原厂固件而言，OpenWrt不是一个单一、静态的固件，而是提供了一个可添加软件包的可写的文件系统。
    这使使用者可以自由的选择应用程序和配置，而不必受设备提供商的限制，并且可以使用一些适合某方面应用的软件包来定制你的设备。
    对于开发者来说，OpenWrt是一个框架，开发者不必麻烦的构建整个固件就能得到想要的应用程序；
    对于使用者来说，这意味着完全定制的能力，与以往不同的方式使用设备，OPKG包含超过3500个软件。
    默认使用LuCI作为web交互界面。

    `OpenWrt <https://openwrt.org>`_

    AllJoyn，由高通公司主导的高通创新中心（Qualcomm Innovation Center）所开发的开放原始码专案，
    主要用于近距离无线传输，透过Wifi或蓝牙技术，进行定位与点对点档案传输。

    后来成为了 Linux 基金会赞助的项目之一。

    `AllSeen Alliance <https://allseenalliance.org/>`_

第一步：准备编译环境
----------------------------------

OpenWrt 官方的文档在这： https://wiki.openwrt.org/doc/howto/buildroot.exigence

首先需要保证汝的硬盘上还剩下至少 4GiB 左右的剩余空间啦~

然后安装编译所需的工具，Arch 的话大概 :code:`base-devel` 组就够了吧 😂


    # pacman -S base-devel git

官方文档 “Table of known prerequisites and their corresponding packages”
一节列出了编译不同组件各自需要的软件包，
还是装上吧😂,用 :code:`--needed` 参数跳过已经安装的包。

    # pacman -S asciidoc bash bc binutils bzip2 fastjar flex git
    gcc util-linux gawk gtk2 intltool zlib make cdrkit ncurses patch
    perl-extutils-makemaker python2 rsync ruby sdcc unzip wget gettext
    libxslt zlib boost libusb bin86 sharutils jdk7-openjdk

除了编译 lilo 需要的 bcc <- 这个在 AUR： https://aur.archlinux.org/packages/bcc/

第二步：准备源代码
--------------------------------------

这个时候就要打开汝的终端啦😋

和其它项目类似，不同的 git 分支也是不同的版本呗~：

    git clone git://github.com/openwrt/openwrt.git

    （master 分支，也就是 openwrt 的 trunk 版本）

    git clone -b chaos_calmer git://github.com/openwrt/openwrt.git

    （openwrt 的 chaos_calmer (15.05) 版本）

    git clone -b barrier_breaker git://github.com/openwrt/openwrt.git

    （openwrt 的 barrier_breaker (14.09) 版本）

如果要像咱一样添加第三方软件（例如 Alljoyn）的话，用文字编辑器打开汝的 :code:`feeds.conf`
（有可能是 :code:`feeds.conf.default`，记得编辑完保存成 :code:`feeds.conf`），加上适当的feed：

（咱还是用 Alljoyn 做例子呐~）

    src-git alljoyn https://git.allseenalliance.org/gerrit/core/openwrt_feed;barrier_breaker

    （因为咱是 barrier_breaker 啦，官方还支持  Attitude Adjustment 和 12.09，像下面那样）

    src-git alljoyn https://git.allseenalliance.org/gerrit/core/openwrt_feed;attitude_adjustment

    （Attitude Adjustment）

    src-git alljoyn https://git.allseenalliance.org/gerrit/core/openwrt_feed;openwrt_12.09

    （official OpenWrt v12.09 tagged release）

记住按汝自己的版本选择一个添加进去就好😂

然后更新 feeds 列表：

    ./scripts/feeds update -a

    PS: 有拉黑过 Wosign 和 StartCom 证书的用户会在 clone Alljoyn 仓库时遇到证书错误，像这样😂：

        fatal: unable to access 'https://git.allseenalliance.org/gerrit/core/openwrt_feed/':
        SSL certificate problem: unable to get local issuer certificate

    ( Linux Foundation 竟然没用自己赞助的 Let's Encrypt 😹)

    所以只好委曲求全先让 git 对证书错误睁一只眼闭一只眼吧 😿:

        export GIT_SSL_NO_VERIFY=true

        ./scripts/feeds update -a

        unset GIT_SSL_NO_VERIFY

然后安装需要的包，安装以后稍后的 :code:`make menuconfig` 中就可以见到了 😂

    ./scripts/feeds install {package_name}

    要偷懒的话可以先全装上 😂

    ./scripts/feeds install -a

    和咱一样要装 Alljoyn 的就再这样 😂😂

    ./scripts/feeds install -a -p alljoyn

第三步：设置编译选项
-----------------------
（咱不知道 menuconfig 怎么说😂）

接下来运行 :code:`make menuconfig` 打开设置界面：

.. image:: /images/openwrt/menuconfig.png
   :alt: 大概像这个样子😂

⬆️大概像这个样子，自己编译过 Linux 内核的话应该不会陌生吧😂

然而选项太多所以还是需要自己选择 😂

* Target System 选择成汝路由器的平台，这个可以在 https://wiki.openwrt.org/toh/start 的每个型号的 TechData 中找到。

* Target Profile 选择汝路由器的型号，或者保留默认的 Default Profile (all drivers) 选项，不过这会生成所有设备的映像😂

    对于选择的选项：

        [\*] built-in ，表示这项功能内置在生成的映像中（小心映像空间爆炸装不上😂)

        [ ] excluded  ，就是不含啦😂

        <M> module  ，这项功能会在编译时生成将来能通过 opkg 安装的软件包呗~

        < > module capable （😂，这啥……）

    和咱一样要装 Alljoyn 的话,Alljoyn 在 Network 一节里。

编辑完成以后就用下边的 Save 选项保存呗~

    \*\*\* End of the configuration.

    \*\*\* Execute 'make' to start the build or try 'make help'.

应该会有这么两行😂

第四步：开始编译
-------------------------------

接下来就开始编译呗：

    make

或者后台编译：

    ionice -c 3 nice -n19 make -j 2

或者打开喋喋不休模式😂：

    make V=s

然后可以洗洗睡了（雾😹

--------------

:del:`转天早上醒来如果没出错的话就能在  <buildroot_dir>/bin （ <buildroot_dir> 换成汝 OpenWrt 目录的文件夹）`
:del:`看到生成的映像了呗~`
