当汝有个 DualShock 4 时……
====================================================

:slug: when_you_have_a_ds4
:lang: zh
:date: 2017-06-28
:tags: software,PS4
:desc: 当有 DualShock 4 的 friends 焦躁不安时（划掉
:issueid: 34

.. PELICAN_BEGIN_SUMMARY

:del:`当有 DualShock 4 的 friends 焦躁不安时……`

.. PELICAN_END_SUMMARY

.. contents::

啥是 DualShock 4 ？
-----------------------------

就是 PlayStation 4 的控制器啦 😂

    https://asia.playstation.com/chs-hk/accessories/dualshock4/

    ⇪网页标题: 接口设备 | DUALSHOCK 4 | PlayStation

    关于 DualShock 4 的介绍在这里 😂

玩 Steam 的 friends ？
-------------------------------

首先打开 Steam 的大屏幕模式啦~

.. image:: /images/DS4_Arch/0.png
   :alt: 大屏幕模式大概位于用户名的旁边。
   
然后把控制器设置中的 “PlayStation 4 控制器支持” 打开。

.. image:: /images/DS4_Arch/1.png
   :alt: 启动 Steam 的 PlayStation 4 控制器支持

有那个心情的话可以去调整一下偏好设置（

.. image:: /images/DS4_Arch/2.png
   :alt: 某些偏好设置

支持控制器的游戏会有一个控制器图标 😂

.. image:: /images/DS4_Arch/3.png
   :alt: 支持控制器的游戏会有一个控制器图标

需要的话就调整游戏对应的控制器设置

.. image:: /images/DS4_Arch/4.png
   :alt: 游戏对应的控制器设置的位置

但是咱不承接按键配置服务（如果有人送咱想设置的游戏的话咱倒是可以试试 😂

Windows ?
--------------------------

可以试试一个叫 `DS4Windows <http://ds4windows.com/>`_ 的小工具，
不管是下载还是安装都在那了 😂

Arch Linux ?
--------------------------  

    其它发行版应该也可以 😂

可以试试一个叫 `ds4drv <https://github.com/chrippa/ds4drv>`_ 的小工具，
Arch 的话可以从 AUR 里装（

接着在 ~/.config/ds4drv.conf 写个配置文件，可以看看
`官方的样例 <https://github.com/chrippa/ds4drv/blob/master/ds4drv.conf>`_
或者 `咱写的 <https://wiki.yoitsu.moe/wiki/Ds4drv_config>`_ 😂

写完了就启动一下试试？

----

    对咱又水了一篇 😂

