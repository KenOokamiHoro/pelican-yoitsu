和 Android 玩耍的那些日子
====================================================

:slug: playing_with_android
:lang: zh
:date: 2018-08-08
:tags: Android, root, 杂记
:desc: 也可以说是 root Android 的过程 ……

.. PELICAN_BEGIN_SUMMARY

也可以说是 root Android 的过程 ……

.. PELICAN_END_SUMMARY

最近 Android Pie 发布了，于是随便写写杂感 （雾）

.. contents::

Android 2.x ：蛮荒时代和 ZergRush
---------------------------------------------------
（记得那是咱自己的第一部 Android 手机， Motorola Defy XT535） 
拿来的时候还是 Android 2.3，当然是不能用现在的审美去看 Gingerbread
的界面啦……（不过 2.3 时期的 TouchWiz 是真的比 Motorola 那几乎没怎么改
的漂亮，虽然现在……）至于当时怎么想到去 root 了呢，动机大概是
想不起来了，大概只是为了移除某些系统应用而已 😂 。2.x 时期最著名的
大概就是 ZergRush 漏洞了，通吃大多数 2.2-2.3 版本……

后记：那 XT535 后来收到了 4.0.4 更新，然后又坚持了两年，最后掉了…… 

Android 3.x ：没用过
---------------------------------------------------
于是跳过 😂

Android 4.x：CyanogenMod，CWM，CF-Auto-Root，Superuser，SuperSU 和 Xposed
----------------------------------------------------------------------------------
和 XT535 同时入手的其实还有 Samsung Galaxy Tab2，当时那上面已经是 Android 
4.0.3 了。虽然三星魔改的很厉害，但是乍一看就和旁边的 2.3 不一样不是？ 😂

以及到了 4.0 时代， ZergRush 就不能用咯。当时咱还处于一种啥都不懂的状态
（当然现在也只是懂些皮毛而已啦），上网搜索到了一个叫做 CF-Auto-Root 的家伙。
（ https://autoroot.chainfire.eu/ ）

用过三星手机的应该都知道，人家是没有 fastboot 模式的，但是有个 Download 模式
（因为早些时候的 Download 模式上有个施工造型的 Android 机器人，所以有时有人
也把它叫做挖煤模式）。然后需要用到一个叫做 odin 的工具刷机。

.. image:: https://odindownload.com/images/odin-download.jpg
    :alt: 大概长这样

当时 root 手机的方法，基本上都是先把一个称作 Recovery 的东西放在手机上，
然后利用一个小 zip 包把 su 可执行文件放在对应的位置上，再安装一个应用来控制
root 权限就 OK 啦~

    其实汝的手机上是有一个 Recovery 的，重置或者系统更新的时候会需要。不过
    官方的 Recovery 通常功能不多而且会检查更新的签名，所以就需要刷入一个第三方
    的 Recovery 来实现更多功能。比较著名的第三方 Recovery 的话，以前有 ClockWorkMod
    （简称 CWM），现在有 Team Win Recovery Project（简称 TWRP）

而 CF-Auto-Root 把这些过程简化到一个供 odin 刷入的 tar 包中，于是咱当时就用它 root 了。

至于 Root 权限管理应用的话，当时比较流行的是 Superuser （忘了是谁开发的了）
和 SuperSU （Chainfire，也是 CF-Auto-Root 的作者，虽然已经是前开发者了）。两个当时都很优秀
（虽然现在都凉了）

然后有一次咱尝试系统加密玩脱了，系统进不去了，于是死马当活马医的装了个 CyanogenMod 进去，
打开了新世界的大门（雾）。

    以至于现在咱用不习惯任何非类原生 Android 界面了 😂

大概是 4.4 时期，有了个叫做 Xposed 的东西。大概是一个通用的系统框架，
可以实现上至系统界面调节下到应用权限控制等一系列千奇百怪的功能。咱当时也
装了不少的模块用了一阵子。

后记：那个 Galaxy Tab 2 一直坚持到 CyanogenMod 13.0 （ Android 6.0.1），性能是真的不行了，
于是就闲置在家里，然后屏幕外面的玻璃被压碎了……

Android 5.x - 6.0 时期：没啥特别的
-------------------------------------------------------

大概是 15 年的时候买了台 Galaxy A8 ，然后兴致勃勃的用以前的方法 root 的时候，
遇到了一个叫做 KNOX 的神奇大坑。

    4.3 以前的三星手机刷入自定义操作系统以后，启动时会显示一个感叹号三角形，
    Download 模式里也会记录刷入了自定义操作系统。不过当时可以很轻松的去掉
    （例如用 Triangle Away）。后来有了 KNOX 以后，修改操作系统的行为会导致
    KNOX WARRANTY VOID 变成 0x1 从而失去保修。以及某些功能将无法使用（例如
    My Knox 和后来的 S Health，Samsung Pay 等等），而且据说因为这是个硬件
    设计于是不可逆……

自此咱就再也没买过新的三星 Android 手机……

如果不考虑那些的话，还有一个问题，修改过系统分区的话，就不能收到厂商的系统更新了。
于是不动 /system 的 system-less root 方法应运而生，典型的方法是修改内核映像（boot.img）……

以及那时候自从 ART 代替 Dalvik 以后， Xposed 适配新版本的速度慢了下来。

Android 7.x - 8.x 时期：SafetyNet，Magisk ，LineageOS 和 Project Treble
-------------------------------------------------------------------------

7.0 一上来就是猛料，比如所谓的 SafetyNet ，反正咱现在也没搞清楚这是啥……
以及不知道是不是 7.0 的 Xposed 鸽了太久的原因， 有人写出了 Magisk。 和 Xposed
大概具有类似的功能，但是不动系统分区，于是接收 OTA 更新能稍微容易一点……
以及介于 Superuser 年久失修和 Chainfire 宣布不再参与 SuperSU 开发以后，
Magisk 还有一个 Root 权限管理程序。总之也有不少人用啦~

这个时期也出现了很多不必须 root 权限的系统管理工具，例如 Brevent 和 AppOps，
它们都是通过 adb 启动一个外部脚本来协助完成某些任务，给了不 root 或没法 root
手机的人一些选择。

以及那年商业化失败的 CyanogenMod 宣布关闭，不过过了一阵子以后又有一群有志之士
成立了 LineageOS 社区，目前正在活跃开发中。

Oreo 时有了 Project Treble 和 A/B 无缝系统更新，前者把 Android 系统的部件和 OEM
定制的部分分开，声称能使 OEM 适配最新版 Android 系统的速度提升。后者可以做到更新时可以
继续使用设备，而且不容易坏（笑）。

对于喜欢搞机的开发者们呢？看起来 Project Treble 使第三方 ROM 移植起来稍微容易了一些，
比如汝可以在 xda 上找到 Treble 兼容的 AOSP 和 LineageOS 的通用系统映像（GSI），然后
刷入进汝支持 Treble 的手机上，大多数的功能应该都能运作。 不过 A/B 无缝更新可能就是个
美丽的麻烦了，因为没有了单独的 Recovery 分区，所以给这种手机适配 TWRP 的进度似乎慢了下来。


Android P ？
------------------------------------------------------------------

咱现在用的是 Nokia 7 plus ，当时有官方的 Android P Beta 支持，于是咱就这么用了下去。
除了前几个 DP 每次都要全新安装以外其实海星 😂 。 Magisk 在 Android P 上也比较稳，原来
不少需要魔改的功能也或多或少的集成进系统里了。然而以前那个愉快的折腾的日子似乎回不来了呢……

PS： #when_I_buy_a_phone
-----------------------------------------------------------------

如果咱要换手机的话：

* 能解 Bootloader 锁的通常很快都会有 LineageOS 适配，优先考虑。

* 上面一条不满足的话，如果是类原生系统的体验也可以考虑。

* 最好不是异形屏幕（就是所谓的刘海啥的），屏幕比例最好是普通的 16:9/16:10 一类。

* 有物理键盘最好。

于是现在（这篇文章完成的时候）这么下来，貌似就不剩几个了吧 😂
