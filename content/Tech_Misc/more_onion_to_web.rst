切点洋葱 - 搭建一个洋葱服务
===========================================================

:slug: more_onion_to_web
:lang: zh
:date: 2020-05-04
:desc: 为汝的 Web 服务多一层匿名网络

:del:`“如果你愿意一层一层剥开我的心……”`

所以洋葱服务（Onion Service）是啥？
---------------------------------------------------------------

    洋葱服务 (Onion Service ，旧名为“隐身服务/隐藏服务/ Hidden Service”) 
    是一种只能透过洋葱路由网络访问的网络服务 (例如网站)。

那么有人为啥要使用洋葱服务呢？
---------------------------------------------------------------

* 洋葱服务在 Tor 网络中传递，因此可以隐藏服务器的真实地址（例如 IP 地址和地理位置等等）。
* Tor 用户与洋葱服务之间所有的流量都是端到端加密的，所以汝不必担心像没有 HTTPS 这样的问题。
* 洋葱服务的网址是为自动生成，因此网站的架设者或管理员无需另行购买网络域名。其网址皆是以 .onion 结尾的，
  这样的设计可以让洋葱路由系统确保所有网络连接都通往正确的站点，并且其连接数据未被窜改。

访问一个洋葱服务网站时，除了一般 Tor 网络中会经过的节点以外，还会额外通过若干个中继。
（图上的是 DuckDuckGo 提供的洋葱服务 https://3g2upl4pq6kufc4m.onion/ ）

.. image:: /images/anonymous_networking/onion_service_example.png
   :alt: 访问一个洋葱服务网站时

我需要搭建一个洋葱服务么？
---------------------------------------------------------------

如果汝遇到了这些情况，那汝大概会对如何搭建一个洋葱服务引起兴趣（大概吧……）：

* 匿名、不被截获和篡改的传输一个文件。（正好有 OnionShare 可以帮忙这么做）。
* 在没有公共 IP 地址（甚至没有域名）的情况下提供 Web 服务。
* 匿名的和另一位匿名者交流。
* 汝也重视汝的读者的隐私权。 :del:`（要是汝不是作者怎么办？）`
* 或者别的？ （详见：`Tor at the Heart <https://blog.torproject.org/category/tags/heart-internet-freedom>`_ ）

切洋葱之前当然要做好保护措施啊……
---------------------------------------------------------------

于是亦然（？），要搭建一个洋葱服务的话，首先要有一个能够正常连接的 Tor 。

介于应该没人会一直开着个浏览器（？），所以最好是有个独立的 Tor 在运行（就是那啥所谓的 Expert Bundle 啦……）。

以及现在用 GNU/Linux 提供 Web 服务的家伙应该不少吧，于是汝大概能从汝所使用的 GNU/Linux 发行版的
软件仓库中找到 tor：

* Debian 系的话，可以参考官方的文档添加官方的仓库 ： https://www.torproject.org/docs/debian.html.en

* Fedora / CentOS 的话，可以从 `EPEL <https://fedoraproject.org/wiki/EPEL>`_ 仓库中安装 tor ：

* Arch 、Gentoo 或者 LFS ？ :del:`汝都会用这些了那不用咱教怎么装了吧……`

* 以及看情况修改 torrc 文件， `还是记得看文档 <https://2019.www.torproject.org/docs/tor-manual.html.en>`_

准备一个 Web 服务
---------------------------------------------------------------

这个就看人下菜了（？），如果有只用洋葱服务访问的需要的话，可以调整 Web 服务器的配置文件，
让她只接受本地地址的连接。 例如 Nginx 和 Apache 的话，大概可以这么做：

    listen localhost:8000;

:del:`不放心的话还可以用防火墙啊（x）`

生成一个洋葱服务地址
---------------------------------------------------------------
洋葱服务使用的 .onion 地址是随机产生的，地址是在配置洋葱服务时用新生成的公钥计算而成的。
所以叫做“生成”而不是注册，以及虽然有方法可以提前算出来生成特定地址的公钥但是不建议尝试……

用汝最顺手的编辑器和 root 权限（？）打开 Tor 的配置文件（通常是 /etc/tor/torrc ），
然后加上这些：

.. code-block:: bash
    
    # 每个不同的洋葱服务都需要一个独立的文件夹来存储其所需要的私钥和主机名，
    # 要创建不同的洋葱服务时，需要修改 HiddenServiceDir 所指向的文件夹。
    #（注：不要手动创建 HiddenServiceDir 的路径，否则会出现权限的错误而无法启动。
    # 在配置完成后启动 Tor 后，程序会自动生成权限合适的对应文件夹。）

    HiddenServiceDir /var/lib/tor/path_to_your_service

    # HiddenServicePort {汝希望汝的洋葱服务开在哪个端口上} {汝这个端口的洋葱服务交给谁处理}

    HiddenServicePort 80 127.0.0.1:8080

保存完毕后重新启动 Tor ，如果一切 OK 的话，可以在汝设置的 HiddenServiceDir 中看到一个
名为 hostname 的文件，里面就是汝新鲜的洋葱服务的域名啦~

同时提供普通网络服务和洋葱服务时？
---------------------------------------------------------------

* 可以考虑让来自 Tor 的出口 IP 地址的连接自动重定向至洋葱服务地址，例如
  `这样 <https://github.com/placeholdr/nginx-tor-redirect>`_
  
* 也有可能需要调整汝的应用（或者主题什么别的）来适应洋葱服务和 Tor 浏览器。

更进一步？读读看这些文章
---------------------------------------------------------------

    当然都只有英文版（汗）

* `Tor 洋葱服务最佳实践 <https://riseup.net/en/security/network-security/tor/onionservices-best-practices>`_
  ，来自 Riseup 的洋葱服务运行提示。

* `为 Tor 设置洋葱服务 <https://2019.www.torproject.org/docs/tor-onion-service.html.en>`_ 
  ，Tor Project 官方的部署指南，第三部分以后提供了一些高级话题。

* `洋葱服务的 Vanguard 插件 <https://blog.torproject.org/announcing-vanguards-add-onion-services>`_ 
  ，Tor Project 提供的保护洋葱服务的插件。

:del:`你会鼻酸，你会流泪……`
