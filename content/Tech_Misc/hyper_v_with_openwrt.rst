Hyper-V 和 OpenWrt 搭建软路由
====================================================

:slug: hyper_v_with_openwrt
:lang: zh
:date: 2021-03-15
:tags: Virtualization, Hyper-V, Windows
:desc: 通过 Hyper-V 和 OpenWrt 为电脑搭建一个小型子网。

笔记本上虚拟机越开越多，于是怎么给它们划分网络就是一个问题，例如代理软件什么的。

因为咱不想每一个虚拟机上就单独设置一遍，就想起来了咱给咱手上不支持代理设置的设备
（例如游戏主机什么的）连的那个运行有代理软件的路由器了。然后发现已经有人尝试过了，那就开工咯。

    介于随便汝等说咱是更想授之以渔还是就是在偷懒都好的想法，咱不会给出事无巨细的操作方法。
    当然如果汝也想跟着做的话，遇到问题时也可以来问咱（或者上网搜索一番）。 
    
    以及基于 Hyper-V 的特殊性，启用完以后可能会导致老的虚拟机软件无法启动。
    嘛最新的 VMware Workstation 和 VirtualBox 应该是已经支持了的。

准备工作 - Hyper-V 管理器、映像和创建虚拟机
----------------------------------------------------------------------

要使用 Hyper-V 的话，汝得有一个 Windows 8.1/10 专业版或者企业版，
或者 Windows Server 2012 以后的 Windows Server 才行。同时也要求汝的 CPU 支持必要的虚拟化技术。
（Intel VT-x 或者 AMD-V 什么的）可以通过 PowerShell 里的 systeminfo 命令确定是否可以开启 Hyper-V 。

在满足了前置要求之后，可以这样启用 Hyper-V：

.. code-block:: powershell

    # 如果提示找不到命令的话，换以管理员身份运行的 PowerShell 窗口。
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All

或者用“启用或关闭 Windows 功能”也是可以的。

下载和转换 OpenWrt 映像
----------------------------------------------------------------------

接下来到 OpenWrt 的网站上下载映像
（这时候最新的版本是 19.07，于是链接就是 https://downloads.openwrt.org/releases/19.07.7/targets/x86/64/ ）

.. image:: /images/hyper_v_with_openwrt/openwrt_web.jpeg
   :alt: OpenWrt 下载页面的样子，这里咱们选择包含有内核和 rootfs 的 combined 映像。

这里咱们选择包含有内核和 rootfs 的 combined 那俩，
区别在于 squashfs 那个安装好以后系统分区像在普通路由器一样是只读的，可以实现像是升级或者复位等功能。
不过咱大概会用检查点来实现这些，那就直接用 ext4 的好了。下载和解压以后汝大概就可以得到一个像是
openwrt-19.07.7-x86-64-combined-ext4.img 这样的文件了。

但是 Hyper-V 只能用微软自己的 VHD 或者 VHDX 格式，所以还需要转换一下。
转换的方法有很多，咱这里给出咱的一个例子。

创建一个虚拟磁盘。这里用了 diskpart 命令。如果汝更偏好用磁盘管理操作的话，
可以参考 
`这篇文章 <https://docs.microsoft.com/zh-cn/windows-server/storage/disk-management/manage-virtual-hard-disks>`_ 

.. code-block:: text

    Microsoft DiskPart version 10.0.21332.1000
    Copyright (C) Microsoft Corporation.
    On computer: DESKTOP-H6MANBV

    # 创建一个虚拟磁盘文件
    # create vdisk file=<汝要存放虚拟磁盘文件的位置> maximum=<它的最大大小，以 MiB 为单位> type=expandable
    # 假如汝有充足的硬盘空间的话，可以把上面的 type=expandable 改为 type=fixed 创建一个固定大小的虚拟磁盘。
    # 可以提高一些虚拟磁盘的性能。当然动态扩展对 OpenWrt 来说也够用就是了……
    DISKPART> create vdisk file=c:\test.vhd maximum=8192 type=expandable
        100 percent completed
    DiskPart successfully created the virtual disk file.

    # 选择刚才创建的虚拟磁盘
    # select vdisk file=<汝要存放虚拟磁盘文件的位置>
    DISKPART> select vdisk file=c:\test.vhd
    DiskPart successfully selected the virtual disk file.

    # 挂载刚才选择的虚拟磁盘。
    DISKPART> attach vdisk
        100 percent completed
    DiskPart successfully attached the virtual disk file.

    # 初始化虚拟磁盘为 MBR 分区表。
    # OpenWrt 官方编译的版本不支持 UEFI 启动，所以就用 MBR 了。
    DISKPART> convert mbr
    DiskPart successfully converted the selected disk to MBR format.

    # 在下一步操作完成之后卸载虚拟磁盘
    DISKPART> detach vdisk
    DiskPart successfully detached the virtual disk file.

接下来把这个映像写入虚拟磁盘就 OK 啦。（以及咱发现 Rufus 能把虚拟磁盘识别出来）

.. image:: /images/hyper_v_with_openwrt/rufus_flash.png
   :alt: Rufus 能给虚拟磁盘写入 RAW 映像。

创建虚拟交换机和虚拟机
----------------------------------------------------------------------

有点像其它 Hypervisor 里的虚拟网络啦，虚拟交换机类型里的外部网络就相当于桥接，
内部就相当于仅主机网络。于是这里创建一个外部和一个内部交换机。

.. image:: /images/hyper_v_with_openwrt/hyper_v_virtual_switch.png
   :alt: Hyper-V 里的虚拟交换机

留意外部交换机这里的“允许管理操作系统共享此网络适配器”这个选项，把这个选项取消选择以后，
这块网卡就只能由虚拟机访问了。等全部设置妥当以后，汝就可以把这个选项关掉让主机从虚拟机联网了。

.. image:: /images/hyper_v_with_openwrt/hyper_v_generation.png
   :alt: Hyper-V 里的虚拟机代数，第一代是传统的 BIOS 引导

创建虚拟机的时候，因为刚才提起过的 OpenWrt 官方编译的版本不支持 UEFI 启动的问题，所以就用第一代了。

.. image:: /images/hyper_v_with_openwrt/hyper_v_network.png
   :alt: 为虚拟机连接新的虚拟交换机

以及新建虚拟机的时候只能添加一个网络适配器，那这里就先把上面新建的内部交换机加上好了。

.. image:: /images/hyper_v_with_openwrt/hyper_v_add_adapter.png
   :alt: 在虚拟机设置为虚拟机连接新的虚拟交换机

创建完成以后可以在虚拟机设置里添加新的网络适配器，虚拟硬盘就是刚刚转换好的磁盘映像（所以汝刚才卸载了没有？）。

在连接和启动虚拟机之后，汝大概就可以看到这样的界面：

.. image:: /images/hyper_v_with_openwrt/openwrt_initial.png
   :alt: OpenWrt x86 首次启动的界面

所以当然是先用 passwd 命令设置 root 用户的密码啦。

查看和调整网卡设定
----------------------------------------------------------------------

如果汝像咱刚才一样先添加的内部交换机， ip link 命令应该可以看到虚拟机从上游获得了 IP 地址，也可以正常的联网。

（为了节省空间，这里省略了获得的和本地 IPv6 地址）

.. code-block:: bash

    root@OpenWrt:~# ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
            valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
            valid_lft forever preferred_lft forever
    2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master br-lan state UP qlen 1000
        link/ether 00:15:5d:0a:a9:01 brd ff:ff:ff:ff:ff:ff
    3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP qlen 1000
        link/ether 00:15:5d:0a:a9:02 brd ff:ff:ff:ff:ff:ff
        inet 192.168.10.147/24 brd 192.168.10.255 scope global eth1
            valid_lft forever preferred_lft forever
    4: br-lan: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP qlen 1000
        link/ether 00:15:5d:0a:a9:01 brd ff:ff:ff:ff:ff:ff
        inet 192.168.10.1/24 brd 192.168.10.255 scope global br-lan
            valid_lft forever preferred_lft forever

当然如果没有成功也别担心，记下界面名称（eth0 这样的）下面 link/ether 后面的文字，
这就是汝这个虚拟网卡的 MAC 地址啦。

.. image:: /images/hyper_v_with_openwrt/hyper_v_query_mac_address.png
   :alt: 在网络适配器的高级功能选项里查看虚拟网卡的 MAC 地址

然后在虚拟机设置里找到对应的适配器，高级设置里可以看到这个适配器的 MAC 地址。

于是在咱这里，要连到上游的接口就是 eth1 了。接下来用 vim 打开 /etc/config/network 文件。

    它默认的里面只有 vim 让咱也很难办啊，如果汝对 VIM 的操作不是很熟练的话，记得准备一个操作指南在手边。

.. code-block:: conf

    config interface 'loopback'
            option ifname 'lo'
            option proto 'static'
            option ipaddr '127.0.0.1'
            option netmask '255.0.0.0'

    config globals 'globals'
            option ula_prefix 'fddb:38c5:4b0a::/48'

    config interface 'lan'
            option type 'bridge'
            # 把这里的 eth0 换成汝内部交换价连接的适配器上的接口
            option ifname 'eth0'
            option proto 'static'
            # 默认是 192.168.1.1，如果担心和自己网络上已有的端冲突的话也可以修改一下。
            option ipaddr '192.168.50.1'
            option netmask '255.255.255.0'
            option ip6assign '60'

    config interface 'wan'
            # 把这里的 eth1 换成汝外部交换价连接的适配器上的接口
            option ifname 'eth1'
            option proto 'dhcp'

    # 如果汝的上游网络支持 IPv6，那可以添上这一段试试？
    config interface 'wan6'
            option ifname 'eth1'
            option proto 'dhcpv6'
            option reqaddress 'try'
            option reqprefix 'auto'

保存完成以后可以重启网络服务，或者直接重启系统也行。一切顺利的话，
主机上的虚拟交换机应该能正确的获得 IP 地址：

.. code-block:: text

    Ethernet adapter vEthernet (Internal):

        Connection-specific DNS Suffix  . : lan
        IPv6 Address. . . . . . . . . . . : fd51:c6aa:1df:0:99e6:8496:55c7:733e
        IPv6 Address. . . . . . . . . . . : fddb:38c5:4b0a::a32
        IPv6 Address. . . . . . . . . . . : fddb:38c5:4b0a:0:99e6:8496:55c7:733e
        Temporary IPv6 Address. . . . . . : fd51:c6aa:1df:0:b04a:f9d5:61bd:63e4
        Temporary IPv6 Address. . . . . . : fddb:38c5:4b0a:0:8d12:c734:e2f0:8ec8
        Link-local IPv6 Address . . . . . : fe80::99e6:8496:55c7:733e%26
        IPv4 Address. . . . . . . . . . . : 192.168.50.145
        Subnet Mask . . . . . . . . . . . : 255.255.255.0
        Default Gateway . . . . . . . . . : fe80::215:5dff:fe0a:a901%26
                                            fe80::215:5dff:fe0a:a907%26
                                            192.168.50.1

接下来就像使用一个普通的有 OpenWrt 的路由器一样设置它吧，像是装上 luci 和代理软件什么的。

在所有的设置都妥当以后，除了上面提到的允许管理操作系统共享此网络适配器那个选项以外，
汝还可以考虑设置虚拟机开机启动。

（在“管理-自动启动操作”那里，可以设置成“总是”来让虚拟机在主机启动时自动启动。）