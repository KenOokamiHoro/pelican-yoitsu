用 Hyper-V 玩些小花样
====================================================

:slug: build_virtualization_stuffs_with_hyper_v
:lang: zh
:date: 2021-03-26
:tags: Virtualization, Hyper-V, Windows
:desc: 用 Hyper-V （可能还有些别的）玩些奇怪的操作 （嗯？）

从前面那篇 `Hyper-V 和 OpenWrt 搭建软路由 </tech_misc/hyper_v_with_openwrt.html>`_
开始咱就开始在 Windows 上用 Hyper-V 了。 :del:`然后场面逐渐失控……`

这篇文章就是把咱这几天遇到的各种问题和可能的解决方案记下来这个样子。

汝应该尝试 Hyper-V 吗？
-----------------------------------------------------
如果汝有像咱一样的某些需求的话，那 Hyper-V 可能也很适合汝。

* 想在 Windows 开机时自动启动某个虚拟机。 
  
    VMware Workstation 15 还可以用内置的共享虚拟机设置开机启动的，不过 16 开始弃用了。
    VirtualBox 文档里有提用服务设置开机启动虚拟机。 :del:`虽然咱一次都没成功过……`

* 有用另一台电脑访问这台电脑上虚拟机的需求。
* 在用 WSL 2，那其实汝已经算是启用了 Hyper-V 了嘛。
* （或者还有别的？）

不过汝也许会因为这些原因不想尝试。

* 旧版 VMware Workstation 或 VirtualBox 用户。 
  
    启用 Hyper-V 以后会导致旧版 VMware Workstation 或 VirtualBox 无法启动虚拟机。

    也有人觉得在开启 Hyper-V 的主机上开别的虚拟化软件时性能会下降什么的。

* 想在 Windows 上试用 GNU/Linux 桌面。
    
    Hyper-V 的 framebuffer 缓存（是这个意思吧？）只有 8M，于是开 GNU/Linux 桌面可能会很吃力。
    （增强会话是基于 RDP 的，嗯……）

* :del:`Windows 10 家庭版用户 （也没法开）` 

以及咱这几天下来对 Hyper-V 的槽点，大概就只有那个检查点了吧，
虽然有类似快照的“检查点”，但是不能让某个虚拟磁盘不在检查点里。（VMware 的独立虚拟磁盘或者 VirtualBox 的完全写入）

环境准备
-----------------------------------------------------
如果是 Windows 10 专业版（或者专业工作站版/企业版/教育版啥的），就参考前面那篇文章了。

不过咱这回换成 Windows Server 2019 了，所以从服务器管理器安装 Hyper-V 角色就可以了。

以及如果汝只需要 Hyper-V 这一个功能的话，还可以选择用
`Hyper-V Server <https://www.microsoft.com/en-us/evalcenter/evaluate-hyper-v-server-2019>`_ ，
虽然这个没有图形界面，不过可以通过 Windows Admin Center 或者 Windows 远程管理来管理。

安装和使用 Windows Admin Center
-----------------------------------------------------

    Windows Admin Center 是本地部署的基于浏览器的应用，用于管理 Windows 服务器、
    群集、超融合基础设施和 Windows 10 电脑。 它是免费产品，可供在生产中使用。

    `Windows Admin Center 概述 | Microsoft Docs <https://docs.microsoft.com/zh-cn/windows-server/manage/windows-admin-center/overview>`_ 

Windows Admin Center 可以装在 Windows 10 上作为客户端，也可以装在 Windows Server 上作为网络服务，
咱这次就装在 Server 上了。有桌面体验的话直接从
`Microsoft Evaluation Center <https://www.microsoft.com/evalcenter/evaluate-windows-admin-center>`_
下载和安装就好，如果是服务器核心模式或者 Hyper-V Server 的话，用 PowerShell 来下载和安装也是可以的。

.. code-block:: powershell

    # PowerShell 里也是有 wget 的，虽然用法和 GNU/Linux 里面的不太一样。
    wget -Uri https://aka.ms/WACDownload -UseBasicParsing -OutFile c:\WindowsAdminCenter.msi
    # 用 msiexec 安装 Windows Admin Center，这里选择用自生成的证书。
    # SME_PORT 是 Windows Admin Center 的端口。
    ## 不要使用点斜杠相对路径表示法（例如 .\<WindowsAdminCenterInstallerName>.msi）
    ## 从 PowerShell 调用 msiexec。 该表示法不受支持，会导致安装失败。 请删除 .\ 前缀或指定 MSI 的完整路径。
    msiexec /i c:\WindowsAdminCenter.msii /qn /L*v log.txt SME_PORT=443 SSL_CERTIFICATE_OPTION=generate

安装好后 Windows Admin Center 就会从 Windows Update 接收更新了。
然后从浏览器打开 Server 的 IP 地址：

    Microsoft 推荐是用 Microsoft Edge 或者 Google Chrome 来访问 Windows Admin Center 啦。

在输入服务器上管理员的用户名和密码登录以后，就能见到主界面啦。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/windows_admin_center_login.png
   :alt: Windows Admin Center 的主界面

选择要连接的服务器，就能看到服务器的各种状态。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/windows_admin_center_status.png
    :alt: 主机状态

如果已经安装好 Hyper-V 的话，就能在这里管理虚拟机。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/windows_admin_center_hyper_v.png
    :alt: 虚拟机管理器

也可以管理设置和远程连接虚拟机。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/windows_admin_center_login.png
    :alt: 虚拟机连接。

不过用 Windows Admin Center 下载来的远程桌面连接的时候，要输入的是服务器的用户名和密码的样子。

使用 Hyper-V 管理器和 Windows 远程管理（WinRM）管理虚拟机
-----------------------------------------------------------------
效果就是可以用 Windows 上的 Hyper-V 管理器连接到服务器，然后就像 Hyper-V 运行在本地一样。
不过也挺麻烦的就是了。

首先，在服务器上启动 Windows 远程管理服务，以及适当的前期配置。
下面的命令都是在以管理员身份运行的 PowerShell 里完成的。

.. code-block:: powershell

    # 启动 WinRM 快速配置。
    # 这三步中间的询问按提示来就可以了。
    # 如果遇到因为公用网络无法设置防火墙规则的话，解决方法在下面。
    winrm quickconfig
    # 启用 PowerShell 远程访问。
    Enable-PSRemoting
    # 为凭据委派（咱也不知道这是啥）启用 CredSSP。
    Enable-WSManCredSSP -Role server

如果汝遇上因为公用网络无法设置防火墙规则的话，可以用 PowerShell 更改网络类型。

.. code-block:: powershell

    # 用 Get-NetConnectionProfile 获得现在连接的网络适配器的信息
    Get-NetConnectionProfile                                      
                                                                                
        Name             : 网络 2                                                       
        InterfaceAlias   : vEthernet (Internal)                                         
        InterfaceIndex   : 8                                                            
        NetworkCategory  : Private                                                      
        IPv4Connectivity : Internet                                                     
        IPv6Connectivity : LocalNetwork                                                 
                                                                                        
        Name             : 网络                                                         
        InterfaceAlias   : vEthernet (External)                                         
        InterfaceIndex   : 18                                                           
        NetworkCategory  : Public                                                      
        IPv4Connectivity : Internet                                                     
        IPv6Connectivity : Internet       

    # 用 Set-NetConnectionProfile 设置网络的类别，记得把 InterfaceIndex 换成汝需要更改的那个网络。
    Set-NetConnectionProfile -InterfaceIndex 18 -NetworkCategory Private

接下来这些操作在客户端上完成。

.. code-block:: powershell

    # 首先就是把 Hyper-V 管理工具装上啦。
    # 用 “启用或关闭 Windows 功能” 也是可以的。
    add-windowsfeature rsat-hyper-v-tools
    # 然后在客户端启动 WinRM 快速配置。
    winrm quickconfig
    # 设置客户端 WinRM 的信任的主机。
    # 记得把 fqdn-of-hyper-v-host 换成服务器的完全限定域名（FQDN Hostname）
    # 在 Windows 上的话， FQDN Hostname 就是服务器的计算机名加上连接特定的 DNS 后缀。
    # 这个后缀能用 ipconfig 命令看到。例如咱这里的 FQDN Hostname 就是 "WIN-0JL0BB72KFC.lan"。
    Set-Item WSMan:\localhost\Client\TrustedHosts -Value "fqdn-of-hyper-v-host"
    # 启用客户端 CredSSP 认证。
    Enable-WSManCredSSP -Role client -DelegateComputer "fqdn-of-hyper-v-host"

接着打开组策略对象编辑器（gpedit.msc），把
"计算机配置 >管理模板 >系统 >凭据委派 >允许向仅 NTLM 服务器身份验证分配全新凭据"
设置为启用，并在列表里添加 “wsman/汝服务器的 FQDN Hostname” 。像这个样子。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/gpedit.png
    :alt: 设定必要的策略

打开 Hyper-V 管理器，选择“连接到服务器”，输入服务器的计算机名。以及把下面“以另一个用户身份登录”那项
选中。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/hyper_v_connect_server.png
    :alt: 设定服务器

不过设置用户的时候，这里的用户名是“服务器的计算机名\\用户名”的形式。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/hyper_v_user.png
    :alt: 设定服务器的用户

如果一切顺利的话，Hyper-V 管理器应该就能连接到服务器了。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/hyper_v_connected_server.png
    :alt: 连接成功的效果

通过 RemoteApp 运行 Windows 虚拟机上的应用
-----------------------------------------------------------
RemoteApp 就像 VMware 的 Unity 模式或者 VirtualBox 的无缝模式，
不过虚拟机不在运行 RemoteApp 的电脑上而是在远程连接上啦。

不过以官方的方法配置 RemoteApp 的话，汝需要设置远程桌面服务，购买相应的许可证，设置 Active Directory 等等操作。
咱这里就用一个第三方的开源小工具 `RemoteApp Tool <https://github.com/kimmknight/remoteapptool>`_ 好了。

    可以在 https://github.com/kimmknight/remoteapptool/wiki/Windows-Compatibility 上看到兼容性列表，
    基本就只有 Windows 的企业版和 Windows Server 支持了。（好像最新的 Windows 10 专业版也可以？）

安装好 RemoteAppTool 然后打开，界面大概就是这个样子。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/remoteapp_0.png
    :alt: RemoteApp Tool 的界面

点击下面的绿色加号按钮添加要供远程访问的应用。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/remoteapp_1.png
    :alt: RemoteApp Tool 添加应用

然后点击右下角的 “Create Client Connection” 生成远程桌面连接文件。基本上只要修改远程
桌面连接的地址就好。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/remoteapp_2.png
    :alt: RemoteApp Tool 生成 RDP 文件

然后就可以用远程桌面连接客户端连接啦，虽然可能没有本机上的无缝模式流畅。

.. image:: /images/build_virtualization_stuffs_with_hyper_v/remoteapp_3.png
    :alt: RemoteApp 效果演示

以及 Windows 对远程会话的数量是有限制的（Windows 1 个，Windows Server 没有 RDS 许可时 2 个），
不过应该够用吧……

----

以上。