LEDE + iptables6 + Shadowsocks = ?
==========================================================

:slug: lede_and_nat6_and_ss_redir
:lang: zh
:date: 2017-10-28
:tags: LEDE,devices
:desc: 利用支持 LEDE 的路由器 + shadowsocks + iptables6 组建 nat6 + 透明代理 AP 
:issueid:

.. PELICAN_BEGIN_SUMMARY

利用支持 LEDE 的路由器 + shadowsocks + iptables6 组建 nat6 + 透明代理 AP  
:del:`接着搞大新闻……` 😂

.. PELICAN_END_SUMMARY

.. contents::

所以 LEDE 又是个啥？😂
----------------------------
Linux嵌入式开发环境项目（Linux Embedded Development Environment，LEDE），
是OpenWRT专案的一个分支专案，继承原来OpenWRT的开发目标。
OpenWRT开发者社区内因各种决定迟迟未能在达成一致意见，导致长时间没有大幅更新，
这使得一群OpenWRT核心贡献者感到不满，而他们于2016年5月，决定另立新专案。
这个专案在一年后才正式定名为LEDE。LEDE的内核原始码基本继承自原OpenWRT，
但相应的开发者社区采用新的讨论规定和决议流程。
2017年6月，LEDE社区和OpwnWRT社区均通过决定，将原OpenWRT专案合并至LEDE专案之中。

https://lede-project.org

最后的效果大概是？
---------------------------

* 连接到路由器的设备可以访问纯 IPv6 网站（通过 nat6）
* 通过透明代理访问被墙网站 

下载，安装和设置 LEDE 
---------------------------------

* 了解汝的路由器有木有资瓷以及下载 ：https://lede-project.org/supported_devices
* 或者自己编译一个 ：https://lede-project.org/docs/guide-developer/start

如果自己编译的话，可以考虑添加这些依赖（然而咱忘了都在哪了 😂）：

.. code-block:: bash

    libgcc  # LEDE：target/cpu型号/package目录下
    libpthread # 同上
    libpcre # package
    libev # package
    libsodium # package 最新版本需要libsodium版本 >= 1.0.12
    libmbedtls # package
    libcares 
    ip # base
    ipset 
    iptables-mod-tproxy # 代理 UDP 流量需要
    # nat6 需要这两个
    ip6tables 
    kmod-ipt-nat6

刷入以后 ssh 上去（一般是 root@192.168.1.1 ），设置一下密码。

----

如果要用透明代理的话，没编译的话去装依赖啦 😂:

    # opkg 是一个小型的软件包管理器 😂

    opkg install ip-full ipset iptables-mod-tproxy libev libpthread libpcre libmbedtls

接下来下载软件，注意需要根据自己的CPU内核架构来进行选择。可使用如下命令查看架构类型：

    opkg print-architecture

然后去 http://openwrt-dist.sourceforge.net/packages/LEDE/ 下载些 ipk：

.. code-block:: bash

    libcares_x.xx-x_xxxx.ipk
    libsodium_x.x.xx-x_xxxx.ipk
    shadowsocks-libev_x.x.x-x_xxxx.ipk
    dns-forwarder_x.x.x-x_xxxx.ipk
    ChinaDNS_x.x.x-x_xxxx.ipk
    luci-app-shadowsocks_x.x.x-x_all.ipk
    luci-app-chinadns_x.x.x-x_all.ipk
    luci-app-dns-forwarder_x.x.x-x_all.ipk

将下载的包通过sftp之类的工具上传至路由器的 /tmp 目录。（可能需要安装 openssh-sftpserver）

最后就是安装啦：

.. code-block:: bash

    opkg install libcares*.ipk libsodium*.ipk
    opkg install shadowsocks-libev*.ipk luci-app-shadowsocks*.ipk
    opkg install ChinaDNS*.ipk luci-app-chinadns*.ipk
    opkg install dns-forwarder*.ipk luci-app-dns-forwarder*.ipk

设置 iptables6 
-----------------------------------

    需要准备个自己上手的编辑器 😂 LEDE 内置的是 vi，喜欢 vim/nano 的话可以通过
    opkg 安装或者编译时加上。

* 修改 /etc/config/network , 加上或者修改这一段（ULA前缀）：

    NAT6转换的ULA网段要求是2fff::/64网段，因此把ULA前缀改为2fff::/64内的任意网段，
    为了让客户端尽可能使用 IPv6 需要修改这个 😂

.. code-block:: text

    config globals 'globals'
	    option ula_prefix '2fff::/64'

* 修改 /etc/config/dhcp 在 LAN 接口上启用宣告默认路由：

.. code-block:: text

    config dhcp 'lan'
        option ra_default '1'

* 然后查看一下默认网关：

    ip -6 route | grep "default from"

    大概会得到这样的

    default from (ipv6 range) via (gateway) dev (intf) proto static  metric 512

    就需要向下一级宣告默认网关，括号的内容自行替换为上面结果。

    ip -6 r add default via (gateway) dev (intf)

    比如咱的结果是这样：

    default from 2001:da8:a005:218::/64 via fe80::72ba:efff:fe82:c654 dev eth1  metric 512 

    那咱就这么写：

    ip -6 r add default via fe80::72ba:efff:fe82:c654 dev eth1

* 用 ip6tables 设置 NAT

    ip6tables -t nat -A POSTROUTING -o (intf) -j MASQUERADE

    记得把 intf 换成相应的界面名称（例如咱的eth1），然后拿起连接到这个网络的设备
    试一下 IPv6 能不能用了呗~

* 最后可以考虑写个脚本自动化这个过程 （其实咱是抄的 😂） ：

.. code-block:: bash

    #!/bin/sh
    # filename: /etc/hotplug.d/iface/90-ipv6
    # please make sure this file has permission 644

    # check if it is the intf which has a public ipv6 address like "2001:da8:100d:aaaa:485c::1/64"
    # interface_pulbic 是带有公网IPV6地址的接口地址，大多数情况下是 wan6
    interface_public="wan6"
    [ "$INTERFACE" = "$interface_public" ] || exit 0

    res=`ip -6 route | grep "default from"`
    gateway=`echo $res | awk '{print $5}'`
    interface=`echo $res | awk '{print $7}'`

    if [ "$ACTION" = ifup ]; then
        ip -6 r add default via $gateway dev $interface
        if !(ip6tables-save -t nat | grep -q "v6NAT"); then
            ip6tables -t nat -A POSTROUTING -o $interface -m comment --comment "v6NAT" -j MASQUERADE
        fi
    else
        ip6tables -t nat -D POSTROUTING -o $interface -m comment --comment "v6NAT" -j MASQUERADE
        ip -6 r del default via $gateway dev $interface
    fi

把这个脚本放进 /etc/hotplug.d/iface/90-ipv6 ，设置好权限（例如644），
效果就是每次ipv6连接和断开就自动添加和删除路由规则 😂

设置 Shadowsocks
------------------------------------

    😂 嗯……

用 Luci 设置（和需要更多设置）的话请参考最后参考来源那篇文章
（咱要求低到能用就行 😂）

打开或修改 /etc/config/shadowsocks 

.. code-block:: text

    # 基本设置
    config general
        # 启动延迟
        option startup_delay '0'

    # 透明代理设置
    config transparent_proxy
        # UDP 中继服务器是哪个，没有的话就是 nil
        # 然而内部用的啥名字咱没看明白 😂 于是还是用 Luci 改了
        option udp_relay_server 'nil'
        # 透明代理的本地端口和 mtu
        option local_port '1234'
        option mtu '1492'
        list main_server 'cfg104a8f'
        list main_server 'cfg0c4a8f'

    # 设置 socks5 代理
    config socks5_proxy
        list server 'nil'
        option local_port '1080'
        option mtu '1492'

    # 设置端口转发
    config port_forward
        list server 'nil'
        option local_port '5300'
        option destination '8.8.4.4:53'
        option mtu '1492'

    # 访问控制
    config access_control
        # 路由器自身的代理类型 （1 好像是普通代理）
        option self_proxy '1'
        # 直连地址列表
        option wan_bp_list '/etc/chinadns_chnroute.txt'
        # 强制使用代理的 IP 地址列表，一行一个。
        list wan_fw_ips '8.8.8.8'
        list wan_fw_ips '8.8.4.4'
        option lan_target 'SS_SPEC_WAN_AC'
        # 强制直连的 IP 地址列表，一行一个。
        list wan_bp_ips '59.67.0.0/9'
        list wan_bp_ips '172.18.0.0/9'

    # 设置服务器（和 Shadowsocks 客户端一样）
    config servers
        option alias 'example'
        option fast_open '1'
        option no_delay '0'
        option server 'ip_address'
        option server_port '80'
        option timeout '60'
        option password 'password'
        option encrypt_method 'aes-256-cfb'
        option plugin 'obfs-local'
        option plugin_opts ''

设置 DNS-forwarder
-------------------------------------

    把 DNS 请求转发给特定的端口~

创建或修改 /etc/network/dns-forwarder :

.. code-block:: text

    config dns-forwarder
        # 上游 DNS 服务器
        option dns_servers '8.8.8.8'
        # 开不开 😂
        option enable '1'
        # 本地的地址和端口
        option listen_port '5311'
        option listen_addr '127.0.0.1'

创建或修改 /etc/network/chaindns :

    通过比较国内和国外 DNS 解析的差别区分是不是国内网站~ 

.. code-block:: text

    config chinadns
        # 国内路由表的位置
        option chnroute '/etc/chinadns_chnroute.txt'
        # ChinaDNS 的端口（和上面的 DNS 转发要不一样）
        option port '5353'
        # 开不开 😂
        option enable '1'
        # 双向过滤 😂
        option bidirectional '1'
        # 上游服务器，第一个是汝 ISP 的DNS（
        option server '0.0.0.0,127.0.0.1#5311'

修改 /etc/network/dhcp :

.. code-block:: text

    config dnsmasq
        # 加一行 ChinaDNS 的服务器：
        list server '127.0.0.1#5353'

然后重启路由器应该就 OK （😂


参考来源
-------------------------------------

* `LEDE下的ipv6 NAT6 <https://lixingcong.github.io/2017/04/24/ipv6-nat-lede/>`_
* `Shadowsocks + ChnRoute 实现 OpenWRT / LEDE 路由器自动翻墙 <https://cokebar.info/archives/664>`_
