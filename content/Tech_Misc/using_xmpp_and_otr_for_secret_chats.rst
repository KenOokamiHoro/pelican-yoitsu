利用 XMPP 和 OTR 进行私密聊天
====================================================

:slug: using_xmpp_and_otr_for_secret_chats
:lang: zh
:date: 2018-10-9
:tags:
:desc: 基本上就是某篇文章的翻版……
:issueid: 

.. PELICAN_BEGIN_SUMMARY

基本上就是某篇文章的翻版……

https://hardenedlinux.github.io/cryptography/2018/02/01/pidgin_xmpp_otr_debian.html 

关于 OTR 的原理啥的因为咱不懂，于是就接着抛这同一块砖引不同的玉了 😂

    以及数了数发现文章数量年趋下降？（可能是因为不少东西开始往别的地方写了的缘故）

.. PELICAN_END_SUMMARY

.. contents::

演示环境
---------------------------------------------------------

PC ：Parabola GNU/Linux Libre 

    其实有 Pidgin 用的发行版就 OK 😂 ，安装 pidgin，
    pidgin-otr 和 pidgin-xmpp-receipts 软件包（可能随发行版不同而不同）

Android：Xabber

    虽然 Conversations 也支持但是只有 Conversations 侧发起 OTR 会话才能用，好迷 😂

启动加密
--------------------------------------------------------

从工具 - 插件中启用 OTR：

.. image:: /images/XMPP/pidgin_load_plugin.png 
    :alt: Pidgin 通过插件可以支持 OTR

然后就可以在会话中发现 OTR 菜单。

.. image:: /images/XMPP/pidgin_otr_menu.png 
    :alt: Pidgin 会话中的 OTR 菜单

.. image:: /images/XMPP/pidgin_otr_session.png 
    :alt: OTR 会话已启动

启动以后的 OTR 会话大概就是这个样子。

如果汝眼睛注意到的话，会发现菜单栏上的图标上有个感叹号，以及下面会提示未验证（Unverified）。
这个问题一会儿再解决 😂

Xabber 的话，点击下方锁形图标里的“启动加密”。

.. image:: /images/XMPP/xabber_start_otr.png
    :alt: 在 Xabber 中启动 OTR

加密连接建立成功后，锁会闭合。不过里面有个问号表示 OTR 指纹未验证。

.. image:: /images/XMPP/xabber_otr.png
    :alt: 在 Xabber 中启动 OTR


验证 OTR 指纹 - 概述
-------------------------------------------

为啥要验证 OTR 指纹呢，因为汝不能确定汝收到的指纹对应的私钥的持有者和
汝收到的消息的发送者是不是同一个人嘛 😂 （毕竟大部分 XMPP 服务器只通过用户名
和密码进行认证，密码强度不够的话还是很容易被盗用的）

而验证显然不能通过刚建立的 OTR 会话来完成验证（先有鸡还是先有蛋？），所以需要借助一些外部手段完成验证啦~ 

.. image:: /images/XMPP/pidgin_otr_verification.png
    :alt: Pidgin 中支持的验证方式

Pidgin 和 Xabber 支持 OTR 标准的三种验证途径（这个菜单可以从 OTR - Authenticate buddy 中打开）：

* 通过指纹手动验证。

* 通过共享的秘密进行验证

* 通过问答进行验证

手动验证
---------------------------------------------

手动验证是最简单的方式，画面中会展示汝和对方的 OTR 指纹，在确认两边显示的指纹相同时，
汝可以标记为该指纹已通过验证。

.. image:: /images/XMPP/pidgin_fingerprints.png
    :alt: Pidgin 中的 指纹验证

* 因为要求通过另一个安全的信道交换指纹，所以能够当面验证当然是最好的啦，毕竟不会受到其它因素干扰。

* 如果做不到的话，就看汝自己的需求和安全性要求啦，例如通过电话和加密的邮件也是种选择

通过验证以后 Pidgin 会提示“私密对话已建立”，而 Xabber 中锁形标志中间会变成对勾表示验证完毕。

通过共享的秘密/提问和回答进行验证
----------------------------------------------

.. image:: /images/XMPP/pidgin_share_secrets.png
    :alt: Pidgin 中通过共享的秘密验证

.. image:: /images/XMPP/pidgin_question.png
    :alt: Pidgin 中通过问题验证

共享的秘密和提问验证的核心是，汝和朋友拥有相同的秘密，如果对方能回答出和这个秘密
有关的问题，那么对方很有可能就是和汝拥有相同秘密的那位朋友啦~ 

不过，这个验证的过程中双方不会看到对方的输入，只是如果双方的输入相同（共享的秘密）或者
对方的答案和汝设置的答案是相同的（提问和回答），双方的指纹就会标记成已信任。

其实这个方法并没有直接验证指纹来的实用……

多份指纹或者指纹发生了改变？
-------------------------------------------------

如果汝在多个设备上登录了一个 XMPP 账户，那汝就会有多个 OTR 指纹，重新登录也会使 OTR
指纹发生变化，汝的朋友也是如此。

如果汝在一开始只验证了部分指纹，而对方开始用新设备和汝进行 OTR 聊天时，汝会重新发现
验证提示的。

这时汝可以像以前一样进行验证，或者要求对方通过已经验证的实例把新设备的 OTR 指纹给汝发送一份，例如：

    “汝今天这设备上的 OTR 指纹咱好像不认识啊，方便用咱俩
    验证过的那部设备把这个设备上的 OTR 指纹发给咱嘛？”

如果汝双方有验证过 openPGP 密钥的话，也可以让对方用汝认识的密钥签名他的 OTR 指纹后把签名后的消息
发给汝，作用是相同的。

不过实际操作时要注意，记得是让对方把指纹发给汝，而不是把汝要验证的指纹发给对方。

准备一份指纹清单
---------------------------------------------------

如果汝日常会在好几部设备上使用 XMPP ，可以考虑整理一份指纹清单，在第一次验证时就让
汝的好友把汝现有的指纹验证完毕。

消息的主题大概像这样：

.. code-block:: text

    I hereby claim:

    * I am bob, who owns the following PGP key(s):

        6B25C1C7 7E739D1B 6C5E86B0 D3CED892 A6AEB68B

    * At the same time, I am the owner of the following OTR key(s):

        3340D56D 2C750734 F9869A1B 26CFAC08 04823F52
        1063A0B9 F9EE4D3B 06250142 2A720531 DD2D0DC4

    * The following OTR key(s) are used for mobile phone,
        they may get revoked at any time.

        49AF52C4 53FF6323 635728B6 9325628C 0FF2E547

    * And I declare revocation of the following OTR key(s), their
        private keys are destroyed, please don't trust them:

        8DF79391 B8082682 42D5483E 976503B8 E458AEA8

    * To prove the claims above, this message is signed by my PGP key
        mentioned above.

    You could also collect all fingerprints linked to your frequently
    used xmpp accounts into a list like this, clear-sign it with gnupg,
    and send it to your trustful friends, in order for them to trust you.

然后把这则消息通过 gpg 进行明文签名，效果大概像这样（下面这条消息也是咱的指纹清单）：

.. code-block:: text

    -----BEGIN PGP SIGNED MESSAGE-----
    Hash: SHA256

    I hereby claim:

      * I am KenOokamiHoro, who owns the following PGP key(s):

        C41D545A 576AC1FE 4DBE90C9 B6CF41D1 879E8002

      * At the same time, I am the owner of the following OTR key(s):

        B9A0DE73 1F9689D7 856F1C40 41C6D1D0 059A2E70
        EC63BC9D FAFC37FA C4331842 1357CCF6 D67961F9

      * The following OTR key(s) are used for less stable mobile devices,
        they may get revoked at any time.

        49AF52C4 53FF6323 635728B6 9325628C 0FF2E547
        EFEBAF65 78722ED3 3FB5A8C5 B20B4B14 2A20F48E

      * To prove the claims above, this message is signed by my PGP key
        mentioned above.

    You could also collect all fingerprints linked to your frequently
    used xmpp accounts into a list like this, clear-sign it with gnupg,
    and send it to your trustful friends, in order for them to trust you.
    -----BEGIN PGP SIGNATURE-----

    iHUEARYIAB0WIQQjv2kr4UE70DFaNrtMlLVI0dRI/gUCW6n1sQAKCRBMlLVI0dRI
    /n5PAQDobS25Xn06yBI+HhOQpi97J9JZuyvPOGbc6L8bm0t/1AD/Ye4xvA9+9NPt
    zh9i9QVrupW3FvINUd8q/M9Y1wqOsQI=
    =knz+
    -----END PGP SIGNATURE-----


有一个问题是某些客户端会对消息排版（如 Pidgin 会用 html 排版），而这会破坏 clear-sign 的一致性。
一个有效的解决方法是用gpg --store -a将 clear-sign 的文本编码成不怕排版的 radix-64 形式，
用gpg -d即可还原。该命令默认会对输入作透明的压缩，故比压缩再 base64 的“传统”做法操作更简便。

就像这样：

.. code-block:: text

    -----BEGIN PGP MESSAGE-----

    owF1U0uS2zYQ9Zqn6GWSkS1+QcJVXoAfSLQ9+o48GXtFSZCEiCQ0ACmZvltOkE3O
    kVOkRdmpisvmAkR14/V7rxv465/1i09//v3i5fWLs1E+gdloBst8NMlSuM+WSzbK
    +qQ1LszhNSzHzA2IZeVwEFqsO9iUhaxeWxbAb5BDUcE7UU/VsajkWGk1gMtBgbrU
    BpqDgJ0qS3WR9b5nOYruF/NrjwVIfCcN/IBBEBKWODwDP40zaicUYpJwzDoQhTSL
    bNu9sbGmr2mKSkAjKzG48V9jSCg0qN13pNOHxf9JY8rsNAs9cDglEU1DiALCncS3
    wXcSkjqpDXZAmZuFdg/IEuLFCU2BM554IWeo2/OcyHfB8YIwSTiBlISUYMGbyoef
    KIBCC2iN2GJWQymMAdMU61JApdYSf1txlhthBj0v+uigKjrYiwa0OKsjAosGirrr
    vb+6+fEp44Gb+BB4nBPP9YCgKjeKCVAP5+ZGCdicu1nghzc/PIsZJwGEUei6WeqB
    x+OARUkAsWvHfuz44DLX5n6UffWj4KTVWfSt7YdvoFhjYIARaaBCJ8VeAG6N3Nco
    Ey9J1X0beM9aibqR6prrkSj+SbWwUW2JkdIo3Jal2KC9soQd9k3ok5Z1Y6CU9dV5
    o6BTrYadFs8tFis7q2/l5+p0gmKDla6HEaGgQIxpcDmKXuAARYtCv7yKA9nARTYH
    2NftaT+winoLRuCC8W8UjW5Ns2tRh5aYQrysQekt3q/r4LAL1fVsf+yKeGX94C2x
    h9Xi6zOy5HiVscVTzmL7MZ/P/zi7R+2v8IKlvJjo5r58/yG3t4t8uF8lj6R2zJy9
    Sxbxf3FrWAczNk/VeukGv9c26eL8bnyYzk+Shm/p249td55NR+sNeR+tK7sZOiwd
    Pgn/85nROzqZNdaXA5V0/kG3p0ePk3M+WW2j5+E9fXIuz1Mzz99Yb471l7ubkWyS
    /sjGvw==
    =jMb7
    -----END PGP MESSAGE-----

如果使用这个方法的话，就算现在建立的 OTR 通道没被信任也不要紧（如果汝信任对方的 PGP 密钥的话）

组合验证
----------------------------------------------

这好几种方法其实是可以组合在一起的：

* 如果正好遇上了或者约定好了要见面（例如参加 PGP Signing Party时），可以顺便完成验证指纹。

    不过根据汝的威胁模型，可能要加入窃听者的威胁。

* 如果汝或者汝信任的第三方信任对方的 PGP 公钥，可以用 PGP 完成指纹验证呗~ 

* 如果安全要求稍低的话，可以使用电话，短信或者其它 IM 完成（例如 Wire 或 Signal？）

* 可以的话也可以继续拆分多通道信息交换，通过多个通道获得的信息的组合组成最终的共享秘密。
  不过需要注意尽量不要在某一通道内提到通过其他通道交换的消息的内容。

