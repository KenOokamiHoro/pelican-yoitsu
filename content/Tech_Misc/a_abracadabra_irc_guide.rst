某胡言乱语的 IRC 教程
====================================================

:slug: a_abracadabra_irc_guide
:lang: zh
:date: 2017-08-03
:tags: software,IRC 
:desc: 某个胡言乱语的 IRC 教程……
:issueid: 

.. PELICAN_BEGIN_SUMMARY

:del:`某个胡言乱语爪牙无措不知所云的 IRC 教程` 

.. PELICAN_END_SUMMARY


所以 IRC 是个啥？
-------------------------

    因特网中继聊天（英语：Internet Relay Chat，缩写即IRC）是一种历史悠久、
    应用广泛、成熟稳定的网络即时通讯协议，被广泛地应用于在线通讯和网络聊天中。
    IRC最早由芬兰人雅尔口·欧伊卡林恁（Jarkko Oikarinen）
    1988年8月创造以取代一个叫做MUT的程序，随后便一直在互联网中扮演重要角色。
    凡是支持互联网的操作系统，几乎都可以使用IRC。虽然在2003年以后，
    许多功能更加丰富的聊天程序和服务取代了只能进行纯文字交流的IRC，
    许多曾经的大型IRC服务器日渐式微，但对于许多应用来说，依然是一种方便可靠的通讯方式。

    IRC是一种RFC标准公开规定的，分布式的主从式架构——
    即服务端对客户端进行服务的网络即时通讯协议，采用TCP和SSL协议。在IRC协议中，
    不同的独立服务器可以互相进行中继，组成一个整体，提供更加大型聊天网络服务，
    这也是IRC名字中“中继”的由来。

    IRC是一种协议，或者说仅仅是提供了聊天的一种方式，
    而不是指单一具体的某个聊天电脑程序、软件或者网络服务器。
    换句话说，只要有服务器和客户端软件实现了IRC协议，
    就能够以IRC设计的方式进行在线聊天。
    例如，IRC服务器可以是某个组织公开所运营的大型网络，不需要客户注册和登录即可使用；
    也可以是私自架设的内部IRC服务器，公司内部使用IRC进行交流。

    -- https://zh.wikibooks.org/wiki/IRC

（话说本来想把这个教程写进 Wikibooks 去的但是就是严肃不起来啦 😂）

所以 IRC 就是一个 IM 协议啦，和 XMPP ，Matrix 一类的差不多。
虽然古老但是经久耐用啊（跑

    后面都连最大的 IRC 服务器 https://freenode.net 好了 😂

    irc://irc.freenode.net:7000 

    更多的端口和连法（例如 plain、Tor ）请参阅 https://freenode.net/kb/answer/chat 

各种 IRC 客户端推（an）荐（li）兼连接教程 😂
------------------------------------------------

因为 IRC 协议开放，于是就有很多客户端可以用啦。 
`Wikipedia 上的这个页面 <https://en.wikipedia.org/wiki/Comparison_of_Internet_Relay_Chat_clients>`_ 
有各种 IRC 客户端的比较，咱就只挑几个介绍一下怎么连接好了（跑

``````````````````````
webchat.freenode.net
``````````````````````

freenode 的网页版，临时用下还好，不过还是用个客户端好呐~ 

    https://webchat.freenode.net

.. image:: /images/IRC_Guide/freenode_web.png
   :alt: freenode_web

从上到下分别是昵称，频道，是否使用认证和 Google 的验证码，填好后一把梭 connect 就行 😂

``````````````````````
CIRC
``````````````````````

可以从 Chrome web store 下载：
https://chrome.google.com/webstore/detail/circ/bebigdkelppomhhjaaianniiifjbgocn?hl=zh-CN

.. image:: /images/IRC_Guide/circ_1.png
   :alt: circ

用 /nick 指定一个昵称啦~

.. image:: /images/IRC_Guide/circ_2.png
   :alt: circ

然后跟着指示操作就成?

https://github.com/flackr/circ/ <- 文档在这里 😂

````````````````````````
ChatZilla
````````````````````````

是 Firefox 的一个扩展啦：
https://addons.mozilla.org/en-US/firefox/addon/chatzilla/

如果需要汉化的话：
https://addons.mozilla.org/en-US/firefox/addon/chatzilla-zh-cn-language-pack/

.. image:: /images/IRC_Guide/chatzilla_1.png
   :alt: chatzilla

可以先戳左边的昵称改一下昵称（默认是用户名）然后戳那个 freenode 连接啦~

.. image:: /images/IRC_Guide/chatzilla_2.png
   :alt: chatzilla

http://chatzilla.hacksrus.com/intro <- 文档在这里(?) 😂

```````````````````````
Polari
```````````````````````

GNOME 的一部分：
https://wiki.gnome.org/Apps/Polari <- 文档也在这(?) 😂

.. image:: /images/IRC_Guide/polari_1.png
   :alt: polari

直接戳那个 + ~

.. image:: /images/IRC_Guide/polari_2.png
   :alt: polari

可以看到 Polari 内置 freenode 啦 ~

.. image:: /images/IRC_Guide/polari_3.png
   :alt: polari

有需要的话右键网络名称改下昵称呗~

.. image:: /images/IRC_Guide/polari_4.png
   :alt: polari

然后就可以接着戳 + 加入聊天室啦 😂

.. image:: /images/IRC_Guide/polari_5.png
   :alt: polari


`````````````````````````
Thunderbird
`````````````````````````

喂喂咱只是个邮件客户端啊 😂

.. image:: /images/IRC_Guide/thunderbird_1.png
   :alt: thunderbird

创建聊天账户（或者戳“聊天”然后添加账户）

.. image:: /images/IRC_Guide/thunderbird_2.png
   :alt: thunderbird

选择聊天网络（当然是 IRC 啦 ……）

.. image:: /images/IRC_Guide/thunderbird_3.png
   :alt: thunderbird

用户名和服务器（

.. image:: /images/IRC_Guide/thunderbird_4.png
   :alt: thunderbird

这里应该填 SASL 的密码（？），不过现在啥都没有就直接下一步吧 😂

.. image:: /images/IRC_Guide/thunderbird_5.png
   :alt: thunderbird

高级选项，应该写的够清楚了吧。只要保证用 SSL 就行（

.. image:: /images/IRC_Guide/thunderbird_6.png
   :alt: thunderbird

保存！

.. image:: /images/IRC_Guide/thunderbird_7.png
   :alt: thunderbird

在 Thunderbird 中加入频道的话，在聊天标签页中戳“加入聊天”，
然后输入相应的频道名称就好啦~

https://support.mozilla.org/en-US/kb/instant-messaging-and-chat <- 文档在这里 😂

`````````````````````````````
WeeChat
`````````````````````````````

喵喵喵？

.. image:: /images/IRC_Guide/weechat_0.png
   :alt: weechat

大概像这个样子……看咱 connect 一把梭啦 😂

    # 设置服务器和端口

    /set irc.server.freenode.addresses "chat.freenode.net/7000"

    # 开 SSL

    /set irc.server.freenode.ssl on

    # 设置昵称和备用昵称 （默认是汝的用户名）

    /set irc.server.freenode.nicks "mynick,mynick2,mynick3,mynick4,mynick5"

    # 设置自动连接

    /set irc.server.freenode.autoconnect on

    # 连接

    /connect freenode

`或者可以像咱一样搞个 relay 😂 </tech_misc/weechat_glowingbear_irc.html>`_

https://freenode.net/kb/answer/registration

官方的速成指南： https://weechat.org/files/doc/stable/weechat_quickstart.en.html

或者更详（luo）细（suo）的文档：

https://weechat.org/files/doc/stable/weechat_user.en.html

更换和注册昵称
------------------------------------

https://freenode.net/kb/answer/registration

有时汝可能会收到这样的消息：

    This nickname is registered. Please choose a different nickname, 
    
    or identify via /msg NickServ identify <password>.

这就表示汝现在使用的 nickname 已经有人占啦 😂 这种时候汝首先应该换一个 nickname，
像这样（大部分 IRC 客户端应该都支持）：

    /nick Foo

然后汝是不是也想注册一个 nickname 呢？那首先汝先 /nick 一个没别人占而且合适的 
nickname，然后：

    /msg NickServ REGISTER password youremail@example.com

把 password 换成汝自己设置的密码，youremail@example.com 换成汝的一个邮箱。

    顺便说一句大部分 IRC 客户端里 /msg 可以向一个用户发送 Private Message （私聊消息）？

然后应该会说些话（具体是啥咱忘了 😂），嘛打开汝的邮箱找 freenode.net 发来的邮件咯。
邮件里应该会有这一行：

    /msg NickServ VERIFY REGISTER nick token

nick 是汝注册的 nickname ，token 是一串字母。把这行复制下来发出去就 OK 啦 😄

下次再登录的时候用 /msg NickServ identify <password> 验证（

一个账号上也可以注册多个 nickname：

* 首先先登录上现有的昵称。

* 然后使用 /nick 命令 切换到另一个昵称。

* 最后用 /msg NickServ GROUP 归组昵称。

如果客户端支持的话，可以参考 https://freenode.net/kb/answer/sasl 的文档启用 SASL
登录，就不用每次都输入密码啦 😂 （或者让客户端记住密码）

可以与 NickServ 私聊时输入 HELP 获得更多用法。

加入频道
-------------------------

    https://freenode.net/kb/answer/findingchannels 
    像这样搜索频道，不过大多数人应该目标明确……

大部分的客户端都可以用 /join #channel 的命令来加入频道，或者有加入频道的按钮。

进不去？ 有的频道有不同的要求（例如只允许注册用户，强制 SSL ，需要邀请等等），
自己看客户端的报错咯 😂

    其实还有可能是汝被 ban 啦，但是新人只要不乱说话应该不会 😂 …… 

加入频道之后请马上看 topic （可能在加入时以一条消息的方式显示，
或者在客户端的某个位置。或者可以用 /topic 命令查看）。 一个频道的 topic 一般包含
了这个频道的基本规则，新消息和可用的资源（例如相关联的项目的官方网站）。如果汝是
去提问的，可能会在链接的资源里找到答案 :-)

交流
------------------------

* 大部分的频道都建议直奔主题，所以不要问“在不在”，“有人吗”之类的问题，直接提出问题就好啦~ 
    不过还是要记得提问的智慧……

* 要提及某个用户（类似于其它 IM 上的 @），习惯上是这样：
    要提及用户的昵称: 消息文本

    大多数的 IRC 客户端会在这种情况下通知被提及的用户，有的客户端只要消息中有用户的
    昵称就会通知 😂，不过记得不要滥用（有人觉得频繁的被通知是一种骚扰）

    有的客户端会帮汝补全昵称（自动或者按 Tab 键补全），或者点击昵称就会自动添加上面
    那样的提及语句，如果有的话可以利用一下。

* 多数IRC客户端具备信息记录功能。不过 IRC 服务器不会。
    再不过有的频道会使用某些方式（例如机器人）对记录进行存档（可能会在 Topic 上写出来），有需要的话可以问问。

* 千万别刷屏！😂 也就是不要短时间内发好几条消息（或者一条太长的消息）。
    各个频道具体的限制的话可以去看看 Topic ，或者去问问管理员
    （Operator，有时会缩写成 op，汝的 IRC 客户端应该会特别的表示它们）。

    刷屏可能（哦不，是一定）会使汝被频道封禁，严重的话可能会被服务器封禁。

    如果想发送文件或者一长段文字（例如软件的日志）的话，请使用 Pastebin 服务（一会儿再说）。

* 有的频道里会有机器人，记得遵守频道里怎么用和啥时候用机器人的规则。

* 然后，遵守 `freenode 一般规则 <https://freenode.net/policies>`_ ，频道的其它规则和普通的礼仪 😂

使用 Pastebin 类服务
----------------------------------

Pastebin 是一类服务的称呼，可以叫“网络剪贴板”？

汝可以把信息发表到 Pastebin 上，会得到一个链接。然后把对应的网站链接发表在IRC频道内，解决刷屏问题。

解决刷屏问题。有些Pastebin还支持程序代码高亮，或者张贴图片，这样就可以解决IRC分享图片的问题啦~

嘛咱用过这两个：

* elimage 和 vimergy , 都是 https://github.com/Vim-cn 做的，一个贴图一个贴代码。

* pb ( https://github.com/ptpb/pb ) 

其实类似的服务有不少，例如
`Pastebin.com <https://pastebin.com>`_ （但是被墙了），
各种自由软件项目通常也都会有自己的 Pastebin ，例如
`GNOME <https://paste.gnome.org>`_, 和
`openSUSE <https://paste.opensuse.org>`_ 。


嘛 Pastebin 的网站上一般都有用法介绍，咱就不啰嗦啦 😂

创建和注册频道
-----------------------------------------

嘛不知道汝有啥动机要创建频道那就直接上了 😂

加入个没人的频道会让汝自动获得 Operator 权限，但是一掉就丢了……
所以需要注册一下：

``````````````````````````````
注册前速查表？
``````````````````````````````

* 汝已经在 freenode 上注册了？

* 要注册个啥频道？

    - https://freenode.net/kb/answer/namespaces

    - 以 # 开头的是主要（Primary）频道（实在想不出怎么翻译了 😂），
      注册这种频道可能需要一个在 freenode 上注册的组织 （ 
      https://freenode.net/groupreg ）。

    - 以 ## 开头的是话题（Topical）频道，好像没有啥特殊要求？

* 合不合乎频道政策（https://freenode.net/changuide）
  和指导（https://freenode.net/changuide）？ 

`````````````````````
注册频道
`````````````````````

首先先加入想要注册的频道，这时汝该会自动获得 Operator 权限。

然后和 ChanServ 私聊一下就好：

    /msg ChanServ REGISTER ##channel

大概会收到这样的消息：

    ##channel is now registered to nickname.
    
    Channel guidelines can be found on the freenode website:
    http://freenode.net/changuide

    This is an "about" channel as per
    http://freenode.net/policies#channel-ownership

这样就表示注册成功啦~

当汝下次加入这个频道时，可以和 ChanServ 私聊重新拿回 Operator 权限。

    /msg ChanServ OP ##channel nickname 

频道的 Operator 可以用 /mode 命令为频道设置不同的模式
（参见 https://freenode.net/kb/answer/channelmodes ）。

和 NickServ 类似，可以与 ChanServ 私聊时输入 HELP 获得更多用法。

----

最后记得 `Freenode 的知识库 <https://freenode.net/kb/all>`_ 是汝的 friends 哦（雾