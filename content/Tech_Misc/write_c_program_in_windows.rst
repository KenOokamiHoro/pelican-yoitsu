在 Windows 中写 C 程序
====================================================

:slug: write_c_program_in_windows
:lang: zh
:date: 2016-06-13
:tags: software
:desc: (╯￣︿￣)╯ ┻━┻ 又要在 Windows 上写 C 程序啦~
:series: C语言初阶扯淡（雾
:issueid: 38

.. PELICAN_BEGIN_SUMMARY

说着说着一个学年就要过去了啦~

要说这半年（现在不是六月了么）印象最深刻的是啥，就是帮忙解决了身边不少同学的相同的问题……

.. PELICAN_END_SUMMARY

.. contents::

:del:`--farseerfc 果然是先知啊（雾`

    ヨイツの賢狼ホロ 😋, [11.06.16 20:43]
    😂😂

    ヨイツの賢狼ホロ 😋, [11.06.16 20:43]
    /me 刚刚解决了些问题👍

    ヨイツの賢狼ホロ 😋, [11.06.16 20:44]
    解决了些社团同学的问题😂

    farseerfc 😂, [11.06.16 20:44]
    [In reply to ヨイツの賢狼ホロ 😋]
    感覺牙縫裡還夾著頭髮（

    ヨイツの賢狼ホロ 😋, [11.06.16 20:44]
    [In reply to farseerfc 😂]
    😂啥

    莉莉艾塔·K·A (Лилы-Айта К.А.) , [11.06.16 20:44]
    [In reply to farseerfc 😂]
    ?!

    farseerfc 😂, [11.06.16 20:44]
    [In reply to ヨイツの賢狼ホロ 😋]
    一些骨頭在萌狼肚子裡（解決

    ヨイツの賢狼ホロ 😋, [11.06.16 20:45]
    😂

    Felix Yan, [11.06.16 20:45]
    太可怕了

    Felix Yan, [11.06.16 20:45]
    狼最近食欲很好啊

好了玩笑开完了（再纠结这个的话要不要亲自体验一下？😋）。其实就是因为这学期的 C 语言课，然后就栽在用啥写程序这个问题上了……

然而那些搞得咱想 :ruby:`**|😋` 的老师竟然还在用 VC6?? 对，就是那个老掉牙的而且在 Windows 7 以后的系统不好装的 Visual C++ 6 (╯^﹏^)╯ ┻━┻

所以在 :ruby:`问的不耐烦|实在吃不下` 之后，咱决定写点啥了……

最简单的方法：换 Linux 啦~
-----------------------------------------

    此处 Linux = 基于 Linux 内核的操作系统 😂

确实 Linux 上写 C 程序更方便些啦~

* 大多数的 Linux 发行版上都带有 GCC (啥?汝不会连 GCC 都不知道是啥吧 , `赶紧补习一下吧~ <https://zhwp.org/GCC>`_

* 可能还会有手册页,包括标准库的大多数内容.

* 另外,如果想继续深入学习的话,Linux 应该也算一门技能吧\.\.\.\.\.\.

然而:

* Linux 是个操作系统,这就注定了汝可能要花好长的时间先去学习如何使用这个操作系统😂

* \.\.\.\.\.\.

所以咧?

如果汝有心情在这一领域深入下去 (而不是为了通过考试) ,可以尝试一下~ 😋

或者在 Windows 上搭建一个类 Unix 子系统？
-----------------------------------------

最适合觉得装在真机或者虚拟机上太麻烦的人啦~

类 Unix 子系统其实就是运行在 Windows 上的一个程序啦~ 它用来提供POSIX系统调用的API，
这样在 Linux 等类 Unix 系统上写成的程序就可以通过重新编译来在 Windows 下运行啦~

目前名气最大的类 Unix 子系统有两个，分别是 `Cygwin <https://cygwin.com/>`_ 和 `MSYS2 <https://msys2.github.io/>`_ ,
如果非要咱钦定一个的话\.\.\.\.\.\.那一定是 MSYS2 啦,因为有咱喜欢的 pacman 😋

考虑到长度的关系具体怎么安装咱下次再说吧 (挖坑的节奏\.\.\.\.\.\.)

挑一个合适的集成开发环境？
-----------------------------------------

    集成开发环境（Integrated Development Environment，简称IDE）是一种辅助程式开发人员开发软体的应用软体，
    在开发工具内部就可以辅助编写原始码文本、并编译打包成为可用的程序，有些甚至可以设计图形介面。

    IDE通常包括程式语言编辑器、自动构建工具、通常还包括除错器。有些IDE包含编译器／直译器，
    如微软的Microsoft Visual Studio，有些则不包含，如Eclipse、SharpDevelop等，
    这些IDE是通过调用第三方编译器来实现代码的编译工作的。有时IDE还会包含版本控制系统和一些可以设计图形用户界面的工具。
    许多支援物件导向的现代化IDE还包括了类别浏览器、物件检视器、物件结构图。
    虽然目前有一些IDE支援多种程式语言（例如Eclipse、NetBeans、Microsoft Visual Studio），但是一般而言
    ，IDE主要还是针对特定的程式语言而量身打造（例如Visual Basic）。

    ---- `Wikipedia:集成开发环境 <https://zh.wikipedia.org/wiki/%E9%9B%86%E6%88%90%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83>`_

所以已经有先人想到了这个问题制作了很多(?)的集成开发环境啦~
然而咱并没用过 (咱都是直接用 Arch Linux 的😏) ,这里列出几个咱见过的集成开发环境.

* `C-Free <http://www.programarts.com/cfree_ch/>`_ 好像是中国人开发的 IDE,好想要付费的样子 🤔

* `Code::Blocks <http://www.codeblocks.org/>`_ 跨平台的自由软件。

* `Dev-Cpp <http://www.bloodshed.net/devcpp.html>`_ 也是跨平台的自由软件. (话说学校的电脑里明明装着 Dev-Cpp 为啥还用 VC6  (╯￣﹏￣)╯ ┻━┻ )

* `Visual Studio <https://www.zhihu.com/question/40929777/answer/90029159>`_ ,这其实是个大型 IDE，对初学者来讲太重了些。不过最近更新的 Visual Studio 2015 Update 1 加入了 clang 作为编译器的功能，也可以拿来做 C 编译器啦~


GCC+文字编辑器？
-----------------------------------------

其实上面提到的 IDE 里面一定是有编译器的,多半是GCC😂

其实 GCC 是有向 Windows 移植的,就是 MinGW 啦:

    MinGW（Minimalist GNU for Windows），又称mingw32，是将GCC编译器和GNU Binutils移植到Win32平台下的产物，包括一系列头文件（Win32API）、库和可执行文件。

    另有可用于产生32位及64位Windows可执行文件的MinGW-w64项目，是从原本MinGW产生的分支。如今已经独立发展.

    ---- `Wikipedia:MinGW <https://zh.wikipedia.org/wiki/MinGW>`_

实际应用上咧，多半会有已经打包好的 MinGW，可以帮助汝快速的准备工作啦~，例如 `tdm-gcc <https://www.zhihu.com/question/40929777/answer/90015056>`_

然后就是找个合适的文字编辑器啦~ 无论是 `Atom <https://atom.io>`_ , 
`Visual Studio Code <https://www.visualstudio.com/products/code-vs>`_ ,
或者 `Notepad++ <https://notepad-plus-plus.org/>`_ 等等，挑一个自己顺手的就行啦~

-----------------------

好吧咱又算 :ruby:`抛砖引玉|挖坑` 了一次 😂😂😂