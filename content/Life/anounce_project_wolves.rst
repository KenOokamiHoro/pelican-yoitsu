Project Wolves （仮）先行稼动开始~
=============================================

:slug: anounce_project_wolves
:lang: zh
:date: 2017-05-02
:tags: Telegram, Wolves
:desc: 
:series: Project Wolves （仮）
:issueid: 

.. PELICAN_BEGIN_SUMMARY

推出 Project Wolves（虽然名字还是暂定）😋

.. PELICAN_END_SUMMARY

.. contents::

所以这是啥？
----------------------

其实就是个 Telegram Channel 啦 🤦‍ ，虽然有个奇怪的名字，
还随便用 bot 糊了把 mastodon / Twitter 同步 😂

这 Channel 有啥特别之处？
---------------------------

😂 好像真没啥特别的（毕竟才建起来？）

硬塞一个特点的话就是 Writer 都是狼？😂

算了以后慢慢塑造好了……

那么入口呢？
-------------------------

哇汝想来了啊 😋 :del:`勇气可嘉……`

* 如果有 Telegram 的话 ：https://t.me/Project_Wolves

* 或者 Twitter ：https://twitter.com/WolvesOfYoitsu

    Twitter 字数限制比 Mastodon 少啦~

* 或者 Mastodon : https://sn.angry.im/@wolves 

    首先感谢 :fref:`petercxy` 的 instance 😊

    然后转发到 Mastodon 的机器人是咱最近糊出来的，所以可能会炸…… 😂

`````````````````

    [xmppbot] [Lisa] KenOokamiHoro: 老板问我今天晚上加班么我说不加，问我为啥，我说有约。问我是上回周五约我那个么，我说不是，我每周五都有不同的人约。老板想了一下说：你约完把他们都杀了么。

    [[HoroServ]] 他们都被吃掉了

    [ヨイツの賢狼ホロ 😋(🍎🍏👤*)] 😋😋😋

    [Chino 🐇 (Frantic1048 )] 这很萌狼(

😂 

接下来画啥饼？
------------------------------

* 接入 IRC / RSS 支持？

* ……