Title: 怀旧手机游戏不正经研究 - 引子
Date: 2021-11-22 00:00
Category: Life
Slug: legacy_mobile_games_0
Series: 怀旧手机游戏不正经研究
Authors: Horo

为啥人（？）老啦就喜欢怀旧咧？

## 先回忆下咱的手游史

咱最早在手机上玩游戏的时间大概能追溯到十多年前，当时咱住在咱的某一个亲戚家里，作为一个熊孩子（x），偶尔也会摆弄摆弄他的手机。以及到了现在咱还记得那台手机，三星 SGH-i718+。

![Samsung SGH-i718](/images/legacy_mobile_games_0/samsung-sgh-i718-02.jpg)

刚才从网上搜索了下，大概就像这个样子。看到那个 Windows 键的话，~~老学校~~玩家应该已经知道了，这部手机搭载的是 Windows Mobile 系统。（以及搜索的时候不经意间发现网上说这是三星第一台国行 Windows Mobile 手机）

![Bubble Breaker:Amazon.com:Appstore for Android](/images/legacy_mobile_games_0/61kmM0DcyLL.jpg)

虽然内置的游戏只有一个戳泡泡游戏来着（Bubble Breaker?），但是咱不知道怎么发现的（也许是忘了吧）这个手机可以上网，还可以下载 JavaME 游戏来玩，于是接下来的事情就可想而知了……

> 唉，从五块钱 1M 或者 30M 走过来的表示说多了都是泪啊 😂
>
> 以及后来咱也从这位亲戚那里玩到了 Windows Phone 7/8 和 Android 手机，虽然经常是他自己都搞不懂怎么用来请教咱的样子……

后来咱搬去父母家了，虽然他们也忙着做生意经常不着家，于是为了联系方便，就给咱买了一部手机。 Nokia 6700 Slide。

![Nokia 6700 slide](/images/legacy_mobile_games_0/Nokia_6700_slide_silver_front.jpg)

虽然那个时候的咱并不知道啥智能手机塞班系统什么的，但是它也能和咱最早玩到的那部手机一样可以安装游戏。

以及它竟然还支持 3G 网络，加上咱当时用的也是联通卡，于是流量和话费迅速消耗…

然后在不知道什么时候惨遭标准结局（丢了）…… 在挨了顿骂几个月以后，家里还是给买了新手机，还是诺基亚。

![Nokia C7](/images/legacy_mobile_games_0/nokia-c7-ofic-1.jpg)

就是 Nokia C7 啦，后来才发现咱直接跳过了 S60v5 整个时代。（虽然咱还是能发现有同学买了 S60v5 系统的手机来着，例如堪称街机的 5230 ）

![Nokia 5230](/images/legacy_mobile_games_0/nokia-5230-1.jpg)

除了触屏这个显而易见的特性以外，C7 还有当时颇为珍贵的 Wi-Fi 功能。（以及后来才知道当时国内为了推广自有标准 WAPI 强行让国行手机阉割 Wi-Fi 这档事）于是找地方蹭网一度也是咱的活动之一，直到家里装了宽带和无线路由器。

虽然没有数字按键了，玩之前玩过的 Java 游戏有些不方便。但是咱靠它玩到了很多塞班 3 游戏啊，大部分的画面都比那些 Java 游戏好多了。（嗯，咱那时候完全没听说过 N-Gage……）

![Asphalt 5 (2009 video game)](/images/legacy_mobile_games_0/167950.jpg)

例如现在还有更新的狂野飙车（Asphalt）系列，最早是 Gameloft 在 2004 年发表在 NDS 和 N-Gage 的游戏，后来也移植到了 iOS 、Android 等新的手机平台。图片上是咱记忆里在 C7 上玩的 Asphalt 5 。

再后来的某一天呢，咱父母的手机坏了，于是就以再给咱买一台作为交换换走了咱这台 C7。当时 Android 也开始流行起来了，于是鬼使神差间咱买了三星 Galaxy Tab 2 7.0 。

![Samsung Galaxy Tab 2 7.0 P3100](/images/legacy_mobile_games_0/samsung-galaxy-tab-2-2.jpg)

没错，一台能打电话的平板！😂 所以咱那个时候怎么想到买它的呢。

于是接下来就是延续到现在的折腾 Android 的故事了， ~~就和咱接下来要聊的话题没太大关系了……~~

至于 iOS 嘛，咱买的第一部 iPhone 是 iPhone SE，于是就和怀旧更没啥关系了。

>好啦好啦，虽然老的 iOS 和 Android 游戏也有不少佳作啦，但是现在的新系统上面几乎都运行不了那些老游戏啦。
>
>至于去淘一下老手机平板什么的， ~~经费有点不足……~~

## 那么接下来呢？

在看了好几个同样爱好怀旧游戏的爱好者的视频以后，咱也被一股不可名状的引力所吸引开始在网上收购一些老手机了。（以及在自己和亲戚那淘一淘货）

于是接下来咱有点兴趣的问题就是，咱想玩 Java ME/Symbian/N-Gage 游戏的话，用哪部手机比较合适？

> 当然啦，假如自己还有能用的手机的话，那肯定是用现成的最节省成本。于是问题就演变成了如果要淘一淘二手的话哪些型号性价比更高了。

以及有些平台已经有差不多的模拟器了（例如模拟 JavaME 的 [J2ME Loader](https://github.com/nikita36078/J2ME-Loader) 和模拟 Symbian 的 [EKA2L1](https://github.com/EKA2L1/EKA2L1) ），那么在手机上用模拟器是否也还行呢？假如汝不是特别介意手指搓玻璃的话。

> 以及带物理键盘的 Android 手机搭配模拟器会不会效果更好来着，例如上一篇文章里那台多亲 F21 Pro。其它的有按键的 Android 手机（像是三星的~~土豪专用~~心系天下系列和黑莓的最后几部键盘 Android 手机）应该也能类比。
>
> ~~其实还是没钱买~~

那么是不是还可以扩展到其它的老电玩平台呢……像是 3DS/PSV 或者 PS3/Wii 之类的。

> 这大概就是长期计划了……说来刚刚打开某软件搜索价格的时候发现咱之前两百块收的 PSTV 已经涨的飞起了，虽然并没有什么关系（x）

于是接下来就是等快递了。 ~~（连鸽子都能说的这么清新脱俗）~~



