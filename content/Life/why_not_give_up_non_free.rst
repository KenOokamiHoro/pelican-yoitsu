为啥汝不放弃不自由？
==================================================

:slug: why_not_give_up_non_free
:lang: zh
:date: 2016-03-22
:tags: opinions,freedom
:series: 自由,一直在路上
:desc: 是什么让汝甘愿选择不自由而不是放弃封闭走向自由？
:issueid: 17

.. PELICAN_BEGIN_SUMMARY

是什么让汝甘愿选择被不自由限制而不是放弃封闭走向自由？

.. PELICAN_END_SUMMARY

.. contents::

这个算不算引子?
-----------------------------------------------

    解放一个习惯于被奴役的民族比奴役一个习惯于自由的民族更难
    
    ----孟德斯鸠 (启蒙思想家)

:irc:`archlinux-cn` 里的 :fref:`farseerfc` 说自己要离开 Telegram 了,于是用 fishroom 换掉了兢兢业业若干年 :del:`虽然偶尔会黑化` 的阿卡林......

原因好像是 Telegram 有要变的封闭的趋势:

    Peter Cai - PGP C297B594, [21.03.16 20:53]
    我不认为他们会开

    Peter Cai - PGP C297B594, [21.03.16 20:53]
    我也不希望开源。。
    
    farseerfc 😂 0xC13D4796, [21.03.16 20:53]
    從一開始就說要開，然後到現在連協議都封閉了，只有官方授權的客戶端開發者能看到最新協議……
    
    wicast C, [21.03.16 20:53]
    开源也没啥卵用感觉…
    
    Peter Cai - PGP C297B594, [21.03.16 20:53]
    除非能出现第二个freenode
    
    Peter Cai - PGP C297B594, [21.03.16 20:53]
    不然开了等于没用
    
    wicast C, [21.03.16 20:54]
    卧槽…连协议都封闭了么…
    
    teleboto, [21.03.16 20:54]
    [toxsync] (quininer) 其实不如把现有功能做成 xmpp 插件
    
    Peter Cai - PGP C297B594, [21.03.16 20:54]
    我看文档和以前一样
    
    wicast C, [21.03.16 20:54]
    吃枣药丸
    
    Peter Cai - PGP C297B594, [21.03.16 20:54]
    反正本来就看不懂
    
    teleboto, [21.03.16 20:55]
    [toxsync] (yatseni) 是freenode是悲剧吗
    
    farseerfc 😂 0xC13D4796, [21.03.16 20:55]
    cutegram 的開發者抱怨了很久了，所以現在二步驗證什麼的都還在從開源出的客戶端代碼慢慢逆向協議
    
    Peter Cai - PGP C297B594, [21.03.16 20:55]
    https://core.telegram.org/mtproto
    
    teleboto, [21.03.16 20:55]
    [xmppbot] [Lisa] teleboto: ⇪网页标题: MTProto Mobile Protocol
    
    Peter Cai - PGP C297B594, [21.03.16 20:55]
    🌚这个一直是这样
    
    teleboto, [21.03.16 20:55]
    [toxsync] (quininer) 中心服务爱好者快转 wire 吧
    
    Peter Cai - PGP C297B594, [21.03.16 20:56]
    wire有什么好转的。。
    
    Peter Cai - PGP C297B594, [21.03.16 20:56]
    。。还不如转WhatsApp
    
    teleboto, [21.03.16 20:56]
    [toxsync] (quininer) wire 很好啊 =-=
    
    wicast C, [21.03.16 20:56]
    [In reply to farseerfc 😂 0xC13D4796]
    有不开源的部分？
    
    teleboto, [21.03.16 20:56]
    [toxsync] (quininer) wire 是完整的 axolotl 协议实现
    
    teleboto, [21.03.16 20:57]
    [toxsync] (yatseni) qtox 1.3.0有什么改进？
    
    teleboto, [21.03.16 20:57]
    [toxsync] (quininer) yatseni: markdown 支持？
    
    farseerfc 😂 0xC13D4796, [21.03.16 20:57]
    https://core.telegram.org/schema 這邊公開到 layer 23 ， cutegram 支持到 layer 25
    
    teleboto, [21.03.16 20:57]
    [xmppbot] [Lisa] teleboto: ⇪网页标题: Current TL-schema
    
    Peter Cai - PGP C297B594, [21.03.16 20:57]
    telegram至少还有bot API
    
    wicast C, [21.03.16 20:57]
    真是吃枣药丸🌚
    
    farseerfc 😂 0xC13D4796, [21.03.16 20:58]
    [In reply to wicast C]
    這倒沒有的樣子，總之協議現在在代碼裏……
    
    wicast C, [21.03.16 20:58]
    Tg也就前端的东西做的还行
    
    teleboto, [21.03.16 20:58]
    [toxsync] (quininer) bot api 这部分是完全闭源的吧？
    
    Peter Cai - PGP C297B594, [21.03.16 20:59]
    是的。
    
    Peter Cai - PGP C297B594, [21.03.16 20:59]
    但是至少有。。
    
    Peter Cai - PGP C297B594, [21.03.16 20:59]
    其他中心化服务哪个有。。
    
    Peter Cai - PGP C297B594, [21.03.16 20:59]
    wire这玩意连自己艹都没办法
    
    farseerfc 😂 0xC13D4796, [21.03.16 21:00]
    https://github.com/telegramdesktop/tdesktop/blob/master/Telegram/SourceFiles/mtproto/mtpCoreTypes.h#L371 現在 tdesktop 在 layer 49 了已經……
    
    David Huang, [21.03.16 21:00]
    [In reply to Peter Cai - PGP C297B594]
    telegram总有一天要走向封闭的（
    
    teleboto, [21.03.16 21:00]
    [toxsync] (quininer) 不用说"总有一天"，一直是封闭的
    
    Peter Cai - PGP C297B594, [21.03.16 21:01]
    [Forwarded from teleboto]
    [toxsync] (quininer) 不用说"总有一天"，一直是封闭的
    

然后咱突然想到了百合姐的某篇文章:

    在那个世界里我活得很好。即使是遭受重大的人生挫折时也不曾对那个世界感到疲惫，依旧会写代码，写博客。

    可现在，我累了。

    其实我有好些想写博客的材料，但是都没有写。我也有好些项目的想法，可是它们还在我的 TODO wiki 里。Arch Linux 中文社区还有许多要做的事情，可我也不想去做了。

    我不知道是为什么。也许是孤单吧。一直以来都只有自己。即使 Arch Linux 社区，在做事的人也渐渐少了。而曾经在网上认识的朋友，渐渐地都有了自己的生活。随着 Google Hangout 取代 Google Talk、新浪微博取代 Twitter，还有微信，这些封闭的东西取代了自由的工具，可以放有用或者有意思的机器人的地方却越来越荒芜了。

    也许是年纪渐渐大了。青春越来越少，而我却依旧在飘荡。 
    
    ---- `依云's Blog:我想我失去了最后的领地 <http://lilydjwg.is-programmer.com/posts/179502.html>`_   
    
连咱也何尝不是如此? 虽然咱会用像 XMPP , Tox 和 GNU/Linux 一类的自由软件,也曾经试图以 Richard Stallman 为目标,然而还是没法做到. Pia!<(=ｏ ‵-′)ノ☆

最后咱还是不得不承认咱没办法完全放弃不自由呐~ 那么到底是为啥咧?


自由对自己的代价太大  _(:з」∠)_
--------------------------------------------

( 也就是获得自由带来的损失比不自由所带来的限制更大? )

举一个咱自己的栗子 (\´･ω･\`) :

前几天入手一部预装 :ruby:`Windows|Windoge` 10 的平板,想当然的咱想装上 Arch Linux 啦,结果当咱装上以后:

* 触屏和网卡是废的 ( (╯T∧T)╯ ┻━┻ 装 Arch Linux 时没有网可是致命的 ) 

* 分辨率太高以致于默认的字太小😂😂😂 ( GNOME 的 HiDPI 只能整数倍调整差评,KDE plasma 调一调倒是能看了...... )

于是只好留着 Windows 😂😂😂

所谓的自由根本没法实现？
-------------------------------------------

比如 iOS 和 Windows Phone ,汝都没法更换上面的操作系统,也不知道上面的系统都干了些啥......

尽管 Telegram 越来越封闭 , 现在它还有相对开放的 Client / Bot API,而更 "流行" 的 QQ / 微信 / WhatsAPP / Skype /.... 咧?

尽管 Android 手机 / Windows 设备可以刷机,然而流行的硬件却没有一个是彻底开放的,谁能相信后门不会在固件或是硬件上?

 (╯°△°)╯ ┻━┻

没法带领身边的朋友逃离不自由?
--------------------------------------------

还是举咱自己的栗子?  (╯>▽<)╯ ┻━┻ :

* 尽管咱卸载了 QQ ,但还是装在了另一台手机上......

    周围的人和学校的辅导员都还在用 QQ , 咱试图向社团的同学安利 Telegram ,然而......
    
     (╯=﹁"﹁=)╯ ┻━┻ :del:`(亏还把自己称作计算机精英协会......干脆全吃掉然后解散好了(雾))`
     
* 虽然咱自己很少用 Windows 了,然而经常被拉去修 Windows 电脑:

    Baka Windows 总是会越来越慢,而原因基本上都是招来了病毒,木马或是oo全家桶😂😂
    
    就算咱耐心的一个一个卸载,过几天还是会以同样的理由被拉回去重修......
    
    以后就干脆直接重装系统了 (╯\´︿\`)╯ ┻━┻
    
    \/me :del:`再遇到这样的事干脆直接砸了好了`
     
* 明明有那么多种纯文字的标记语言可以用可咱还是要用 Office (ノ=Д=)ノ┻━┻

    (╯T▽T)╯ ┻━┻ 哪怕是 LibreOffice 也好啊 (然而并没有)
    
    (╯=﹁"﹁=)╯ ┻━┻ 没 LibreOffice 哪怕是新版的 Microsoft Office 能部分兼容 OpenDocument 也好啊 (然而几乎全都是 03 )
    
    嘿嘿嘿......像造点大新闻了呐~
    
    :del:`某学校 M$ Office 用户一夜间全部失踪` ( 雾 again~ ) 
    
等等等等......

-------------------------------------

    「真是太感谢了。我终于能融入这个新世界了。」

    「我们没法像过去那样自由自在了，但是……」

    「但是，这个世上还有我们的位置，还有我们的任务。虽然故乡还未曾得见，但不要哭鼻子哦？不能给这个年轻人添麻烦。」
    
给渴望自由的人的空间越来越少了呐~所谓的自由,大概一直在路上吧......
