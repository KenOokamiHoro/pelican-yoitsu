咱和（）的 2020 年？
====================================================

:slug: five_years
:lang: zh
:date: 2021-03-11
:tags: 周年纪念
:desc: 就不要不合时宜的问为啥隔了接近三个月才写了……

就不要 :del:`不合时宜的` 问为啥隔了接近三个月才写了…… （？）

以及第四年的总结 
`在咱的 Matters 上。 <https://matters.news/@kenookamihoro/%E5%92%B1%E5%92%8C-%E7%9A%84-2019-%E5%B9%B4-zdpuAv9K8mhb7NUJ2o4pe9tjgnra82PuVfdpCtjfquAXp5BU1>`_

为啥这一年只写了八篇文章？
------------------------------------------------------

啊这……咱写字的地方越来越分散了啊……

* `咱的 WordPress 上 <https://blog.horo.moe/>`_ 还有两篇文章。
* `咱的 Matters 上 <https://matters.news/@kenookamihoro>`_ 除了只和 Matters 有关的文章以外，
  还有大概十篇文章没写在这里。

.. image:: /images/five_years/google_search_console_2020.png
  :alt: Google Search Console 

Google Search Console 的表现，是怎么样呢？

这一年……？
--------------------------------------------------------

因为众所周知的原因（COVID-19 啦……），于是几乎都是呆在家里， :del:`然后被批判一番不务正业……`
那就记一下这一年玩过的游戏好了。

* DJMax Respect V ,除了正式发售时突然降价引起抢先体验玩家不满和全程联网外加笨蛋反作弊插件以外，
  只是 DJMax 这个老字号就能让咱接受前面的大部分缺点了。
* 女神异闻录5 皇家版，给女神异闻录系列出加强版是 ATLUS 老传统了。
* 集合啦！动物森友会，成功登上咱 Switch 游戏时间排行第一。
* Project DIVA MEGA39s，咱为什么要买它…… FTDX 加上 DLC 它不香么……
* Fluffy Store 和 ATRI -My Dear Moments- ，两部被推荐来看后觉得不错的视觉小说。（所以
  fault - SILENCE THE PEDANT 还是没有出）
* Untitled Goose Game，其实 2019 年在 PS4 上玩过了，今年借着在 Steam 上发售又玩了一遍。
  （然而并没有人一起玩更新的双人模式……）
* 妄想破绽和原神……一言难尽*2。
* Halo: The Master Chief Collection，用手柄坑完了除了 ODST 以外的正常难度剧情，嗯……
  至少咱的老笔记本还跑得动。
* 女神异闻录5 乱战 魅影攻手，第一次录下了全程 :del:`然后给友人做素材工具人（？）`
* Cyberpunk 2077，除了分不清是 Bug 还是剧情演出以外还好啦。 :del:`这话到底是赞美还是批评……`
* Spice & Wolf VR2, `“什麼?你說今天12/10還有2077? 2077能有赫蘿嗎?” <https://steamcommunity.com/id/048738940/recommended/1348700/>`_ 
  新的动画鉴赏模式虽然短了点……
* Stellaris， :del:`P社玩家没有一个无辜的……`

当然除了这些以外还有些一直在玩的老游戏，也就是那些音游什么的。PSN 给咱发年度报告的时候去年游玩时间最长的
游戏还是 FTDX ……

----

剩下的一时想不起来了，就先这样吧。