啥？？两年了？？？
====================================================

:slug: two_years
:lang: zh
:date: 2017-12-31
:tags:
:desc: 其实还差几天😂

.. PELICAN_BEGIN_SUMMARY

    其实还差几天😂

.. PELICAN_END_SUMMARY

其实还差几天😂

.. contents::

状况
---------------------------

    其实咱四月才装上 Piwik ，于是下面的数据都是四月以后的了……

.. image:: /images/two_years/hits.png
    :alt: PV

嗯……每天大概有二三十个人来看咱的样子（

.. image:: /images/two_years/maps.png
    :alt: 地图

嗯…… 

.. image:: /images/two_years/devices.png
    :alt: 设备s

嗯……大概知道这个 vivo 是谁撑起来的了 😂

.. image:: /images/two_years/os.png
    :alt: 操作系统谱系

嗯……用 Android 和 GNU/Linux 的越来越多了嘛，不知道有没有一天能超过 Windows 用户）

.. image:: /images/two_years/os_and_browsers.png
    :alt: os_and_browsers.png

嗯……和大局一样，Chrome依旧领跑。（怎么还有 QQ 浏览器和 MIUI ？？😂

嗯…… 😂

活动
-------------------------

* 今年写的文章只有去年的一半……看来摸了不少🐟啊😂

* 搞了个 X230，自己刷上了 Coreboot 换上了新的无线网卡，装上了 Parabola（ :del:`似乎走上了一条不归路……` 😂）

* 自己建的 Telegram 群全都解散啦😂看来咱就是没有管理的能力啊……

* 突然从 FT 跳进了一个更大的坑，没错就是 :del:`NIMABE` BEMANI 😂 

* 然后突然又和某个如梦方醒的学长一起学算法去了……

* 给别人安利音乐游戏然后自己被虐成渣…… 😂

* 剩下的全忘啦😂

计划
--------------------------

    其实有没有计划好像都没啥区别😂

* 想考虑放个 PayPal 捐款链接上去（就是不知道哪里获取……）

* 或者像 uva 一样放个 coinhive js 上去（不过好像不少人讨厌的样子……）

* 不知道该写些啥……

期望
---------------------------

不只要成为回忆而已~

（诶咱在说些啥）

