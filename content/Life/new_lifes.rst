各自开始的新生活
====================================================

:slug: new_lifes
:lang: zh
:date: 2019-05-27
:tags:
:desc: 总有一些开始和结束值得留念。

总有一些开始和结束值得留念。

结束了。
-----------------------------------------------------------

.. code-block:: text

    布偶君 <shadowrz@disroot.org>
    离别告知，能扩散多远就扩散多远
    如果你们能看到这句话的话，
    我告诉你们点东西。

    以前的我，各种意义上都很正常。嗯，这很好。
    也许是直到和你们接触开始，
    家长发现我自己总是和别人有不一样的想法和性格。

    然后，各种各样的矛盾接踵而至……
    ……我的性格影响了好多人——它变坏了。
    ……争执，吵闹，宛如发疯……

    昨天我和父母吵了起来，他们一定要看我在用电话做什么。我要是不让他们看，就会觉得是电话害了我(我现在才发现)
    然后我只有妥协的命(不然他们不会让我玩的，这样的话也会是离别)。
    于是他们(只)看到了 #archlinux-cn-offtopic 的聊天记录，
    然后……他们……
    ……觉得你们都是一群疯子……
    希望我永远不要记起你们……
    还说做不到就要不停吃药，死也可以……
    还要彻底删掉你们，让你们碰不到我……

    …………

    为什么，为什么会变成这样啊！！！！
    我和你们这些年……为什么让我变成了一个不能在世界存活(没人接受我的性格)的状态啊！！！！！！！

    告诉我回答啊啊啊啊啊啊啊啊啊啊啊！！！！！！！！！！！！！！

    -- 
    布偶君 | 雨宫恋叶 | ShadowRZ (同一身份)
    2019-05-22

Update (2019/05/23, ref https://wxw.moe/@ShadowRZ/102142677608282951 ):

.. code-block:: text

    我再声明几点：

    1. 只是退群+不和你们往来而已
    2. 不想反对父母，不要对我提这样的建议
    3. 对话都给父母看了（除私信）
    4. 以后不在处理任何通知，包括小号
    5. 我不会再依存于你们
    6. 现在父母也希望非常非常了解我

    望周知。
    2019-05-23
    （为什么好好的告别，说了三天）

愿他安好。

（不知道以后还有没有机会在“没有黑暗的地方”相见……）

开始了。
-----------------------------------------------------------

    开始了？

    https://twitter.com/Ken_Ookami_Horo/status/1128784581400727553 

* 去深圳啦 ~
* 在去之前买了很多稀奇古怪（？）的家伙（例如某地狱牌交换机，某粗粮牌玻璃板和某雨林牌泡面盖什么的 😂）
* 见到了大学同学 x1 （以后说不定会有 x2 , x3 ?）
* 当然不是去玩的 😢 （结束了长达六个月的失学无业辣鸡时光（x））
* 从公司收到台新笔记本，然后发现手边的内存装不上去只好现买 😂
* 说不定有机会……？
* 就这样。

