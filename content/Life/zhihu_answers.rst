知乎自答两则
============================================

:slug: zhihu_answers
:lang: zh
:date: 2016-08-20
:tags: zhihu,opinions,凑数
:issue_id: 49


.. PELICAN_BEGIN_SUMMARY

学 :fref:`farseerfc` ,顺便测试一下咱写的 creatissue 脚本 😂😂

.. PELICAN_END_SUMMARY

.. contents::

懒得加内文里的链接了呢~

使用 Arch Linux 做桌面有何优势和注意事项？
---------------------------------------------------------

https://www.zhihu.com/question/46322733/answer/101649684

既然是前 Debian Testing 用户，就认为汝有一定的 Linux 发行版使用经验呗~

顺便猜汝问的是“日常使用 Arch Linux”呗.

1. Arch Linux 有啥特性？

其实 Arch Linux 和其它发行版一样就只是一个发行版呐~然而每一个发行版的生态不一样，
Arch 这边信奉的是以 “Keep It Simple, Stupid” 为核心的 Arch 之道啦。哦对了，还有一个名字叫做 Pacman 的软件包管理器。
滚动更新算不算是一个特性咧？

2. Arch Linux 的优势？

应该是软件更新速度快和（更新时）比较稳定？ （可以说都是拜 Arch 的滚动发行版属性所赐呗~）

还有比较完善的 Wiki ~

还有比较完善的 Wiki ~

还有比较完善的 Wiki ~
（重要的话说三遍2333）

Arch compared to other distributions (简体中文) 上有 Arch Linux 和几个发行版的比较，去看下呗~

3. 安装 Arch Linux 要注意啥？

Beginners' guide (简体中文) 和 Installation guide (简体中文) 都是非常好的参考来源呐~
按 wiki 的说法，前者适合新安装 Arch Linux 的用户，后者适合有经验的 Linux 用户呗。

4. 使用 Arch Linux 时要注意啥？

首先要学习如何使用 Arch Linux 的招牌（？） pacman 啦。
Pacman (简体中文) 上有些常用命令，如果汝有使用其它软件包管理器 （例如 Debian 里的 apt）的经验，可以看一下 Pacman/Rosetta ，
那里列出了几种不同的软件包管理器的不同命令的比较。

关于各种操作（例如安装各类软件，外观定制，系统维护等等） General recommendations 里有各种操作的索引，去看一下呗~

如果遇到了问题，ArchWiki ，Arch Linux Forums ， Arch Linux 中文论坛 和 IRC channels (简体中文) 都是解决问题的合适的地方咯~ （当然要遵守各自的规则啦）

5. 啥？有人说 Arch Linux 容易滚挂？

到底是从啥时候开始有这个说法的

怎样才能尽量避免archlinux滚挂？ - Arch Linux 这里的几个回答非常棒，咱就简单的总结一下呗~

System maintenance (简体中文) 有些系统维护的常见技巧。

大家都认为长期不更新的 Arch Linux 更容易挂……所以至少要记得定期更新一下系统吧~

订阅 Arch Linux 新闻 （邮件列表 arch-announce  或者RSS： https://www.archlinux.org/feeds/news/ ）消息不是很频繁，一般是在有重大更新时会提醒大家。

不要在一无所知的情况下打开 [testing] 仓库。如果想打开的话，
Arch Linux TU 兼 Developer 成员 @晏然FelixYan 写了篇 Arch Linux [testing] 系列仓库简介 可以参考。

有时 pacman 会提示生成了一些 .pacnew 文件，这是为了避免覆盖一个之前被修改过的已存在文件呐
，汝最好在更新完毕以后马上合并这些改动啦~（如果不处理，不当的配置可能导致软件功能出问题，甚至完全无法使用。）

不要使用某些 pacman 命令 （例如 --force，但是官方发通告要求这样做时除外~）

最后，手边最好准备一个 Live 环境（万一一不小心把系统搞挂了……）

6. 最后……

每个人都可以为 Arch Linux 贡献自己的一分力量，如果汝有意愿的话， wiki 上的 Getting involved 页面有各种贡献的途径呐~

还有一件事…… 欢迎来 Arch Linux 中文社区玩！
（ IRC 频道 #archlinux-cn @ freenode 是中心，同时还有 Telegram ， XMPP，Tox 和 Gitter 多平台联通，可能是第二水的 IRC 中文频道？）
其他平台的加入方式在 IRC 上问问其他人就好啦~

Arch Linux的用户都有理想主义倾向吗？
--------------------------------------
https://www.zhihu.com/question/49439472/answer/116599094


（话说知乎啥时候能内置水平线标记……）

其实不止 Arch Linux ，几大主流发行版不都是一群有着相同理想主义倾向的用户/开发者们一手构建起来的呗~

Debian ：以创建一个自由的类 Unix 操作系统为己任，大部分尽可能的采用自由软件
（后来从 Debian 衍生出的完全自由的发行版 gNewSense 获得了自由软件基金会的赞助 ）。

Fedora ：和 Debian 同样更多的专注于自由软件。和 Debian 偏向稳定不同，
Fedora 的开发者更多的会和上游紧密协作，尽可能的快速推动新技术的应用和完善。

openSUSE：这个咱没用过，只知道他/她/它们做了个很出色的系统配置工具 YaST ，
还有能给 openSUSE 和其他各种发行版构建软件包的 Open Build Service (OBS)。
至于 openSUSE 为啥没有其它发行版那么流行的原因，活跃于 openSUSE 社区的苏姐的回答应该解释的比较全面 ：
openSUSE 的人气为何远不如 Ubuntu 和 Fedora ？ - 瑪麗蘇的回答

Gentoo：应该是除了 LFS （Linux From Scratch）以外定制程度最高的 Linux 发行版，
用户可以自由的选择自己喜欢的组件，无论是安装方式，需要的程序，
以至于init 程序（ Gentoo 是目前为数不多的不默认采用 Systemd 的 Linux 发行版之一）。
许多用户喜欢的就是 Gentoo 的高度可定制性，
例如 长期使用Arch，Gentoo等滚动更新的发行版是怎样的一种体验？ - 李小的回答 和
长期使用Arch，Gentoo等滚动更新的发行版是怎样的一种体验？ - ZX Huo 的回答 。

Arch Linux：自称 " a lightweight and flexible Linux® distribution that tries to Keep It Simple." ，
一个轻量化，可定制，试图遵循 KISS 原则 （ Keep It Simple, Stupid，对应中文为“保持简单，且一目了然”）的 Linux 发行版。
简洁，现代，实用和以用户为中心构成了 Arch Linux 用户和开发者们一贯共识的 “Arch 之道”（Arch Linux - ArchWiki）

（Ubuntu ？那是啥，好吃吗？啦啦啦……）

关于 Arch Linux 的可定制性，现任 Trusted User 之一的 fc farseer 这么写到：

  我自己用 Ubuntu 從 8.04 經歷多次版本升級升到 12.04 ，期間雙系統裝裹 OpenSuSE 和Arch ，
  然後在 OpenSuSE 上完成了畢業設計（那時候只有suse提供了Xen補丁內核，這是我的畢業設計需要），
  之後本科畢業後新臺機直接裝 Arch，現在3年多了一切穩定完好。這 3 年多我經歷了 Arch 從 rc.conf 到 systemd 的轉變，
  經歷了 grub legacy 到 grub 2 的轉變，換過好幾個 DE 和 WM 然後現在穩定在 Awesome3.4 兩年多，
  系統分區從原本的 ext4 換到過 btrfs 分區然後現在組 btrfs raid1 ，我換過顯卡，換過好幾塊硬盤，
  期間滾掛過好幾次然後都修好了，自己作死折騰btrfs壞過也從備份中恢復好了。重要的是這個系統一直活着而且活得很好，
  pacman.log裏的裝機歷史能一直追溯到3年前裝的第一個包，我知道這裏面只有我需要的包，
  只有我做過的配置，發生任何問題我都知道是系統的哪裏的問題。這是 Arch給我的安心感，
  是Ubuntu不能給我的。反觀我用Ubuntu的那段時間，每次 dist-upgrade 都要麼立刻掛掉要麼用一段時間掛掉，
  要麼就是升級的方案實在太將就然後換新的重裝。都說 debian穩定ubuntu穩定，
  而那時的我沒有任何穩定的感覺，那時的我害怕每一個小包的升級，因爲我不知道升級了一個包之後會對別的包造成什麼不可預知的後果。
  服務器系統那種有管理員管理的計算機集羣需要的穩定性，和桌面用戶需要的穩定性，在我看來是不同的概念。
  我不怕一個升級之後東西壞掉然後需要我花兩個小時找方案把它修好，
  但是我怕爲了某個新版本的庫而不得不升級的時候，
  整個系統都變得面目全非導致我不得不花兩個小時重裝系統，然後這個全新的系統我不再認識了。

  --长期使用Arch，Gentoo等滚动更新的发行版是怎样的一种体验？ - fc farseer 的回答

所以嘛，既然选择了滚动更新的发行版，就要学会适应这种快速的变化呗~

另外还有一句，Arch Linux 的滚动更新模型是不支持部分升级的。

https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported

还有，Archer 哪里说要秒杀各大软件公司了啦 ？明明咱们连有多少用户都不怎么在意：

  许多 Linux 发行版都试图变得更“用户友好”，Arch Linux 则一直是，永远会是“以用户为中心”。
  此发行版是为了满足贡献者的需求，而不是为了吸引尽可能多的用户。
  Arch 适用于乐于自己动手的用户，他们愿意花时间阅读文档，解决自己的问题。

  报告问题、完善 Wiki 社区文档、为其它用户提供技术支持。
  Arch 用户仓库 收集用户贡献的软件包。Arch 开发者都是志愿者，活跃的贡献者很快就能称为开发人员。

不要以偏概全好不好……
