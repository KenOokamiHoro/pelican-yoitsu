啥？一年了？？
====================================================

:slug: one_year
:lang: zh
:date: 2017-01-17
:tags:
:desc: 真的么😂
:issueid: 57

.. PELICAN_BEGIN_SUMMARY

    真的么😂

.. PELICAN_END_SUMMARY

.. contents::

啥？这样一年就过去了？？😂

咱这一年都干了啥？
-----------------------

* :del:`觅食……` 😋
* 注册了个 Telegram 帐号，然后去 #archlinux-cn 吹水， :del:`再删号` ……
* 买了 Surface Book ，又从社团借（？）来不少好玩的东西。
* 一个小时里总有那么几十分钟在迷茫😂，然后干了不少可笑的事（比如把 GitHub 帐号删了几天以后又重新注册这样的……）
* 沉迷音游不能自拔（虽然手残……）😂
* 为了 Project DIVA Future Tone 买了 PS4 😂

    .. image:: /images/PDFT.jpg
        :alt: 就是这个

    ⬆️ 就是这个😂
* 赶工粗制滥造了两套 Pelican 主题（根本就没有达到可以复用的层次……）😂
* 挖了不知道多少坑没填 😂😭

最近沉迷 Future Tone 不能自拔 😂
--------------------------------------

虽然一直被虐……

.. image:: /images/one_year/2.jpg
    :alt: 就是这个

:del:`一定是因为玩这个的人不多……`

.. image:: /images/one_year/3.jpg
    :alt: 就是这个

/me 手残没法治😂

.. image:: /images/one_year/4.png
    :alt: Extreme 就只有被虐的份😂

Extreme 就只有被虐的份😂

接下来咱要干啥？
-------------------------

* :del:`接着觅食……` 😋
* 整理下长草的 wiki 然后放点东西上去……
* 写些小程序（和微信那个啥关系都没有……）
* 为社团下学期 GNU/Linux 桌面培训（自称）准备讲义😂
* :del:`继续玩 Project DIVA Future Tone ……`
