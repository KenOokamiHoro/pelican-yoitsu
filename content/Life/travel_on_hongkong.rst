香港之行s
===========================================================

:slug: travel_on_hongkong
:lang: zh
:date: 2019-08-27
:tags: Travel,Hong Kong
:desc: 咕咕咕……

这篇文章其实并不是去之后几天写的，期间的各种情况 :del:`可能` 肯定
已经发生了各种变化，所以只能用来当作纪念了（？）

:del:`咱去深圳有很大的原因是为了这个（x）`

通行证（中国内地公民限定）
------------------------------------------------------------
只有中国内地居民前往香港需要准备一个叫做往来港澳通行证的证件才能前往
香港和澳门（其它国家或地区的公民需要什么准备才能前往香港咱就没有深究啦~）。

* 如果汝的户籍所在地不能申请个人旅游签注也不用担心，团队旅游签注过关时
  不再要求查验团队名单，所以可以直接用。
* 深圳户籍居民另可办理一年多次赴港签注，每周最多可前往香港一次。
* 介于某企鹅公司的总部在深圳的缘故（大雾），办理通行证需要预约，
  而预约通行证办理需要在微信上完成 🌚

:del:`就当作 15 块钱买张去香港的门票好了（大雾）`

.. image:: /images/travel_on_hongkong/IMG_20190622_090831.jpg
   :alt: 落马洲支线管制站前 

一些清单（？）
---------------------------------------------------------------

---------------------------------------------------------------
过境
---------------------------------------------------------------
从深圳前往香港的途径有好几种：

* 福田口岸（4号线福田口岸站）和罗湖口岸（1号线罗湖站附近），通关
  后前往港铁落马洲/罗湖站换乘东铁线。

* 在深圳北站/福田站搭乘广深港高速铁路前往香港西九龙站。

* 深圳湾口岸和皇岗口岸（但是咱没去过不知道细节）

---------------------------------------------------------------
通信
---------------------------------------------------------------
到了香港之后汝中国内地的手机卡就算是漫游了咯，虽然最近联通和电信在
搞啥“促销”（1元/M，25元后当日不计费，200M后限速），不过还是觉得不太够用？

* 口岸周围或者网上好像可以买到若干日用的上网卡（但是咱没买过）

* 港铁九龙塘/旺角东站有 SmarTone 的自助售货机，可以买到当地的储值卡（预付费 SIM 卡）。

* 土豪玩家可以考虑 Google Fi（大嘘），或者当地的手机运营商合约（称作“上台”）。

.. image:: /images/travel_on_hongkong/IMG_20190727_120609.jpg
   :alt: 港铁九龙塘站的自助售货机

如果有计划经常往返香港和内地，建议准备一张香港的手机卡，不仅能省钱还有其它用处（嘘……）


---------------------------------------------------------------
消费
---------------------------------------------------------------
虽然国内的几家大头（银联，支付宝和微信支付）在香港逐渐普及起来了，但是不少地方还是只收港币
现金的（例如港铁站的服务中心、街上的小型商店等等）。所以……

* 可以在内地换好港币现金，可能会被收一笔有人会觉得很亏的手续费。
* 也可以在内地通过线上购汇再转账的方式购入一定量的港币，在香港提取（例如通过中国银行的
  Mastercard 借记卡）。不过取款和查询余额也要手续费……
* ……

顺便也推荐办一张八达通啦，几乎到处都能用。（咱会说还能当 amusement IC 卡用嘛（x）

`八达通 Octopus <https://www.octopus.com.hk/>`_


一些印象
---------------------------------------------------------------

---------------------------------------------------------------
港铁
---------------------------------------------------------------

作为全球最繁忙的铁路系统之一（拼不过北京地铁的原因可能是北京人多（雾）），
咱首先的印象就是快。高峰期几乎一辆一辆的来（诶？）

.. image:: /images/travel_on_hongkong/IMG_20190811_133029.jpg
   :alt: 港铁荃湾站分散的闸机

因为没有安检，所以车站的闸机可以设置的很松散。东铁线因为是在曾经九广铁路的
基础上发展起来的，不少车站都在地面上，甚至可以实现入闸后直接上车。 

.. image:: /images/travel_on_hongkong/IMG_20190709_183232.jpg
    :alt: 市区线的车厢

:del:`头一次见到带那么多灯的车厢（x`

以及运营时间很长（普遍是从 05：30-次日01：30，某些情况下例外），除了东铁线的过境车站
（罗湖和落马洲，因为两个口岸开的晚关的早）

.. image:: /images/travel_on_hongkong/IMG_20190622_135809.jpg
    :alt: 某次的票价

速度快加上人均 GDP 的差异带来的影响就是票价，和深圳地铁全程最高 14 元（不含乘坐
11 号线商务车厢的票价，说实话这个商务车厢的设计好像也是从港铁东铁线上学来的耶）
相比，港铁的票价……说多了都是泪啊 😂

（上面那张图里 40 多港币的票价大多数是因为东铁线过境车站（落马洲和罗湖）
的票价比剩下的都贵的缘故）

.. image:: https://upload.wikimedia.org/wikipedia/commons/4/45/HK_Kln_Tong_MTR_Station_%E4%B9%9D%E9%BE%8D%E5%A1%98%E7%AB%99_platform_%E9%A0%AD%E7%AD%89%E8%BB%8A_First_Class_Octopus_%E5%85%AB%E9%81%94%E9%80%9A_terminal_point_evening_Oct-2013.JPG
    :alt: 东铁线的头等车厢

东铁线的头等车厢，额外费用是是当程东铁线车费：

* 或者东铁线最低票价，如果有核准头等车厢的记录但最短乘车途径不含东铁线的话。
* 该程车程涉及马鞍山线车程而另一站不在大围以北（例如：车公庙↔乐富），则须缴付相等于该段东铁线（大围↔九龙塘）
  的车程普通等车费作为“头等额外费”。
* 头等车厢不设补票，如乘客未持有效头等车票（头等单程票和已取得头等确认的八达通）
  而进入头等车厢，一旦被检票员发现，须一律缴交附加费 HK$500。

看深圳地铁11号线的商务车厢票价多简单，直接就是票价的两倍
（最短换乘路线中如果不包含11号线时除外，4元）……

此外港铁还有连接的换乘站（例如香港（东涌线和机场快线）和中环（荃湾线和港岛线），两个
车站的付费区互相连接），和不相连的换乘站（例如九龙（东涌线），柯士甸（西铁线）和香港西九龙（高速铁路））

（虽然好像没什么值得记的……）

顺便再提一句，北京地铁 4/14/16号线和大兴线，杭州地铁1/5号线和深圳地铁4号线都是
港铁内地的合资公司（北京京港地铁有限公司和杭州杭港地铁有限公司）
或全资子公司（港铁轨道交通（深圳）有限公司）运营的，汝些许能在这些
线路上面看到港铁的一些影子。

港铁甚至还能影响内地的地铁公司，
`例如深圳地铁那个神似港铁的 Logo （大嘘） <https://jkcreativedesign.blogspot.com/2015/08/logo.html>`_

---------------------------------------------------------------
高速铁路
---------------------------------------------------------------

就是广深港高速铁路的香港段啦，港铁称为“高速铁路”。（虽然从标志到
设施还是那熟悉的港铁味道（？））

.. image:: /images/travel_on_hongkong/IMG_20190818_185147.jpg
    :alt: The Austins 里的指向牌，上面有高速铁路的标志

The Austins 里的指向牌，上面有高速铁路的标志。

.. image:: /images/travel_on_hongkong/MVIMG_20190818_185936.jpg
    :alt: 香港西九龙站的高速铁路售票机

香港西九龙站的高速铁路售票机，能用八达通付款同时也能为八达通增值。

.. image:: /images/travel_on_hongkong/IMG_20190818_100742.jpg
    :alt: 港铁动感号高速动车组的外部

中国大陆唯一一款出口型高速动车组——港铁动感号高速动车组。

    这车上是有 WiFi 的，但是内地段上需要内地手机号或微信登录才能使用，
    而且不能浏览某些网站。香港段上就没有这些问题。

.. image:: /images/travel_on_hongkong/IMG_20190818_104138.jpg
    :alt: 两岸口岸

一线之隔的内地和香港口岸，内地口岸相当于香港中的一块飞地（？）

.. image:: /images/travel_on_hongkong/IMG_20190818_191425.jpg
    :alt: 准备上车

杂记
---------------------------------------------------------------
.. image:: /images/travel_on_hongkong/IMG_20190629_220616.jpg
    :alt: ……

终于摸到原版机啦（大嘘），:del:`以及魔理沙怎么成 tama 猫了……`

.. image:: /images/travel_on_hongkong/IMG_20190629_103403.jpg
    :alt: ……

香港的街机就是用的 1HK$ 的辅币，每局价格 6-12 HK$ 不等
（SOUND VOLTEX 这里是 2HK$ = 1 CREDIT）

.. image:: /images/travel_on_hongkong/IMG_20190629_142434.jpg
    :alt: 深海旗鱼酥

深海旗鱼酥，为啥咱看一次就想笑一次呢…… 🤣

.. image:: /images/travel_on_hongkong/IMG_0547.jpg
    :alt: 停止工作

……已停止运行……

----

.. image:: /images/travel_on_hongkong/IMG_20190709_221225.jpg
    :alt: 再见还是欢迎回来？

再见还是欢迎回来？ 😂
