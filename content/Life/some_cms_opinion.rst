简单总结一下咱自己用过的一些 CMS
============================================

:slug: some_cms_opinion
:lang: zh
:date: 2016-03-14
:tags: CMS,opinions,凑数
:issueid: 13

.. PELICAN_BEGIN_SUMMARY

仔细数数咱也用过不少 :ruby:`CMS|内容管理系统` 了呐~，或许咱应该小小的总结一下？

PS:本文会带有大量个人的主观意见😂😂


.. PELICAN_END_SUMMARY

.. contents::

WordPress:大众和普通的CMS
---------------------------------------------------

WordPress 的大名想必各位都知道吧。

    WordPress是一个注重美学、易用性和网络标准的个人信息发布平台。WordPress虽为免费的开源软件，但其价值无法用金钱来衡量。

    WordPress的图形设计在性能上易于操作、易于浏览；在外观上优雅大方、风格清新、色彩诱人。

    使用WordPress可以搭建功能强大的网络信息发布平台，但更多的是应用于个性化的博客。
    针对博客的应用，WordPress能让您省却对后台技术的担心，集中精力做好网站的内容。

    ---- `WordPress 中文首页 <https://cn.wordpress.org>`_

WordPress 也是咱第一个用的内容管理系统啦，作为希望用户专注于内容的 CMS ，WordPress 有几点做的非常棒：

* 内置可视化编辑器，无需掌握大量标记语法就可以开始写文章。

* 每次大版本更新都会更新一套主题 (Twenty 系列),都非常赞~

* 五分钟安装向导名副其实 (真的只要五分钟一个 WordPress 网站就可以投入使用了2333)

当然缺点也是有的 :

* 作为用 :del:`世界最好的` PHP 语言编写的 Web应用,需要一个资瓷 PHP 的 Web 服务器来运行😂😂

* 树大招风,WordPress 也容易成为垃圾评论和骇客的目标 ( 前几星期的 Linux Mint 事件 就是骇客通过 WordPress入侵了他们的服务器 😂😂😂 )

* 用默认主题显得同质化,自定义主题又太复杂......

所以咱最后还是换掉了 WordPress 😂😂😂😂

MediaWiki:潜力无限的Wiki平台
---------------------------------------------------------

没听过 MediaWiki 汝总该听说过维基百科吧,维基百科就是运行在维基媒体基金会开发的 MediaWiki 上呐~

    MediaWiki是一个最初为维基百科量身打造的自由wiki套件，用PHP语言写成。

    现在，非营利的维基媒体基金会下辖的所有wiki项目、互联网上的众多其他wiki网站也基于此套软件搭建。

    ---- `MediaWiki.org <https://www.mediawiki.org>`_

简单来说 MediaWiki 是一套 Wiki 系统,不过通过扩展也可以实现各种其他功能 ( 例如 `WikiData <https://www.wikidata.org>`_ ) .

咱要说 MediaWiki 又有啥优点咧?

* 因为是 Wiki 系统嘛,所以方便 :ruby:`协作|一同填坑` 😂😂

* 文档丰富,基本上覆盖了大部分内容 ( 给用户的帮助在重写ing~ )

* 扩展丰富,可以实现更多功能

当然还是老样子,缺点也是有的:

* 和 WordPress 一样,作为用 :del:`世界最好的` PHP 语言编写的 Web应用,需要一个资瓷 PHP 的 Web 服务器来运行😂😂

* (可能是唯一?) 不太适合博客型网站 (因为不方便生成RSS)

* 部件繁杂,不知道哪里出问题就吐核了 (不过会有堆栈跟踪,偶尔会派上些用场😂😂)

* :del:`没有权限汪们想要的精确到条目的权限控制😂😂`

* MediaWiki 标记太复杂啦 (╯・＿・)╯ ┻━┻  ( 或者可以 `安装可视化编辑器 </tech/visualeditor_for_mediawiki.html>`_ ,可还是好复杂 (╯X﹏X)╯ ┻━┻  )

不过咱还在坚持用它当咱自己的资料库呗~

Ghost:轻博客的开端?
----------------------------------------------

    Ghost是用JavaScript编写的博客平台，基于MIT许可证开放源代码。Ghost的设计主旨是简化个人网站发布以及网上出版的过程。

    Ghost是一款个人博客系统，它是使用Node.js语言和MySQL数据库开发的，同时支持MySQL、MariaDB、SQLite和PostgreSQL。用户可以在支持Node.js的服务器上使用自己的博客。

    -- `Wikipedia 上的 "Ghost (博客平台)" 条目 <https://zh.wikipedia.org/wiki/Ghost_%28%E5%8D%9A%E5%AE%A2%E5%B9%B3%E5%8F%B0%29>`_

咱曾经 `安装过 Ghost </tech/ghost_blog_archlinux.html>`_ ,安装过程看着挺吓人,但是按照官方的文档来也可以一次成功~

咱的第一印象就是:比 WordPress 轻多了,真的.

Ghost 最大的优点是原生支持 Markdown 和 HTML ,汝就可以愉快的书写咯~~

不过 Ghost 是 Node.js 应用，安装 Node.js 首先就是个大公程。

另外作为开发中的软件，还有很大的空间……

Pelican:笔记本?
---------------------------------------------------

第一次接触到 Pelican 是 :fref:`farseerfc` 的博客+安利 (雾),
然后正为 MediaWiki 的 RSS 扩展苦手的咱干脆就换成了 Pelican.

farseerfc 说 Pelican 有这些优点:

* 純 Python 實現。 這意味着我可以換用任何 Python 解釋器而不必擔心兼容性問題。比如我就換成了 PyPy。

    因为是纯 Python 实现,可以 `通过Travis-CI 自动生成 html <http://farseerfc.me/travis-push-to-github-pages-blog.html>`_,
    也可以通过 msys2 在 Windows 上运行.没有方便的 Linux 环境再也不是偷懒的理由了呐~

* 多語言支持。因爲 Pelican 的作者似乎是個法國人。不過這個似乎大部分人不需要…… 我是想儘量把一篇博客寫成三種語言作爲鍛鍊吧。

    fc 的博客就有三个语言啦~(虽然咱现在没用到)

* ReST 。這樣我就可以用 Leo 的 @auto-rst 直接寫 ReST了。簡單方便快捷有效。

    ReST 的学习曲线有些陡峭,但掌握之后就是一马平川了哟☺🙂😄

咱说 Pelican 还有这些优点:

* 生成的是静态网站,找个 Web 服务器就可以放 (或者放在 Github Pages 上,极客风格 (?) 十足 )😂😂😂

* 使用的是 jinja 模板引擎 (咱照着默认的 simple 主题做了现在这套主题, :del:`虽然只是能看而已` 😂😂😂)

不过 Pelican 也不是完美的:

* 静态网站的评论是个难题啊 ( 只能靠第三方服务,例如 Disqus )

* 没有类似 WordPress , MediaWiki 和 Ghost 的文章编辑界面 ( 虽然 retext 很好用~ )

* Linux 风格的 Makefile 让迁移有些小小的难度 ( 不然为啥会在Windows 上用msys2...... )

不过现在咱还要接着用 Pelican 呐~😂😂
