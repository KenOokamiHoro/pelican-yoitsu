舞律炫步体（tu）验（cao）
====================================================

:slug: playing_dancerush_stardom_chinese
:lang: zh
:date: 2019-02-10
:tags: 音游, BEMANI, DANCERUSH
:desc: 咱摸到 DRS 啦（大嘘）

嗯…… 大概就是标题那样，在咕咕咕和没零件交错后咱玩到了
国行版的 DANCERUSH STARDOM ，中文名叫舞律炫步 😂

等等 DANCERUSH STARDOM 是什么东西？
------------------------------------------------------
DANCERUSH STARDOM 是KONAMI目前（2019年1月）最新的 BEMANI 系列游戏。
事先以 STEPSTAR, ShuffleDancerz, DANCE GENERATIONS, 和 DANCE RUSH 的名义进行了场测,
直到 JAEPO 2018 展会才以 DANCERUSH STARDOM 作为正式名义推出。

DANCERUSH STARDOM 是一款和舞蹈进化非常相似的跳舞机.
但并没有使用之前舞蹈进化使用的微软Kinect for Windows来捕捉玩家的动作，
而是采用了英特尔 RealSense 410 (RS410)作为新的动作感应器， DANCERUSH 还有一个非常大
的感应地板, 作为主游玩感应设备。

`可以去它们的官方网站看到具体是啥样子 <https://p.eagate.573.jp/game/dan/1st/entrance.html>`_ 。

早些时候就有了世宇科技（？）要代理 DRS 的消息，还在中山展出过（可以叫场测嘛……），
虽然说的是一月开始铺货，但是因为下方的踏板没能和框体一起送到的缘故咕咕咕到
2 月 2 日才开始运营……

那么这游戏该怎么玩？
-----------------------------------------------------
`官方有教程视频 <https://www.youtube.com/watch?v=M8d5Wjl9QtQ>`_ 。

那国行有啥区别咧？
------------------------------------------------------
.. image:: /images/playing_dancerush_stardom_chinese/cabinet.jpg
    :alt: 国行框体的标题画面

^ 国行框体的标题画面（那个“请在按下面按”是什么鬼？）

中国专用 e-amusement 了解一下？

* 和以前引进的 BEMANI 街机一样，肯定不能连官方的 e-amusement 啦，于是就搞出了个
  “中国专用 e-amusement” 那样的东西
* 所以理所当然的砍掉了刷卡器和数字键盘。登录？又是微信扫码的日常套路……

.. image:: /images/playing_dancerush_stardom_chinese/login.jpg
    :alt: 扫码登录

* 价格奇坑（待会儿再讲）
* 既然某 Y 字头网站在国内不存在，自然没有录影功能。
* 默认简体字界面，当然语音也中文化了。不过能在开始时选择其它语言，和官机一样。
* 没有储星星等各种解禁元素，取而代之的是星星提升等级解禁……
* 然而全曲只有 76 首（20190130），而且一如既往的删了些？（例如咱没见到 Butterfly）
* emmm ......

为啥说国行价钱坑？
------------------------------------------------------
默认只有 1 Stage （官机哪怕是 LIGHT 也是两 Stage 保证咧），后来发现世宇还搞了个啥会员？

.. image:: /images/playing_dancerush_stardom_chinese/eamucn_pricing.jpg
    :alt: 中国专用 e-amusement 的会员价格

就这个，不知道这会员是一个游戏一个游戏的卖还是和官方一样（e-amusement 是有 Subscription 的，
然后不同的游戏可能会单独卖个像会员的东西，例如 beatmania IIDX 的 Platium Members Card）。
顺便说一句这个 am 点是他们自己搞出来的啥，1 am 点 = 1 元人民币，看起来挺像 PASELI 的哈……

于是不氪 VIP 是进不了 EXTRA STAGE 的（官方的话是 STANDARD 模式两曲合计 180 分以上就能进，以及这游戏
满分是 100 分），更甚不氪会员都没法存档，嗯可以的……

   drs这个收费 30和50都是废物.jpg -- 一位不愿意透露姓名的 L 姓玩家

以及氪了 VIP 的话 Gauge 不会下降？（EXTRA STAGE 本来是 10 次 Bad 就游戏结束的，咱打完发现一把 Bad 的后
开始怀疑这点了……）

那咱觉得怎么样呢？
------------------------------------------------------
啊有的玩已经很不容易了……

如果汝有音游基础（特别是街机音游玩家）或者舞蹈基础（推测？）的话，这个游戏还是挺简单的。
（反正咱玩了一下午可以瞎蹦蹦 9 级曲了，目前最难 10 级不敢试……）

游戏除了一般的教程的话还有几种常见动作的指导和训练，刚上手（严格来说是上足？）的时候建议玩玩看。

杂记
------------------------------------------------------
* 虽然教程汉化了还是有路人直接跳过教程…… 于是就能见到 emmmm……

* 和隔壁鹅舞形成了鲜明的对比……

.. image:: /images/playing_dancerush_stardom_chinese/compare.jpg
    :alt: 鲜明的对比？

