Windows Subsystem for Linux + Arch Linux
===================================================

:slug: wsl_with_arch_linux
:lang: zh
:date: 2016-10-04
:tags: Arch Linux,Windows
:desc: 在 Bash on Windows 上运行 Arch Linux
:updated: 2020-05-15
:issueid: 51

.. PELICAN_BEGIN_SUMMARY

把 Bash on Ubuntu on Windows 上的 Ubuntu 换成 Arch Linux 😋

.. PELICAN_END_SUMMARY

.. contents::

这啥？😂
---------------------

从 Windows 10 Insider Preview 开始，加入了 Windows Subsystem for Linux (适用于 Linux 的 Windows 子系统) 功能.

Windows Subsystem for Linux（简称WSL）是一个为在Windows 10上能够原生运行 Linux 二进制可执行文件（ELF 格式）的兼容层。
它是由微软与 Canonical 公司合作开发，目标是使纯正的 Ubuntu Trusty Tahr映像能下载和解压到用户的本地计算机，并且映像内的工具和实用工具能在此子系统上原生运行。

WSL提供了一个微软开发的 Linux 兼容内核接口（不包含Linux代码），来自 Ubuntu 的用户模式二进制文件在其上运行

WSL 的具体应用就是 Bash on Ubuntu on Windows 啦，在 Windows 上实现了一个 Ubuntu 子系统。

和常见的 Windows 上运行 Linux 的一系列方法相比：

* 相比虚拟机，不必运行整个系统，相应减少了资源占用（当然不保证提高编译效率🌚

* 相比 Cygwin，Msys2 一类的解决方案，运行在 Ubuntu 上的软件（可能）不需要重新编译就能在 Bash on Ubuntu on Windows 上使用，也能使用 Ubuntu 软件仓库提供的软件包

* ……

听起来是不是很诱人啊😋

然而……
------------------------------‘

上面的子系统是 Ubuntu…… (╯•̀-•́)╯ ┻━┻

上面的子系统是 Ubuntu……  (╯・﹏・)╯ ┻━┻

上面的子系统是 Ubuntu……  (╯＠△＠)╯ ┻━┻

:del:`重要的事情说三遍` 😂😂

于是把里面的 Ubuntu 换成其它系统的想法就应运而生了……

其实已经有人这么做过了虽然并不完美 😂：
https://github.com/Microsoft/BashOnWindows/issues/992

直到最近 Insider Preview 更新到 14396，终于实现了 chroot 系统调用
（ `-\> Change Log <https://msdn.microsoft.com/en-us/commandline/wsl/release_notes>`_ ），
才使得在 WSL 上运行其它发行版的可用性更近了一步😂

作为现 Arch Linux 用户，当然想在 WSL 上运行 Arch Linux 啦😋

    在好多年之后，不光 bash 换成了 wsl，系统也放在了一些更难发现的地方里，所以下面的方法不一定还能用了…… Microsoft 官方也建议用 DistroLauncher 生成自定义发行版了。

    

----

开工 😏
-----------------------------

首先得首先是要把 Bash on Ubuntu on Windows 装上 😂

    * 更新到最新的 Windows 10 Insider Preview （现在是 14396.1000 ）

    * 从”设置-更新与恢复-适用于开发人员“中把开发人员模式打开。

    * 从”控制面板-程序和功能-启用或关闭 Windows 功能“中打开”适用于 Linux 的 Windows 子系统“

    * 重新启动，打开命令提示符或者 Windows PowerShell ，输入 bash 回车然后按提示完成安装……

首先需要一个现成的 Arch Linux ，在上面用 arch-install-scripts 装好一个新的 Arch Linux，然后打包成 Tarball。

然后进入 Bash on Ubuntu on Windows ，把刚刚的 Tarball 解开到某个位置。

    如果汝手边没有可用的 Linux 系统的话，也可以试试 ArchISO 的 rootfs：

    * 从 ArchISO 中提取出 /arch/x86_64/airoot.sfs 文件放在 Bash on Ubuntu on Windows 能读取的目录下，然后复制/移动到能写入的目录。

    * 安装  squashfs-tools ，然后以 root 用户运行 :code:`unsquashfs airoot.sfs` ，把 ArchISO 的 Live 系统提取出来。

然后把 Bash 窗口关掉，到 C:\\Users\\<这是汝的用户名>\\AppData\\Local\\Lxss 文件夹中。

    这个文件夹可能在文件资源管理器里看不到咧？可以选择在文件夹选项
    中把“隐藏受保护的操作系统文件”选项取消，或者直接在导航栏输入路径打开。

rootfs 文件夹就是 Bash 中的 / 啦，从里面找到刚解压出来的 tarball 的文件夹放在 lxss 目录下。

把原来的 rootfs 文件夹重命名一下，再把刚解压出来的 tarball 的文件夹重命名为rootfs。

打开命令提示符或者 Windows PowerShell ，:del:`输入 bash` 等等先别着急打开 bash ，
先用 :code:`lxrun /setdefaultuser root` 把默认的用户换成root
（或者其它已经在 tarball 的 /etc/passwd 里的用户，例如 AOSC 的 tarball 里默认还会有一个 aosc 用户）

然后再打开 bash ，Duang~

.. image:: /images/WSL_Arch/1.png
    :alt: WSL with Arch Linux

那个 screenfetch 是咱后来装的 😂

现在还有点小问题 😂
-------------------------------

fakeroot 在 WSL 里有点问题（可是咱忘了记下来日志了觉得咱自己好菜啊），只好写个假的 fakeroot 糊弄过去 😂：

.. code-block:: bash

    #!/bin/bash

    if [ "$1" = "-v" ]; then
        echo 1.0
        exit
    fi

    export FAKEROOTKEY=1

    exec "$@"

    unset FAKEROOTKEY

现在记得在 /etc/pacman.conf 里忽略 fakeroot 包 _(:з」∠)_

按 `WSL 的 Change Log <https://msdn.microsoft.com/en-us/commandline/wsl/release_notes>`_ ,现在最新的 Build 14396
会在某些 Socket 连接（例如 ssh / X ）时蓝屏报错 “ATTEMPTED EXECUTE OF NOEXECUTE MEMORY” ，这个只能等他们修复了_(:з」∠)_

.. ……
    不知道说什么好。
    还是不知道该说啥。
    算啦，谁何尝不是另一个人世界中的过客，
    少了咱一个又怎么样。
    嗯……
