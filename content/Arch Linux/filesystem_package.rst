一瞥 Arch Linux 中的 filesystem 软件包
===================================================

:slug: filesystem_package
:lang: zh
:date: 2016-07-25
:tags: Arch Linux,packages
:desc: 闲来无事，然后就没有然后啦😂
:issueid: 7

.. PELICAN_BEGIN_SUMMARY

闲来无事，然后就没有然后啦😂

.. PELICAN_END_SUMMARY

.. contents::

动机？
----------------------------------------
今天看了看知乎上的某个问题：
`长期使用Arch，Gentoo等滚动更新的发行版是怎样的一种体验？ <https://www.zhihu.com/question/37720991>`_

好几个 Arch Linux 用户都是在一开始列出了 :code:`/var/log/pacman.log` 的第一行，用的是 :code:`head` 命令。

.. code-block:: bash

    # 咱没有各位的历史悠久啦~
    $ head -1 /var/log/pacman.log
    [2014-11-18 19:37] installed filesystem (2014.10-1)

然后这个 filesystem 是啥咧？ 

然后 :fref:`farseerfc` 这么写到：

    pacman -Ql filesystem 看下它都有什麼唄。
    這個包就是提供最基礎的 Arch Linux 的目錄結構和一些不屬於任何別的包的配置文件。
    這樣 Arch Linux 裏面任何系統文件都在 pacman 管轄範圍內了。

嗯……😋

😋 开始行动 
------------------------------------------
那就首先照着 fc 的写法来一遍呗~

.. code-block:: bash

    [horo@yoitsu-surfacebook ~]$ pacman -Ql filesystem
    filesystem /bin
    filesystem /boot/
    filesystem /dev/
    filesystem /etc/
    filesystem /etc/arch-release 
    filesystem /etc/crypttab 
    filesystem /etc/fstab
    filesystem /etc/group
    filesystem /etc/gshadow
    filesystem /etc/host.conf
    filesystem /etc/hosts
    filesystem /etc/issue
    filesystem /etc/ld.so.conf
    filesystem /etc/ld.so.conf.d/
    filesystem /etc/motd
    filesystem /etc/mtab
    filesystem /etc/nsswitch.conf
    filesystem /etc/passwd
    filesystem /etc/profile
    filesystem /etc/profile.d/
    filesystem /etc/profile.d/locale.sh
    filesystem /etc/resolv.conf
    filesystem /etc/securetty
    filesystem /etc/shadow
    filesystem /etc/shells
    filesystem /etc/skel/
    filesystem /home/
    filesystem /lib
    filesystem /lib64
    filesystem /mnt/
    filesystem /opt/
    filesystem /proc/
    filesystem /root/
    filesystem /run/
    filesystem /sbin
    filesystem /srv/
    filesystem /srv/ftp/
    filesystem /srv/http/
    filesystem /sys/
    filesystem /tmp/
    filesystem /usr/
    filesystem /usr/bin/
    filesystem /usr/include/
    filesystem /usr/lib/
    filesystem /usr/lib/modprobe.d/
    filesystem /usr/lib/modprobe.d/usb-load-ehci-first.conf
    filesystem /usr/lib/os-release
    filesystem /usr/lib64
    filesystem /usr/local/
    filesystem /usr/local/bin/
    filesystem /usr/local/etc/
    filesystem /usr/local/games/
    filesystem /usr/local/include/
    filesystem /usr/local/lib/
    filesystem /usr/local/man/
    filesystem /usr/local/sbin/
    filesystem /usr/local/share/
    filesystem /usr/local/share/man
    filesystem /usr/local/src/
    filesystem /usr/sbin
    filesystem /usr/share/
    filesystem /usr/share/man/
    filesystem /usr/share/man/man1/
    filesystem /usr/share/man/man2/
    filesystem /usr/share/man/man3/
    filesystem /usr/share/man/man4/
    filesystem /usr/share/man/man5/
    filesystem /usr/share/man/man6/
    filesystem /usr/share/man/man7/
    filesystem /usr/share/man/man7/archlinux.7.gz
    filesystem /usr/share/man/man8/
    filesystem /usr/share/misc/
    filesystem /usr/src/
    filesystem /var/
    filesystem /var/cache/
    filesystem /var/empty/
    filesystem /var/games/
    filesystem /var/lib/
    filesystem /var/lib/misc/
    filesystem /var/local/
    filesystem /var/lock
    filesystem /var/log/
    filesystem /var/log/old/
    filesystem /var/mail
    filesystem /var/opt/
    filesystem /var/run
    filesystem /var/spool/
    filesystem /var/spool/mail/
    filesystem /var/tmp/

嗯 filesystem 的作用大概有这几个：

* 首先摆好一个 Linux 发行版基本的目录结构。

    大多数 Linux 发行版的目录结构都是遵循 `文件系统层次结构标准 <https://zh.wikipedia.org/zh/文件系统层次结构标准>`_
    的啦，Arch 也不例外~

* 然后放些 Arch 特有的文件

    例如默认设置啦（ :del:`这不是废话么` ），甚至还有一个手册页（汝不妨试试 :code:`man archlinux` ?）

所以嘛……
--------------------------

* 这下所有的文件都在 pacman 的纪录之下了呗~

    `ArchWiki 上有篇条目写着如何寻找不属于任何软件包的文件 <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Identify_files_not_owned_by_any_package>`_


* :del:`所以咱很无聊咯~` 😂😂
