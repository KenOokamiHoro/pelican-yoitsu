在 Arch Linux 上用 Prosody 搭建 XMPP 服务器
==========================================================

:slug: prosody_on_archlinux
:lang: zh
:date: 2017-02-05
:tags: Arch Linux,XMPP,Prosody
:desc: 在 Arch Linux 上用 Prosody 搭建 XMPP 服务器 😋
:issueid: 58


.. PELICAN_BEGIN_SUMMARY

在 Arch Linux 上用 Prosody 搭建 XMPP 服务器😋

.. PELICAN_END_SUMMARY

.. contents::

XMPP 是啥？
----------------------------

😂 连这个都不知道？

    https://tonghuix.io/2015/03/xmpp-chat/

    ⇪网页标题：弃用QQ和微信！全面转向基于XMPP(Jabber)的即时聊天

这篇文章简单的介绍了下 XMPP 的基本特点，也推荐了些不同平台的客户端。

为啥是 Prosody ？
-----------------------------

因为好像大家都推荐😂

😂 那么怎么搞？
-----------------------------

按惯例先上 ArchWiki : https://wiki.archlinux.org/index.php/Prosody 😂

和 Prosody 自己的文档: https://prosody.im/doc/

首先安装 Prodosy （和它的依赖）：

    # pacman -S prosody lua51-sec

lua51-sec 为 Prosody 提供了加密支持。所以还是装上它呗~

然后修改下配置文件 /etc/prosody/prosody.cfg.lua :

    顺便说一句 Lua 的注释是每一行前面的连字号呐~

下面写些咱做的修改😂

* 在 modules_enabled 中取消启用 version 和 uptime 模块，顺便启动些其他的模块。

* 如果需要允许在客户端上注册的话，把 allow_registration 设置成 true 。

* 开启 TLS 加密：

.. code-block :: lua

    ssl = {
        --- 私钥文件的路径
        key = "/etc/prosody/certs/privkey.pem";
        --- 证书文件的路径
        certificate = "/etc/prosody/certs/fullchain.pem";
        --- 要点是证书和私钥要对 prosody 用户可读😂
        --- 咱一开始就是没搞对然后 prosody 默默的以不加密模式运行了 😂
        --- 可以使用的协议，Prosody 0.10以上版本，可以使用"tlsv1_2+"；
        --- 若是较旧版本，可以使用"sslv23"开启所有协议支持，
        --- 然后在options中关闭除TLSv1.2之外的协议支持。
        --- 但是咱因为兼容性需要用的还是 tlsv1+ 😂
        protocol = "tlsv1+";
        --- 加密选项，
        options = {
            --- 旧版加上这几行关掉老协议
            "no_tlsv1",
            "no_tlsv1_1",
            "no_sslv2",
            "no_sslv3",
            --- 剩下的其实是 prosody 的默认选项😂
            "no_ticket",
            "no_compression",
            "cipher_server_preference",
            "single_dh_use",
            "single_ecdh_use"
            };
        --- 为客户端到服务器（c2s）和服务器到服务器（s2s）打开认证
        verify = { "peer", "peer" };
        --- ciphers 是所使用的加密算法，写法就是 openSSL 的 cipher string 😂
        --- 这是默认配置
        ciphers = "HIGH+kEDH:HIGH+kEECDH:HIGH:!PSK:!SRP:!3DES:!aNULL";
        --- 某个全是前向安全算法的配置 😂
        --- 有没有大佬推荐个更好的配置啊 😂
        ciphers = "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
        --- dhparam 文件的路径，要对 prosody 用户可读😂
        dhparam = "/etc/prosody/certs/dhparams.pem";
    }

    -- Force clients to use encrypted connections? This option will
    -- prevent clients from authenticating unless they are using encryption.

    -- 对客户端到服务器强制启用加密
    c2s_require_encryption = true
    -- 对服务器到服务器强制启用加密
    s2s_require_encryption = true

    -- Force certificate authentication for server-to-server connections?
    -- This provides ideal security, but requires servers you communicate
    -- with to support encryption AND present valid, trusted certificates.
    -- NOTE: Your version of LuaSec must support certificate verification!
    -- For more information see https://prosody.im/doc/s2s#security
    -- 是否验证远程服务器的证书？

    s2s_secure_auth = true

    -- Many servers don't support encryption or have invalid or self-signed
    -- certificates. You can list domains here that will not be required to
    -- authenticate using certificates. They will be authenticated using DNS.
    -- 为某些不资瓷加密或者没设置好的辣鸡服务器换用DNS验证……

    --s2s_insecure_domains = { "gmail.com" }

    -- Even if you leave s2s_secure_auth disabled, you can still require valid
    -- certificates for some domains by specifying a list here.
    -- 如果 s2s_secure_auth = false ，可以在这里设置那些服务器一定启用验证
    --s2s_secure_domains = { "jabber.org" }

    -- Select the authentication backend to use. The 'internal' providers
    -- use Prosody's configured data storage to store the authentication data.
    -- To allow Prosody to offer secure authentication mechanisms to clients, the
    -- default provider stores passwords in plaintext. If you do not trust your
    -- server please see https://prosody.im/doc/modules/mod_auth_internal_hashed
    -- for information about using the hashed backend.
    -- 认证方式，"internal_hashed" 一般情况下就够了。
    authentication = "internal_hashed"

* 某些其它设置……

    --- 新用户注册通知，会发到管理员的帐号上。
    registration_notification = "User $username just registered on $host from $ip"
    --- 如果启用了 welcome 插件，在这里自定义欢迎消息
    welcome_message = ""

试用😂
-----------------------

然后测试下配置文件对不对(啥输出都没有就正常😂)

    lua -p /etc/prosody/prosody.cfg.lua

一切正常的话就启动服务试试呗~

    # systemctl start prosody

如果没开启允许客户端注册的话，用 prosodyctl 注册账户

    # prosodyctl adduser <JID>

----

参考来源 😂
----------------------------

    https://wiki.archlinux.org/index.php/Prosody

    ⇪网页标题：Prosody - ArchWiki

    https://blog.ayanami.re/archives/3/

    ⇪网页标题：自建 XMPP 的二三事 - El Virilea via Kralisch
