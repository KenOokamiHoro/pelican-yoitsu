#archlinux-cn 活动手册（?）
==========================================================

:slug: archlinux_cn_channel_guide
:lang: zh
:date: 2016-11-22
:tags: Arch Linux
:desc: Arch Linux 中文社区聊天频道 #archlinux-cn 伪活动手册 😋
:issueid: 53


.. PELICAN_BEGIN_SUMMARY

Arch Linux 中文社区聊天频道 #archlinux-cn :del:`伪` 活动手册 😋

.. PELICAN_END_SUMMARY

.. contents::

:del:`话说又一个月啥也没写了啊……果然不够才思泉涌……`

最近 #archlinux-cn 的新人越来越多了呐~ :del:`(这不是很好么，咱就不愁没的吃了是呗~)` 😋

于是为了 :ruby:`帮助新人|凑数` 这篇文章就出炉啦 😂


前言
-----------------------

如果还不知道 Arch Linux 中文社区是啥的话……😋

去看看这个呗
`Arch Linux 中文社区非官方生存手册 <https://blog.yoitsu.moe/arch-linux/life/archlinux_cn_community_unoffical_newbie_guide.html>`_

各个接入点
-----------------------

    :del:`我们的同志遍布五湖四海，甚至打入了某些组织的内部……`

简单说就是连接到 Arch Linux 中文社区聊天频道的方法有很多啦……

* IRC

    IRC（Internet Relay Chat的缩写，“因特网中继聊天”）是一种通过网络的即时聊天方式。
    其主要用于群体聊天，但同样也可以用于个人对个人的聊天。

    就是 :irc:`archlinux-cn` 啦~

    直接戳上面的链接会打开 freenode 的 webchat 界面，
    输入昵称然后完成Google 的 NoCaptcha 验证后，点击连接（Connect）就可以进入本IRC频道啦~

    正在使用某种 IRC 客户端的话参阅汝所使用的 IRC 客户端的文档在 irc.freenode.net 上加入
    #archlinux-cn 就可以啦~

    想进一步了解 IRC 的使用方法的话推荐下维基百科的教程呗：

    `Wikipedia:IRC教程 <https://zh.wikipedia.org/wiki/Wikipedia:IRC%E6%95%99%E7%A8%8B>`_

* XMPP

    XMPP（Extensible Messaging and Presence Protocol，前称Jabber）
    是一种以XML为基础的开放式即时通讯协定，是经由网际网路工程工作小组（IETF）通过的网际网路标准。

    习惯 XMPP 的用户可以添加 talk@archlinuxcn.org 为联系人，成功添加以后会收到一条消息呗~

        talk@archlinuxcn.org: 欢迎加入 Arch Linux 中文 XMPP 群！

        注意！非Arch系统的问题请提前声明系统

        你的昵称默认为「<这里应该是个随机昵称然而咱不记得是啥了……>」，
        使用「-nick 新昵称」来修改。

    设置一个新昵称然后打下招呼吧 😄

* Telegram

    Telegram Messenger 是一个跨平台的即时通讯软件，它的客户端是自由及开放源代码软件，
    但是它的服务器是专有软件。

    使用者可以相互交换加密与自解构的讯息，以及相片、影片、文件，支援所有的档案类型。

    官方提供移动端(Android、iOS、Windows Phone)、
    桌面端(Windows、macOS、Linux)和网页端(Web version)应用；

    同时官方开放应用程序接口，因此会有一些第三方的客户端。

    后来为了防止破坏 Telegram 的群链接就取消啦，所以先用其它的方法进入频道，
    把自己的 Telegram Username 告诉其它人，再让已经在 Telegram 群里的人邀请呗~

* Gitter

    用 Github 或者 Twitter 账户登录，然后加入
    https://gitter.im/archlinuxcn/generic 这个聊天室呗~

* Martix

    用汝自己的 Martix 客户端 （比较常见的是以前叫做 vector 的 Riot）添加
    #freenode_#archlinux-cn:matrix.org
    这个房间就可以啦~

熟读 topic 确保人身安全 （雾 😂
-------------------------------------

Topic set by farseerfc
(~farseerfc@archlinux/tu/farseerfc) on Fri, 04 Nov 2016 21:11:36

Arch Linux 欢迎喵 \*^-^\* | :del:`卖萌`

不是 arch 请事先声明发行版 | 因为大家默认都是 Arch 😂 另外衍生发行版也要明确

ttf-dejavu 注意新闻 | FFmpeg 3.1.1 | 中间大概会插两行官网的新闻和新的软件包

https://wiki.archlinux.org | ArchWiki

https://bbs.archlinuxcn.org | 中文论坛

https://github.com/archlinuxcn | 中文社区软件仓库

长文 https://cfp.vim-cn.com | 因为 IRC 没断行于是直接贴长文会刷屏😂，用法见那个网站

贴图 https://img.vim-cn.com | 😂 如果用 Telegram 或者 Martix 的话可以直接发图，传话机器人能完成转换

不要对着 teleboto xmppbot 说话，它们都是桥接机器人，人类的名字在它们后面 | 桥接机器人就是打通各个聊天软件的机器人啦~ 😋

机器人列表
---------------------

除了上面说的桥接机器人以外，目前待命中的机器人还有(括号里是操作者的名字)：

具体的功能还是各位慢慢发掘呗~

* xmppbot 里的 Lisa ( lilydjwg )

* varia ( gauge )

* labots ( lastavengers )

* Horobot ( FiveYellowMice )

* MoBot ( Youmu )

分支频道
------------------------

* :irc:`archlinux-cn-offtopic` ,计划中的水群频道，然而一直没实现 😂

----

说好的 FAQ 呢  (╯‵_′)╯ ┻━┻ ？？
