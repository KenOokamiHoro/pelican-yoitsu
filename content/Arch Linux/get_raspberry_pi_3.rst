入手 Raspberry Pi 3
===================================================

:slug: get_raspberry_pi_3
:lang: zh
:date: 2016-04-12
:tags: Arch Linux,devices,rpi
:series: Arch Linux
:desc: 社团为了完成某个和咱没有关系的项目买了块树莓派3诶，然而不妨碍咱玩就是了~
:issueid: 8

.. PELICAN_BEGIN_SUMMARY

社团为了完成某个和咱没有关系的项目买了块树莓派3诶，然而不妨碍咱玩就是了~

.. PELICAN_END_SUMMARY

.. contents::

汝不会连树莓派是啥都不知道吧 😂😂
-----------------------------------

    树莓派（英语：Raspberry Pi），是一款基于Linux的单板机电脑。它由英国的树莓派基金会所开发，目的是以低价硬件及自由软件刺激在学校的基本计算机科学教育。

    树莓派基金会提供了基于ARM架构的Debian、Arch Linux和Fedora等的发行版供大众下载，还计划提供支持Python作为主要编程语言，支持BBC BASIC(通过RISC OS映像或者Linux的"Brandy Basic"克隆)、C语言和Perl等编程语言。

    ---- `Wikipedia:树莓派 <https://zh.wikipedia.org/zh-cn/%E6%A0%91%E8%8E%93%E6%B4%BE>`_

若干准备动作 _(:з」∠)_
---------------------------

树莓派3的配置大概像这样：

    * :ruby:`SoC|系统单晶片` : Broadcom BCM2837（CPU，GPU DSP和SDRAM、USB）
    
    * CPU : ARM Cortex-A53 64位 (ARMv8系列) 1.2GHz (四核心)
    
    * GPU :Broadcom VideoCore IV
    
    * RAM : 1GB
    
    * 和上一代比多内置了了无线网卡和蓝牙模块
    
当然因为只是块板子嘛,还要准备些外设:

* 一个显示器 ( 当然其实可以用 :ruby:`GPIO|通用输入/输出` 连接串口 ) 和 HDMI 线

* 一张 MicroSD 卡 

* USB 接口的鼠标和键盘

* 至少 5V/2A 的电源适配器和 Micro USB 线 ( 这个给汝的手机充电的插座和数据线应该能满足呗~ )

* 以太网线 ( 如果汝打算用有线连接 )

于是在准备完成以后......

为树莓派写入系统 ( Arch Linux ARM )
-------------------------------------

作为 Arch Linux 用户,当然最好是接着用 Arch 啦,还好有 Arch Linux ARM 😂

在 `Arch Linux ARM 文档 <https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3#installation>`_ 
的光辉照耀下,一切正常.

然后把写入好系统的 MicroSD 卡插入树莓派,连上显示器和电源,开机.

为树莓派写入系统 ( Raspbian )
------------------------------------

Rsapbian 是基于 Debian 修改成的,树莓派基金会官方支持的操作系统啦~

`下载在这里 <https://www.raspberrypi.org/downloads/raspbian/>`_ , `安装教程在这里 <https://www.raspberrypi.org/documentation/installation/installing-images/README.md>`_ .

:del:`然而后面的操作都是在 Arch Linux ARM 上的 😂😂`

安装 Xfce 桌面环境
-------------------------

首先要连上网,准备好接受 ArchWiki :del:`的圣光` 呗~

    * `有线网络 <https://wiki.archlinux.org/index.php/Network_configuration>`_
    
    * `无线网络 <https://wiki.archlinux.org/index.php/Wireless_network_configuration>`_

默认的 alarm 用户竟然没有 sudo 权限 Pia!<(=ｏ ‵-′)ノ☆ , 不过没啥关系, 
`接着被圣光笼罩呗~ <https://wiki.archlinux.org/index.php/Sudo>`_

默认的 http://mirror.archlinuxarm.org/ 会自动解析到离用户较近的镜像,
也可以手动换成 :ruby:`中科大|崔土豪` 源, `方法在这 <https://lug.ustc.edu.cn/wiki/mirrors/help/archlinuxarm>`_ .

换好源以后更新一下系统 ( 记得两个 y 是强制刷新~ )

.. code-block:: bash

    $ sudo pacman -Syyu

然后安装需要的软件 ( xorg xfce 和一个登录管理器,这里是 lightdm )

.. code-block:: bash

    $ sudo pacman -S xorg xfce lightdm lightdm-greeter
    
至于如何配置 lightdm 咱就偷下懒推荐一下百合仙子 :fref:`lilydjwg` 的 `从 slim 到 lightdm <http://lilydjwg.is-programmer.com/2015/11/4/from-slim-to-lightdm.187512.html>`_ 啦~

然后启动 :code:`lightdm` 服务:

.. code-block:: bash

    $ sudo systemctl start lightdm
    
树莓派3的性能写写博客还是可以的啦~

:del:`只是在生成 HTML 的时候系统负载会飙升  😂😂`

.. image:: /images/rpi3_blogging.png
    :alt: rpi3 写博客中~
