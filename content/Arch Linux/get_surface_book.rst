入手 Surface Book
===================================================

:slug: get_surface_book
:lang: zh
:date: 2016-05-11
:tags: Arch Linux,devices,凑数
:series: Arch Linux
:desc: 这可不是oo什么的......
:modified: 2016-05-13
:issueid: 9

.. PELICAN_BEGIN_SUMMARY

:del:`这可不是oo什么的......` 😂😂

简单说就是咱搞了台 Surface Book 回来 (不是顶配所以不要说咱土豪啦😂😂

.. PELICAN_END_SUMMARY

.. contents::

搞 Windows _(:з」∠)_
-----------------------------

* 升级到了最新的预览版 ( 用 Microsoft 账户登录就好)

* 装了 Office 2016 (汝会明白为啥的😏

* 把 osu! 复制到了 Surface Book 上 (用 Surface Pen 玩 osu! 的感觉真是酸爽😏

当然作为 Arch Linux 用户怎么不会去装 Arch Linux 呐?

* 于是和 `上篇文章 <https://blog.yoitsu.moe/linux/linux_newbie_mixboot.html>`_ 一样,用 Windows 内置的磁盘管理收缩了一个空闲分区出来. <(ノ=﹁"﹁=)ノ┻━┻
* 因为懒得用 hashtool 给内核签名,于是咱直接关了安全启动:

    开机时按住音量加键和电源键进入 Sueface UEFI,在 "Security" 一节里把 "Secure Boot" 设置成 "Disabled" 就好.

装 Arch Linux ~(>_<~)
---------------------------------

因为咱原来有个装着 Arch Linux 的移动硬盘,所以咱就计划直接把移动硬盘上的 Arch Linux 复制到 Surface Book 上.

但是因为 Linux 里其实是有些虚拟文件系统 ( 例如 :code:`/dev` , :code:`/proc` 一类的 ),
所以只好先用另一个 Arch Linux 的 Live ISO 启动 (╯￣皿￣)╯ ┻━┻

    截至咱写这篇文章的时候, Arch ISO 里的内核 ( 像是4.4? ) 没法驱动 Surface Book 的键盘底座,所以坠吼搞一个 USB 键盘 😂

* 首先按照 `ArchWiki 的 Beginning Guide <https://wiki.archlinux.org/index.php/Beginners%27_guide>`_ ,把刚刚分配出来的空闲空间新建一个分区.

* 然后把原来的 Arch Linux 分区和新的分区挂载到两个位置 (假设源分区是 :code:`/mnt` ,目标分区是 :code:`/target` )

* 接下来用 rsync 来同步内容:

.. code-block:: bash

    # rsync的 -a 参数表示 "归档" 选项.
    # -A 是保留 :ruby:`ACL|访问控制列表`
    # -X 是保留扩展属性

    # rsync -aAX /mnt/ /target

* 然后如果有需要,参照 `Installation Guide <https://wiki.archlinux.org/index.php/Installation_guide>`_ 调整一下其他的参数.

* 后来从 AUR 编译了 :aur:`linux-surfacepro3` 内核,键盘和触控板就可以用了. 😶

最后上一张图 😂

.. image:: /images/surfacebook.jpg
    :alt: 最后上一张图
