Raspberry Pi + Arch Linux ARM + Shadowsocks = ?
==========================================================

:slug: alarm_and_ss_redir
:lang: zh
:date: 2017-05-09
:tags: Arch Linux,devices,rpi
:desc: 利用树莓派+Arch Linux ARM+shadowsocks-透明代理 AP
:issueid:

.. PELICAN_BEGIN_SUMMARY

利用树莓派+Arch Linux ARM+shadowsocks-透明代理 AP :del:`搞大新闻……`

…… （此处应有风滚草 😂

.. PELICAN_END_SUMMARY

.. contents::

首先是致谢（画风不太对劲……
---------------------------------

* 百合仙子 :fref:`lilydjwg` 的 
  `Linux 下的 Wi-Fi 分享 <https://blog.lilydjwg.me/2016/9/13/wi-fi-share-in-linux.205870.html>`_

* :fref:`petercxy` 的 
  `在 ArchLinux 上配置 shadowsocks + iptables + ipset 实现自动分流
  <https://typeblog.net/set-up-shadowsocks-with-iptables-and-ipset-on-archlinux/>`_

* 百合和夏狼 :fref:`sherlock_holo` 还帮咱纠正了点 iptables 的配置。

谢谢啦~ 😋

一些准备工作
----------------------------

* 去买块树莓派，:del:`吃了` 装好 Arch Linux ARM ：

    例如 https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3 ？

* 用 hostapd 做好热点。

* 在树莓派上设置好 shadowsocks 的自动分流。

    不过要修改 ss-redir 的配置文件，让 ss-redir 监听在 0.0.0.0 上 😂

这时连接汝的 AP 时应该能正常访问没被墙的网站，树莓派上也能访问被墙的网站（ curl 一下试试？）

就是多几行 iptables 规则而已 😂
---------------------------------------

如果汝和咱一样用了 
`PeterCxy 的简单粗暴的脚本 <https://github.com/PeterCxy/shadowsocks-auto-redir.sh>`_
的话，打开那个脚本 （应该在 /opt/shadowsocks-auto-redir/shadowsocks-auto-redir )。

然后找到这一行 ,在下面加上一行：

.. code-block:: bash 

    # Redirect to ss-redir port
    LOCAL_PORT=`jq -r ".local_port" $CONFIG_PATH`

    do_iptables -t nat -A SHADOWSOCKS -p tcp -j REDIRECT --to-port $LOCAL_PORT

    # do_iptables -t nat -A PREROUTING -i <汝 AP 的接口名称> -p tcp -j REDIRECT --to-ports $LOCAL_PORT
    # 这一行大概就是把来自汝 AP 的 TCP 连接转发给 $LOCAL_PORT 端口的 ss-redir 来处理？
    do_iptables -t nat -A PREROUTING -i wlan0_ap -p tcp -j REDIRECT --to-ports $LOCAL_PORT

感觉自己还是用要学习一个 iptables 呐~

    http://ipset.netfilter.org/iptables.man.html

保存以后重新启动一下服务来试试效果呗~
