用 Arch Live 修 Arch 的某些姿势
==========================================================

:slug: repair_arch_thorugh_live
:lang: zh
:date: 2017-05-31
:tags: 
:desc: 修电脑三年（划掉
:issueid:

.. PELICAN_BEGIN_SUMMARY

修电脑三年（划掉 😂

.. PELICAN_END_SUMMARY

.. contents::

啥时候需要 Live 介质修 Arch ？
------------------------------------

比如：

* 笨蛋 Windows 升级以后把 bootloader 赶跑了

* 内核炸了没法启动又没有备用内核

* 基础库炸了（例如 glibc ）

* pacman 自己因为某种原因炸了（还是例如 glibc 炸了）

* 忘了密码……

等等…… 😂

准备 Live 介质和进入 Live 环境
-------------------------------------

这个应该不用多说了吧 😂 简而言之 MBR 就 dd UEFI 就格式化完直接把 ISO 里的文件
复制到 U 盘上咯~

如果有 Windows 的话就用 rufus 😂

然后启动进去进行必要的配置 
（ `例如参考咱原来写的那个？ </arch-linux/installing_arch_linux_for_complete_newbies.html>`_ ），
不过因为只是修复而已，连上网设置好镜像就行（如果汝原来的系统里的 pacman 没坏的
话，就连上网就行咯~）

然后把原来的 Arch 挂载到某个目录 （例如 /mnt ）。

chroot 修复
--------------------------------------

当然是 arch-chroot 一把梭咯 😂

    arch-chroot /mnt 

然后遇到啥问题就治啥咯~ 

修坏掉的 pacman 
--------------------------------------

用 live 的 pacman 操作原来的系统的话，记得带上 --root 和 --dbpath 选项。

例如这样重装 pacman :

    pacman -S pacman --root /mnt --dbpath /mnt/var/lib/pacman

或者这样隔空滚系统（雾：

    pacman -Syu --root /mnt --dbpath /mnt/var/lib/pacman

或者这样重装所有包（大雾：

    pacman -S $(pacman -Qq --dbpath /mnt/var/lib/pacman) --root /mnt --dbpath /mnt/var/lib/pacman

记得看看这些 😋
-----------------------------------

* https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks

* https://wiki.archlinux.org/index.php/Installation_guide

    如果没法治疗的话，还可以重装啊（跑 😂