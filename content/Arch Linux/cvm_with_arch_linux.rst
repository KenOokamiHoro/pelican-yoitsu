在腾讯云服务器中重装 Windows 系统为 Arch Linux
===================================================

:slug: cvm_with_arch_linux
:lang: zh
:date: 2016-12-28
:tags: Arch Linux,VM
:desc: 在腾讯云服务器中重装 Windows 系统为 Arch Linux
:issueid: 56

.. PELICAN_BEGIN_SUMMARY

灵感来自 https://danknest.org/install-linux-on-qcloud-windows-cvm 😂

.. PELICAN_END_SUMMARY

.. contents::

为啥？😂
---------------------

    腾讯云只给 Linux 系统的云服务器赠送 :del:`8G` 20G 系统盘以强行推销数据盘，
    而 :del:`由于这样抠门的限制在 Windows 系统上是显然行不通的，` 给 Windows 系统赠送 50G 系统盘。

    😒

    所以…… 😏

前期准备
----------------------

准备 Arch Linux 和 Debian 的安装 ISO 就行 😹

.. image:: /images/QVM_Arch/2.png
    :alt: 前期工作……

然后打开 控制面板-网络连接 记下现在的网络信息 （谁叫没有 DHCP 用😂

.. image:: /images/QVM_Arch/1.png
    :alt: 网络信息

然后照 `这里 </arch-linux/installing_arch_linux_for_complete_newbies.html#id4>`_

准备一个新分区出来，大小比 Arch Linux ISO 大点，然后记得格式化成 FAT32 😂

.. image:: /images/QVM_Arch/5.png
    :alt: 分区


接着把两个 ISO 挂载上。

.. image:: /images/QVM_Arch/6.png
    :alt: 挂载 ISO

用 Debian 安装程序加载器生成启动项
-------------------------------------

用兼容性选项设置 Debian 安装盘下的 setup.exe 以 Windows Vista 运行（不然会报错说不支持 Windows NT 6.x
( 好像只支持 Vista 就是 6.0 的样子 😂 )

.. image:: /images/QVM_Arch/7.png
    :alt: 兼容性设置

然后运行，不过先别忙着重启……

.. image:: /images/QVM_Arch/9.png
    :alt: running……

这个时候把 Arch ISO 的文件复制到新的分区上，然后照着 ISO 改一下卷标 😂

.. image:: /images/QVM_Arch/15.png
    :alt: 改卷标……



改启动项 + 重启
---------------------------------------

把 ISO 或者硬盘上 /arch/boot/x86_64/ 里的 vmlinuz 和 archiso.img 复制到
C 盘的根目录，必要的话改成小写


.. image:: /images/QVM_Arch/17.png
    :alt: 这

.. image:: /images/QVM_Arch/18.png
    :alt: 那

然后打开 C:/win32-loader/grub.cfg 改成这样 （记得把卷标换成汝自己的）：


    linux /vmlinuz archisobasedir=arch archisolabel=ARCH_201612

    initrd /archiso.img

    boot

.. image:: /images/QVM_Arch/19.png
    :alt: 😂

最后重启，用 CVM 的控制台登录，选 Debian 啥的那项😂:

.. image:: /images/QVM_Arch/21.png
    :alt: 😂

等若干秒：

.. image:: /images/QVM_Arch/22.png
    :alt: 😂

😂😂

连上网
-------------------------------------

（之前说过企鹅云不给 DHCP 😂

于是拿出刚刚抄下来的网络信息备用。

首先，用 ip link 确定一下接口 （咱是ens3 😂

.. image:: /images/QVM_Arch/23.png
    :alt: ip link

然后添加 IP 地址：

    ip addr add {用 CIDR 表示的 IP 地址} dev {汝的网卡名称}

    (CIDR = `无类别域间路由 <https://zh.wikipedia.org/wiki/无类别域间路由>`_ )

例如咱的子网掩码是 255.255.192.0 就这么写：

    ip addr add 10.141.60.78/18 dev ens3

接着添加网关：

    ip route add default via {网关 IP 地址}

最后编辑 /etc/resolv.conf 改成抄下来的 DNS 服务器地址 😂:

.. image:: /images/QVM_Arch/24.png
    :alt:  /etc/resolv.conf

然后联网试试？😂

.. image:: /images/QVM_Arch/25.png
    :alt:  😂

------------------

然后在 CVM 上直接装或者打开 sshd 用自己的电脑连上去啥的就自己围观 ArchWiki 好啦：

https://wiki.archlinux.org/index.php/Installation_guide

https://wiki.archlinux.org/index.php/Install_from_SSH
