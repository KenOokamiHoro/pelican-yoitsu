利用 CUPS 和 Samba 在 Arch Linux 和 Windows 间共享打印机
===========================================================

:slug: cups_sharing_printer
:lang: zh
:date: 2016-09-14
:tags: Arch Linux
:issueid: 49

.. PELICAN_BEGIN_SUMMARY

社团新买的打印机到啦,于是就要设置一下呗~ 😋

.. PELICAN_END_SUMMARY

.. contents::

CUPS,Samba 都是些啥？
----------------------------------

    CUPS（以前为 Common Unix Printing System，UNIX 通用打印系统的缩写，但现无官方全名）
    是一个类Unix操作系统的组合式印刷系统，允许一台电脑作为打印服务器。
    CUPS接受一个客户端的电脑进程，并送到相应的打印机。

    虽然有其他的打印程序包例如LPRNG，但CUPS是相当流行和相对容易使用的。
    它是Arch linux及许多其他Linux发行版缺省的打印系统。

    -- `ArchWiki:CUPS <https://wiki.archlinux.org/index.php/CUPS_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)>`_

    Samba是一个用于局域网中的计算机间文件共享的软件，这么说您大概还摸不着头脑，那么网上邻居您听说过吧？
    对，Samba就是干这个的。当年那个有点软公司设计了一套局域网计算机间的文件共享协议，
    起名叫做SMB，就是Server Message Block的缩写。当时所有的Windows系统就都集成这种协议，
    因此这个协议在局域网系统中的影响还比较大。后来，国际互联网，也就是Internet逐渐流行了起来，
    有点软公司希望他们的这个协议能够一个用在Internet上，因此对其进行的整理，
    更名为CIFS,也就是Common Internet File System。从名字可以看出，他们的期望是很高的
    ，不过实际呢……反正，你现在用网上邻居上搜狐么？当然，不管怎样，CIFS或者说SMB协议在局域网中传输文件还是非常方便的。
    当然，我们Linux系统之间也有很好的局域网共享文件的协议，叫做NFS，
    网络文件系统的缩写。但无奈那该死的查皮不支持这个协议嘛，所以要想网络上的查皮和我之间能共享文件的话，
    要么我这里有人能懂CIFS协议，要么查皮那里有软件能解析NFS协议。
    然而毕竟还是我这里的软件大度一些，所以就有了用于支持SMB协议的软件——Samba。

    -- `笨兔兔的故事 - Ubuntu中文论坛 <http://forum.ubuntu.org.cn/viewtopic.php?f=112&t=162040>`_

首先……😋
------------------------

在 Windows 上连接好打印机，安装上驱动，再通过“设备与打印机”共享这个打印机，Windows 上的设置就完成啦~

记得记下来 Windows 电脑的 IP 地址和打印机的名称，还有给共享打印机的账户设置个密码。

然后……😋😋
------------------------
打开汝的 Arch Linux ，先安装必要的软件包：

    # pacman -S cups, ghostscript gsfonts samba

然后激活并启动 CUPS 服务:

    # systemctl enable org.cups.cupsd.service --now

这个时候就可以通过 http://localhost:631 访问到 CUPS 的 Web 界面啦~

.. image:: /images/cups_sharing_printer/-1.png
    :alt: CUPS Web 界面

添加打印机 😋😋😋
--------------------------

首先点击 "Administration" 打开管理界面:

.. image:: /images/cups_sharing_printer/0.png
    :alt: CUPS Administration 界面

然后点击 "Add printer",这时会要求汝登录.咱只试过 root 登录能成功.....

.. image:: /images/cups_sharing_printer/1.png
    :alt: 登录到管理界面

接下来选择要连接到哪种打印机啦,连接 Windows 打印机就选择
"Windows Printer via SAMBA" (通过 Samba 连接 Windows 打印机) 呗~

.. image:: /images/cups_sharing_printer/2.png
    :alt: 选择一种协议

接下来输入打印机的 URI 啦,
对于 Windows 打印机 ,URI 大概像这样:

    :code:`smb://{用户名}:{密码}@{Windows 的 IP 地址}/{打印机名称}`

.. image:: /images/cups_sharing_printer/2b.png
    :alt: 要连接到哪个打印机?

接着用人类的语言描述一下这台打印机（例如名字和位置啦）~

.. image:: /images/cups_sharing_printer/3.png
    :alt: 描述这台打印机

然后依照打印机的品牌和型号选择适合的驱动程序呗：

    咱这台 Samsung M2070 的打印机要从三星的网站上下载驱动 \_(:з」∠)\_

    http://www.samsung.com/printersetup

.. image:: /images/cups_sharing_printer/4.png
    :alt: 选择打印机驱动程序

接着修改默认打印参数，保存，新的打印机就添加好啦~ 😋

.. image:: /images/cups_sharing_printer/5.png
    :alt: 修改默认打印参数

为了测试打印机，可以从 Maintaince 菜单中选择 “Print Test Page” 打印一张测试页试试 😋

.. image:: /images/cups_sharing_printer/6.png
    :alt:  Maintaince 菜单

还有记得添加汝自己成为可以使用打印机的用户：

.. image:: /images/cups_sharing_printer/7.png
    :alt: Administration 菜单

.. image:: /images/cups_sharing_printer/8.png
    :alt: 添加允许使用打印机的用户

然后桌面环境下的程序应该可以看到打印机了啦~（例如 GNOME）

.. image:: /images/cups_sharing_printer/9.png
    :alt: GNOME 下的打印机菜单

😋
