Arch Linux 上的 GLNMP
==========================================================

:slug: glemp_on_arch
:lang: zh
:date: 2021-03-01
:tags: Arch Linux
:desc: 在 Arch Linux 上安装俗称的 LEMP 系统（？）

    :del:`如果汝要是来问咱为啥两篇文章间隔的时间越来越长的话……`

前言
----------------------------------------------------------

请允许咱引用一下 :fref:`lilydjwg` 的一段话：

    我之所以现在记录这事儿，「现在」的原因是，我要在另一个系统上再测试一遍再发布出来，
    「记录」的原因是，下一次我就不用想要执行哪些命令了。

所以这篇文章更多是留给咱自己看的，绝对不可能适合所有人（例如万一有人是为了塞给 V2Ray
一类的代理软件用什么的）。

记得 Read the Fine Manual 是汝等的好伙伴就好啦。

----------------------------------------------------------
那为啥要叫 GLNMP ？
----------------------------------------------------------

如果汝会这么问的话，那应该是有听说过 LAMP/LNMP 系统吧，大概就是
Linux，Apache（或者 Nginx），MySQL（现在可能更多在用 MariaDB） 和 PHP（还有可能是 
Python 甚至 Perl？）的组合。因为除了此上下文环境中的“Linux”实际上指的是 GNU/Linux 操作系统。
所以咱就这么叫了。

----

接下来就从一个刚装好的 Arch Linux 开始了。

Nginx （先运行起来）
----------------------------------------------------------

https://wiki.archlinux.org/index.php/Nginx

Nginx 官方有两个分支， mainline 和 stable。 看名字应该就能知道哪个更新哪个更稳定了。

安装好和启动服务之后，就可以打开 http://localhost 看看默认的主页了（就那个 Welcome to Nginx 啥的），
当然现在这些肯定不够，所以先把剩下的装上再回来设置了。

MariaDB
-----------------------------------------------------------

https://wiki.archlinux.org/index.php/MariaDB

在 MySQL 被 Orcale 收购之后，一些对 MySQL 的未来表示担心的人发起了 MariaDB 这个项目。到现在越来越多的
WebApp 和 GNU/Linux 发行版都开始（或已经）用 MariaDB 代替 MySQL 了。

在安装完 mariadb 以后记得初始化一下：

.. code-block:: bash

    ## 安装 MariaDB 的基础数据
    # mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
    ## 启动服务，进行第一次启动的设置，除了设置密码那一步的话基本上都能保持默认。
    ## 像是禁用 root 用户的远程登录，移除默认的 annoymous 用户和移除测试用的数据库和权限啥的。
    # systemctl start mariadb
    # mysql_secure_installation
    # systemctl restart mariadb

如果有需要的话，可以在 /etc/my.cnf.d/ 里修改适合自己的配置，例如：

.. code-block:: conf

    # 让 mysqld 只使用本地 Unix Socket 连接。
    [mysqld]
    skip-networking

    # 默认使用 utf8mb4 编码。
    [client]
    default-character-set = utf8mb4

    [mysqld]
    collation_server = utf8mb4_unicode_ci
    character_set_server = utf8mb4

    [mysql]
    default-character-set = utf8mb4

调整好设置以后，可以用 mysql 命令来测试连接：

.. code-block:: bash

    # mysql -u root -p
    Enter password:
    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 3
    Server version: 10.5.9-MariaDB Arch Linux

    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    MariaDB [(none)]>

例如创建一个数据库和用户，然后为刚创建的用户分配刚创建的数据库的所有权限：

.. code-block:: bash

    MariaDB> CREATE DATABASE mydb;
    MariaDB> CREATE USER 'user'@'localhost' IDENTIFIED BY 'some_pass';
    MariaDB> GRANT ALL PRIVILEGES ON mydb.* TO 'user'@'localhost';
    MariaDB> FLUSH PRIVILEGES;
    MariaDB> quit

PHP
-------------------------------------------------------------------------------

https://wiki.archlinux.org/index.php/PHP

Arch 习惯是只留最新的版本的，除非官方仓库中别的包在过渡的时候。虽然其它版本也能从 AUR 里装，
但是没有保证就是了……

以及有一些扩展也在仓库里，看汝使用的应用有什么要求就装上吧。

然后就是修改 PHP 的配置文件 (/etc/php/php.ini) 和 PHP-FPM 的配置文件 (/etc/php-fpm.d/)
了，基本上就是修改下上传文件的大小，启用扩展和其它的别的。

Let's Encrypt
--------------------------------------------------------------------------------

    :del:`这都 1021 年了还需要人提醒给自己的网站上 TLS ？`

咱是参考 `通过 Cloudflare DNS 验证来申请 Let's Encrypt 证书 <https://blog.lilydjwg.me/2018/11/24/apply-certificates-with-let-s-encrypt-verified-with-cloudflare-dns.213810.html>`_ 
这篇文章用 Certbot 和 Cloudflare 来验证域名的，汝可能喜欢用像是 acme.sh 之类的别的方法的话，那照着
自己的喜好来的话也没有关系。

组合配置
--------------------------------------------------------------------------------

    DigitalOcean 有个 Nginx 配置文件生成器说不定有用： https://nginxconfig.io/

比较常见的组织 Nginx 配置文件的方法就是用不同的配置文件区分不同的网站，像 /etc/nginx/sites-available
和 /etc/nginx/sites-enabled 这样的。

咱的话，习惯把配置文件中 PHP 和 TLS 的配置再拆出来共用的样子。

PHP 的配置大概像这样：

.. code-block:: conf

    # /etc/nginx/include/php.conf
    location ~ \.php$ {
        # 404
        try_files $fastcgi_script_name =404;

        # default fastcgi_params
        include fastcgi_params;

        # fastcgi settings
        fastcgi_pass                    unix:/run/php-fpm/php-fpm.sock;
        fastcgi_index                   index.php;
        fastcgi_buffers                 8 16k;
        fastcgi_buffer_size             32k;

        # fastcgi params
        fastcgi_param DOCUMENT_ROOT     $realpath_root;
        fastcgi_param SCRIPT_FILENAME   $realpath_root$fastcgi_script_name;
        #fastcgi_param PHP_ADMIN_VALUE  "open_basedir=$base/:/usr/lib/php/:/tmp/";
    }

TLS 的配置大概像这样，推荐参考 `MoillaWiki:Server Side TLS <https://wiki.mozilla.org/Security/Server_Side_TLS>`_ ，Mozilla
也有提供一个配置文件生成器的样子，咱目前使用的配置有些差别。

.. code-block:: conf

    # /etc/nginx/include/ssl.conf
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers 'TLS-CHACHA20-POLY1305-SHA256:TLS-AES-256-GCM-SHA384:TLS-AES-128-GCM-SHA256:TLS13-AES-256-GCM-SHA384:TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-128-GCM-SHA256:TLS13-AES-128-CCM-8-SHA256:TLS13-AES-128-CCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256';
    ssl_prefer_server_ciphers on;
    ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
    ssl_session_timeout  10m;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off; # Requires nginx >= 1.5.9
    ssl_stapling on; # Requires nginx >= 1.3.7
    ssl_stapling_verify on; # Requires nginx => 1.3.7
    add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
    add_header X-Frame-Options sameorigin;
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";
    add_header Referrer-Policy "no-referrer";
    server_tokens off;
    #add_header Content-Security-Policy "default-src https: ; object-src 'none'";
    #add_header X-Robots-Tag "none" ;
    #add_header X-Download-Options "noopen";
    #add_header X-Permitted-Cross-Domain-Policies "none";

组合在一起的其中一个例子：

.. code-block:: conf

    server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name $_server;
        include /etc/nginx/include/ssl.conf;
        ssl_certificate /etc/letsencrypt/live/$_server/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/$_server/privkey.pem;

        root /home/horo/www/$_server;
    }

在大部分都安排妥当以后，就可以设置让必要的服务在开机时自动启动啦。

----

:del:`差点就忘了 reStructuredText 怎么写……` ，虽然 Pelican 是有 markdown 支持的。