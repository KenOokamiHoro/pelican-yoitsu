systemd-boot 从安装到吃电脑（误
===================================================

:slug: using_systemd_boot
:lang: zh
:date: 2017-04-16
:tags: Arch Linux,bootloader,UEFI
:desc: systemd-boot 从安装到吃电脑（误😂
:issueid:

.. PELICAN_BEGIN_SUMMARY

systemd-boot 从安装到吃电脑（误 😂

.. PELICAN_END_SUMMARY

.. contents::

Systemd-boot 是啥？
------------------------

    systemd-boot (以前被称为gummiboot)
    是可以执行 EFI 镜像文件的简单 UEFI 启动管理器。
    启动的内容可以通过一个配置(glob)或者屏幕菜单选择。Arch 默认安装的 systemd 提供了这个功能。

    配置很简单，但是只能启动 EFI 可执行程序，
    例如 Linux 内核 EFISTUB, UEFI Shell, GRUB, Windows Boot Manager等。

Systemd: 😋 => gummiboot

现在 Arch 的安装 ISO 在 UEFI 启动时用的就是这个启动管理器 😂

安装之前
--------------------------

* **这是个 UEFI 启动管理器，于是 BIOS 系统是不能用的** 😂

* **首先汝的 EFI 系统分区最好足够大而且挂载在 /boot 上……** 😂

    * 用 UEFI 该知道 EFI 系统分区是啥了吧😂

    * （不知道从哪里听说的）推荐大小是 512M ，Windows 自动创建的应该也够用（ ~260M? ）

* Arch Linux 的话还要安装上 efibootmgr 和 dosfstools 这两个包。

请 systemd-boot 入 EFI 系统分区 😋
--------------------------------------

    # bootctl install --path=/boot

    ⬆️ 像这样安装 systemd-boot 咯，--path 是 EFI 系统分区的位置。

    如果不在电脑的内置硬盘上安装的话，加上 --no-variables ，就不会碰本机的 efivars 啦 😄

    （下面可都当作把 EFI 系统分区挂载在 /boot 了咯~

配置文件怎么写？
---------------------------------------

（教会 systemd-boot 去哪里找汝的 GNU/Linux 系统的内核和如何启动等其它参数咯 😃

systemd-boot 的配置文件都放在 /boot/loader 呐~ ，里面还有一个 entries 文件夹
用来存放不同的启动项。

    如果存在的话,bootctl 会自动为 "Windows Boot Manager (Windows 启动管理器)"
    (\\EFI\\Microsoft\\Boot\\Bootmgfw.efi), "EFI Shell" (\\shellx64.efi) 和
    "EFI Default Loader" (\\EFI\\Boot\\bootx64.efi) 增加启动选项哦。于是就不用写这三个了呐~

基本配置 /boot/loader/loader.conf 像这样：

.. code-block:: bash

    default  arch    <- 默认启动选项的文件名（没有 .conf 哦
    timeout  4       <- 选择启动选项的时间（如果没有这一项的话，启动时就不会弹出菜单
                        而是直接启动默认启动项呐~不过启动时按住按键还是会显示启动菜单的啦😸
    editor   0       <- 要不要允许在启动选项前按 e 键来更改启动选项呢？

一个 systemd-boot 配置文件大概像这样：

.. code-block:: bash

    title          在启动管理器中显示的标题
    version        内核版本,只在有多个 title 时需要.
    machine-id     通过 /etc/machine-id用于区分不同设备的名称, 只在有多个title 和 version 时需要
    efi            要启动的EFI应用程序的位置,以 EFI 系统分区为相对路径哦;
                   例如 /vmlinuz-linux （记住 systemd-boot 不会找 EFI 系统分区以外的分区上的文件 >_<
    options        传递给 EFI 应用程序或内核启动的参数,可选.
                   比如启动 linux 内核就需要 initrd=efipath 和 root=dev 选项呐~

不过汝要是启动 Linux 内核的话，可以不用 efi 而这么写

.. code-block:: bash

    linux          < vmlinuz的位置 >
    initrd         path-to-initramfs

initrd 会自动转换成 efi path 和 options initrd=path 咯~

– 这个语法只是为了方便,在功能上并没有区别的啦 (つ°ω°)つ

动手写配置文件啦~ 🤣
---------------------------------

首先用 lsblk 确定汝的根分区是哪一个设备:

.. code-block:: bash

    $ lsblk
    NAME        MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
    nvme0n1     259:0    0 238.5G  0 disk
    ├─nvme0n1p1 259:1    0   260M  0 part  /boot
    ├─nvme0n1p2 259:2    0   128M  0 part
    ├─nvme0n1p3 259:3    0  50.1G  0 part
    ├─nvme0n1p4 259:4    0    60G  0 part
    ├─nvme0n1p5 259:5    0   900M  0 part
    ├─nvme0n1p6 259:6    0   124G  0 part  /
    └─nvme0n1p7 259:7    0   3.1G  0 part

比如咱的是 /dev/nvme0n1p7 (就是哪一个的 MOUNTPOINT 是 / 咯) 😂

然后获得它的 PARTUUID 😋

.. code-block:: bash

    # blkid -s PARTUUID -o value /dev/nvme0n1p6
    d39fe55a-0aa1-455e-84d7-0325e917b47b

所以下面咱就用 d39fe55a-0aa1-455e-84d7-0325e917b47b 作为咱的 PARTUUID 咯~

    汝的一定和咱的不一样，所以别一个字不差的照抄！ 😡

`````````````````````````````
最简单的配置文件
`````````````````````````````

如果汝的 / 既不是 LVM 又没加密的话:

.. code-block:: bash

    title          Arch Linux
    linux          /vmlinuz-linux
    initrd         /initramfs-linux.img
    options        root=PARTUUID=d39fe55a-0aa1-455e-84d7-0325e917b47b rw

* 可以把 title 换成汝喜欢的名字，如果汝用了其它内核（例如 linux-mainline ），
  记得修改一下 linux 和 initrd 选项。

* 如果需要加载 Intel microcode 的话，就再来一行 initrd 咯~

* 如果还有其它的内核参数，加在 rw 后面咯~


``````````````````````````
用了 LVM？
``````````````````````````

    😣 等等！如果汝 LVM 逻辑卷组的外面没有一个单独的 /boot 的话就不能用 systemd-boot 了~

.. code-block:: bash

    title          Arch Linux (LVM)
    linux          /vmlinuz-linux
    initrd         /initramfs-linux.img
    options        root=/dev/mapper/<VolumeGroup-LogicalVolume> rw

记得把 VolumeGroup 和 LogicalVolume 换成汝自己的 LVM 逻辑卷的卷组和卷名称啊~

当然用 UUID 也可以啦~

````````````````````````
加密的 / ？
````````````````````````

.. code-block:: bash

    title           Arch Linux Encrypted
    linux           /vmlinuz-linux
    initrd          /initramfs-linux.img
    options         cryptdevice=PARTUUID=d39fe55a-0aa1-455e-84d7-0325e917b47b:<mapped-name> root=/dev/mapper/<mapped-name> quiet rw

mapped-name 是解密后的分区映射到的位置咯~，只要 cryptdevice 和 root 里的一样就行！~

````````````````````````
btrfs 子卷？
````````````````````````

.. code-block:: bash

    title          Arch Linux
    linux          /vmlinuz-linux
    initrd         /initramfs-linux.img
    options        root=PARTUUID=d39fe55a-0aa1-455e-84d7-0325e917b47b rw rootflags=subvol=ROOT

记得用汝自己的子卷名称换掉 rootflags=subvol=ROOT 里的 ROOT 啦 ~

`````````````````````````
其它 UEFI 应用程序？
`````````````````````````
例如 EFI Shell ？

.. code-block:: bash

    title  UEFI Shell x86_64 v2
    efi    /EFI/shellx64_v2.efi

参考来源
--------------------

* `ArchWiki:Systemd-boot <https://wiki.archlinux.org/index.php/Systemd-boot>`_
* http://www.freedesktop.org/wiki/Software/systemd/systemd-boot/
