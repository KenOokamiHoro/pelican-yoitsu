用内置硬盘安装 Arch Linux 
==========================================================

:slug: installing_arch_linux_without_a_usb_stick
:lang: zh
:date: 2017-07-31
:tags: Arch Linux,Windows,安装,长篇
:desc: 用内置硬盘安装 Arch Linux，但是为啥不去买个 U 盘呢？😂


.. PELICAN_BEGIN_SUMMARY

用内置硬盘安装 Arch Linux，但是为啥不去买个 U 盘呢？😂

.. PELICAN_END_SUMMARY

.. contents::

准备原材料
----------------------------------------

* 就一个 ArchISO 😂

* 用磁盘管理（或者其它汝中意的分区软件）分出一个比 ISO 大的分区，然后格式化成 FAT32。

在 Windows 中挂载 ESP 
---------------------------------------

用 diskpart 就好啦 ……

.. code-block:: text

    diskpart
    list disk # 列出硬盘
    select disk 0 # 选择一块硬盘 （一般内置硬盘都是第0块）
    list partition # 列出分区
    select partition 1 # 选择一个分区
    assign letter=b # 分配一个盘符
    exit

或者 mountvol 😂

.. code-block:: text

    mountvol b: /s # 把 b: 换成一个没在用的盘符

然后以管理员身份重启文件资源管理器（不然写不进去 😂）

.. code-block:: text

    taskkill /im explorer.exe /f
    explorer.exe

准备文件
----------------------------------------

* 把 Arch ISO 的文件复制到新的分区上，然后照着 ISO 改一下卷标 😂

.. image:: /images/QVM_Arch/15.png
    :alt: 改卷标……

* 把 Arch ISO 里的 EFI/boot/bootx64.efi 复制到 ESP 的EFI/boot/bootx64.efi 上（记得备份），需要的话也可以把 EFI Shell 复制过来。

* 把 Arch ISO 里的 arch/boot/x86_64 文件夹原样复制到 ESP 上（需要 microcode 更新的可以带上 arch/boot/intel-ucode.img

* 把 Arch ISO 里的 loader 文件夹原样复制到 ESP 上 😂

    如果没复制 arch/boot/intel-ucode.img 的话，去 archiso-x86-64.conf 把 initrd  /arch/boot/intel_ucode.img 删掉（

----

然后重启试试？ 😂