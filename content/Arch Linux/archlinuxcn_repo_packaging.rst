Arch Linux 中文社区仓库从使用到打包（大雾）
==========================================================

:slug: archlinuxcn_repo_packaging 
:lang: zh
:date: 2019-01-20
:tags: Arch Linux, pacman, packaging
:desc: Arch Linux 中文社区仓库打包从 makepkg 到 lilac.py （大嘘


    Arch 鼓励每一个用户参与和贡献，报告和帮助修复 bugs，提
    供软件包补丁和参加核心项目：Arch 开发者都是志愿者，通过持续的贡献成为团队的一员。

    -- https://wiki.archlinux.org/index.php/Arch_Linux_(简体中文)

/me 自从用了 Arch Linux 以后，就有了那种想要做些什么的感觉了呢（大雾）

Arch Linux 中文社区仓库是啥，我该怎么用？
----------------------------------------------------------

    Arch Linux 中文社区仓库 是由 Arch Linux 
    中文社区驱动的非官方用户仓库，包含一些额外的软件包以及已有软件的 
    git 版本等变种。部分软件包的打包脚本来源于 AUR。

Emmmm 这个介绍应该够看了吧……

要使用的话，编辑 /etc/pacman.conf ，加入中文社区的仓库地址：

.. code-block:: conf
   
   [archlinuxcn]
   Server = https://cdn.repo.archlinuxcn.org/$arch

这个 Server 的 CDN 除了国内几乎都能用，国内的话可以去 
https://github.com/archlinuxcn/mirrorlist-repo
看一下可用的镜像。

然后安装 keyring （不然汝是装不上里面的包的）：

.. code-block:: bash

    # pacman -Syyu archlinuxcn-keyring

如果在使用的过程中遇到了啥问题的话，除了 `上社区交流群吹水 <https://fars.ee/~readme.html>`_ 
以外，也可以去 `中文社区仓库在 Github 上的 Issues 页面 <https://github.com/archlinuxcn/repo/issues>`_
提问。（问之前记得看看模板和现有的问题，以及老生常谈的提问的智慧）

然而上面的这些都不是重点（雾），要是汝有心意的话，为啥不来一起打包呢（期待的眼神）

申请成为中文社区仓库的维护者
------------------------------------------------------

直接参考 `<https://github.com/archlinuxcn/repo/wiki/申请当维护者>`_ 就好啦~

如果汝有幸通过了的话，可以接着往下看了（滑稽）

向中文仓库新增软件包之开始之前
-----------------------------------------------------

开始之前的话，有一些软件包需要装：

* base-devel （这个应该在汝用 AUR 的时候就装上了吧）
* devtools （可以生成一个干净的环境来编译软件包）
* devtools-cn-git （上面的小修改版，如果汝的软件包的依赖有 [archlinuxcn] 的软件包时可以使用）
* archcn-tools-meta （一些工具的合集的元包）

如果可以的话，最好打开 testing 和 community-testing 仓库 :del:`来提前为用户踩坑……`

向中文仓库新增软件包之手动打包篇
------------------------------------------------------

   既然汝有心意加包（？）了的话，应该对 makepkg 的使用很谙熟了吧，这里就
   偷下懒不赘述了 😂

中文社区仓库对手动打包的要求大概是这样的：

   如果是在本地打包，请将包文件上传至编译服务器自己主目录下的临时目录 ，上传完毕后移动到
   /home/repo 目录，以避免网络中断或者其它原因造成不完整的包被添加到源中。

如果汝有翻过 wiki 的话会发现咱漏掉了在服务器上打包那部分对不对？（不要在意那些细节.webp.png）
在生成完软件包后，用 gpg 生成一个分离签名：

.. code-block:: bash

   $ gpg --detach-sign /path/to/your/package.pkg.tar.xz 

签名完毕以后汝就会在打好的软件包目录中发现同名的 sig 文件，把这个文件和软件包一起上传到编译机上就 OK 啦~ 

* 不要直接上传到 ~/repo 中，而是先上传到汝的主文件夹的一个位置上再通过 ssh mv（移动） 过去 😂

向中文仓库新增软件包之紫丁香篇（？）
-----------------------------------------------------------

然而实际操作中手动打包的场合越来越少了，现在大家都会用一些技巧来自动的更新啦，例如 :del:`紫丁香` lilac。

Lilac 是 Arch Linux CN 目前主要的打包机器人，主要由百合负责开发 ( :fref:`lilydjwg` )。

要想找 lilac 帮忙的话，有两个文件不可少：

* lilac.yaml ， 一些诸如维护者和更新时机的元数据。
* lilac.py , lilac 看到这个文件的时候才会尝试自动更新的哦~

lilac.yaml的详细结构和语法可以在 https://archlinuxcn.github.io/lilac/ 上找到。
如果不知道怎么写的话，翻翻仓库里已经有的 lilac.yaml 咯~

在提交前记得运行一下仓库里的 ./pre-commit 来检查一下自己的的 lilac 的设置有没有问题，
可以通过设置 git hooks 做到在提交之前自动运行（其实就是一个 ln）

   ln -sr pre-commit .git/hooks/pre-commit

`Lilac 常见问题 <https://github.com/archlinuxcn/repo/wiki/lilac-%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98>`_ 
收集了一些大家在用 lilac 的时候遇到的问题，如果汝有遇到类似的问题的话可以参考，自己的经验也可以写在上面。

以收养（？） AUR 的软件包为例举一个使用 lilac 的栗子
------------------------------------------------------------------

   AUR 里的包大概是 [archlinuxcn] 里的主要成分 😂

这里就用咱在 AUR 和 [archlinuxcn] 里都有维护的 parsoid 举例了 ~ 

首先从 AUR 获得 PKGBUILD 和一系列安装时会用到的文件，然后放在仓库的 archlinuxcn/ 文件夹中。

然后在里面新建一个 lilac.yaml:

.. code-block:: yaml

   # maintainers 就是这个包的维护者啦，如果编译遇到啥问题不通过的话
   # lilac 会给相应的维护者发邮件哦~
   # 基本上设置成汝 Github 的用户名就可以，如果汝没有设置公开邮件地址的话，
   # 记得另外设置个 email 属性。
   maintainers:
   - github: KenOokamiHoro
   # build_prefix 就是编译时要使用哪一个 devtools 里的命令啦（大概），
   # 对于大多数包来说， extra-x86_64 就 OK。
   # 可以使用的各种类型去翻 schema 啦 
   build_prefix: extra-x86_64
   # pre_build 和 post_build 是 lilac 在编译之前和之后运行的函数，
   # 对于大多数 AUR 的包， aur_pre_build 和 aur_post_build 就 OK。
   pre_build: aur_pre_build
   post_build: aur_post_build
   # update_on 是更新来源的列表，对于想在 AUR 更新时更新的这种状况而言，
   # - aur: <AUR 上的软件包名称> 就好啦~ 
   # 记得第一项应当是软件的来源，剩下的是重新打包的时机。
   update_on:
   - github: wikimedia/parsoid
     use_max_tag: true
   - aur: parsoid

然后新建一个空白的 lilac.py 用来提示（？） lilac 自动更新。

在自己编译 OK 和 pre-commit 测试通过以后就可以提交啦，lilac 会在每日北京时间1, 9, 17点17分在社区编译机上运行，
以检查软件的新版本及自动打包。

https://build.archlinuxcn.org/ 上可以查看软件包的编译状态和上次更新时间等相关信息。
在 #archlinux-cn 频道里可以通过 !cnbuild 命令看个大概（大嘘


所以那个 lilac.py 是一直都是空白的嘛？
-----------------------------------------------------
如果汝要在编译之前执行某些操作，汝可能需要在 lilac.py 里写些东西，例如：

* AUR 中的 PKGBUILD 已经 out-of-date ，但是希望通过 AUR 中的 PKGBUILD 来构建软件包，需要修改 pkgver 和 hash 值时；
* AUR 中的 PKGBUILD 修改包括依赖、构建函数等才能正确构建软件包时；
* 通过 sourceforge 等其他网站直接检查版本更新，但希望通过修改 AUR 中的 PKGBUILD 来构建软件包时。
* ……

`这里有一些例子 <https://github.com/archlinuxcn/lilac/tree/master/templates>`_ ，
需要的话也可以翻翻仓库里不为空的 lilac.py 学习一下。

