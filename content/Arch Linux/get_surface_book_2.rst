入手 Surface Book （续集）
===================================================

:slug: get_surface_book_2
:lang: zh
:date: 2017-02-22
:tags: Arch Linux,devices
:series: Arch Linux
:desc: 为 Surface Book 添加触屏和电磁笔支持😂
:issueid: 60

.. PELICAN_BEGIN_SUMMARY

为 Surface Book 添加触屏和电磁笔支持😂

感谢 https://github.com/ipts-linux-org/ipts-linux-new
和 https://github.com/jimdigriz/debian-mssp4 的资源😂

.. PELICAN_END_SUMMARY

.. contents::

还是搞 Windows _(:з」∠)_
-----------------------------

从 Windows 中把系统盘的 Windows/INF/PreciseTouch/Intel 文件夹复制出来备用。

装内核 😂
-----------------------------

从 AUR 安装 :aur:`linux-surface-pro-4-git`

然而这个内核有两个问题😂:

* 如果是在键盘和屏幕分离时启动会 Kernel Panic
* 如果没设置好接下来的 ipts 会在关机或重启时 Kernel Panic

😂

链接驱动文件 😂
------------------------------

首先把刚刚从 Windows 偷来的文件复制到相应的位置上：

    # mkdir -p /usr/lib/firmware/intel/ipts

    # cp /path/to/Windows/INF/PreciseTouch/Intel /usr/lib/firmware/intel/ipts/

然后链接相应的文件（如果提示目标已存在，就删掉再试一次咯😂

    # ln -s iaPreciseTouchDescriptor.bin /lib/firmware/intel/ipts/intel_desc.bin

    # ln -s SurfaceTouchServicingDescriptorMSHW0076.bin /lib/firmware/intel/ipts/vendor_desc.bin

    # ln -s SurfaceTouchServicingKernelSKLMSHW0076.bin /lib/firmware/intel/ipts/vendor_kernel.bin

    # ln -s SurfaceTouchServicingSFTConfigMSHW0076.bin /lib/firmware/intel/ipts/config.bin

    /me 有 0076/0078/0079 ,其中 0076 可用，0078 没法加载，0079 触屏方向相反了😂

----

好了，重启一下试试？😂
