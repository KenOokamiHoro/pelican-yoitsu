速记
=======================================

:slug: quick
:date: 2016-04-21
:save_as: quick.html
:issueid: 42

sudo -i vs su
--------------------------

* sudo 用用戶自己的密碼，su 用 root 密碼？

* sudo -i 是 login shell (-bash)  su 是普通 interactive shell (bash) 

* sudo 环境变量传递机制很复杂(大多数不传)。su 默认都传，只覆盖少数几个，su - 会清理掉环境变量.

