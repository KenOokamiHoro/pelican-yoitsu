友情链接
=======================================

:slug: links
:date: 2016-01-16
:save_as: links.html
:display: none
:issueid: 41

下面的链接列表以结识的区域分类，排名不分先后。如果汝想和咱结交的话，
`来这看看呗~  </addlinks.html>`_

`以及曾经存在的链接在这里。  </links_archive.html>`_

.. contents ::

因为有 :ruby:`伙伴们|食物` 在，咱就 :ruby:`不是孤独一人|不愁没吃的` 呐~

一些 :ruby:`社群|羊圈` （雾）
--------------------------------------

.. friend:: archlinuxcn
    :nick: `Arch Linux 中文社区 <https://www.archlinuxcn.org/>`_
    :logo: https://avatars3.githubusercontent.com/u/3817598

    Arch Linux是一个轻量的、灵活的Linux发行版，遵循K.I.S.S.原则。

    目前我们有专门为x86_64架构优化的官方软件包。作为官方软件包的补充，
    我们还有一个社区维护的，数量和质量每天都在增长和进步的软件源。

    我们强大的社区热情而乐于助人，同时我们以能够用自己的技术能力使用
    Arch并作为Arch的主干力量而自豪。

.. friend::  aosc 
    :nick: `Anthon Open Source Community <https://aosc.io>`_
    :logo: https://avatars0.githubusercontent.com/u/6059785

    (Not) Always Overuse Shell Community ，独立 GNU/Linux 发行版 AOSC OS 的一群用户和开发者。

.. friend:: nyaacat
    :nick: `Minecraft 服务器 "喵窝" <https://www.nyaa.cat/>`_
    :logo: https://nyaa.cat/images/logo-color.png

    不仅仅是游戏服务器，还是温暖的大家庭。 

    にゃんぱすー 游戏进行中喵~

.. friend:: letitfly
    :nick: `LetITFly BBS <https://bbs.letitfly.me>`_
    :logo: https://i.loli.net/2017/09/19/59c136403662f.png 

    LetITFly BBS 的前身是 MAT BBS，但并不局限于 MyAndroidTools。 
    我们致力于让 Android 的使用体验更好，同时我们也讨论 Windows 和 GNU/Linux 相关。 

Arch Linux 中文社区
---------------------------------------

.. friend:: farseerfc
    :nick: `farseerfc  <https://farseerfc.me/>`_
    :gravatar: farseerfc@gmail.com

    正在日本 :del:`就读的博士生` 助教一枚，Arch Linux TU（授信用户，维护官方软件仓库的一群人）一名，同时是个人生赢家😂

.. friend:: lilydjwg
	:nick: `lilydjwg 依云 <https://blog.lilydjwg.me/>`_
	:gravatar: lilydjwg@gmail.com

	:irc:`archlinuxcn` 社区管理者之一，美味或流浪的坚持用XMPP的百合仙子。

.. friend:: phoenixlzx
    :nick: `phoenixlzx 凤凰卷 <http://blog.phoenixlzx.com/>`_
    :gravatar: i@phoenixlzx.com

    :irc:`archlinuxcn` 社区管理者之一，好吃的凤凰卷兼
    :irc:`nyaacat` `MC喵窝 <http://nyaa.cat/>`_ 管理员之一。

.. friend:: SilverRainZ
    :nick: `SilverRainZ 谷月轩 <http://silverrainz.me/>`_
    :logo: https://avatars3.githubusercontent.com/u/8090459

    有时自称LA，一个 `自己写了内核 <https://github.com/LastAvenger/OS67>`_
    还曾经和 Hurd 开发者 :del:`谈笑风生` 而且会画画的 :del:`一个恶人` ~

    随 IRC 逐渐消逝于历史的 IRC 客户端 
    `Srain <https://srain.im/2020/02/29/introducing-srain.html>`_ 绝赞开发中。

.. friend:: felixonmars
    :nick: `felixonmars <http://blog.felixc.at/>`_
    :gravatar: felixonmars@archlinux.org

    人称肥猫，Arch Linux中文社区网站的维护者之一+Arch Linux TU+刷包狂人（雾）+撑起了Arch Linux半壁江山的领袖（超大雾

    目前为了避免被吃掉还在努力增肥中😂😂

.. friend:: acgtyrant
    :nick: `御宅暴君  acgtyrant <http://acgtyrant.com/>`_
    :gravatar: acgtyrant@gmail.com

    身兼聽力殘疾，一直立身力行地一日三省，口風相當緊，中文審美嚴苛無比，計算機科學絕讚登堂中，批判性思維異常過人，完美無瑕之守序中立，偶尔飙中二腔，大愛 ACG 與 Arch Linux.

.. friend:: LQYMGT
    :nick: `LQYMGT <https://lqymgt.github.io/>`_
    :gravatar: lqymgt@gmail.com

    :irc:`archlinux-cn` 中的偶像，.sm 最多的人。

.. friend:: quininer
    :nick: `quininer <http://quininer.github.io/>`_
    :gravatar: quininer@live.com

    :del:`U need tox （雾），小心 Ta 傲娇 block 汝呐~（大雾），`
    写着一个JavaScript驱动的帅气博客。（没雾）

.. friend:: frantic1048
    :nick: `Frantic1048 Chino Kafuu 智乃 香风 <https://frantic1048.com/>`_
    :gravatar: archer@frantic1048.com

    萌萌的智乃，自己很萌的同时喜欢一切萌物，前端技艺精湛，貌似正在构建新的博客框架。
    :del:`而且还很好吃……`

    :del:`”不可食用！ -- @frantic1048“`

.. friend:: kireinahoro
    :nick: `Jsteward <https://jsteward.moe>`_
    :logo: https://avatars0.githubusercontent.com/u/7804730

    Using Arch Linux and Gentoo. ACG enthusiast and extreme Miku fan. 
    
    xubin990510@gmail.com Telegram: @jsteward PGP: 7250 7913 7D8A 5B65 
    
    jsteward @ Keybase

.. friend:: cuihao
    :nick: `Cuihao 崔灏 <https://blog.i-yu.me/html/zh/>`_
    :logo: https://avatars2.githubusercontent.com/u/889871

    因为 :ruby:`灏|hao,四声` 的缘故经常被唤作崔土豪,曾经是 `USTC镜像源 <https://mirrors.ustc.edu.cn>`_ 的维护者
    和 USTC Linux用户组的主席（好棒～），现在是Arch Linux 中文社区源的维护者之一.

.. friend:: petercxy
   :nick: `PeterCxy  <https://en.typeblog.net/>`_
   :logo: https://avatars2.githubusercontent.com/u/4532423

   彼得蔡，颠倒的阿卡林型次元,生活颠倒+存在感不足+二次元宅......

   据说高中用 AIDE 在手机上徒手撸出了 BlackLight 的大大，博客几经改版每一次都越来越漂亮.

   :del:`"这个女孩子狼人好可爱啊……我想死在这样的狼人女孩子手里(" -- @PeterCxy`

.. friend:: hjc4869
   :nick: `David Huang hjc4869  <https://hjc.im/>`_
   :logo: https://avatars0.githubusercontent.com/u/1969802

   信息安全专业的苦逼大学狗，偶尔折腾Linux和BSD，业余Windows开发者。 :del:`osu! 排名30k的菊苣......`

.. friend:: xyh
   :nick: `Xieyuheng 謝宇恆  <https://xieyuheng.github.io/>`_
   :logo: https://avatars2.githubusercontent.com/u/4354888

   喪以不執 所執者喪 執喪相比 方死方生

   研究语言设计和构造主义数学,偶尔喵化的文艺小妖狐~

.. friend:: csslayer
   :nick: `CS Slayer  <https://www.csslayer.info/wordpress/>`_
   :gravatar: wengxt@gmail.com

   :del:`CSS 里并没有 layer 这属性` 😂😂

    偶尔被社区众人唤作老K，同时参与开发 KDE , Fcitx 和 Chakra ，也是 `ikde 社区 <https://www.ikde.org/>`_ 的维护者。

    还是个有爱的爸爸 :del:`，只是夜深人静的时候会开始污` 😂😂

    :del:`和他签订契约，成为fcitx开发者吧！`

.. friend:: biergaizi
    :nick: `比尔盖子 niconiconi <https://tomli.blog/>`_
    :logo: https://tomli.blog/wp-content/uploads/2015/09/cropped-125081bfab0220f6d4074305ce2fe408.png

    Linux, Python, Programming, Open Source, Free Software, GNU。

.. friend:: liwanglin12
    :nick: `LWL liwanglin12 <https://blog.lwl12.com/>`_
    :logo: https://avatars1.githubusercontent.com/u/10279535?v=3&s=400

    Life without limits. 学生党·IT控·爱折腾

    PGP ID 2EEC5242, Key finger 6878 3EF6 724F E189 84C9 A272 4DE1 0929 2EEC 5242

.. friend:: void001
    :nick: `VOID{001} <https://void-shana.moe/>`_
    :logo: https://avatars2.githubusercontent.com/u/6134883?v=3&s=400

    属性: 宅，萝莉控，面瘫，轻微社交恐惧

    喜欢: {夏娜，挑战, 高自由度，编程，动漫，羽毛球，有秩序 }

    讨厌: ~喜欢

.. friend:: poiscript
    :nick: `PoiScript <https://blog.poi.cat>`_
    :logo: https://avatars2.githubusercontent.com/u/17513314

    「PoiSukuriputo」

.. friend:: skysmelody
    :nick: `青色旋律 <https://skysmelody.org>`_
    :gravatar: blog@skysmelody.org

    这个博客以后会作为分享比较长的文字的地方。青色旋律还是比较希望写一些技术相关的内容，但博客应该也会有很多其他类型的文字，比如音乐相关的话题， Minecraft, 关于自由软件和开源的想法之类的。偶尔大概也会有一些思考和感悟。

    :del:`在另一条时间线上叫做林静琴，是某间神社的巫女……`

.. friend:: chsh.moe
    :nick: `AFKing Leo <https://szclsya.me>`_
    :logo: https://avatars0.githubusercontent.com/u/10810336

    是会搭建 Matrix Home server 的 friends 呢……
    
        我是 Leo. 如果你在哪看到了一个叫 szclsya 的家伙，那大概率就是我.


.. friend:: brucetut
    :nick: `Bruce Zhang a.k.a Brucetut 布鲁斯兔兔 <https://blog.brucezhang.cf/>`_
    :logo:  https://avatars1.githubusercontent.com/u/6873988

    一只兔兔，反抗菊苣大队优秀成员。Our fingers can code this world!

        #linuxba 新一代水王 -- :fref:`origincode`

.. friend:: xuanwo
    :nick: `Xuanwo 漩涡 <https://xuanwo.io/>`_
    :logo: https://avatars0.githubusercontent.com/u/5351546

    位于青云的后端工程师， :del:`圆角大师` ……

.. friend:: awing
    :nick: `a-wing 新一 <https://a-wing.top/>`_
    :logo: https://a-wing.top/assets/avatar.png

    不知名无人机公司的研发工程师（就是个敲代码的。。。）
    Bug 攻城狮。专业写bug。233333333333333333 ～

    Hi! 上天不?

        Avatar 和 :fref:`void001` 一样是夏娜好耶）
    

.. friend:: axionl
    :nick: `艾雨寒 a.k.a 初等記憶體 <https://axionl.me/>`_
    :logo: https://avatars0.githubusercontent.com/u/8396456

    AOSC 病友 | Arch(Linux)(Us)er ! | 不会计算机，不是老师，不懂黑阔，是美术生 ค(TㅅT)ค

        咖啡沒醒，此人沒睡 | 言文

.. friend:: liolok
    :nick: `liolok 李皓奇 <https://liolok.com/>`_
    :logo: https://avatars3.githubusercontent.com/u/34574198

    岂有文章倾社稷，从来奸佞覆乾坤。

.. friend:: nanpuyue
    :nick: `南浦月 <https://blog.nanpuyue.com/>`_
    :logo: https://avatars3.githubusercontent.com/u/1524609

    南老師，在線直播，聲音甜美。アスナ粉，自稱裝機民工的 linux 桌面環境開發者。

    90后，日常使用 Linux，会一点 Python 和 C，时刻牢记学习一个，提高水平。

        望天外云卷云舒

.. friend:: megumifox
    :nick: `Megumi_fox 惠 <https://blog.megumifox.com/>`_
    :logo: https://avatars3.githubusercontent.com/u/23371099

    hello, world.

    TLDR：是一只萌萌的狐狸，好像蕴藏着很大的能量的样子？

.. friend:: tomacat
    :nick: `Tomacat 托马猫 <https://tomcat.one/>`_
    :logo: https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Pattern_example.svg/480px-Pattern_example.svg.png

        这个名称带来了不少好听的外号，比如大豪哥的汤姆一猫，奎尼的托马猫，布鲁斯大黑阔的脱毛猫。

    不管啦是猫都很萌 😺 

.. friend:: noirgif
    :nick: `Noirgif / gifschwartz <https://nir.moe/>`_
    :logo: https://avatars3.githubusercontent.com/u/23432548

    偶尔被唤作黑洞图老师，“老”式起名创始人（？）
    
.. friend:: alynxzhou
    :nick: `Alynx Zhou <https://sh.alynx.xyz/>`_ 
    :logo: https://sh.alynx.one/images/FutureGazerSmall.webp

        :del:`北京交通大学` 前北下关军事基地，:del:`计算机与信息技术学院` 反恐精英专业在读。
        `北京交通大学自由与开源软件镜像站 <https://mirror.bjtu.edu.cn/>`_ 目前的维护者，一只喜欢 Kalafina 的猫。

        “比起依赖别人，我更喜欢被人依赖，如果我喜欢的人能让我默默付出我就很满足了。”

    看 Logo 就知道是电磁炮厨啦，而且是 Arch Linux CN 社区里稀有的 GNOME 用户~ （好耶）

.. friend:: vifly
    :nick: `vifly <https://viflythink.com>`_
    :logo: https://viflythink.com/img/logo.png

        如果是从现实角度来说，我是一个普通的大学生，喜欢折腾技术与阅读，
        值得一提的就是目前在学习深度学习（炼丹）。

        vifly 的自留地，既包括技术折腾记录，也有对于各种东西的感想。


Anthon Open Source Community
----------------------------------------------------------------------------------

.. friend:: jeffbai
    :nick: `白铭骢 Jeff Bai <https://mingcongbai.wtf/>`_
    :logo: https://avatars1.githubusercontent.com/u/5006263

    AOSC创始人 :del:`兼特首` ,开源爱好者：打包狂人，翻译狂魔，FSF 会员.

        Hello lads.

.. friend:: azu_nyan
    :nick: `Azu | Nyan  <https://www.tombu.info/>`_
    :logo: /images/J7_CeSoa.png

    Skidmore College （准）学生，伪宅。折腾是生命的动力。千万别以为Ta是开发者或者什么别的技术人才，这是不正确的。

.. friend:: arthur2e5
    :nick: `Mingye Wang / 王铭烨  <https://about.me/Arthur2e5>`_
    :logo: https://avatars0.githubusercontent.com/u/6459309?v=3&s=400

    一个野生逗比小学生、间歇性自由软件原教旨主义者，并且他忽悠了很多人，很多很多人。:del:`（因为某些特殊原因在某QQ群被调教，并被备注为女装狂魔）`

.. friend:: SaltedFish
    :nick: `SaltedFish / Kay Miller / 咸鱼 <https://v2bv.net/>`_
    :logo: https://avatars3.githubusercontent.com/u/16459559?v=3&s=400

    学生 / 黑枪初学 / Linuxer / INGRESS 低等级懒军玩家 / 在 AOSC 打酱油 / :del:`收割灵魂的咸鱼`

    :del:`不想猎取灵魂的咸鱼不是好灵魂画手`

.. friend:: origincode
    :nick: `OriginCode <https://originco.de/>`_
    :logo: https://avatars1.githubusercontent.com/u/17294071

    :del:`小学` 初中生，开源爱好者，Arch Linux / AOSC OS User，AUR 上某包的维护者。

        作为一个初中生，俺很怕。。。这几只狼以后不会要把我分了吧。。。 

咱们需要更深入些……
-----------------------------------------------

.. friend:: yanderemoe
    :nick: `Yandere@萌 <https://blog.yandere.moe>`_
    :logo: https://avatars2.githubusercontent.com/u/25140626

        病娇酱的后花园，一个危险的地方，可能进去就出不来了……

    同时提供 Tor Hidden Service 访问： http://gqnl3d7yfcge6ayhubuzvfgzx3pe76mvhqacrmm6as2howkfctodsxad.onion/

.. friend:: novamoe
    :nick: `Nova Kwok <https://nova.moe/>`_
    :logo: https://avatars0.githubusercontent.com/u/24852034

        爱好摄影，记录身边的美好，一些照片放在了500px: @n0vad3v。

        曾经痴迷于英文书法，和朋友们共同创立了ECENPAC，
        并提供基础设施建设（服务器，域名，SSL，CDN等）。

        重庆交通大学LUNA协会创始人，校内镜像站，快速文件交换站，
        作业提交平台部署，Minecraft校内服务器架构。

    同时提供 Tor Hidden Service 访问： http://novamoeaqei4kkr7aio3pgsxex566atzg7pbr3vqarln6i24kak2stad.onion 

其它各处的见闻~
------------------------------------------------

.. friend:: program-think
    :nick: `编程随想的博客 <https://program-think.blogspot.com>`_
    :logo:  https://avatars0.githubusercontent.com/u/4027957

    一个不知道是谁的博客，只知道他（她）是个程序员（媛）。原来写了些编程相关的博文，后来也开始传授一些网络安全相关的技巧，偶尔也分享些电子书。

.. friend:: fiveyellowmice
    :nick: `FiveYellowMice <https://fiveyellowmice.com>`_
    :gravatar:  hkz85825915@gmail.com

    “I'm a Chunibyo who is too old to be a Chunibyo.”

    一位早就不该中二的中二少年，信仰开放和自由。

.. friend:: bennythink
    :nick: `Benny Think 小土豆 <https://dmesg.app/>`_
    :logo: https://avatars0.githubusercontent.com/u/14024832

        还是不要问俺是谁，俺只是一个默默无名的傻子...

        我支持自由，反对任何形式的”阉割“。这自由的含义，懂的人自然会懂；不懂的人也多说无益。为什么要唤醒假装睡觉的脑残呢？

    :del:`一颗不是来自育碧的土豆，而且不好吃……`

.. friend:: kookxiang
    :nick: `刘祥 kookxiang <https://ikk.me>`_
    :logo: https://avatars3.githubusercontent.com/u/2725379?

    技能: PHP / JavaScript / Node.JS / Golang / Java / Android .目前在阿里巴巴前端组 :del:`实力搬水` 。

.. friend:: huihuimoe
    :nick: `huihuimoe 灰灰 <https://huihui.moe>`_
    :logo: https://avatars3.githubusercontent.com/u/1707683

    噗呼٩(๑òωó๑)۶www，魔法少女灰灰是也，妳，是魔杖持有者麼？

.. friend:: gorgiaxx
    :nick: `gorgiaxx 小白 妄想症患者 <https://gorgias.me>`_
    :logo: https://avatars2.githubusercontent.com/u/9295584

    技术宅一只，一个想成为黑客的后端攻城师.强迫症患者，喜欢折腾各种东西，不达到目的誓不罢休

.. friend:: shadowrz
    :nick: `ShadowRZ 布偶君 <https://shadowrz.wordpress.com/>`_
    :logo: https://avatars2.githubusercontent.com/u/23130178     

        从这里开始，一切便是如此的混乱。转向另一边，便是……

        Infinity, endless.

    :del:`经常会精分……`

.. friend:: chaotic_dimension
    :nick: `Chaotic Dimension 雨宫恋叶 <https://shadowrz.github.io/about.html>`_
    :logo: https://avatars2.githubusercontent.com/u/23130178  

    还没有坏掉。

    有时候会有更多想法，也有奇奇怪怪的，甚至还有一些听上去就不能让人相信的思绪
    （从作品的一些设定碎片中就能看出来）。

.. friend:: eaimty
    :nick: `EAimTY <https://www.eaimty.com>`_
    :logo: https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Pattern_example.svg/480px-Pattern_example.svg.png

    一个没什么技术的开源爱好者，一个苦逼的学生狗。


.. friend:: nyanrabbit
    :nick: `奔跑的蜗牛壳 <https://www.tougetu.com/>`_
    :logo: https://www.tougetu.com/usr/themes/GreenGrapes2/img/head.jpg

    :del:`某土豆的同伙` ，不疯魔不成活。


.. friend:: benpigchu
    :nick: `Ben Pig Chu <https://benpigchu.com/>`_
    :logo: https://avatars0.githubusercontent.com/u/9023067

    【A little boy hidden in the PaperBox】
    :del:`咱给他安利了 Arcaea 以后把咱打得落花流水……` 


.. friend:: olingcat
    :nick: `Oling Cat <https://oling.cat/>`_
    :logo: https://avatars2.githubusercontent.com/u/1046455

    笨猫一只，喵~

.. friend:: wwyqianqian
    :nick: `WWY 千千 <https://wwyqianqian.github.io/>`_
    :logo: https://avatars2.githubusercontent.com/u/22388300

    千千，曾喜欢 VOCALOID CHINA，曾玩 PenBeat，尽管身体羸弱，但是在其他各种方面非常努力呢。

.. friend:: birdzhang
    :nick: `BirdZhang <https://birdzhang.xyz/>`_
    :logo: https://birdzhang.xyz/img/avatar.png

    一直笨死的大鸟，给红米5 Plus 移植上了 SailfishOS，在咱给 Nexus 6 移植时也给了不少帮助。

.. friend:: rexskz
    :nick: `Rex Zeng <https://www.rexskz.info/>`_
    :logo: https://avatars0.githubusercontent.com/u/27483702
    
    :html:`R·e<sup>x</sup> / Zeng` , 音游狗、安全狗、攻城狮、业余设计师、段子手、苦学日语的少年。

.. friend:: sionkazama
    :nick: `Sion Kazama <https://blog.sion.moe>`_
    :logo: https://avatars1.githubusercontent.com/u/13185633

    Telegram 种植园（Telegram 非官方中文站）的发起人（虽然现在已经甩锅了），
    `Telegram 简体中文语言包 <https://t.me/zh_CN>`_ 和 
    `Telegram 中文群组索引 <https://t.me/zh_groups>`_ 的前主要参与者。

        是一个沉浸在绝望、疯狂、虚无中诗音，可能反社会，可能还有人性。

.. friend:: hcl_i
    :nick: `hcl HydricAcid <https://blog.hcl.moe/>`_
    :logo: https://s3.amazonaws.com/keybase_processed_uploads/4ca20c086d9e1bdc5e185a999dafb505_360_360.jpg
    
        也许是一个技术宅、极客。

        ACG爱好者，主推东方Project，但STG、FTG都不擅长。

        物理研究者，相信“实验是检验真理的唯一标准”。

        爱好折腾计算机。

        会写一点代码，业余Linux运维。


.. friend:: rxliuli
    :nick: `rxliuli <https://blog.rxliuli.com>`_
    :logo: https://blog.rxliuli.com/medias/avatar.jpg
   
    吾辈是 rxliuli（中文名是 琉璃），喜欢现代前端的全沾开发者 ヾ (＠^∇^＠) ノ 

        不过吾辈这个自称咱首先想起的就是摩尔加纳了。