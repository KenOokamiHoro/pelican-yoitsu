失效链接归档
=======================================

:slug: links_archive
:date: 2016-01-16
:save_as: links_archive.html
:display: none

这是它们曾经存在过的证明。

.. friend:: xiaolan
    :nick: `Xiaolan （蓝潇） <https://xiaolan.me/>`_
    :gravatar:  xiaolan@protonmail.ch

    如同博主本人所说，没什么特别的主题，想到什么写什么....


.. friend:: nrechn
    :nick: `nll.fi <https://nll.fi>`_
    :gravatar: nrechn@gmail.com

    一名本科生，攻读计算机科学的IT学士学位。同时也是系统工程师和全栈开发者。

.. friend:: alice
    :nick: `Alice <http://tcjr7vik4rpcqmfe.onion/>`_
    :logo: https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Pattern_example.svg/480px-Pattern_example.svg.png

        A long time ago, in a cryptoland, far, far away of the cyberspace…

    同时提供 i2p 访问: http://iq26r2ls2qlkhbn62cvgb6a4iib7m5lkoulohdua5z6uvzlovjtq.b32.i2p


.. friend:: pumiao
    :nick: `冼睿通 Pumiao <https://xrt.pw/>`_
    :logo: https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Pattern_example.svg/480px-Pattern_example.svg.png

    逗比+滑稽一枚。（咱想不到该说啥了嘛~）

.. friend:: likunyan
    :nick: `李坤严（夏日大三角） <https://likunyan.com/>`_
    :logo: https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Pattern_example.svg/480px-Pattern_example.svg.png

    工作于中国厦门，从事IT行业，做过运维、程序员（PHP+MySQL+SDK），下班除了生活，喜欢搞鼓VPS（Linux），还有开源软件，比如 MediaWiki，Etherpad，Seafile，以及一些小应用。

.. friend:: ruqili
    :nick: `Ruqi Li <http://zh.roomchat.im>`_
    :logo: https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Pattern_example.svg/480px-Pattern_example.svg.png

    MediaWiki站长交流Telegram群里第三个说话的人（第一个是咱，第二个是@Pumiao（滑稽））

.. friend:: sherlock_holo
    :nick: `Sherlock Holo  <https://sherlock-holo.github.io/>`_
    :logo: https://avatars1.githubusercontent.com/u/10096425

    :del:`有人传闻咱和她和另外一只狼计划着吃掉 #archlinux-cn 里面的食物……`

    :del:`最近正和咱和 /dev/horo 一起觅食中（雾`

    😋

.. friend:: amane_tobiichi
    :nick: `Amane Tobiichi 余忆留声机 <https://amane.live/>`_
    :logo: https://avatars2.githubusercontent.com/u/14824064

    平成10年出生的一条老咸鱼。

        一种相思，两处闲愁

.. friend:: nicho1as
    :nick: `Nicho1as Wang <https://www.nicho1as.wang/>`_
    :logo: https://avatar.yoitsu.xyz/avatar/6e60029a759ba2f23e6c7f0bdb24b7dc

    一只自称程序猿的CS辣鸡.

.. friend:: nyanrabbit
    :nick: `NyanRabbit 兔子 ╧╧╰(:з╰∠)_ <https://www.rabbittu.com/>`_
    :logo: https://rabbittu.com/wp-content/uploads/2016/05/favicon-2.png

    伊妮卡丽因，某只会喵的兔子。