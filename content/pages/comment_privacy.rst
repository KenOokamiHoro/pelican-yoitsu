评论隐私声明
=======================================

:slug: comment_privacy
:date: 2018-06-11
:save_as: comment_privacy.html
:display: none

TL; DR
----------------------------------
没想好（


Tor Hidden Service 访客？
----------------------------------
Hidden Service 上只有 isso 可以用。 😂


NoScript 或类似扩展用户？
----------------------------------
请根据汝要在哪里发表评论放行相应的 URL ：

Disqus: disqus.com 和 kenookamihoro.disqus.com

V2MM: v2mm.tech

isso: isso.yoitsu.moe 

Tor Hidden Service 里的 isso：vwa5tknnfphzef2n.onion

Disqus
----------------------------------

    首先绕不过的是 Disqus 自己的 
    `服务条款 <https://help.disqus.com/terms-and-policies/terms-of-service>`_ 和 
    `隐私政策 <https://help.disqus.com/terms-and-policies/disqus-privacy-policy>`_ 😂

Disqus 会设置各种 Cookies （包括但不限于它自己和其它社交网站的，如果汝用了社交网站帐号登录了的话），
这就不是咱能控制的到的地方了 😂

以及虽然支持不使用 Disqus 帐号评论（但是收不到邮件通知），但是 Disqus 自己要求所有的 Guest comments
需要由所有者批准……

如果在中国大陆的话，请自行科学上网以使用 Disqus 😂

V2MM
----------------------------------

    其实它本身是个论坛…… 
    `那就去看看版规好了？ <https://v2mm.tech/topic/322/5%E5%88%86%E9%92%9F%E4%BA%86%E8%A7%A3-v2mm-%E6%96%B0%E4%BA%BA%E9%A1%BB%E7%9F%A5>`_

不过疑似有坑？

    PS: 下边那个评论框真坑人，邮件没验证不让评论就提前说，非得等我写好提交了之后才告诉我出错了，
    然后我写了半天的文字就没了…… 然后又嫌我刚注册不让评论…… 不让评论你早说啊………… 

    -- https://www.bennythink.com/python-bfa-router.html#comment-3128


isso
----------------------------------

这是个自己搭建的小型的评论服务器， `官网在这 <https://posativ.org/isso/>`_

它会用个 cookie 把汝的名称网站和邮箱保存下来（虽然三个都不是必须的，必须要有的只有评论内容而已）

当汝发出评论时，咱会收到像这样的邮件：

.. code-block:: text

    {nick} wrote:

    {comment_content}

    IP address: {x.x.x.0}
    Link to comment: 

    ---
    Delete comment: 

IP 地址的最后一位总是0，如果汝还是十分介意的话，用代理或者访问 Hidden Service 吧 😂