Title: Google 的 FLoC 是个糟糕透顶的主意
Date: 2021-04-21 22:34:00
Slug: googles-floc-terrible-idea
Authors: Horo

> 翻译自电子前哨基金会 （Electronic Frontier Foundation） DeepLink BLog 的 
> Google’s FLoC Is a Terrible Idea ： https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea ,
> 原作者 [Bennett Cyphers](https://www.eff.org/about/staff/bennett-cyphers)。
>
> EFF 网站的内容以知识共享 署名 4.0 协议授权，本译文也如此。

2021 年 4 月 9 日追记：我们开设了一个名为
["Am I FLoCed"](https://amifloced.org/) 的网站，如果汝有在用 Google Chrome 浏览器的话，汝可以来看看汝有没有“幸运的”成为
Google 主导的新的“队列联合学习”（Federated Learning of Cohorts ，或者缩写成 FLoC）这一定向广告实验的小白鼠咯。

----

第三方 Cookie 正在消亡， Google 正尝试创造它的替代品。

没必要为这玩意表示哀悼。二十年来，第三方 Cookie 成了阴暗、肮脏的广告监控行业的关键部分，价值高达数十亿美元；咱们早该淘汰像跟踪 Cookies 这样的
持久的第三方标识符了。不过呢，广告业最大的“玩家”在基础发生变化的时候也不是毫无作为。

Google 正在用一套新技术引领取代第三方 Cookie 的变革。虽然它的提案中的某些部分表示他们并没有从对监控这一商业模型的持续的反对中吸取教训。
这篇文章关注的是提案之一的 [“队列联合学习”](https://github.com/WICG/floc)，这也许是这其中最具野心，破坏性也最大的一部分。

FLoC 意为把原来那些第三方跟踪器做的那些事情转嫁给汝等的浏览器：在这种情况下，汝最近的浏览活动会被归纳为一个行为标签分享给广告商。
这种方法看起来解决了第三方 Cookies 曾经遇到的那些隐私问题，却又在这一过程中产生了新的问题。它也许还会加剧行为广告带来的许多严重的不和隐私相关发问题，像是歧视和劫掠目标。

作为“隐私倡导者”，Google 说 FLoC （和他们声称的[“隐私沙盒”](https://www.blog.google/products/chrome/building-a-more-private-web)里的一
些其他元素）的表现会比现在任由“数据经纪人”和广告巨头肆无忌惮的跟踪和画像更好。
不过这个整个愿景的基础——我们只能从‘老式的跟踪方法’和‘新式的跟踪方法’之中选择一个——本身就是个伪命题。
这并不是个非此即彼的问题。与其重新发明跟踪轮子，咱们为啥不去想象一个没有目标投放广告（和它带来的无数问题）的更好的世界？

这是一条岔路，后方是第三方 Cookies，这也许是互联网发明以来最大的错误。咱们的前方则是两种选择。

一条是，用户可以决定与他们选择互动的每个网站分享什么信息。当用户下次打开一个标签页时，不必担心他们过去的浏览记录会反被用来操纵他们。

另一条则是，每个用户的行为被一目了然的从一个网站传送到另一个网站。他们最近的历史，被提炼成几个片段，被 "民主化"，然后和参与每个网页服务的几十个
无名角色分享。用户在每次互动时，都会以“这是我这周的活动，请借此关照我”这种自招的告白开始。

用户和（真正的）隐私倡导者都要拒绝 FLoC 和其他的试图重新发明基于行为的定向广告的错误尝试。 希望 Google 放弃 FLoC 转而为构建真正的用户友好的互联而努力。

## FLoC 是个啥？

2019 年，Google 提出了[“隐私沙盒”](https://www.blog.google/products/chrome/building-a-more-private-web)，一种对网络隐私的未来愿景。
它的中心是一套满足现在第三方 Cookies 提供给广告商的诸多使用场景的协议，其中没有 Cookies。
在成立了一个主要由广告商组成的“ Web Advertising Business Group”（网络广告商务工作组？）以后，Google 又向 W3C 提交了这一系列的提案。其后的数个月，Google 和一众广告商又提交了一打以鸟类的名字命名的“技术标准”：
[PIGIN （这啥？）](https://github.com/michaelkleber/pigin), 
[TURTLEDOVE （斑鸠？）](https://github.com/WICG/turtledove), 
[SPARROW （麻雀？）](https://github.com/WICG/sparrow), 
[SWAN （天鹅？）](https://github.com/1plusX/swan),
[SPURFOWL（白鹭？）](https://github.com/AdRoll/privacy/blob/main/SPURFOWL.md), 
[PELICAN （鹈鹕？](https://github.com/neustar/pelican), 
[PARROT （鹦鹉？）](https://github.com/prebid/identity-gatekeeper/blob/master/proposals/PARRROT.md)，
以及[更多不胜枚举](https://github.com/w3c/web-advertising#ideas-and-proposals-links-outside-this-repo)。
每一个“鸟”提案都在实现所谓的定向广告生态系统中一个目前由 Cookie 实现的功能。

FLoC 设计上用来帮助广告商不用第三方 Cookies 进行行为定位。启用 FLoC 的浏览器会收集用户浏览偏
具有相似偏好的用户（这里的“相似”要怎么定义？）会分组到同一队列中。每个用户的浏览器会和网站和广告商分享一份自己属于哪些群组的队列列表。根据他们的提案，每个队列中至少会有几千个用户（虽然没有保证）。

话太长不想看？那汝大可以这么想：汝的 FLoC ID 就是汝最近活动的简单摘要。

Google 的概念验证文档（Proof of Concept，PoC）用每个用户访问的网站的域名作为分组的依据。然后利用名为 [SimHash](https://en.wikipedia.org/wiki/SimHash) 的算法来生成群组。
这个算法进行的计算可以在用户的设备上完成，因此不需要中心服务器去收集那些行为数据。但是“还是需要一个中心管理员来执行隐私保证”。为了防止队列为了识别用户规划的太小，
Google 提议由一个中心统计分配给每个群组的用户数量。如果有的群组用户太少，可以将其与其他类似的群组合并，直到每个群组有足够的用户。

> FLoC 对广告商有用的原因是，用户的队列群组会透露关于他们的行为的必要信息。

虽然大部分的细节在提案中还没有定论。这份提案的草稿表示用户的队列 ID 可以通过 JavaScript 访问，但是并没有清楚的指出会不会对可以访问的对象有所限制，以及队列 ID 有无其它的共享方法。
FLoC 可以基于 URL 或页面的内容（而不是之前提起的域名）来集群，也可以使用基于联合学习的系统（它的名字不就是这么暗示的？）代替 SimHash 来生成群组。
它也没有明确队列群组的数量，Google 在实验中使用了 8 位长的队列标识符（也就是队列有 256 个），实际情况下的数量应该会更大；文档中[建议](https://github.com/WICG/floc#overview)的长度是 16 位，
也就是四个十六进制数。队列越多，它们就越具体。更长的队列 ID 意味着广告商了解了更多用户的兴趣，也能更容易的描绘他们的指纹。

不过期限这件要素 ''是'' 约定好的，FLoC 队列会以每周为一个周期利用上周的浏览数据重新计算。这使得 FLoC （看起来）有些难以用于长期识别，但这也使它们更有力地计量了用户在一段时间内的行为。

## 新的隐私问题

FLoC 意在将定向广告带入保护隐私的未来。不过它的核心设计涉及到了会和广告伤分享新的信息。于是就不出所料的引入了新的隐私风险。

### 指纹
首先令人担忧的就是指纹。老生常谈的浏览器指纹就是通过收集用户浏览器中离散的信息为这个浏览器创建一个独特和稳定的标记的过程。
EFF 的 [“Cover Your Tracks”](https://www.eff.org/deeplinks/2020/11/introducing-cover-your-tracks) 演示了这个过程是如何运作的。
一言以蔽之：汝的浏览器看起来越和其他人的不一样，就更容易制作一个独特的指纹。

Google “保证”说绝大多数的 FLoC 队列都会包含几千名用户，因此一个队列 ID 应该不能把汝从数千个同一队列中的其他用户区分出来。然而，这还是给指纹识别者带来了巨大的领先优势。
要是跟踪者从汝的队列 ID 出发的话，它就只需要从几千个（而不是上百万）浏览器中把汝挑出来了。
用信息论的术语来说的话，FLoC 队列包含几比特的[熵](https://zh.wikipedia.org/wiki/%E7%86%B5_(%E4%BF%A1%E6%81%AF%E8%AE%BA))，在 Google 的实验中最大是 8 bit。
鉴于这些信息不太可能与浏览器暴露的其他信息相关联，所有它们说不定更加有力。
跟踪者利用现有的独特指纹跟踪一个 FLoC 用户说不定更容易了呢。

Google 也明白这会是个挑战，不过他们却是承诺会在[更长远的“隐私预算”计划中](https://github.com/bslassey/privacy-budget)来解决指纹的问题。
解决指纹问题是一个令人钦佩的目标，它的提案提供的捷径看起来也很有希望。
不过 [FAQ 中](https://github.com/bslassey/privacy-budget/blob/master/faq.md#whats-the-status-of-the-privacy-budget-what-browsers-support-it)又说这“是个早期阶段的建议，目前还没有浏览器实现”，
于此同时，Google 在[这个月](https://www.chromestatus.com/feature/5710139774468096)开始测试 FLoC。

指纹问题已经众所周知的难以阻止，像 Safari 和 Tor Browser 这样的浏览器为了减少指纹攻击面牺牲了大量功能，这个和追踪者进行的消耗战已经持续了数年之久。
缓解指纹追踪的方法一般都会是去除或者限制不必要的熵来源——然后 FLoC 成为了一种新的熵来源。在解决现有的指纹问题之前，Google 还要再制造一个新的问题吗？

### 跨越内容的暴露
第二个问题不太容易解释：这技术和已经在识别用户的追踪者分享了新的个人数据。FLoC 对广告商有用的原因是，用户的队列群组会透露关于他们的行为的必要信息。

这个项目的 GitHub 页面是这么描述的：

> 这个 API 将个人的一般浏览历史（以及一般兴趣）的一些信息“民主化”，让任何选择加入的网站都能获得......
> 已经知晓某个人的网站（例如他/她/它通过电子邮件地址在这个网站注册）可以记录和展示他们的队列。这意味着有关个人兴趣的信息最终可能会被公开。

就像上面描述的那样， FLoC 队列自己并不能作为标识符。不过能以其它方式标记用户的网站（例如“通过 Google 登录”）就能把通过 FLoC 了解到的信息和个人资料关联。

两类信息可能会因为这种方式暴露：

1. 关于浏览历史的具体信息。追踪者可能会通过逆向工程队列分配算法来确定任何属于特定群体的用户可能（或者一定？）会访问特定的网站。
2. 观察者可能会了解到关于人口统计学或兴趣的一般信息。一般来说某一特定群体的成员很可能是某一特定类型的人。例如，某一特定群组可能会有很多年轻的黑人女性用户，或者中年共和党选民；或者 LGBTQ+ 青年。

这就意味着汝访问的网站不需要提前在整个互联网中调查汝就能在第一次接触中知道汝是个什么样的家伙。除此以外，汝的 FLoC 队列会定期更新，能以其它方式标记汝的网站也能借此跟踪汝浏览行为的变化。
别忘了，一个 FLoC 群组就是对汝最近浏览活动的摘要，仅此而已。

汝应该有权在不同的语境中展示汝身份中不同的部分，例如汝在访问一个医疗信息网站的时候会允许它获得汝的健康状况，但是多半不想让它知道汝的政治倾向。
类似的，汝在逛零售网站的时候也多半不想让它知道汝最近有没有阅读关于治疗抑郁症的资料。
FLoC 就这样腐蚀了汝划分的各种语境，取而代之的是把同样的摘要分享给了和汝互动的每一位。

## 隐私之外
FLoC 被设计来防止通过跨越环境的标识实现个性化的特征分析这一具体问题。
它们的目标都是避免让追踪者获取可以与特定人联系起来的特定信息。
但就像咱们之前展示的那样，FLoC 也能在大量的环境中帮到追踪者。即使 Google 能够通过迭代设计防范这一风险，定向广告的危害并不限于侵犯隐私。
FLoC 的核心目标就和公民自由的其他部分相悖。

定向的力量也是歧视。从定义来看，有针对性的广告允许广告商达到某些种类的人，而排除掉其它的。定向系统可以像为鞋子做广告一样容易的决定谁能看到招聘信息或贷款提议。

这么多年以来，定向广告利用的最多的地方就是剥削、歧视和伤害。关于工作、住房和信贷的广告可以根据目标人群的种族、宗教、性别、年龄或能力等因素不同进行程度的辨别。
基于信用记录或相关历史性特征的定位让高息贷款广告铺天盖地。
基于人口统计学、地点和政治派别的定向广告帮助了一大批出于各种政治动机传播虚假信息和压制选民的传播者。所有类型的行为标的都在增加“令人信服的诈骗”的风险、

> 与其重新发明跟踪轮子，咱们为啥不去想象一个没有目标投放广告（和它带来的无数问题）的更好的世界？

Google、Facebook 和其它不少的大广告平台已经在设法控制对它们平台的某些用途。例如 Google 限制广告商收集[“敏感兴趣类别有关的信息”](https://support.google.com/adspolicy/answer/143465?hl=en)。
但是没有什么作用，铁了心要这么做的人总能找到解决整个平台对[某些类型的定向](https://www.propublica.org/article/facebook-ads-can-still-discriminate-against-women-and-older-workers-despite-a-civil-rights-settlement)
或[某些类型的广告](https://theintercept.com/2016/10/07/google-said-it-would-ban-all-payday-loan-ads-it-didnt)的限制的变通方法。

即使平台有绝对的权力知晓哪些信息能用来针对谁，也往往无法阻止对技术的滥用。 FLoC 用一种[无监督学习](https://en.wikipedia.org/wiki/Unsupervised_learning)算法来构建它的集群,所以没人能直接控制哪些人会被划分到一起。
（对于广告商的）理想状况来说，FLoC 会把有共同的行为和兴趣爱好的人划分到一起。但网络行为还会和各种敏感特征有关——
像是性别、种族、年龄和收入这样的[人口统计学](https://ojs.aaai.org/index.php/ICWSM/article/view/14776)数据
和[“五大”人格特征](https://www.pnas.org/content/117/30/17680)，甚至[心理健康状况](https://link.springer.com/chapter/10.1007/978-3-642-23620-4_22)。
很有可能 FLoC 也会把具有相同的此类特征的人划分到一起，FLoC 的分组也可能直接反映到与药物滥用、经济困难或支持创伤幸存者有关的网站的访问状况。

Google [提议](https://github.com/WICG/floc#excluding-sensitive-categories)称它可以通过监控系统的输出来检查和敏感类别的相关性。如果它发现某个队列与某个受保护群体的关系过于密切，管理服务器可以为算法选择新的参数，并告诉用户的浏览器重新分组。

这个解决方案听起来既奥威尔又西西弗，为了监控 FLoC 的分组和敏感分类有多么相关，Google 要用关于用户种族、性别、宗教、年龄、健康和财务状况的数据进行大量审计。每次发现关联太强的队列的时候，就不得不重新配置整个算法并再次尝试，希望新版本中没有其他 "敏感类别 "受到牵连。这个问题比之前在尝试解决的问题更加困难，而且每次解决的尝试都以失败告终。

如果 FLoC 大规模的应用，利用年龄、性别、收入等因素直接区分用户也许会更难，但还不无可能。
坐拥辅助信息的跟踪者可以通过观察和实验学习 FLoC 分组的“意义”——这队列中包含哪些人。
那些铁了心要这么做的人就饿能继续如此辨别用户。
此外，这种行为对于平台来说，将比现在更难监管。有不良意图的广告商必然会以合理的方式进行否认——毕竟它们只是根据行为（而不是那些“敏感的类别”）在接触人们。
整个系统对于用户和监管者来说也会愈发不透明。

## Google，别这么做

我们在 Google [第一次提出这些提案](https://www.eff.org/deeplinks/2019/08/dont-play-googles-privacy-sandbox-1)的时候曾经把 FLoC 叫做
“隐私保护技术的反面教材”。
我们希望标准流程能阐明 FLoC 的根本性缺陷，让 Google 能重新考虑推进。
事实上，Github 上的几个 Issue 也提出了我们在这里强调的同样的问题。
（例如[这个](https://github.com/WICG/floc/issues/36),[这个](https://github.com/WICG/floc/issues/16)和[这个](https://github.com/WICG/floc/issues/40)。
不过 Google 还是在基础几乎不变的情况下继续开发这个系统。以[“95% 有效的”](https://www.adexchanger.com/online-advertising/the-industry-reacts-to-googles-bold-claim-that-flocs-are-95-as-effective-as-cookies/) Cookies 取代手段
向广告商夸口。[发布于 3 月 2 日的 Chrome 89](https://www.chromestatus.com/features/schedule)已经开始[测试这一技术](https://blog.google/products/ads-commerce/2021-01-privacy-sandbox/)。
有一小批 Chrome 用户（大概有上百万）将会（或已经）参加这一实验。

别搞错了，如果 Google 是在按计划在 Chrome 上实现 FLoC 的话，那应该会给大家“选项”。这个系统可能会让从中受益的广告商选择加入，而让觉得会受到损害的用户选择退出。
然后 Google 会在明知绝大多数用户完全不知道 FLoC 是啥，没啥人会选择关闭的的情况下吹捧说这是这是 "透明度和用户控制 "的一步。
自夸说自己终结了在它半生中帮自己[赚到上十亿美元的](https://www.eff.org/deeplinks/2020/03/google-says-it-doesnt-sell-your-data-heres-how-company-shares-monetizes-and)
邪恶的第三方 Cookie，在互联网上开创了一个“新的私密时代”。

不一定非要这样。隐私沙盒中最重要的部分，譬如放弃第三方标识符和对抗指纹识别，将真正更好地改变网络的现状。Google 本可以把那些旧的监视脚手架拆掉的时候不再换上新的既独特又有害的家伙们的。

我们坚决反对 FLoC。那不是我们想要的，也不是用户应有的世界。 Google 该从第三方追踪的经历中学到正确的教训，为用户（而不是为了广告商）设计浏览器。

