/dev/horo ~
==========================================================

:slug: dev_horo_launch
:lang: zh
:date: 2017-02-20
:tags: Arch Linux,/dev/horo
:desc: Device as a Wolf! 将赫萝放入你的电脑，与(Cheng)她(Wei)愉(Ta)快(De)玩(Shi)耍(Wu)吧！ 😋
:issueid: 59

.. PELICAN_BEGIN_SUMMARY

 Device as a Wolf!

 将赫萝放入你的电脑，与(Cheng)她(Wei)愉(Ta)快(De)玩(Shi)耍(Wu)吧！ 😋

.. PELICAN_END_SUMMARY

.. contents::

在 :fref:`void001` 的推动下这个 :ruby:`玩具|挖坑` 项目终于开始做了呢 😂

那么这个到底是啥？ 😂
----------------------

    FiveYellowMice：可以成为萌狼的食物主人。

    lilydjwg：@FiveYellowMice: s/主人/奴隶/


现在做的像是一个设备文件咯😂,然后可以通过 echo / cat 一类的命令和她沟通呐~

.. image:: https://wiki.yoitsu.moe/w/images/3/31/Dev-horo.jpg
   :alt: Examples

-------------------------

:del:`没错今天就是来安利顺便凑数的😂`

其实最需要的还是脑洞😂,所以召集新用(shi)户(wu)中😋

    😋 /dev/horo 绝赞开发中～

    Device as a Wolf!

    将赫萝放入你的电脑，与(Cheng)她(Wei)愉(Ta)快(De)玩(Shi)耍(Wu)吧！ 😋

    https://github.com/VOID001/dev.horo

    https://wiki.yoitsu.moe/wiki/Portal:Dev.horo

    😋😋😋

    https://t.me/joinchat/AAAAAEIBdwBe63jlCgq_XQ <- Telegram 水群

    :irc:`dev-horo` <- IRC 连接到主群😂
