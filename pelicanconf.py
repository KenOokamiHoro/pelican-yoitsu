#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

SITENAME = "约伊兹的萌狼乡手札"

PATH = 'content'

TIMEZONE = 'Asia/Shanghai'

DEFAULT_LANG = 'zh'
DATE_FORMATS = {
    'zh': ('%Y年%m月%d日 (%a)'),
}
SITEMAP = {
    'format': 'xml',
}
# Feed generation is usually not desired when developing
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
SITEURL = ''
RELATIVE_URLS = False
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

DEFAULT_PAGINATION = 8
DISPLAY_CATEGORIES_ON_MENU = True
# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = "yoitsu"
PLUGIN_PATHS = ['plugins']
ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'
PAGE_URL = "{slug}.html"
PAGE_SAVE_AS = "{slug}.html"
STATIC_PATHS = ['images/favicon.ico', 'images', 'raw', 'robots.txt']
USE_FOLDER_AS_CATEGORY = True

EXTRA_PATH_METADATA = {
    'images/favicon.ico': {'path': 'favicon.ico'},
    'robots.txt': {'path': 'robots.txt'}
}

PLUGINS = ["better_codeblock_line_numbering",
           'tipue_search',
           'neighbors',
           'series',
           'extract_toc',
           'tag_cloud',
           'sitemap',
           'summary',
           'twitter_bootstrap_rst_directives',
           'feed_summary',
           'materialize'
           ]
DIRECT_TEMPLATES = (('index', 'archives', 'search'))
OUTPUT_SOURCES = True
OUTPUT_SOURCES_EXTENSION = '.rst'
# Yoitsu theme config

MAIN_COLOR = "amber"
MAIN_COLOR_DECORATOR = "darken-3"
SECOND_COLOR = "amber"
SECOND_COLOR_DECORATOR = "lighten-4"
ARTICLE_BACKGROUND = "yellow"
ARTICLE_BACKGROUND_DECORATOR = "darken-4"

LICENSE = "知识共享 署名-相同方式共享 4.0协议"
LICENSE_URL = "https://creativecommons.org/licenses/by-sa/4.0/deed.zh"
LICENSE_IMG = "/theme/images/cc-by-sa.png"
# Quotes on Site
SHOW_QUOTE = False
GIST_QUOTE_URL = 'https://api.github.com/gists/07ca2edea6e507bf40f5'
GIST_QUOTE_NAME = 'quotes.json'
# Social widget
DISPLAY_SOCIAL_ON_MENU = True
SOCIAL = (('twitter', 'Twitter', 'https://twitter.com/Ken_Ookami_Horo'),
          ('github', 'Github', 'https://github.com/KenOokamiHoro'),
          ('mail', 'Mail', 'mailto:horo@yoitsu.moe'),
          ('mastodon','Mastodon', 'https://yoitsu.moe/@horo'))
          
GITHUB_ISSUE_URL = 'https://github.com/KenOokamiHoro/kenookamihoro.github.io/issues'
SITE_NOTICE = ()
SITE_NOTICE_LENGTH = len(SITE_NOTICE) if SITE_NOTICE else ""
TAG_CLOUD = True
# AUTHOR CONFIGURE
AUTHOR = 'ホロ'
AUTHOR_IMG = '/theme/images/wiki.png'
AUTHOR_DESC = ('「虽然咱长久以来被尊为神，且被束缚在这块土地上，但咱根本不是什么伟大的神。咱就是咱。咱是赫萝。」',
               '「咱可是贤狼啊。起码还是知道这世界上有很多东西是咱所不了解的。」',)
AUTHOR_BACKGROUND = '/theme/images/background.jpg'
METADATA_DESC = '「咱可是贤狼啊。起码还是知道这世界上有很多东西是咱所不了解的。」 '
GITHUB_REPO = "KenOokamiHoro/kenookamihoro.github.io"
DISQUS_COMMENT = True
LIKER_ID = 'kenookamihoro'
MASTODDON = "https://yoitsu.moe/@horo"